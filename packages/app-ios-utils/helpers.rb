module Helpers
  # Find string in a file and replace with another string
  #
  # @param dir [String] the directory or file path in which to search for `findstr`
  # @param findstr [String] the string to look for from `dir`
  # @param replacestr [String] the string to replace `findstr` with
  def Helpers.find_and_replace(dir, findstr, replacestr)
    Dir[dir].each do |name|
        text = File.read(name)
        replace = text.gsub(findstr,replacestr)
        if text != replace
            puts "Fix: " + name
            system("chmod +w " + name)
            File.open(name, "w") { |file| file.puts replace }
            STDOUT.flush
        end
    end
    Dir[dir + '*/'].each(&method(:find_and_replace))
  end
end
