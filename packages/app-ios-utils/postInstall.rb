#!/usr/bin/ruby

require_relative './helpers.rb'

module PostInstallTasks
  # Patch pods so they build against this project's target
  # based on https://stackoverflow.com/a/37289688/1983583
  def PostInstallTasks.patch_pods_deploy_target(installer)
    installer.pods_project.targets.each do |target|
      target.build_configurations.each do |config|
        config.build_settings.delete 'IPHONEOS_DEPLOYMENT_TARGET'
      end
    end
  end

  # Fix an issue with flipper (how the fuck this get deployed to prod? u.u)
  def PostInstallTasks.patch_flipper_folly_mutex()
    # see {Helpers::find_and_replace}
    Helpers::find_and_replace(
      "Pods/Headers/Private/Flipper-Folly/folly/synchronization/DistributedMutex-inl.h",
      "  atomic_notify_one(state);",
      "  folly::atomic_notify_one(state);"
    )
  end
end
