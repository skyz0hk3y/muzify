#!/usr/bin/env bash

cat << EOF
{
  "token": "${OPENODE_TOKEN}",
  "site_name": "${OPENODE_SITE_NAME}",
  "instance_type": "server"
}
EOF
