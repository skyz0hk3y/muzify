import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableColumn,
} from "typeorm";

export class AppRelease1617880727682 implements MigrationInterface {
  name = "AppRelease1617880727682";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "app_releases",
        indices: [
          new TableIndex({
            name: "release_by_versionName", // => release_by_version_name
            columnNames: ["versionName"],
            isUnique: true,
          }),
        ],
        columns: [
          new TableColumn({
            name: "versionName", // => version_name
            type: "text",
            isUnique: true,
            isPrimary: true,
            isNullable: false,
          }),
          new TableColumn({
            name: "whatsNew", // => whats_new
            type: "text",
            isNullable: false,
          }),
          new TableColumn({
            name: "forceUpdate", // => force_update
            type: "boolean",
            isNullable: true,
            default: false,
          }),
          new TableColumn({
            name: "apk_url_arm64_v8a",
            type: "text",
            isNullable: false,
          }),
          new TableColumn({
            name: "apk_url_armeabi_v7a",
            type: "text",
            isNullable: false,
          }),
          new TableColumn({
            name: "apk_url_x86_64",
            type: "text",
            isNullable: false,
          }),
          new TableColumn({
            name: "apk_url_x86",
            type: "text",
            isNullable: false,
          }),
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("app_releases");
  }
}
