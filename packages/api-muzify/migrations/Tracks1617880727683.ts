import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableColumn,
} from "typeorm";

export class Tracks1617880727683 implements MigrationInterface {
  name = "Tracks1617880727683";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "tracks",
        indices: [
          new TableIndex({
            name: "track_by_id",
            columnNames: ["id", "provider_id"],
            isUnique: true,
          }),
          new TableIndex({
            name: "tracks_by_provider",
            columnNames: ["provider_name"],
            isUnique: true,
          }),
          new TableIndex({
            name: "tracks_by_author",
            columnNames: ["author_id", "author_name"],
            isUnique: false,
          }),
        ],
        columns: [
          new TableColumn({
            name: "id",
            type: "text",
            isUnique: true,
            isPrimary: true,
            isNullable: false,
          }),
          new TableColumn({
            name: "author_id",
            type: "text",
            isUnique: false,
            default: "''",
          }),
          new TableColumn({
            name: "author_name",
            type: "text",
            isNullable: false,
          }),
          new TableColumn({
            name: "title",
            type: "text",
            isNullable: false,
          }),
          new TableColumn({
            name: "artwork_url",
            type: "text",
            isNullable: true,
          }),
          new TableColumn({
            name: "duration",
            type: "int",
            isUnique: false,
            isNullable: false,
            default: 0,
          }),
          new TableColumn({
            name: "provider_id",
            type: "text",
            isUnique: true,
          }),
          new TableColumn({
            name: "provider_name",
            type: "text",
          }),
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("tracks");
  }
}
