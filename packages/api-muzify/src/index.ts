import "dotenv/config";
import "reflect-metadata";

import express from "express";
import {
  defaultErrorHandler,
  DIContainer,
  registerControllers,
  registerLoaders,
  registerServices,
} from "@weedify/core";

import { Constants } from "./constants";
import { DIKeys } from "./DIKeys";

const main = async () => {
  console.log("INFO : Muzify API is starting...", { Constants });

  const app = express();

  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  const loaders = await import("./loaders");
  const services = await import("./services");
  const controllers = await import("./controllers");

  await registerLoaders(loaders, app);
  await registerServices(services, app);
  await registerControllers(controllers, app);

  app.use(defaultErrorHandler());

  app.listen(Constants.API.Port, Constants.API.Host, () => {
    const appUrl = `http://${Constants.API.Host}:${Constants.API.Port}`;
    DIContainer.register(DIKeys.AppListeningUrl, appUrl);
    console.log(`INFO : Muzify API up and running on ${appUrl}`);
  });
};

// Handle Node.JS errors (prod only)
if (!Constants.API.Debug) {
  process.on("rejectionHandled", () => null);

  process.on("uncaughtException", (err) => {
    console.log(`ERROR: ${err.message}`);
  });

  process.on("unhandledRejection", (_, promise) => {
    promise.catch((err) => console.log(`ERROR: ${err.message}`));
  });
}

/**
 *  @type {Console}
 */
var originalConsole = global.console;

// TODO: Use a loader to have a proper Logger (bunyan for example)
// Hackish but minimal & performant debug/prod logs switcher.
global.console = {
  ...originalConsole,
  log(message?: any, ...optionalParams: any[]) {
    if (!Constants.API.Debug && message.includes("DEBUG:")) return;
    originalConsole.log(message, ...optionalParams);
  },
};

main();
