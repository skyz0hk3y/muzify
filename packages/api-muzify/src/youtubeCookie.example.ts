// Example file, copy this as `youtubeCookie.ts` and fill in the blanks by
// going to YouTube while logged with your account, then inspect network requests
// and extract each values from the cookie to report them here.
//
// Datas stored in this file are meant to bypass the YouTube rate limiting, this
// works because YT only rate limit unauthenticated requests to their APIs, and
// have a greater window for authenticated requests.
//
// Downside of this is that whenever the cookie expires (1+ month), you have to
// update this file, with new informations, which is time consuming.
//
// TODO(automation): Make it easier to grab these values via a script.
const YT_PREFS =
  "CONSENT=YES+BE.fr+V10+BX; PREF=tz=Europe.Brussels&f4=4000000; wide=1; GPS=1;";

const VISITOR_INFO1_LIVE = "";
const YSC = "";
const SID = "";
const SIDCC = "";
const SECURE_3PSID = "";
const SECURE_3PAPISID = "";
const SECURE_3PSIDCC = "";
const HSID = "";
const SSID = "";
const APISID = "";
const SAPISID = "";
const LOGIN_INFO = "";

export const YOUTUBE_FULL_COOKIE_STR = `VISITOR_INFO1_LIVE=${VISITOR_INFO1_LIVE}; ${YT_PREFS} YSC=${YSC}; SID=${SID}; __Secure-3PSID=${SECURE_3PSID}; HSID=${HSID}; SSID=${SSID}; APISID=${APISID}; SAPISID=${SAPISID}; __Secure-3PAPISID=${SECURE_3PAPISID}; LOGIN_INFO=${LOGIN_INFO}; SIDCC=${SIDCC}; __Secure-3PSIDCC=${SECURE_3PSIDCC}`;
