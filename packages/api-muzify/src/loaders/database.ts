import { Connection, ConnectionOptions, createConnection } from "typeorm";
import { ILoadable, DIContainer } from "@weedify/core";

import { Constants } from "@app/constants";
import { DIKeys } from "@app/DIKeys";

export class DatabaseLoader implements ILoadable {
  private _client: Connection;

  async register() {
    const {
      migrations,
      models,
      modelsKeys,
      modelsImports,
    } = await this._retrieveApiLogic();

    // Connect to DB.
    console.log("INFO : Will attempt database connection...");
    const connectionOpts = this._getConnectionOptions(Constants.Database.Type);
    this._client = await createConnection({
      // Connection options.
      ...connectionOpts,
      // Entities
      entities: models,
      // Migrations
      migrations,
      migrationsRun: Constants.Database.RunMigrations,
    });

    console.log(
      `INFO : Database connection (${Constants.Database.Type}) successfully initialized !`,
    );

    // Register DB connection to Dependency Injection.
    DIContainer.register(DIKeys.DatabaseConnection, this._client);
    console.log("DEBUG: Registered database connection to DI Registry.");

    // Register DB Models to DI.
    modelsKeys.forEach((key: string) => {
      const EntityClass = modelsImports[key];
      const entityName = key.replace(/Model$/, "Repository");
      console.log(`DEBUG: Will register repository "${entityName}" ...`);
      DIContainer.register(
        entityName,
        this._client.getRepository<typeof EntityClass>(EntityClass),
      );
    });

    if (Constants.Database.RunMigrations) {
      await this._checkRunPendingMigrations(this._client);
    }

    return this._client && this._client.isConnected === true;
  }

  async unregister() {
    const modelsImports = await import("../models");
    const modelsKeys = Object.keys(modelsImports);

    modelsKeys.forEach((key: string) => {
      const entityName = key.replace("Model", "Repository");
      console.log(`DEBUG: Will unregister repository "${entityName}" ...`);
      DIContainer.unregister(entityName);
    });

    if (this._client) {
      await this._client.close();
      console.log(
        `"INFO : Successfully closed the TypeORM database connection.`,
      );
    }

    console.log(
      `DEBUG: Will unregister key "${DIKeys.DatabaseConnection}" ...`,
    );
    DIContainer.unregister(DIKeys.DatabaseConnection);

    return true;
  }

  private async _retrieveApiLogic() {
    const migrationsImports = await import("../../migrations");
    const migrations = Object.values(migrationsImports);

    const modelsImports = await import("../models");
    const modelsKeys = Object.keys(modelsImports);
    const models = Object.values(modelsImports);

    return {
      migrationsImports,
      migrations,
      modelsImports,
      modelsKeys,
      models,
    };
  }

  private async _checkRunPendingMigrations(connection: Connection) {
    // Run pending migrations
    try {
      console.log("INFO : Checking for pending database migrations...");

      const hasPendingMigrations = await connection.showMigrations();
      if (hasPendingMigrations) {
        console.log(
          `INFO : Will apply new migrations...\n`,
          connection.migrations
            .map(
              (migration, index) =>
                `\t${index}. ${migration.name}${
                  index !== connection.migrations.length - 1 ? "\n" : ""
                }`,
            )
            .join(""),
        );
        const ranMigrations = await connection.runMigrations();
        console.log(
          `INFO : Successfully ran ${ranMigrations.length} migrations!`,
        );
      } else {
        console.log("INFO : No pending database migrations found. Perfect!");
      }
    } catch (err) {
      console.log(`ERROR: Cannot run migrations. Err:`, err);
    }
  }

  private _getConnectionOptions(
    type: ConnectionOptions["type"],
  ): ConnectionOptions {
    const optsByConnectionName: Record<string, ConnectionOptions> = {
      postgres: {
        type: "postgres",
        host: Constants.Database.Host,
        port: Constants.Database.Port,
        username: Constants.Database.User,
        password: Constants.Database.Password,
        database: Constants.Database.Name,
        synchronize: Constants.Database.Synchronize,
        logging: Constants.Database.Logging,
      },
      "better-sqlite3": {
        type: "better-sqlite3",
        database: Constants.Database.Name,
      },
    };

    if (!optsByConnectionName[type]) {
      throw new Error(`Cannot find connection options for "${type}".`);
    }

    return optsByConnectionName[type];
  }
}
