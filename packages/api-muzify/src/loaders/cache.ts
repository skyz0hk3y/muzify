import { createClient, RedisClient } from "redis";
import { ILoadable, DIContainer } from "@weedify/core";

import { Constants } from "@app/constants";
import { DIKeys } from "@app/DIKeys";

export class CacheLoader implements ILoadable {
  private _client: RedisClient;

  async register() {
    this._client = createClient(Constants.Cache.Port, Constants.Cache.Host, {
      auth_pass: Constants.Cache.Password || undefined,
      db: Constants.Cache.Db || undefined,
    });

    DIContainer.register(DIKeys.CacheConnection, this._client);
    return true;
  }

  async unregister() {
    if (this._client) {
      const isRedisGone = this._client.quit();
      console.log(
        `${
          isRedisGone ? "INFO : Successfully closed" : "ERROR: Cannot close"
        } the Redis cache connection.`,
      );
    }

    DIContainer.unregister(DIKeys.CacheConnection);
    return true;
  }
}
