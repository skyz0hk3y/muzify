import { DIContainer, Errors } from "@weedify/core";
import { Repository } from "typeorm";

import { DIKeys } from "../DIKeys";
import { AppReleaseModel, TrackModel } from "../models";

export class BackupService {
  private trackRepository: Repository<TrackModel>;
  private appReleasesRepository: Repository<AppReleaseModel>;

  constructor() {
    // FIXME(weedify): Create an @Service decorator to do that auto-magically
    // as well to "standardize" the behavior (see @Controller for example)
    this.trackRepository = DIContainer.retrieve(DIKeys.TrackRepository);
    this.appReleasesRepository = DIContainer.retrieve(
      DIKeys.AppReleaseRepository,
    );
  }

  async backupTracksSql(): Promise<string> {
    // FIXME(orm): Find a way to generate sql code
    const backup = await this.trackRepository.query(`
      SELECT * FROM public.tracks
    `);

    if (backup == null) {
      throw new Errors.EInternalServerError("Cannot backup tracks");
    }

    console.log("backup tracks", backup);

    return backup;
  }

  async backupAppReleasesSql(): Promise<string> {
    // FIXME(orm): Find a way to generate sql code
    const backup = await this.appReleasesRepository.query(`
      SELECT * FROM public.app_releases ORDER BY "versionName" DESC
    `);

    if (backup == null) {
      throw new Errors.EInternalServerError("Cannot backup app releases");
    }

    console.log("backup app_releases", backup);

    return backup;
  }
}
