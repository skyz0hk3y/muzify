import { DIContainer } from "@weedify/core";
import { RedisClient } from "redis";
import { Result } from "ytpl";

import { DIKeys } from "../DIKeys";
import { Track, TrackMetas } from "../types";

export class CacheService {
  private client: RedisClient;

  constructor() {
    this.client = DIContainer.retrieve(DIKeys.CacheConnection);
  }

  hasKey(key: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.client.exists(key, (err, reply) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(reply === 1);
      });
    });
  }

  get(key: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.client.get(key, (err, data) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(data);
      });
    });
  }

  set(key: string, value: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.client.set(key, value, (err, reply) => {
        if (err) {
          console.log(
            "[cache] Cannot update value for key=" + key + ". Error:",
            err,
          );
          reject(err);
          return;
        }

        console.log("[cache] Updated value for key=" + key);
        resolve(reply);
      });
    });
  }

  getTrackMetaCacheId(resourceId: string): string {
    return `track:${resourceId}`;
  }

  async hasTrackMeta(trackId: string): Promise<boolean> {
    return this.hasKey(this.getTrackMetaCacheId(trackId));
  }

  async getTrackMeta(trackId: string): Promise<TrackMetas> {
    const trackMetaText = await this.get(this.getTrackMetaCacheId(trackId));
    return JSON.parse(trackMetaText);
  }

  async setTrackMeta(
    trackId: string,
    track: Record<string, any>,
  ): Promise<string> {
    const trackText = JSON.stringify(track);
    return this.set(this.getTrackMetaCacheId(trackId), trackText);
  }

  getPlaylistCacheId(resourceId: string): string {
    return `playlist:${resourceId}`;
  }

  async hasPlaylist(playlistId: string): Promise<boolean> {
    return this.hasKey(this.getPlaylistCacheId(playlistId));
  }

  async getPlaylist(
    playlistId: string,
  ): Promise<{ playlist: any; results: Track[] }> {
    const playlistText = await this.get(this.getPlaylistCacheId(playlistId));
    return JSON.parse(playlistText);
  }

  async setPlaylist(playlistId: string, playlist: Result): Promise<string> {
    const playlistText = JSON.stringify(playlist);
    return this.set(this.getPlaylistCacheId(playlistId), playlistText);
  }

  getSpotifyPlaylistCacheId(resourceId: string): string {
    return `spotify:playlist:${resourceId}`;
  }

  async hasSpotifyPlaylist(spotifyId: string): Promise<boolean> {
    return this.hasKey(this.getSpotifyPlaylistCacheId(spotifyId));
  }

  async getSpotifyPlaylist(
    spotifyId: string,
  ): Promise<{ playlist: any; results: Track[] }> {
    const playlistText = await this.get(
      this.getSpotifyPlaylistCacheId(spotifyId),
    );
    return JSON.parse(playlistText);
  }

  async setSpotifyPlaylist(
    spotifyId: string,
    playlist: { playlist: any; results: Track[] },
  ): Promise<string> {
    const playlistText = JSON.stringify(playlist);
    return this.set(this.getSpotifyPlaylistCacheId(spotifyId), playlistText);
  }

  getSearchResultsCacheId(resourceId: string): string {
    return `search-results:${resourceId}`;
  }

  async hasSearchResults(query: string): Promise<boolean> {
    return this.hasKey(this.getSearchResultsCacheId(query));
  }

  async getSearchResults(query: string): Promise<TrackMetas> {
    const searchResultsText = await this.get(
      this.getSearchResultsCacheId(query),
    );
    return JSON.parse(searchResultsText);
  }

  async setSearchResults(
    query: string,
    searchResults: Record<string, any>,
  ): Promise<string> {
    const searchResultsText = JSON.stringify(searchResults);
    return this.set(this.getSearchResultsCacheId(query), searchResultsText);
  }
}
