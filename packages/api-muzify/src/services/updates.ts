import { DIContainer } from "@weedify/core";
import { validate, ValidationError } from "class-validator";
import { Repository } from "typeorm";
import { Constants } from "../constants";
import { DIKeys } from "../DIKeys";

import { AppReleaseModel } from "../models";
import { AppRelease, AppReleaseABIs, CreateAppReleaseDTO } from "../types";

export class UpdatesService {
  private appReleaseRepository: Repository<AppReleaseModel>;

  constructor() {
    this.appReleaseRepository = DIContainer.retrieve(
      DIKeys.AppReleaseRepository,
    );
  }

  public async hasRelease(versionName: string): Promise<boolean> {
    const release = await this.appReleaseRepository.findOne({ versionName });
    return release != null;
  }

  public async getLatestReleaseVersionName(): Promise<null | string> {
    const lastRelease = await this.appReleaseRepository.findOne({
      order: {
        versionName: "DESC",
      },
    });

    if (!lastRelease) {
      return null;
    }

    return lastRelease.versionName;
  }

  public async getRelease(versionName: string): Promise<AppRelease> {
    const {
      apk_url_arm64_v8a,
      apk_url_armeabi_v7a,
      apk_url_x86,
      apk_url_x86_64,
      ...release
    } = await this.appReleaseRepository.findOne({
      versionName,
    });

    return {
      ...release,
      apksUrls: {
        "arm64-v8a": apk_url_arm64_v8a,
        "armeabi-v7a": apk_url_armeabi_v7a,
        x86: apk_url_x86,
        x86_64: apk_url_x86_64,
      },
    };
  }

  public async setRelease(release: AppReleaseModel): Promise<AppReleaseModel> {
    const sameVersionRelease = await this.appReleaseRepository.findOne({
      where: { versionName: release.versionName },
    });

    if (sameVersionRelease != null) {
      const updateResult = await this.appReleaseRepository.update(
        { versionName: release.versionName },
        release,
      );
      console.log(
        `INFO : Release (${release.versionName}) updated:`,
        updateResult.affected,
      );
      return this.appReleaseRepository.findOne({
        where: { versionName: release.versionName },
      });
    }

    const insertResult = await this.appReleaseRepository.insert(release);
    console.log(
      `INFO : New release (${release.versionName}) pushed to users:`,
      insertResult.identifiers,
    );
    return release;
  }

  public isValidAbi(abiCode: string): boolean {
    if (!Constants.Updates.DevicesAbisMap[abiCode]) {
      return false;
    }
    return true;
  }

  public getValidAbis(): AppReleaseABIs[] {
    return Object.keys(Constants.Updates.DevicesAbisMap) as AppReleaseABIs[];
  }

  public async isValidCreateAppReleaseDTO(
    properties: CreateAppReleaseDTO,
  ): Promise<{
    isValid: boolean;
    validationErrors: ValidationError[];
    dataModel: AppReleaseModel;
  }> {
    let releaseData: AppReleaseModel = new AppReleaseModel();
    releaseData.versionName = properties.versionName;
    releaseData.forceUpdate = properties.forceUpdate;
    releaseData.whatsNew = properties.whatsNew;
    releaseData.apk_url_arm64_v8a =
      properties.apkUrls["arm64-v8a"] || undefined;
    releaseData.apk_url_armeabi_v7a =
      properties.apkUrls["armeabi-v7a"] || undefined;
    releaseData.apk_url_x86 = properties.apkUrls.x86 || undefined;
    releaseData.apk_url_x86_64 = properties.apkUrls.x86_64 || undefined;

    const validationErrors = await validate(releaseData, {
      forbidUnknownValues: true,
    });

    return {
      dataModel: releaseData,
      isValid: validationErrors.length === 0,
      validationErrors,
    };
  }
}
