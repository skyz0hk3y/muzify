import yts from "yt-search";

export class SearchService {
  constructor() {}

  async search(searchTerms: string): Promise<any[]> {
    return new Promise((resolve, reject) => {
      console.log(`[search] query: ${searchTerms}`);
      return yts(searchTerms, (err, results) => {
        if (err) {
          console.log(`[search] error in yt-search: ${err.message}`);
          return reject(err);
        }

        return resolve(results);
      });
    });
  }
}
