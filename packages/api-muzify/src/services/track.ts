import { Repository } from "typeorm";
import { videoInfo } from "ytdl-core";

import { TrackModel } from "@models/Track";

import { TrackMetas } from "../types";
import { DIContainer } from "@weedify/core";
import { DIKeys } from "../DIKeys";

export class TrackService {
  private tracksRepository: Repository<TrackModel>;

  constructor() {
    this.tracksRepository = DIContainer.retrieve(DIKeys.TrackRepository);
  }

  public async getTrackById(trackId: string): Promise<TrackModel> {
    return this.tracksRepository.findOne({ where: { id: trackId } });
  }

  public async getTrackByProviderId(providerId: string): Promise<TrackModel> {
    return this.tracksRepository.findOne({ where: { providerId } });
  }

  public async getTracksByProviderName(
    providerName: "youtube" | "spotify",
  ): Promise<TrackModel[]> {
    return this.tracksRepository.find({ where: { providerName } });
  }

  public async createOrUpdateTrack(
    trackId: string,
    data: TrackModel,
  ): Promise<TrackModel> {
    const track = await this.tracksRepository.findOne({
      where: { id: trackId },
    });

    if (!track) {
      const insertResult = await this.tracksRepository.insert(data);
      const insertedId =
        insertResult.identifiers.length >= 1
          ? insertResult.identifiers[0]
          : null;

      if (!insertedId) {
        throw new Error(`ORM Error. Cannot insert new track. (id=${trackId})`);
      }

      return this.tracksRepository.findOne({ where: { id: insertedId } });
    }

    const updateResult = await this.tracksRepository.update(
      { id: trackId },
      data,
    );

    if (updateResult.affected <= 0) {
      throw new Error(`ORM Error. Cannot update track. (id=${trackId})`);
    }

    return this.tracksRepository.findOne({ where: { id: trackId } });
  }

  public trackToTrackMetas(track: TrackModel): TrackMetas {
    return {
      videoId: track.id,
      title: track.title,
      author: {
        id: track.authorId,
        name: track.authorName,
        avatar: null, // FIXME: loss of data here :x
      },
      cover: {
        url: track.artworkUrl,
        // FIXME: Find a way to figure this out.
        width: 94, // YouTube's hqdefault image height, so we get a square
        height: 94, // YouTube's hqdefault image height
      },
      duration: track.duration,
    };
  }

  public videoInfoToTrackMetas(videoInfo: videoInfo): TrackMetas {
    if (!videoInfo || !videoInfo.videoDetails) {
      throw new Error("No video info passed to videoInfoToTrackMetas.");
    }

    return {
      videoId: videoInfo.videoDetails.videoId,
      title: videoInfo.videoDetails.title,
      author: {
        id: videoInfo.videoDetails.author.id,
        name: videoInfo.videoDetails.author.name,
        avatar: videoInfo.videoDetails.author.thumbnails[0],
      },
      cover: videoInfo.videoDetails.thumbnails[0],
      duration: parseInt(videoInfo.videoDetails.lengthSeconds, 10),
    };
  }

  getTrackArtist = (track: TrackModel): string => {
    if (!track.authorName) {
      return "<unknown artist>";
    }

    const sanitizedArtist = track.authorName
      .replace(/VEVO/gi, "")
      .replace(/ - Topic/gi, "")
      .replace(/Music/, "")
      .replace(/ Offici(e|a)l/gi, "")
      .trim();

    return sanitizedArtist;
  };

  getTrackTitle = (track: TrackModel): string => {
    if (!track.title) {
      return "<unknown title>";
    }

    const condensedArtist = track.authorName.replace(/\s+/gi, "");

    const sanitizedTitle = track.title
      // .replace(/ - /, '')
      .replace(track.authorName, "")
      .replace(track.authorName.toUpperCase(), "")
      .replace(track.authorName.toLowerCase(), "")
      .replace(track.authorName.replace(/(et|and)/gi, "&"), "")
      .replace(/\(?Clip? ?Officiel?\)?/gi, "")
      .replace(/\(?Audio\)?/gi, "")
      .replace(/VEVO/gi, "")
      .replace(" Officiel)", "")
      .replace(condensedArtist, "")
      .replace(`${condensedArtist}VEVO`, "")
      .replace(`${condensedArtist}Officiel`, "")
      .replace(/Ft. /gi, "feat. ")
      .replace(/Feat. /gi, "feat. ")
      .replace(/Featuring /gi, "feat. ")
      .replace(/.* - (.*)/gi, "$1")
      .replace(/.*"(.*)"(.*)/g, "$1$2")
      .replace(/\(.*\)?/gi, "")
      .replace(/\[.*\]?/gi, "")
      .replace(/^\s+?-\s+?/, "")
      .replace(/^, /, "")
      .replace(/ \|.*$/gi, "")
      .replace(/\)$/gi, "")
      .trim();

    return sanitizedTitle;
  };

  sanitizeTrack = (track: TrackModel): TrackModel => {
    const defaults: Partial<TrackModel> = {
      title: "<unknown title>",
      authorName: "<unknown artist>",
    };

    const sanitizedArtistTrack = {
      ...defaults,
      ...track,
      artist: this.getTrackArtist({ ...defaults, ...track }),
    };

    const sanitizedTrack = {
      ...sanitizedArtistTrack,
      title: this.getTrackTitle({ ...defaults, ...sanitizedArtistTrack }),
    };

    return sanitizedTrack;
  };
}
