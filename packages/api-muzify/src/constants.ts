import { ConnectionOptions } from "typeorm";

import { ConstantsType } from "./types";
import { YOUTUBE_FULL_COOKIE_STR } from "./youtubeCookie";

export const Constants: ConstantsType = {
  API: {
    BaseUrl: String(process.env.BASE_URL),
    Debug: Boolean(process.env.DEBUG === "true"),
    Host: String(process.env.HOST || ""),
    Port: process.env.PORT ? Number(process.env.PORT || 3000) : undefined,
  },
  Cache: {
    Host: process.env.CACHE_HOST || undefined,
    Port: Number(process.env.CACHE_PORT || 6379),
    Password: process.env.CACHE_PASSWORD
      ? String(process.env.CACHE_PASSWORD || "")
      : undefined,
    Db: process.env.CACHE_DB ? String(process.env.CACHE_DB || "") : undefined,
  },
  Database: {
    Type:
      ((process.env
        .TYPEORM_CONNECTION as unknown) as ConnectionOptions["type"]) ||
      "postgres",
    Name: String(process.env.TYPEORM_NAME || "postgres"),
    Host: process.env.TYPEORM_HOST
      ? String(process.env.TYPEORM_HOST || "127.0.0.1")
      : undefined,
    Port: process.env.TYPEORM_PORT
      ? Number(process.env.TYPEORM_PORT || 5432)
      : undefined,
    User: process.env.TYPEORM_USERNAME
      ? String(process.env.TYPEORM_USERNAME || "postgres")
      : undefined,
    Password: process.env.TYPEORM_PASSWORD
      ? String(process.env.TYPEORM_PASSWORD || "")
      : undefined,
    Debug: process.env.TYPEORM_DEBUG
      ? Boolean(process.env.TYPEORM_DEBUG || false)
      : undefined,
    Synchronize: process.env.TYPEORM_SYNCHRONIZE
      ? Boolean(process.env.TYPEORM_SYNCHRONIZE || false)
      : undefined,
    Logging: process.env.TYPEORM_LOGGING
      ? Boolean(process.env.TYPEORM_LOGGING || false)
      : undefined,
    RunMigrations: process.env.DATABASE_RUN_MIGRATIONS
      ? Boolean(process.env.DATABASE_RUN_MIGRATIONS)
      : true,
  },
  Network: {
    RequestOpts: {
      headers: {
        Cookie: YOUTUBE_FULL_COOKIE_STR,
      },
    },
  },
  Secrets: {
    AdminAuthToken: String(process.env.ADMIN_AUTH_SECRET || "unsecure-mode"),
    SpotifyClientCredentials: String(process.env.SPOTIFY_CLIENT_CREDENTIALS),
  },
  Streaming: {
    QualitiesMap: {
      low: "AUDIO_QUALITY_LOW",
      medium: "AUDIO_QUALITY_MEDIUM",
    },
  },
  Updates: {
    DevicesAbisMap: {
      "arm64-v8a": "arm64-v8a",
      "arm64 v8": "arm64-v8a",
      v8a: "arm64-v8a",
      v8: "arm64-v8a",
      arm64: "arm64-v8a",
      "armeabi-v7a": "armeabi-v7a",
      "armeabi v7": "armeabi-v7a",
      v7a: "armeabi-v7a",
      v7: "armeabi-v7a",
      armeabi: "armeabi-v7a",
      Intel: "x86_64",
      "Intel x86-64h Haswell": "x86_64",
      x86_64: "x86_64",
      x86: "x86",
    },
  },
};
