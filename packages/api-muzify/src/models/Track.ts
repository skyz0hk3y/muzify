import { Entity, Column, PrimaryColumn } from "typeorm";
import {
  IsDefined,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsUrl,
} from "class-validator";

@Entity({ name: "tracks" })
export class TrackModel {
  @IsDefined()
  @IsNotEmpty()
  @PrimaryColumn()
  id: string;

  @IsDefined()
  @IsNotEmpty()
  @Column({ name: "author_id" })
  authorId: string;

  @IsDefined()
  @IsNotEmpty()
  @Column({ name: "author_name" })
  authorName: string;

  @IsDefined()
  @IsNotEmpty()
  @Column()
  title: string;

  @IsDefined()
  @IsNotEmpty()
  @IsUrl()
  @Column({ name: "artwork_url" })
  artworkUrl: string;

  @IsDefined()
  @IsNotEmpty()
  @IsNumber()
  duration: number;

  @IsDefined()
  @IsNotEmpty()
  @Column({ name: "provider_id" })
  providerId: string;

  @IsDefined()
  @IsEnum(["youtube", "spotify"])
  @IsNotEmpty()
  @Column({ name: "provider_name" })
  providerName: "youtube" | "sportify";
}
