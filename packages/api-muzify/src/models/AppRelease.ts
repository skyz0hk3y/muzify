import { Entity, Column, PrimaryColumn } from "typeorm";
import {
  IsNotEmpty,
  IsDefined,
  IsSemVer,
  IsBoolean,
  IsUrl,
} from "class-validator";

@Entity({ name: "app_releases" })
export class AppReleaseModel {
  @IsDefined()
  @IsSemVer()
  @PrimaryColumn() // => version_name
  versionName: string;

  @IsDefined()
  @IsNotEmpty()
  @Column() // => whats_new
  whatsNew: string;

  @IsDefined()
  @IsBoolean()
  @Column() // => force_update
  forceUpdate: boolean;

  @IsDefined()
  @IsNotEmpty()
  @IsUrl()
  @Column()
  apk_url_arm64_v8a: string;

  @IsDefined()
  @IsNotEmpty()
  @IsUrl()
  @Column()
  apk_url_armeabi_v7a: string;

  @IsDefined()
  @IsNotEmpty()
  @IsUrl()
  @Column()
  apk_url_x86_64: string;

  @IsDefined()
  @IsNotEmpty()
  @IsUrl()
  @Column()
  apk_url_x86: string;
}
