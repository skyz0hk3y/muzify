import qs from "qs";

import { NextFunction /* Response */ } from "express";
import { Request } from "express";
import { Constants } from "../constants";
import { Errors } from "@weedify/core";

/**
 * A decorator that checks if the request contains the right set of credentials
 * allows to let the request continue, or not.
 */
export const ProtectedRoute = () => {
  return (target, propertyKey, descriptor) => {
    if (typeof descriptor.value !== "function") {
      throw new SyntaxError(
        "Only functions can be marked as protected by authorization",
      );
    }

    const originalRouteFn = descriptor.value;
    const methodSignature = `${target.constructor.name}#${propertyKey}`;

    descriptor.value = async function (...args) {
      if (args.length !== 3) {
        await originalRouteFn.apply(this, args);
        return;
      }

      const req: Request = args[0];
      // const res: Response = args[1];
      const next: NextFunction = args[2];

      try {
        let bearerToken: string;

        const authorizationHeader = req.header("authorization");
        const tokenQueryParam = req.query["token"];
        const tokenBodyParam = req.body["token"];

        if (authorizationHeader != null) {
          bearerToken = authorizationHeader.replace(/^Bearer /, "");
        }

        if (tokenQueryParam != null) {
          bearerToken = qs.stringify(tokenQueryParam);
        }

        if (tokenBodyParam != null) {
          bearerToken = tokenBodyParam;
        }

        if (bearerToken == null) {
          throw new Errors.EUnauthorized(
            "You cannot access this route. Missing authorization.",
          );
        }

        // This is where real auth' logic (allow/deny req to continue) would take place
        if (bearerToken !== Constants.Secrets.AdminAuthToken) {
          throw new Errors.EForbidden(
            "You cannot access this route. Invalid authorization.",
          );
        }

        await originalRouteFn.apply(this, args);
      } catch (err) {
        next(err);
      }
    };

    console.log(`INFO : @ProtectedRoute -> ${methodSignature}`);
  };
};
