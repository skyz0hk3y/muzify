import { NextFunction, Request, Response } from "express";
import { Controller, BaseController, Get, Inject, Errors } from "@weedify/core";

import { DIKeys } from "@app/DIKeys";
import { BackupService } from "@app/services";

import { ProtectedRoute } from "../../decorators/ProtectedRoute";
import { Connection } from "typeorm";

@Controller("PrivateAdminController", "/private")
export class AdminController extends BaseController {
  @ProtectedRoute()
  @Get("/backup/:entity")
  @Inject(DIKeys.BackupService)
  async backupEntityFromDatabase(
    req: Request,
    _: Response,
    __: NextFunction,
    backupService: BackupService,
  ) {
    const { entity } = req.params;

    console.log(`Generating backup for entity ${entity}`);

    let backupData;
    switch (entity) {
      case "tracks":
        backupData = await backupService.backupTracksSql();
        break;
      case "app_releases":
        backupData = await backupService.backupAppReleasesSql();
        break;
      default:
        break;
    }

    if (!backupData) {
      throw new Errors.EInternalServerError(
        "Looks like there are no data to backup in this entity, or the entity does not exists",
      );
    }

    return {
      backup: backupData,
    };
  }

  @ProtectedRoute()
  @Get("/housekeeping/cleanup-db")
  @Inject(DIKeys.DatabaseConnection)
  async cleanupDb(
    _: Request,
    __: Response,
    ___: NextFunction,
    databaseConnection: Connection,
  ) {
    await databaseConnection.dropDatabase();

    return {
      yeay: true,
    };
  }
}
