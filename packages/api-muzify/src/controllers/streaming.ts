import path from "path";

import { Request, Response } from "express";
import {
  Controller,
  BaseController,
  Get,
  Errors,
  Post,
  Inject,
} from "@weedify/core";

import ffmpeg from "fluent-ffmpeg";

import {
  default as downloadYT,
  getInfo,
  validateID,
  // chooseFormat,
} from "ytdl-core";

import { TrackMetas } from "../types";
import { Constants } from "../constants";
import { CacheService } from "../services/cache";
import { TrackService } from "../services/track";

@Controller("StreamingController", "/stream")
export class StreamingController extends BaseController {
  constructor() {
    super();

    ffmpeg.setFfmpegPath(path.resolve(__dirname, "../../../ffmpeg-bin/ffmpeg"));
    ffmpeg.setFfprobePath(
      path.resolve(__dirname, "../../../ffmpeg-bin/ffprobe"),
    );
  }

  @Get("/:videoId")
  @Inject("CacheService", "TrackService")
  async getInfos(
    req: Request,
    _,
    __,
    cache: CacheService,
    trackService: TrackService,
  ) {
    const { videoId } = req.params;

    console.log(`[GET] /stream/${videoId} called...`);

    const hasTrackMetaInCache = await cache.hasTrackMeta(videoId);
    if (hasTrackMetaInCache) {
      const trackMeta = await cache.getTrackMeta(videoId);
      return {
        __cached: true,
        __cacheId: cache.getTrackMetaCacheId(videoId),
        ...trackMeta,
      };
    }

    const idIsValid = validateID(videoId);
    if (!idIsValid) {
      throw new Errors.EBadRequest("Video ID is invalid.");
    }

    const videoInfo = await getInfo(videoId, {
      requestOptions: Constants.Network.RequestOpts,
    });
    if (!videoInfo) {
      throw new Errors.ENotFound("Cannot get video details.");
    }

    const trackMeta: TrackMetas = trackService.videoInfoToTrackMetas(videoInfo);

    await cache.setTrackMeta(videoId, trackMeta);
    return trackMeta;
  }

  @Get("/:videoId/audio")
  @Inject("CacheService", "TrackService")
  async getStreamAudio(
    req: Request,
    res: Response,
    _,
    cache: CacheService,
    trackService: TrackService,
  ) {
    const { videoId } = req.params;
    const { format = "webm", quality = "medium" } = req.query;
    // const { range } = req.headers;

    console.log(
      `[GET] /stream/${videoId}/audio?format=${format}&quality=${quality} called...`,
    );

    const isFormatSupported =
      ["webm", "mp4", "mp3"].includes(format as string) === false;
    const isQualitySupported =
      Object.keys(Constants.Streaming.QualitiesMap).includes(
        quality as string,
      ) === false;

    if (isFormatSupported) {
      throw new Errors.EBadRequest(
        "Invalid format. Must be either `mp4` or `webm`.",
      );
    }

    if (isQualitySupported) {
      throw new Errors.EBadRequest(
        "Invalid quality. Must be either `low` or `medium`.",
      );
    }

    const idIsValid = validateID(videoId);
    if (!idIsValid) throw new Errors.EBadRequest("Video ID is invalid.");

    let videoInfo: TrackMetas;
    const hasTrackMetaInCache = await cache.hasTrackMeta(videoId);

    if (hasTrackMetaInCache) {
      videoInfo = await cache.getTrackMeta(videoId);
    } else {
      const ytInfos = await getInfo(videoId, {
        requestOptions: Constants.Network.RequestOpts,
      });
      videoInfo = trackService.videoInfoToTrackMetas(ytInfos);
      await cache.setTrackMeta(videoId, videoInfo);
    }

    let contentLength = "";
    const preferFormat = format === "mp3" ? "mp4" : format;
    const preferQuality = Constants.Streaming.QualitiesMap[String(quality)];

    const audioStream = downloadYT(videoId, {
      filter: (format) => {
        if (
          format.container === preferFormat &&
          format.audioQuality === preferQuality
        ) {
          contentLength = format.contentLength;
          return true;
        }
        return false;
      },
      // FIXME: MAKE RANGE WORKING FOR BETTER STREAMING RESULT.
      // range: rangeYT,
      requestOptions: Constants.Network.RequestOpts,
    });

    audioStream.on("info", () => {
      // REFACTOR(streaming): This logic should be moved to a service function.
      const rangeHeader = req.headers.range || null;
      const rangePosition =
        rangeHeader != null
          ? rangeHeader.replace(/bytes=/, "").split("-")
          : null;
      const startRange = rangePosition ? parseInt(rangePosition[0], 10) : 0;
      const endRange =
        rangePosition && rangePosition[1].length > 0
          ? parseInt(rangePosition[1], 10)
          : parseInt(contentLength) - 1;
      const chunkSize = endRange - startRange + 1;

      // MP3 is potentially CPU intensive...
      if (format === "mp3") {
        res.writeHead(206, {
          "Content-Type": "audio/mp3",
          "Content-Length": chunkSize,
          "Content-Range":
            "bytes " + startRange + "-" + endRange + "/" + contentLength,
          "Accept-Ranges": "bytes",
        });
        ffmpeg(audioStream).format("mp3").pipe(res);
      } else {
        res.writeHead(206, {
          "Content-Type": `audio/${preferFormat}`,
          "Content-Length": chunkSize,
          "Content-Range":
            "bytes " + startRange + "-" + endRange + "/" + contentLength,
          "Accept-Ranges": "bytes",
        });
        audioStream.pipe(res);
      }
    });

    audioStream.on("error", (err) => {
      throw new Errors.EInternalServerError(err.message);
    });
  }

  // FIXME(security): Rate-limit this via a @RateLimitedRoute() decorator
  @Post("/get-infos-batch")
  @Inject("CacheService", "TrackService")
  async getInfosBatch(
    req: Request,
    _,
    __,
    cache: CacheService,
    trackService: TrackService,
  ) {
    const { trackIds = [] }: { trackIds: string[] } = req.body;
    console.log(
      `[POST] /stream/get-infos-batch called ${
        trackIds.length === 0
          ? "without body..."
          : `with body:\n${trackIds.join(", ")}`
      }`,
    );

    if (!trackIds || trackIds.length === 0) {
      throw new Errors.EBadRequest(
        "Field `trackIds` cannot be null and must contain at least 1 trackId.",
      );
    }

    const results: TrackMetas[] = await Promise.all(
      trackIds.map(async (trackId) => {
        const idIsValid = validateID(trackId);
        if (!idIsValid) {
          return Promise.reject(new Errors.EBadRequest("Video ID is invalid."));
        }

        const hasTrackMetaInCache = await cache.hasTrackMeta(trackId);
        if (hasTrackMetaInCache) {
          const trackMeta = await cache.getTrackMeta(trackId);
          return Promise.resolve(trackMeta);
        }

        const videoInfo = await getInfo(trackId, {
          requestOptions: Constants.Network.RequestOpts,
        });

        if (!videoInfo) {
          throw new Errors.ENotFound("Cannot get video details.");
        }

        const trackMeta: TrackMetas = trackService.videoInfoToTrackMetas(
          videoInfo,
        );

        await cache.setTrackMeta(trackId, trackMeta);
        return Promise.resolve(trackMeta);
      }),
    );

    return {
      results: results.filter((result) => {
        return result.videoId != null;
      }),
    };
  }
}
