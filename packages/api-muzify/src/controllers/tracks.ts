import { Controller, BaseController, Get, Inject, Errors } from "@weedify/core";
import { getInfo, validateID } from "ytdl-core";

import { TrackService } from "@services/track";

import { Constants } from "../constants";
import { DIKeys } from "../DIKeys";
import { TrackMetas } from "../types";
import { TrackModel } from "../models";

@Controller("TracksController", "/tracks")
export class TracksController extends BaseController {
  constructor() {
    super();
  }

  @Get("/:trackId")
  @Inject(DIKeys.TrackService)
  async findTrackById(req, _, __, trackService: TrackService) {
    const { trackId }: { trackId: string } = req.params;

    console.log(`[GET] /tracks/${trackId} called...`);

    if (!trackId) {
      throw new Errors.EBadRequest("Missing required route param /:trackId.");
    }

    const cachedTrack = await trackService.getTrackById(trackId);
    if (cachedTrack) {
      return {
        __cached: true,
        __cacheId: cachedTrack.id,
        ...trackService.trackToTrackMetas(cachedTrack),
      };
    }

    const idIsValid = validateID(trackId);
    if (!idIsValid) {
      throw new Errors.EBadRequest("Video ID is invalid.");
    }

    const videoInfo = await getInfo(trackId, {
      requestOptions: Constants.Network.RequestOpts,
    });

    if (!videoInfo) {
      throw new Errors.ENotFound(
        `Cannot find the requested track. (trackId=${trackId})`,
      );
    }

    const trackMetas: TrackMetas = trackService.videoInfoToTrackMetas(
      videoInfo,
    );

    const track: TrackModel = await trackService.createOrUpdateTrack.call(
      trackService,

      trackId,
      trackService.sanitizeTrack({
        id: trackId,
        artworkUrl: trackMetas.cover.url,
        authorId: trackMetas.author.id,
        authorName: trackMetas.author.name,
        duration: trackMetas.duration || 0,
        providerId: trackId,
        providerName: "youtube",
        title: trackMetas.title,
      }),
    );

    console.log(
      `INFO : Added new Track to database (authorName=${track.authorName} title=${track.title} trackId=${track.id})`,
    );

    return {
      ...trackService.trackToTrackMetas(track),
    };
  }
}
