import { URLSearchParams } from "url";
import { Request, Response } from "express";
import { BaseController, Controller, Errors, Get, Inject } from "@weedify/core";
import { fetch, Headers } from "cross-fetch";
import { default as searchPlaylist, validateID } from "ytpl";
import { default as SpotifyToYouTube } from "spotify-to-youtube";
import { default as SpotifyWebApi } from "spotify-web-api-node";

import { Constants } from "../constants";
import { Track } from "../types";
import { CacheService } from "../services";
import { DIKeys } from "../DIKeys";

@Controller("PlaylistController", "/playlist")
export class PlaylistController extends BaseController {
  constructor() {
    super();
  }

  @Inject(DIKeys.CacheService)
  @Get("/:listId")
  async getPlaylistInfos(req: Request, res: Response, __, cache: CacheService) {
    const { listId: _listId } = req.params;
    const listId = decodeURIComponent(_listId);

    console.log(`[GET] /playlist/${listId} called ...`);

    if (!validateID(listId)) {
      throw new Error("Invalid playlist id.");
    }

    const hasPlaylistInCache = await cache.hasPlaylist(listId);
    if (hasPlaylistInCache) {
      return res.json({
        __cached: true,
        __cacheId: cache.getPlaylistCacheId(listId),
        ...(await cache.getPlaylist(listId)),
      });
    }

    const results = await searchPlaylist(listId);
    await cache.setPlaylist(listId, results);

    return {
      listId,
      results,
    };
  }

  @Inject(DIKeys.CacheService)
  @Get("/spotify/:spotifyId")
  async getSpotifyPlaylist(
    req: Request,
    res: Response,
    _next,
    cacheService: CacheService,
  ) {
    const { spotifyId } = req.params;
    // const { reload } = req.query;

    console.log(`[GET] /playlist/spotify/${spotifyId} called ...`);

    const hasSpotifyPlaylist = await cacheService.hasSpotifyPlaylist(spotifyId);
    if (hasSpotifyPlaylist) {
      const cachedPlaylist = await cacheService.getSpotifyPlaylist(spotifyId);
      return res.json({
        error: null,
        data: {
          __cached: true,
          __cacheId: cacheService.getSpotifyPlaylistCacheId(spotifyId),
          ...cachedPlaylist,
        },
      });
    }

    const data = new URLSearchParams();
    data.append("grant_type", "client_credentials");

    const tokenRes = await fetch(`https://accounts.spotify.com/api/token`, {
      method: "POST",
      headers: new Headers({
        Authorization: `Basic ${Constants.Secrets.SpotifyClientCredentials}`,
        "Content-Type": "application/x-www-form-urlencoded",
      }),
      body: data,
    });

    const tokenJson = await tokenRes.json();
    if (!tokenJson || !tokenJson.access_token) {
      throw new Errors.EInternalServerError("Cannot get Spotify access token");
    }

    const spotifyApi = new SpotifyWebApi();
    spotifyApi.setAccessToken(tokenJson.access_token);

    const spotifyToYouTube = SpotifyToYouTube(spotifyApi);

    const fetchPlaylistSegment = async (url: string) => {
      const playlistRes = await fetch(url, {
        method: "GET",
        headers: new Headers({
          Accept: "application/json",
          Authorization: `${tokenJson.token_type} ${tokenJson.access_token}`,
          "Content-Type": "application/json",
        }),
      });

      const playlistJson = await playlistRes.json();
      if (!playlistJson) {
        throw new Errors.ENotFound(
          `Cannot find tracks for playlist ${spotifyId}`,
        );
      }

      return playlistJson;
    };

    const fetchPlaylistDetails = async (id: string) => {
      const playlistRes = await fetch(
        `https://api.spotify.com/v1/playlists/${id}?fields=id,name,owner(display_name),images,public`,
        {
          method: "GET",
          headers: new Headers({
            Accept: "application/json",
            Authorization: `${tokenJson.token_type} ${tokenJson.access_token}`,
            "Content-Type": "application/json",
          }),
        },
      );

      const playlistJson = await playlistRes.json();
      if (!playlistJson) {
        throw new Errors.ENotFound(
          `Cannot find tracks for playlist ${spotifyId}`,
        );
      }

      return playlistJson;
    };

    const playlistDetails: Record<string, any> = await fetchPlaylistDetails(
      spotifyId,
    );
    let playlistItems: Record<string, any>[] = [];
    let nextUrl = `https://api.spotify.com/v1/playlists/${spotifyId}/tracks?limit=100`;

    while (nextUrl != null) {
      const playlistJson = await fetchPlaylistSegment(nextUrl);
      nextUrl = playlistJson.next;
      playlistItems = [...playlistItems, ...playlistJson.items];
    }

    const tracks: Track[] = await Promise.all(
      playlistItems.map(
        async (item: Record<string, any>): Promise<Track> => {
          try {
            const youtubeId = await spotifyToYouTube(item.track.id);
            return Promise.resolve({
              id: youtubeId,
              title: item.track.name,
              artist: item.track.artists[0].name,
              url: `${Constants.API.BaseUrl}/stream/${youtubeId}/audio`,
              artwork: `https://i.ytimg.com/vi/${youtubeId}/hqdefault.jpg`,
              album: item.track.album.name,
              date: item.track.album.release_date,
              duration: item.track.duration_ms / 60, // in seconds
            });
          } catch (err) {
            return Promise.resolve(null);
          }
        },
      ),
    );

    const spotifyPlaylist = {
      playlist: {
        id: playlistDetails.id,
        name: playlistDetails.name,
        ownerName: playlistDetails.owner.display_name,
        artwork: playlistDetails.images[0].url,
        isPublic: playlistDetails.public,
      },
      results: tracks.filter((t: Track | null) => t != null),
    };

    await cacheService.setSpotifyPlaylist(spotifyId, spotifyPlaylist);

    return res.json({
      error: null,
      data: spotifyPlaylist,
    });
  }
}
