import { Controller, BaseController, Get, Inject } from "@weedify/core";
import slugify from "slugify";
import { DIKeys } from "../DIKeys";

import { CacheService, SearchService } from "../services";

@Controller("SearchController", "/search")
export class SearchController extends BaseController {
  @Get("/music")
  @Inject(DIKeys.CacheService, DIKeys.SearchService)
  async searchMusic(
    req,
    _,
    __,
    cache: CacheService,
    searchService: SearchService,
  ) {
    const { q } = req.query;

    const safeQuery = slugify(q, {
      lower: true,
      strict: true,
    });

    console.log(`[GET] /search/music?q=${safeQuery} called ...`);

    const hasSearchResulsInCache = await cache.hasSearchResults(safeQuery);
    if (hasSearchResulsInCache) {
      return {
        __cached: true,
        __cacheId: cache.getSearchResultsCacheId(safeQuery),
        searchTerms: q,
        results: await cache.getSearchResults(safeQuery),
      };
    }

    const results = await searchService.search(q);
    cache.setSearchResults(safeQuery, results);

    return {
      searchTerms: q,
      results,
    };
  }
}
