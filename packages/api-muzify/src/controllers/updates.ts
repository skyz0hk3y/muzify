import {
  Controller,
  BaseController,
  Get,
  Post,
  Errors,
  DIContainer,
} from "@weedify/core";
import { Request, Response } from "express";

import { Constants } from "@app/constants";
import { UpdatesService } from "@services/updates";

@Controller("UpdatesController", "/updates")
export class UpdatesController extends BaseController {
  private updatesService: UpdatesService;

  constructor() {
    super();

    this.updatesService = DIContainer.retrieve("UpdatesService");
  }

  @Get("/check")
  async checkForUpdates(req: Request, res: Response) {
    const { abi_code: abiCode } = req.query;

    console.log(`[GET] /updates/check?abi_code=${abiCode} called...`);

    const safeAbiCode = String(abiCode) || "armeabi-v7a";
    if (!this.updatesService.isValidAbi(safeAbiCode)) {
      throw new Errors.EBadRequest(
        `Invalid processor kind for ABI code "${abiCode}".
        Allowed: ${this.updatesService.getValidAbis()}`,
      );
    }

    const latestRelease = await this.updatesService.getLatestReleaseVersionName();
    if (!latestRelease) {
      throw new Errors.EInternalServerError(
        "No release has been tagged as latest.",
      );
    }

    const release = await this.updatesService.getRelease(latestRelease);
    if (!release) {
      throw new Errors.ENotFound(`No release found for v${latestRelease}.`);
    }

    const { apksUrls, ...releaseData } = release;

    const apkForProcessorKind = apksUrls[safeAbiCode];
    if (!apkForProcessorKind) {
      // TODO: Error system; warn developers about that.
      throw new Errors.EInternalServerError(`No apk has been uploaded for processor kind with ABI code "${abiCode}".
      This is likely an error coming from the releaser.
      It will be fixed once a new release is uploaded by an authorized role.`);
    }

    return res.json({
      apkUrl: apkForProcessorKind,
      ...releaseData,
    });
  }

  @Get("/changelog")
  async getChangelogForRelease(req: Request) {
    const { version: versionName } = req.query;

    console.log(`[GET] /updates/changelog?version${versionName} called...`);

    const hasRelease = await this.updatesService.hasRelease(
      String(versionName),
    );
    if (!hasRelease) {
      throw new Errors.ENotFound(
        `No release has been found for version ${versionName}.`,
      );
    }

    const release = await this.updatesService.getRelease(String(versionName));

    return {
      versionName: release.versionName,
      versionCode: release.versionCode,
      changelog: release.whatsNew,
    };
  }

  @Post("/upload")
  async uploadRelease(req: Request) {
    const { headers, body, query } = req;
    const token = headers.authorization;

    console.log(`[POST] /updates/upload called...`);

    if (token !== Constants.Secrets.AdminAuthToken) {
      throw new Errors.EUnauthorized(
        "Access to this route is restricted. Role is not sufficient to access this endpoint.",
      );
    }

    const {
      dataModel: appReleaseData,
      isValid: isRequestBodyValid,
      validationErrors,
    } = await this.updatesService.isValidCreateAppReleaseDTO({
      versionName: body.versionName,
      forceUpdate: body.forceUpdate,
      whatsNew: body.whatsNew,
      apkUrls: {
        "arm64-v8a": body.apk_url_arm64_v8a,
        "armeabi-v7a": body.apk_url_armeabi_v7a,
        x86: body.apk_url_x86,
        x86_64: body.apk_url_x86_64,
      },
    });

    if (!isRequestBodyValid) {
      throw new Errors.EBadRequest(validationErrors.join(", "));
    }

    const alreadyExists = await this.updatesService.hasRelease(
      body.versionName,
    );

    if (!query.force && alreadyExists) {
      throw new Errors.EBadRequest(
        `A release for version ${body.versionName} has already been deployed.
        Please increment version or use the force flag if you know what you do.`,
      );
    }

    console.log(`INFO : Creating release ${body.versionName}:`, appReleaseData);
    const result = await this.updatesService.setRelease(appReleaseData);

    return {
      _statusCode: 202,
      release: result,
    };
  }
}
