import fetch from "node-fetch";

import { BaseController, Controller, DIContainer, Get } from "@weedify/core";
import { Connection } from "typeorm";
import { RedisClient } from "redis";

import { DIKeys } from "@app/DIKeys";

const getStatusForBool = (bool: boolean) => (bool ? "healthy" : "not healthy");

const getSearchStatus = async () => {
  const res = await fetch(
    `${DIContainer.retrieve(DIKeys.AppListeningUrl)}/search/music?q=PNL`,
  );
  if (!res.ok) {
    return false;
  }

  const json = await res.json();
  if (json.error) {
    return false;
  }

  return true;
};

@Controller("HealthcheckController", "/_")
export class HealthcheckController extends BaseController {
  private cache: RedisClient;
  private database: Connection;

  constructor() {
    super();
    this.cache = DIContainer.retrieve(DIKeys.CacheConnection);
    this.database = DIContainer.retrieve(DIKeys.DatabaseConnection);
  }

  @Get("/healthcheck")
  async status() {
    console.log(`[GET] /_/healthcheck called...`);

    const entitiesHealth = {
      cache: {
        version:
          this.cache != null ? this.cache.server_info.redis_version : null,
        status:
          this.cache != null
            ? getStatusForBool(this.cache.connected)
            : getStatusForBool(false),
      },
      database: {
        status:
          this.database != null
            ? getStatusForBool(this.database.isConnected)
            : getStatusForBool(false),
      },
      "Muzify Search API": {
        status: getStatusForBool(await getSearchStatus()),
      },
      "Spotify API": {
        status: getStatusForBool(true), // FIXME: Do we really want to do that?
      },
    };

    return {
      status: getStatusForBool(
        Object.values(entitiesHealth).every((o) => o.status === "healthy"),
      ),
      timestamp: Date.now(),
      entities: entitiesHealth,
    };
  }
}
