import { ConnectionOptions } from "typeorm";

export type ConstantsType = {
  API: {
    BaseUrl: string;
    Debug: boolean;
    Host: string;
    Port?: number;
  };
  Cache: {
    Host: string;
    Port: number;
    Password?: string;
    Db?: string;
  };
  Database: {
    Type: ConnectionOptions["type"];
    Name: string;
    Host?: string;
    Port?: number;
    User?: string;
    Password?: string;
    Debug?: boolean;
    Synchronize?: boolean;
    Logging?: boolean;
    RunMigrations?: boolean;
  };
  Network: {
    RequestOpts: {
      headers: Record<string, string>;
    };
  };
  Secrets: {
    AdminAuthToken: string;
    SpotifyClientCredentials: string;
  };
  Streaming: {
    QualitiesMap: Record<string, string>;
  };
  Updates: {
    DevicesAbisMap: Record<string, AppReleaseABIs>;
  };
};

export type TrackMetas = {
  videoId: string;
  title: string;
  cover: {
    url: string;
    width: number;
    height: number;
  };
  author: {
    id: string;
    name: string;
    avatar: {
      url: string;
      width: number;
      height: number;
    };
  };
  duration: number;
};

export type TrackType = "default" | "dash" | "hls" | "smoothstreaming";

type ResourceObject = any;
type PitchAlgorithm = string | number;

export interface TrackMetadata {
  duration?: number;
  title: string;
  artist: string;
  album?: string;
  description?: string;
  genre?: string;
  date?: string;
  rating?: number | boolean;
  artwork?: string | ResourceObject;
}

export interface Track extends TrackMetadata {
  id: string;
  url: string | ResourceObject;
  type?: TrackType;
  userAgent?: string;
  contentType?: string;
  pitchAlgorithm?: PitchAlgorithm;
  [key: string]: any;
}

export type AppReleaseABIs = "arm64-v8a" | "armeabi-v7a" | "x86_64" | "x86";
export type AppRelease = {
  apksUrls: Record<AppReleaseABIs, string>;
  forceUpdate: boolean;
  versionName: string;
  versionCode?: number;
  whatsNew: string;
};
export type CreateAppReleaseDTO = {
  versionName: string;
  forceUpdate: boolean;
  whatsNew: string;
  apkUrls: Record<AppReleaseABIs, string>;
};
