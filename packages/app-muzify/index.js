import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import {AppRegistry} from 'react-native';
import TrackPlayer from 'react-native-track-player';

import {name as appName} from './app.json';
import {App} from './src/App';
import {store} from './src/store';
import {playbackService} from './src/playbackService';
import {ModalProvider} from './src/core/providers/ModalProvider';
import {ThemeProvider} from '@react-navigation/native';

const AppWithProviders = () => (
  <Provider store={store}>
    <ThemeProvider>
      <ModalProvider>
        <App />
      </ModalProvider>
    </ThemeProvider>
  </Provider>
);

const registerApp = async () => {
  // Register app main component.
  AppRegistry.registerComponent(appName, () => AppWithProviders);

  // Register playback background service.
  TrackPlayer.registerPlaybackService(() => playbackService.bind(null, store));
};

registerApp();
