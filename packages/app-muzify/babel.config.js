const path = require('path');

module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: ['react-native-reanimated/plugin'],
  // plugins: [
  //   [
  //     'module-resolver',
  //     {
  //       alias: {
  //         react: require.resolve('react', {
  //           paths: [path.join(__dirname, '../../')],
  //         }),
  //         // '^react/(.+)': ([, name]) =>
  //         //   require.resolve(`react/${name}`, {
  //         //     paths: [path.join(__dirname, './')],
  //         //   }),
  //         '^react-native': require.resolve('react-native', {
  //           paths: [path.join(__dirname, '../../')],
  //         }),
  //         '^react-native-vector-icons/(.+)': ([, name]) =>
  //           require.resolve(`react-native-vector-icons/${name}`, {
  //             paths: [path.join(__dirname, '../../')],
  //           }),
  //         '^react-native-track-player': require.resolve(
  //           'react-native-track-player',
  //           {
  //             paths: [path.join(__dirname, '../../')],
  //           },
  //         ),
  //         '^react-native-track-player/(.+)': ([, name]) =>
  //           require.resolve(`react-native-track-player/${name}`, {
  //             paths: [path.join(__dirname, '../../')],
  //           }),
  //         '^react-native-safe-area-context': require.resolve(
  //           'react-native-safe-area-context',
  //           {
  //             paths: [path.join(__dirname, '../../')],
  //           },
  //         ),
  //         '^react-native-gesture-handler': require.resolve(
  //           'react-native-gesture-handler',
  //           {
  //             paths: [path.join(__dirname, '../../')],
  //           },
  //         ),
  //         '^@react-navigation/(.+)': ([, name]) =>
  //           require.resolve(`@react-navigation/${name}`, {
  //             paths: [path.join(__dirname, '../../')],
  //           }),
  //       },
  //       extensions: [
  //         '.ios.js',
  //         '.ios.ts',
  //         '.ios.tsx',
  //         '.android.js',
  //         '.android.ts',
  //         '.android.tsx',
  //         '.native.js',
  //         '.native.ts',
  //         '.native.tsx',
  //         '.js',
  //         '.ts',
  //         '.tsx',
  //       ],
  //     },
  //   ],
  // ],
};
