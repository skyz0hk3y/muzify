import React, {
  FC,
  JSXElementConstructor,
  ReactElement,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  ListRenderItem,
} from 'react-native';
import {Track} from 'react-native-track-player';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// vscode:fixme supported via tsconfig
import EmptyStateSearch from '../../assets/empty-state-search-2x.png';

import {useTranslation} from '../../i18n';

import {Caption} from '../../core/components/Caption';
import {EmptyState} from '../../core/components/EmptyState';
import {useTheme} from '../../core/hooks/useTheme';

import * as AudioPlayerActions from '../../audio-player/actions';
import {TrackMoreContext} from '../../audio-player/TrackMoreContext';
import {
  getCurrentPlaylistIdSelector,
  getCurrentTrackSelector,
  getIsTrackLoading,
} from '../../audio-player/selectors';

import {
  getArtistsNamesSelector,
  getLocalTracksIdsSelector,
  getPlaylistsSelector,
  getTracksDownloadsSelector,
  getTracksForPlaylistSelector,
  getTracksGroupedByArtistSelector,
} from '../../library/selectors';

import {
  TrackWithMeta,
  selectMostPlayedTracks,
  selectMostRepeatedTracks,
} from '../selectors';

import {TracksListHorizontal} from '../../audio-player/components/TracksListHorizontal';
import {Routes, SCREEN_FLATLIST_MARGIN_BOTTOM} from '../../constants';
import {PlaylistTracksListHorizontal} from '../../library/components/PlaylistTracksListHorizontal';
import {IconButton} from '../../core/components/IconButton';
import {useAudioPlayer} from '../../audio-player/hooks/useAudioPlayer';

export type ScreenSection = {
  id?: string;
  name: string;
  label: string;
  when: boolean;
  data: string[];
  tracksIds?: string[];
};

export type HomeTabProps = {};

export const HomeTab: FC<HomeTabProps> = () => {
  const flatlistRef = useRef<FlatList<ScreenSection>>(null);
  const dispatch = useDispatch();

  const {playTrack} = useAudioPlayer();
  const {theme} = useTheme();
  const {t} = useTranslation('core');
  const {navigate} = useNavigation();
  const {openTrackMoreForTrack} = useContext(TrackMoreContext);

  const mostPlayedTracks = useSelector(selectMostPlayedTracks);
  const mostRepeatedTracks = useSelector(selectMostRepeatedTracks);
  const isTrackLoading = useSelector(getIsTrackLoading);
  const currentTrack = useSelector(getCurrentTrackSelector);
  const currentPlaylistId = useSelector(getCurrentPlaylistIdSelector);
  const playlists = useSelector(getPlaylistsSelector);
  const artistsNames = useSelector(getArtistsNamesSelector);
  const tracksByArtists = useSelector(getTracksGroupedByArtistSelector);
  const getTracksForPlaylist = useSelector(getTracksForPlaylistSelector);

  const onTrackSelect = useCallback(
    async (trackId: string, playlistId: string, tracks: Track[]) => {
      await dispatch(AudioPlayerActions.setPlaylistId(playlistId));
      await playTrack(trackId, playlistId, tracks);
    },
    [dispatch, playTrack],
  );

  const onTrackSelectPlaylist = useCallback(
    async (trackId: string, playlistId: string) => {
      await dispatch(AudioPlayerActions.setPlaylistId(playlistId));

      const playlistTracks = await getTracksForPlaylist(playlistId);
      await playTrack(trackId, playlistId, playlistTracks);
    },
    [dispatch, getTracksForPlaylist, playTrack],
  );

  const shouldShowEmptyState =
    mostPlayedTracks.length === 0 &&
    mostRepeatedTracks.length === 0 &&
    artistsNames.length === 0;

  // Main flatlist data.
  const screenSections = useMemo(() => {
    return [
      ...playlists.map((playlist) => ({
        id: playlist.id,
        name: 'playlists-tracks',
        label: playlist.name.replace(/^[.*] /, ''),
        when: playlist.tracksIds.length >= 1,
        data: playlist.tracksIds.slice(0, 25),
        tracksIds: playlist.tracksIds.slice(0, 25),
      })),
      ...artistsNames
        .sort(
          (a, b) =>
            (tracksByArtists.get(b)?.length || 0) -
            (tracksByArtists.get(a)?.length || 0),
        )
        .map((artist) => ({
          name: 'artists-tracks',
          label: `This Is ${artist}`,
          when: (tracksByArtists.get(artist)?.length || 0) >= 1,
          data: (tracksByArtists.get(artist) || []).slice(0, 25),
        })),
    ] as ScreenSection[];
  }, [artistsNames, playlists, tracksByArtists]);

  const renderScreenSections: ListRenderItem<ScreenSection> = useMemo(
    () => ({
      item,
    }): ReactElement<any, string | JSXElementConstructor<any>> | null => {
      if (item.when === false) {
        return null;
      }

      if (item.id && item.name === 'playlists-tracks') {
        const onGoToPlaylistClick = () => {
          navigate(Routes.LIBRARY, {
            screen: Routes.LIBRARY_PLAYLIST_DETAILS,
            params: {
              playlistId: item.id,
              playlistTitle: item.label,
            },
          });
        };

        return (
          <View
            key={`${item.name}-${item.label}-${item.id}`}
            style={styles.screenSection}
          >
            <View
              style={[
                styles.section,
                styles.row,
                styles.spaceBetween,
                styles.alignVertCenter,
              ]}
            >
              {/* eslint-disable-next-line react-native/no-inline-styles */}
              <Caption numberOfLines={1} style={{maxWidth: '90%'}}>
                {item.label}
              </Caption>
              <IconButton
                size={'small'}
                backgroundColor="transparent"
                onPress={onGoToPlaylistClick}
              >
                <Icon name="arrow-right" size={20} color={theme.colors.text} />
              </IconButton>
            </View>
            <PlaylistTracksListHorizontal
              id={`${item.name}-${item.label}-${item.id}`}
              playlistId={item.id}
              tracksDisabled={isTrackLoading}
              onTrackMorePress={openTrackMoreForTrack}
              onTrackSelect={onTrackSelectPlaylist}
            />
          </View>
        );
      }

      return (
        <View key={`${item.name}-${item.label}`} style={styles.screenSection}>
          <View style={styles.section}>
            <Caption>{item.label}</Caption>
          </View>
          <TracksListHorizontal
            id={`${item.name}-${item.label}`}
            tracks={(item.data as unknown) as Track[]}
            tracksDisabled={isTrackLoading}
            onTrackMorePress={openTrackMoreForTrack}
            onTrackSelect={(trackId, tracks) =>
              onTrackSelect(trackId, item.id, tracks)
            }
          />
        </View>
      );
    },
    [
      isTrackLoading,
      navigate,
      onTrackSelect,
      onTrackSelectPlaylist,
      openTrackMoreForTrack,
      theme.colors.text,
    ],
  );

  // Code for auto-scrolling (to jump to current track automatically)
  // Disabled atm because we need a way to detect if user is scrolling
  // aka. scrollview is dirty, so we can prevent making the user mad when he tries
  // to search for a track to play next the current one (scrolls) and the scrollview
  // jumps back to where it was previously + 1 track
  //
  // FIXME: Make it possible to check scrollview dirtyness. See above comment ^
  //
  useEffect(() => {
    if (!flatlistRef.current) {
      return;
    }

    if (currentPlaylistId != null && currentTrack != null) {
      const currentSectionIdx = screenSections.findIndex(
        (s) => s.id === currentPlaylistId,
      );

      if (
        currentSectionIdx >= 1 &&
        currentSectionIdx <= screenSections.length - 1
      ) {
        flatlistRef.current.scrollToIndex({
          animated: true,
          index: currentSectionIdx,
          viewPosition: 0.5,
        });
      }
    }
  }, [currentPlaylistId, currentTrack, screenSections]);

  return (
    <SafeAreaView>
      <View
        testID="genius-home"
        style={[
          styles.container,
          {backgroundColor: theme.colors.backgrounds[2]},
        ]}
      >
        <View style={styles.content}>
          {shouldShowEmptyState ? (
            <EmptyState
              testID={'genius-empty-state'}
              source={EmptyStateSearch}
              title={t('search:empty-state.title')}
              subtitle={t('search:empty-state.subtitle')}
            />
          ) : (
            <FlatList
              ref={flatlistRef}
              style={[
                styles.flatlist,
                currentTrack && {
                  marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM,
                },
              ]}
              onScrollToIndexFailed={(_info) => undefined}
              contentContainerStyle={styles.sections}
              contentInsetAdjustmentBehavior={'automatic'}
              nestedScrollEnabled={true}
              maxToRenderPerBatch={1}
              scrollEventThrottle={16}
              keyExtractor={(item, index) => `${item.name}-${index}`}
              data={screenSections}
              renderItem={renderScreenSections}
            />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
  },
  scrollView: {
    // paddingBottom: 72,
    // paddingVertical: 8,
    paddingBottom: 8,
  },
  scrollViewContent: {
    // paddingHorizontal: 16,
    // nBottom: 64,
  },
  horizontalScrollView: {
    paddingTop: 8,
    marginBottom: 8,
  },
  flatlist: {
    paddingHorizontal: 0,
  },
  actionsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  screenSection: {
    marginBottom: 8,
  },
  sections: {
    // paddingVertical: 8,
  },
  section: {
    paddingHorizontal: 16,
  },
  verticalSection: {
    flexDirection: 'row',
    paddingLeft: 8,
    paddingRight: 0,
  },
  row: {
    flexDirection: 'row',
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
  alignVertCenter: {
    alignItems: 'center',
  },
});
