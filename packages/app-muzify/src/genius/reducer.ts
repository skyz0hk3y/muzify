import {
  GeniusActions,
  GeniusActionsType,
  GeniusStateType,
  MODULE_NAME,
} from './types';

const INITIAL_STATE: GeniusStateType = {
  trackPlaysCountById: {},
  trackRepeatsCountById: {},
  totalPlaybackTime: 0,
};

export const getGeniusPersistConfig = (storage: any) => ({
  key: MODULE_NAME,
  storage: storage,
  whitelist: [
    'trackPlaysCountById',
    'trackRepeatsCountById',
    'totalPlaybackTime',
  ],
});

export const geniusReducer = (
  state = INITIAL_STATE,
  action: GeniusActionsType,
): GeniusStateType => {
  switch (action.type) {
    case GeniusActions.RESET_GENIUS_STATS: {
      return {
        ...INITIAL_STATE,
      };
    }
    case GeniusActions.SET_GENIUS_INITIAL_STATE: {
      return {
        ...state,
        ...action.payload.state,
      };
    }
    case GeniusActions.INCREMENT_TRACK_PLAYS_COUNT: {
      const {trackPlaysCountById} = state;
      const {trackId} = action.payload;

      const trackPlaysCount = trackPlaysCountById[trackId];

      return {
        ...state,
        trackPlaysCountById: {
          ...trackPlaysCountById,
          [trackId]: 1 + (trackPlaysCount || 0),
        },
      };
    }
    case GeniusActions.INCREMENT_TRACK_REPEATS_COUNT: {
      const {trackRepeatsCountById} = state;
      const {trackId} = action.payload;

      const trackRepeatsCount = trackRepeatsCountById[trackId];

      return {
        ...state,
        trackRepeatsCountById: {
          ...trackRepeatsCountById,
          [trackId]: 1 + (trackRepeatsCount || 0),
        },
      };
    }
    case GeniusActions.ADD_PLAYBACK_TIME: {
      const {totalPlaybackTime} = state;
      const {durationSeconds} = action.payload;

      return {
        ...state,
        totalPlaybackTime: totalPlaybackTime + (durationSeconds || 0),
      };
    }
    default: {
      return state;
    }
  }
};
