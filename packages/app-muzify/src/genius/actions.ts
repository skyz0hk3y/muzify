import {Dispatch} from 'redux';
import {GeneratedPlaylistId} from '../constants';
import {addTrackToPlaylist} from '../library/actions';
import {RootState} from '../store';

import {
  AddPlaybackTimeActionType,
  GeniusActions,
  GeniusStateType,
  IncrementTrackPlaysCountActionType,
  IncrementTrackRepeatsCountActionType,
  ResetGeniusStatsActionType,
  SetGeniusInitialStateActionType,
} from './types';

/* Actions */

export const resetGeniusStatsSync = (): ResetGeniusStatsActionType => ({
  type: GeniusActions.RESET_GENIUS_STATS,
  payload: undefined,
});

export const setGeniusInitialState = (
  state: GeniusStateType,
): SetGeniusInitialStateActionType => ({
  type: GeniusActions.SET_GENIUS_INITIAL_STATE,
  payload: {
    state,
  },
});

export const incrementTrackPlaysCountSync = (
  trackId: string,
): IncrementTrackPlaysCountActionType => ({
  type: GeniusActions.INCREMENT_TRACK_PLAYS_COUNT,
  payload: {
    trackId,
  },
});

export const incrementTrackRepeatsCountSync = (
  trackId: string,
): IncrementTrackRepeatsCountActionType => ({
  type: GeniusActions.INCREMENT_TRACK_REPEATS_COUNT,
  payload: {
    trackId,
  },
});

export const addPlaybackTimeSync = (
  trackId: string,
  durationSeconds: number,
): AddPlaybackTimeActionType => ({
  type: GeniusActions.ADD_PLAYBACK_TIME,
  payload: {
    trackId,
    durationSeconds,
  },
});

/* Actions creators */

export const resetGeniusStats = () => {
  return async (dispatch: Dispatch<ResetGeniusStatsActionType>) => {
    dispatch(resetGeniusStatsSync());
  };
};

export const incrementTrackPlaysCount = (trackId: string) => {
  return async (dispatch: Dispatch<IncrementTrackPlaysCountActionType>) => {
    dispatch(incrementTrackPlaysCountSync(trackId));
    dispatch<any>(
      addTrackToPlaylist(GeneratedPlaylistId.MOST_PLAYED_TRACKS, trackId),
    );
  };
};

export const incrementTrackRepeatsCount = (trackId: string) => {
  return async (dispatch: Dispatch<IncrementTrackRepeatsCountActionType>) => {
    dispatch(incrementTrackRepeatsCountSync(trackId));
    dispatch<any>(
      addTrackToPlaylist(GeneratedPlaylistId.MOST_REPEATED_TRACKS, trackId),
    );
  };
};
