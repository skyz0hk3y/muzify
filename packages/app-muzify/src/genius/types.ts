export const MODULE_NAME = 'genius';

/* State type */

export type GeniusStateType = {
  trackPlaysCountById: Record<string, number>;
  trackRepeatsCountById: Record<string, number>;
  totalPlaybackTime: number;
};

/* Actions keys */

export enum GeniusActions {
  // ADD_RECENT_TRACK = 'genius/addRecentTrack',
  RESET_GENIUS_STATS = 'genius/reset',
  SET_GENIUS_INITIAL_STATE = 'genius/setInitialState',
  INCREMENT_TRACK_PLAYS_COUNT = 'genius/incrementTrackPlaysCount',
  INCREMENT_TRACK_REPEATS_COUNT = 'genius/incrementTrackRepeatsCount',
  ADD_PLAYBACK_TIME = 'genius/addPlaybackTime',
}

/* Actions Types */

export type ResetGeniusStatsActionType = {
  type: GeniusActions.RESET_GENIUS_STATS;
  payload: undefined;
};

export type SetGeniusInitialStateActionType = {
  type: GeniusActions.SET_GENIUS_INITIAL_STATE;
  payload: {
    state: GeniusStateType;
  };
};

export type IncrementTrackPlaysCountActionType = {
  type: GeniusActions.INCREMENT_TRACK_PLAYS_COUNT;
  payload: {
    trackId: string;
  };
};

export type IncrementTrackRepeatsCountActionType = {
  type: GeniusActions.INCREMENT_TRACK_REPEATS_COUNT;
  payload: {
    trackId: string;
  };
};

export type AddPlaybackTimeActionType = {
  type: GeniusActions.ADD_PLAYBACK_TIME;
  payload: {
    trackId: string;
    durationSeconds: number;
  };
};

/* Reducer actions type */

export type GeniusActionsType =
  | ResetGeniusStatsActionType
  | SetGeniusInitialStateActionType
  | IncrementTrackPlaysCountActionType
  | IncrementTrackRepeatsCountActionType
  | AddPlaybackTimeActionType;
