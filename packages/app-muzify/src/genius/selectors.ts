import {Track} from 'react-native-track-player';
import {sanitizeTrack} from '../core/helpers/sanitizeTrack';

import {RootState} from '../store';

export interface TrackWithMeta extends Track {
  _trackPlaysCount?: number;
  _trackRepeatsCount?: number;
  _mostPlayedTracksIndex?: number;
  _mostRepeatedTracksIndex?: number;
}

const HORIZONTAL_TRACKS_COUNT = 11;

export const selectMostPlayedTracks = (state: RootState): TrackWithMeta[] => {
  const {library, genius} = state;

  const keys = Object.keys(genius.trackPlaysCountById).slice(
    0,
    HORIZONTAL_TRACKS_COUNT,
  );
  const tracksPlaysCounts: TrackWithMeta[] = keys.reduce(
    (
      acc: TrackWithMeta[],
      trackId: string,
      currentIndex: number,
    ): TrackWithMeta[] => {
      // Do not include tracks already the most repeated
      // const isInMostRepeatedTracks = Object.keys(
      //   genius.trackRepeatsCountById,
      // ).includes(trackId);
      // if (isInMostRepeatedTracks) {
      //   return acc;
      // }

      const count = genius.trackPlaysCountById[trackId];
      if (!count) {
        return acc;
      }

      const track = library.tracks.find((t: Track) => t.id === trackId);
      if (!track) {
        return acc;
      }

      const sanitizedTrack = sanitizeTrack(track);

      acc[currentIndex] = {
        _trackPlaysCount: count,
        _mostPlayedTracksIndex: currentIndex,
        ...sanitizedTrack,
      };

      return acc;
    },
    [] as TrackWithMeta[],
  );

  return tracksPlaysCounts.sort(
    (a, b) => (b._trackPlaysCount as number) - (a._trackPlaysCount as number),
  );
};

export const selectMostRepeatedTracks = (state: RootState): TrackWithMeta[] => {
  const {library, genius} = state;

  const keys = Object.keys(genius.trackRepeatsCountById).slice(
    0,
    HORIZONTAL_TRACKS_COUNT,
  );
  const tracksRepeatsCounts: TrackWithMeta[] = keys.reduce(
    (
      acc: TrackWithMeta[],
      trackId: string,
      currentIndex: number,
    ): TrackWithMeta[] => {
      const _trackRepeatsCount = genius.trackRepeatsCountById[trackId];
      if (!_trackRepeatsCount) {
        return acc;
      }

      const track = library.tracks.find((t: Track) => t.id === trackId);
      if (!track) {
        return acc;
      }

      const sanitizedTrack = sanitizeTrack(track);

      acc[currentIndex] = {
        _tracksRepeatsCounts: _trackRepeatsCount,
        _mostRepeatedTracksIndex: currentIndex,
        ...sanitizedTrack,
      };

      return acc;
    },
    [] as TrackWithMeta[],
  );

  return tracksRepeatsCounts.sort(
    (a, b) =>
      (b._tracksRepeatsCounts as number) - (a._tracksRepeatsCounts as number),
  );
};
