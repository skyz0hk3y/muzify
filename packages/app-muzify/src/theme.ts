import {DarkTheme} from '@react-navigation/native';

export const MuzifyTheme = {
  ...DarkTheme,
  dark: true,
  colors: {
    ...DarkTheme.colors,
    primary: '00adcc',
  },
};
