declare module 'react-native-uuid-generator' {
  export default {
    getRandomUUID(callback?: (uuid: string) => void): void | Promise<string>;,
  };
}
