import {Track} from 'react-native-track-player';

export const MODULE_NAME = 'library';

export type LibraryPlaylistOrigin =
  | 'user'
  | 'generated'
  | 'youtube'
  | 'spotify';

export enum LibraryActions {
  SET_LOADING = 'library/setLoading',
  SET_ERROR = 'library/setError',
  SET_LIKED_TRACKS = 'library/setLikedTracks',
  SET_LIKED_TRACK_IDS = 'library/setLikedTrackIds',
  SET_LOCAL_TRACKS_IDS = 'library/setLocalTracksIds',
  ADD_LOCAL_TRACK_ID = 'library/addLocalTrackId',
  ADD_TRACKS = 'library/addTracks',
  ADD_LIKED_TRACK = 'library/addLikedTrack',
  REMOVE_LIKED_TRACK = 'library/removeLikedTrack',
  CLEAR_TRACK_DOWNLOADS = 'library/clearTrackDownloads',
  TRACK_DOWNLOAD_PENDING = 'library/trackDownloadPending',
  TRACK_DOWNLOAD_START = 'library/trackDownloadStart',
  TRACK_DOWNLOAD_SUCCESS = 'library/trackDownloadSuccess',
  TRACK_DOWNLOAD_PROGRESS = 'library/trackDownloadProgress',
  TRACK_DOWNLOAD_FAILURE = 'library/trackDownloadFailure',
  TRACK_DOWNLOAD_CANCEL = 'library/trackDownloadCancel',
  SET_INITIAL_PLAYLISTS_STATE = 'library/setInitialPlaylistsState',
  CREATE_PLAYLIST = 'library/createPlaylist',
  UPDATE_PLAYLIST_NAME = 'library/updatePlaylistName',
  DELETE_PLAYLIST = 'library/deletePlaylist',
  ADD_TRACK_TO_PLAYLIST = 'library/addTrackToPlaylist',
  REMOVE_TRACK_FROM_PLAYLIST = 'library/removeTrackFromPlaylist',
  ADD_TRACKS_TO_LOCAL_CACHE = 'library/addTracksToLocalCache',
}

export type SetLoadingActionType = {
  type: LibraryActions.SET_LOADING;
  payload: boolean;
};

export type SetErrorActionType = {
  type: LibraryActions.SET_ERROR;
  payload: null | {
    code: string;
    message: string;
  };
};

export type AddTracksActionType = {
  type: LibraryActions.ADD_TRACKS;
  payload: {
    tracks: Track[];
  };
};

export type SetLikedTracksActionType = {
  type: LibraryActions.SET_LIKED_TRACKS;
  payload: {
    tracks: Track[];
  };
};

export type SetLikedTrackIdsActionType = {
  type: LibraryActions.SET_LIKED_TRACK_IDS;
  payload: {
    likedTracksIds: string[];
  };
};

export type AddLocalTrackIdActionType = {
  type: LibraryActions.ADD_LOCAL_TRACK_ID;
  payload: string;
};

export type SetLocalTracksIdsActionType = {
  type: LibraryActions.SET_LOCAL_TRACKS_IDS;
  payload: string[];
};

export type AddLikedTrackActionType = {
  type: LibraryActions.ADD_LIKED_TRACK;
  payload: {
    trackId: string;
  };
};

export type RemoveLikedTrackActionType = {
  type: LibraryActions.REMOVE_LIKED_TRACK;
  payload: {
    trackId: string;
  };
};

export type ClearTrackDownloadsActionType = {
  type: LibraryActions.CLEAR_TRACK_DOWNLOADS;
  payload: undefined;
};

export type TrackDownloadPending = {
  type: LibraryActions.TRACK_DOWNLOAD_PENDING;
  payload: {
    trackId: string;
  };
};

export type TrackDownloadStart = {
  type: LibraryActions.TRACK_DOWNLOAD_START;
  payload: {
    trackId: string;
    expectedBytes: number;
  };
};

export type TrackDownloadProgress = {
  type: LibraryActions.TRACK_DOWNLOAD_PROGRESS;
  payload: {
    trackId: string;
    progress: number;
    writtenBytes: number;
    totalBytes: number;
  };
};

export type TrackDownloadSuccess = {
  type: LibraryActions.TRACK_DOWNLOAD_SUCCESS;
  payload: {
    trackId: string;
  };
};

export type TrackDownloadFailure = {
  type: LibraryActions.TRACK_DOWNLOAD_FAILURE;
  payload: {
    trackId: string;
    error: {
      code: string;
      message: string;
    };
  };
};

export type TrackDownloadCancel = {
  type: LibraryActions.TRACK_DOWNLOAD_CANCEL;
  payload: {
    trackId: string;
  };
};

export type SetInitialPlaylistsStateActionType = {
  type: LibraryActions.SET_INITIAL_PLAYLISTS_STATE;
  payload: {
    playlists: Record<string, LibraryPlaylist>;
  };
};

export type CreatePlaylistActionType = {
  type: LibraryActions.CREATE_PLAYLIST;
  payload: {
    playlistId: string;
    originalId?: string;
    playlist: Partial<LibraryPlaylist>;
  };
};

export type UpdatePlaylistNameActionType = {
  type: LibraryActions.UPDATE_PLAYLIST_NAME;
  payload: {
    playlistId: string;
    newName: string;
  };
};

export type DeletePlaylistActionType = {
  type: LibraryActions.DELETE_PLAYLIST;
  payload: {
    playlistId: string;
  };
};

export type AddTrackToPlaylistActionType = {
  type: LibraryActions.ADD_TRACK_TO_PLAYLIST;
  payload: {
    playlistId: string;
    trackId: string;
  };
};

export type RemoveTrackFromPlaylistActionType = {
  type: LibraryActions.REMOVE_TRACK_FROM_PLAYLIST;
  payload: {
    playlistId: string;
    trackId: string;
  };
};

export type AddTracksToLocalCacheActionType = {
  type: LibraryActions.ADD_TRACKS_TO_LOCAL_CACHE;
  payload: Track[];
};

export type LibraryActionsType =
  | SetLoadingActionType
  | SetErrorActionType
  | SetLikedTracksActionType
  | SetLikedTrackIdsActionType
  | AddLocalTrackIdActionType
  | SetLocalTracksIdsActionType
  | AddTracksActionType
  | AddLikedTrackActionType
  | RemoveLikedTrackActionType
  | ClearTrackDownloadsActionType
  | TrackDownloadPending
  | TrackDownloadStart
  | TrackDownloadProgress
  | TrackDownloadSuccess
  | TrackDownloadFailure
  | TrackDownloadCancel
  | SetInitialPlaylistsStateActionType
  | CreatePlaylistActionType
  | UpdatePlaylistNameActionType
  | DeletePlaylistActionType
  | AddTrackToPlaylistActionType
  | RemoveTrackFromPlaylistActionType
  | AddTracksToLocalCacheActionType;

export type TrackDownload = {
  trackId: string;
  status: 'pending' | 'started' | 'in-progress' | 'success' | 'failure';
  progress: number;
  expectedBytes: number;
  writtenBytes: number;
  totalBytes: number;
  startedAtUnix?: number;
  endedAtUnix?: number;
  error?: {
    code: string;
    message: string;
  };
};

export type LibraryPlaylist = {
  id: string;
  originalId?: string;
  name: string;
  tracksIds: string[];
  artwork?: string;
  origin: LibraryPlaylistOrigin;
  createdAtUnix: number;
  lastTrackAddedAtUnix?: number;
};

export type LibraryStateType = {
  loading: boolean;
  error: null | {
    code: string;
    message: string;
  };

  // Tracks
  tracks: Track[]; // This is actually the likedTracks...
  likedTracksIds: string[];
  localTracksIds: string[];

  // Cache
  cachedTracks: Track[]; // This is what `tracks` should have been for.

  // Downloads manager
  // TODO: Move to own reducer
  tracksDownloads: Record<string, TrackDownload>;

  // Playlists
  playlists: Record<string, LibraryPlaylist>;
};
