import {fetch, Headers} from 'cross-fetch';
import {Dispatch} from 'react';
import {ToastAndroid, Platform} from 'react-native';
import {default as TrackPlayer, Track} from 'react-native-track-player';
import {default as BackgroundDownloader} from 'react-native-background-downloader';
import {default as RNFS} from 'react-native-fs';
import {default as Permissions} from 'react-native-permissions';

import {API_BASE_URL, GeneratedPlaylistId} from '../constants';

import {TrackMeta} from '../audio-player/types';
import {
  AddLikedTrackActionType,
  AddLocalTrackIdActionType,
  AddTracksActionType,
  AddTracksToLocalCacheActionType,
  AddTrackToPlaylistActionType,
  CreatePlaylistActionType,
  DeletePlaylistActionType,
  LibraryActions,
  LibraryPlaylist,
  RemoveLikedTrackActionType,
  RemoveTrackFromPlaylistActionType,
  SetErrorActionType,
  SetInitialPlaylistsStateActionType,
  SetLikedTrackIdsActionType,
  SetLikedTracksActionType,
  SetLoadingActionType,
  SetLocalTracksIdsActionType,
  ClearTrackDownloadsActionType,
  TrackDownloadCancel,
  TrackDownloadFailure,
  TrackDownloadPending,
  TrackDownloadProgress,
  TrackDownloadStart,
  TrackDownloadSuccess,
  UpdatePlaylistNameActionType,
} from './types';

import {getLocalPathForTrack} from '../core/helpers/getLocalPathForTrack';
import {i18n} from '../i18n';
import {RootState} from '../store';
import {sanitizeTrack} from '../core/helpers/sanitizeTrack';
import {getStreamingUrlForTrackId} from '../core/helpers/getStreamingUrlForTrackId';
import {PlaylistByIdResponseDTO} from '../core/types';
import {getTrack} from '../audio-player/helpers/getTrack';

/* Actions */

export const setLoading = (isLoading: boolean): SetLoadingActionType => ({
  type: LibraryActions.SET_LOADING,
  payload: isLoading,
});

export const setError = (
  error: null | {code: string; message: string},
): SetErrorActionType => ({
  type: LibraryActions.SET_ERROR,
  payload: error,
});

export const addTracksToLibrary = (tracks: Track[]): AddTracksActionType => ({
  type: LibraryActions.ADD_TRACKS,
  payload: {
    tracks,
  },
});

export const setLikedTracks = (tracks: Track[]): SetLikedTracksActionType => ({
  type: LibraryActions.SET_LIKED_TRACKS,
  payload: {
    tracks,
  },
});

export const setLikedTracksIds = (
  likedTracksIds: string[],
): SetLikedTrackIdsActionType => ({
  type: LibraryActions.SET_LIKED_TRACK_IDS,
  payload: {
    likedTracksIds: [...new Set(likedTracksIds)],
  },
});

export const addLocalTrackId = (
  localTrackId: string,
): AddLocalTrackIdActionType => ({
  type: LibraryActions.ADD_LOCAL_TRACK_ID,
  payload: localTrackId,
});

export const setLocalTracksIds = (
  localTracksIds: string[],
): SetLocalTracksIdsActionType => ({
  type: LibraryActions.SET_LOCAL_TRACKS_IDS,
  payload: [...new Set(localTracksIds)],
});

export const clearTrackDownloads = (): ClearTrackDownloadsActionType => ({
  type: LibraryActions.CLEAR_TRACK_DOWNLOADS,
  payload: undefined,
});

export const trackDownloadPending = (
  trackId: string,
): TrackDownloadPending => ({
  type: LibraryActions.TRACK_DOWNLOAD_PENDING,
  payload: {
    trackId,
  },
});

export const trackDownloadStart = (
  trackId: string,
  expectedBytes: number,
): TrackDownloadStart => ({
  type: LibraryActions.TRACK_DOWNLOAD_START,
  payload: {
    trackId,
    expectedBytes,
  },
});

export const trackDownloadProgress = (
  trackId: string,
  progress: number,
  writtenBytes: number,
  totalBytes: number,
): TrackDownloadProgress => ({
  type: LibraryActions.TRACK_DOWNLOAD_PROGRESS,
  payload: {
    trackId,
    progress,
    writtenBytes,
    totalBytes,
  },
});

export const trackDownloadSuccess = (
  trackId: string,
): TrackDownloadSuccess => ({
  type: LibraryActions.TRACK_DOWNLOAD_SUCCESS,
  payload: {
    trackId,
  },
});

export const trackDownloadFailure = (
  trackId: string,
  error: {code: string; message: string},
): TrackDownloadFailure => ({
  type: LibraryActions.TRACK_DOWNLOAD_FAILURE,
  payload: {
    trackId,
    error,
  },
});

export const trackDownloadCancel = (trackId: string): TrackDownloadCancel => ({
  type: LibraryActions.TRACK_DOWNLOAD_CANCEL,
  payload: {
    trackId,
  },
});

export const setInitialPlaylistsState = (
  playlists: Record<string, LibraryPlaylist>,
): SetInitialPlaylistsStateActionType => ({
  type: LibraryActions.SET_INITIAL_PLAYLISTS_STATE,
  payload: {
    playlists,
  },
});

export const createPlaylist = (
  playlistId: string,
  playlist: Partial<LibraryPlaylist>,
): CreatePlaylistActionType => ({
  type: LibraryActions.CREATE_PLAYLIST,
  payload: {
    playlistId,
    originalId: playlist.originalId,
    playlist,
  },
});

export const updatePlaylistName = (
  playlistId: string,
  newName: string,
): UpdatePlaylistNameActionType => ({
  type: LibraryActions.UPDATE_PLAYLIST_NAME,
  payload: {
    playlistId,
    newName,
  },
});

export const deletePlaylist = (
  playlistId: string,
): DeletePlaylistActionType => ({
  type: LibraryActions.DELETE_PLAYLIST,
  payload: {
    playlistId,
  },
});

export const addTrackToPlaylistSync = (
  playlistId: string,
  trackId: string,
): AddTrackToPlaylistActionType => ({
  type: LibraryActions.ADD_TRACK_TO_PLAYLIST,
  payload: {
    playlistId,
    trackId,
  },
});

export const removeTrackFromPlaylist = (
  playlistId: string,
  trackId: string,
): RemoveTrackFromPlaylistActionType => ({
  type: LibraryActions.REMOVE_TRACK_FROM_PLAYLIST,
  payload: {
    playlistId,
    trackId,
  },
});

export const addTracksToLocalCache = (
  tracks: Track[],
): AddTracksToLocalCacheActionType => ({
  type: LibraryActions.ADD_TRACKS_TO_LOCAL_CACHE,
  payload: tracks,
});

/* Actions creators */

export const addTrackToLocalTracks = (track: Track) => {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    const {
      library: {tracks},
    } = getState();

    if (tracks.find((t) => t.id === track.id)) {
      return;
    }

    dispatch(addLocalTrackId(track.id));
    dispatch(addTracksToLocalCache(tracks));
    // dispatch(setLikedTracks([...tracks, track]));
  };
};

export const importRemotePlaylist = (playlistId: string) => {
  return async (
    dispatch: Dispatch<
      CreatePlaylistActionType | AddTrackToPlaylistAction | SyncTracksActionType
    >,
    // getState: () => RootState,
  ) => {
    try {
      const playlistRes = await fetch(
        `${API_BASE_URL}/playlist/${playlistId}`,
        {
          method: 'GET',
          mode: 'no-cors',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
        },
      );

      const json: PlaylistByIdResponseDTO = await playlistRes.json();
      if (json.error) {
        dispatch(setError(json.error));
        return;
      }

      if (!json.data) {
        return;
      }

      const {results} = json.data;

      const tracksIds: string[] = json.data.results.items.map(
        (item) => item.id,
      );

      dispatch(
        createPlaylist(results.id, {
          id: results.id,
          originalId: results.id,
          name: results.title,
          artwork: results.bestThumbnail.url,
          createdAtUnix: Date.now(),
          lastTrackAddedAtUnix: Date.now(),
          origin: 'youtube',
          tracksIds,
        }),
      );

      dispatch(syncPlaylistTracks(results.id));
    } catch (err) {}
  };
};

export type AddTrackToPlaylistAction =
  | AddTrackToPlaylistActionType
  | AddTracksActionType
  | SetErrorActionType;

export const addTrackToPlaylist = (playlistId: string, trackId: string) => {
  return async (
    dispatch: Dispatch<AddTrackToPlaylistAction>,
    getState: () => RootState,
  ) => {
    // TODO: Check track is in localTracksIds
    // TODO: If not a local track, fetch data from preferred provider

    const state = getState();
    const {
      library: {tracks, localTracksIds, playlists},
    } = state;

    if (
      playlists[playlistId] != null &&
      playlists[playlistId].tracksIds.includes(trackId)
    ) {
      return;
    }

    if (localTracksIds.includes(trackId)) {
      dispatch(addTrackToPlaylistSync(playlistId, trackId));
      return;
    }

    if (tracks.find((t) => t.id === trackId) != null) {
      return;
    }

    const res = await fetch(`${API_BASE_URL}/stream/${trackId}`, {
      method: 'GET',
      mode: 'no-cors',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
    });

    const json = await res.json();
    if (json.error) {
      dispatch(setError(json.error));
      return;
    }

    const trackMeta = json.data;
    const track: Track = {
      id: trackMeta.videoId,
      title: trackMeta.title,
      artist: trackMeta.author.name,
      artwork: trackMeta.cover.url,
      url: getStreamingUrlForTrackId(trackId),
      pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_MUSIC,
    };

    dispatch(addTracksToLibrary([track]));
    dispatch(addTrackToPlaylistSync(playlistId, trackId));
  };
};

type SyncTracksActionType = (
  dispatch: Dispatch<SyncTracksActionTypeDispatch>,
  getState: () => RootState,
) => Promise<void>;

type SyncTracksActionTypeDispatch =
  | SetLoadingActionType
  | SetErrorActionType
  | any;

export const syncPlaylistTracks = (playlistId: string) => {
  return async (
    dispatch: Dispatch<SyncTracksActionTypeDispatch>,
    getState: () => RootState,
  ) => {
    try {
      dispatch(setLoading(true));
      dispatch(setError(null));

      const state = getState();
      const {library} = state;
      const {playlists} = library;

      if (!Object.keys(playlists).includes(playlistId)) {
        // In case this playlistId isn't in the playlist.
        // TODO: Ask the user if we should create/sync this playlist.
        return;
      }

      const playlist = playlists[playlistId];
      if (!playlist) {
        throw new Error('Cannot sync non-existing playlist in library.');
      }

      console.log(playlist);

      const res = await fetch(`${API_BASE_URL}/stream/get-infos-batch`, {
        method: 'POST',
        mode: 'no-cors',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
          trackIds: playlist.tracksIds,
        }),
      });

      const json = await res.json();
      if (json.error) {
        dispatch(setError(json.error));
        return;
      }

      const tracksMetas: TrackMeta[] = json.data?.results || [];

      const fetchedTracks: Track[] = tracksMetas
        // .filter((trackRes) => trackRes.videoId != null)
        // .filter(
        //   (trackRes) => !library.localTracksIds.includes(trackRes.videoId),
        // )
        .map((meta) => ({
          id: meta.videoId,
          url: getStreamingUrlForTrackId(meta.videoId),
          title: meta.title,
          artist: meta.author.name,
          artwork: meta.cover.url,
          rating: false,
          pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_MUSIC,
        }));

      dispatch(addTracksToLocalCache(fetchedTracks));
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: 'rate-limited',
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setLoading(false));
    }
  };
};

type refreshLikedTracksActionDispatchType =
  | SetLoadingActionType
  | SetErrorActionType
  | SetLikedTracksActionType
  | AddTracksToLocalCacheActionType;

export const refreshLikedTracks = () => {
  return async (
    dispatch: Dispatch<refreshLikedTracksActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      dispatch(setLoading(true));
      dispatch(setError(null));

      const state = getState();
      const {library} = state;
      const {tracks} = library;

      const res = await fetch(`${API_BASE_URL}/stream/get-infos-batch`, {
        method: 'POST',
        mode: 'no-cors',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
          trackIds: tracks.map((t) => t.id),
        }),
      });

      const json = await res.json();
      if (json.error) {
        dispatch(setError(json.error));
        return;
      }

      const tracksMetas: TrackMeta[] = json.data?.results || [];

      const fetchedTracks: Track[] = tracksMetas.map((meta) => ({
        id: meta.videoId,
        url: getStreamingUrlForTrackId(meta.videoId),
        title: meta.title,
        artist: meta.author.name,
        artwork: meta.cover.url,
        rating: false,
        pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_MUSIC,
      }));

      dispatch(setLikedTracks([...tracks, ...fetchedTracks]));
      dispatch(addTracksToLocalCache(fetchedTracks));
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: 'rate-limited',
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setLoading(false));
    }
  };
};

export const reattachDownloads = () => {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    console.log('[BackgroundDownloader] Reattaching to lost downloads...');
    let lostTasks = await BackgroundDownloader.checkForExistingDownloads();

    console.log('lost tasks:', lostTasks);

    for (let task of lostTasks) {
      dispatch(trackDownloadStart(task.id, task.totalBytes));
      task
        .progress((percent) => {
          dispatch(
            trackDownloadProgress(
              task.id,
              percent,
              task.bytesWritten,
              task.totalBytes,
            ),
          );
        })
        .done(() => {
          // console.log(`${track.title} has been downloaded in ${destination}`);
          dispatch(trackDownloadSuccess(task.id));
          dispatch(addLocalTrackId(task.id));
          dispatch(
            addTrackToPlaylist(GeneratedPlaylistId.LIKED_TRACKS, task.id),
          );
        })
        .error((error: string, errorCode: number) => {
          dispatch(
            trackDownloadFailure(task.id, {
              code: `${errorCode}`,
              message: error,
            }),
          );
          dispatch(removeLikedTrack(task.id));
          dispatch(deleteLocalTrack(task.id));
          dispatch(
            removeTrackFromPlaylist(GeneratedPlaylistId.LIKED_TRACKS, task.id),
          );
        });
    }
  };
};

export const downloadTrack = (trackId: string) => {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    const state = getState();
    const {cachedTracks, localTracksIds} = state.library;
    const getTrackById = getTrack(cachedTracks, localTracksIds);

    const track = await getTrackById(trackId);
    const downloadUrl = track.url || getStreamingUrlForTrackId(trackId);
    const destination = getLocalPathForTrack(track);

    console.log('[Downloader] Downloading track', {
      track,
      destination,
      downloadUrl,
    });

    return BackgroundDownloader.download({
      id: trackId,
      url: downloadUrl,
      destination,
    })
      .begin((expectedBytes: number) => {
        dispatch(trackDownloadStart(trackId, expectedBytes));
        // console.log(`Will download ${track.title} (${expectedBytes} bytes)`);
      })
      .progress((percent: number, bytesWritten: number, totalBytes: number) => {
        dispatch(
          trackDownloadProgress(trackId, percent, bytesWritten, totalBytes),
        );
        // console.log(
        //   `${track.title} : ${percent}% - ${bytesWritten}/${totalBytes}`,
        // );
      })
      .done(async () => {
        // console.log(`${track.title} has been downloaded in ${destination}`);
        dispatch(trackDownloadSuccess(trackId));
        dispatch(addLocalTrackId(trackId));
        dispatch(
          addTrackToPlaylist(GeneratedPlaylistId.LIKED_TRACKS, track.id),
        );
        dispatch(addTracksToLocalCache([track]));

        if (Platform.OS === 'android') {
          const message = i18n.t('library:download.success', {
            trackTitle: track ? sanitizeTrack(track).title : 'unknown',
            trackArtist: track ? sanitizeTrack(track).artist : 'unknown',
          });
          ToastAndroid.showWithGravityAndOffset(
            message,
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            0,
            200,
          );
        }

        return Promise.resolve(track);
      })
      .error((error: string, errorCode: number) => {
        dispatch(
          trackDownloadFailure(trackId, {
            code: `${errorCode}`,
            message: error,
          }),
        );
        dispatch(removeLikedTrack(trackId));
        dispatch(deleteLocalTrack(trackId));
        dispatch(
          removeTrackFromPlaylist(GeneratedPlaylistId.LIKED_TRACKS, trackId),
        );
        // console.log(
        //   `Error downloading ${track.title}. Err code: ${errorCode}, error: ${error}`,
        // );

        if (Platform.OS === 'android') {
          const message = i18n.t('library:download.error', {
            trackTitle: track ? sanitizeTrack(track).title : 'unknown',
            trackArtist: track ? sanitizeTrack(track).artist : 'unknown',
          });

          ToastAndroid.showWithGravityAndOffset(
            message,
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            0,
            200,
          );
        }

        return Promise.reject(error);
      });
  };
};

export const deleteLocalTrack = (trackId: string) => {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    const state = getState();
    const {tracks, localTracksIds} = state.library;
    const track = tracks.find((t: Track) => t.id === trackId);
    if (!track) {
      return;
    }

    const path = getLocalPathForTrack(track);

    return RNFS.unlink(path)
      .then(async () => {
        dispatch(
          setLocalTracksIds([
            ...localTracksIds.filter((id: string) => id !== trackId),
          ]),
        );

        if (Platform.OS === 'android') {
          const message = i18n.t('library:delete.success', {
            trackTitle: sanitizeTrack(track).title,
            trackArtist: sanitizeTrack(track).artist,
          });

          ToastAndroid.showWithGravityAndOffset(
            message,
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            0,
            200,
          );
        }
      })
      .catch((err) => {
        console.log(err.message);

        dispatch(
          setLocalTracksIds([
            ...localTracksIds.filter((id: string) => id !== trackId),
          ]),
        );

        if (Platform.OS === 'android') {
          const message = i18n.t('library:delete.error', {
            trackTitle: sanitizeTrack(track).title,
            trackArtist: sanitizeTrack(track).artist,
          });

          ToastAndroid.showWithGravityAndOffset(
            message,
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            0,
            200,
          );
        }
      });
  };
};

type AddLikedTrackActionDispatchType = AddLikedTrackActionType | any;

export const addLikedTrack = (trackId: string) => {
  return async (dispatch: Dispatch<AddLikedTrackActionDispatchType>) => {
    dispatch(trackDownloadPending(trackId));
    dispatch(addTrackToPlaylist(GeneratedPlaylistId.LIKED_TRACKS, trackId));
    dispatch({
      type: LibraryActions.ADD_LIKED_TRACK,
      payload: {
        trackId,
      },
    });

    // await dispatch(refreshLikedTracks());

    await Permissions.request(
      Permissions.PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
    );
    await dispatch(downloadTrack(trackId));

    return Promise.resolve(true);
  };
};

type RemoveLikedTrackActionDispatchType = RemoveLikedTrackActionType | any;

export const removeLikedTrack = (trackId: string) => {
  return async (dispatch: Dispatch<RemoveLikedTrackActionDispatchType>) => {
    try {
      await Permissions.request(
        Permissions.PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      );

      dispatch(trackDownloadCancel(trackId));
      dispatch(deleteLocalTrack(trackId));
      dispatch({
        type: LibraryActions.REMOVE_LIKED_TRACK,
        payload: {
          trackId,
        },
      });
      dispatch(
        removeTrackFromPlaylist(GeneratedPlaylistId.LIKED_TRACKS, trackId),
      );
    } catch (err) {
      // TODO(ui): Tell user something went wrong
      console.error('Cannot un-like the track', trackId);
    }
  };
};
