import {ImageURISource} from 'react-native';
import {Track} from 'react-native-track-player';
import {getTrack} from '../audio-player/helpers/getTrack';
import {sanitizeTrack} from '../core/helpers/sanitizeTrack';
import {RootState} from '../store';
import {LibraryPlaylist, TrackDownload} from './types';

export const getArtistsNamesSelector = (state: RootState): string[] => {
  const tracksByArtists = getTracksGroupedByArtistSelector(state);
  return [...tracksByArtists.keys()]
    .sort(
      (a, b) =>
        (tracksByArtists.get(b) as Track[]).length -
        (tracksByArtists.get(a) as Track[]).length,
    )
    .filter((artist) => (tracksByArtists.get(artist) as Track[]).length >= 2);
};

export const getTracksGroupedByArtistSelector = (
  state: RootState,
): Map<string, Track[]> => {
  return state.library.tracks.reduce(
    (acc: Map<string, Track[]>, curr: Track) => {
      const track = sanitizeTrack(curr);
      if (acc.has(track.artist)) {
        const prevTracks = acc.get(track.artist) as Track[];
        acc.set(track.artist, [...prevTracks, track]);
      } else {
        acc.set(track.artist, [track]);
      }

      return acc;
    },
    new Map(),
  );
};

export const getPlaylistByIdSelector = (playlistId: string) => (
  state: RootState,
): LibraryPlaylist => {
  return state.library.playlists[playlistId];
};

export const getPlaylistsSelector = (state: RootState): LibraryPlaylist[] => {
  return Object.values(state.library.playlists);
};

export const getPlaylistCoverSources = (playlistId: string) => (
  state: RootState,
): ImageURISource[] => {
  const {library} = state;
  const playlist = library.playlists[playlistId];

  if (!playlist || !library.tracks) {
    return [];
  }

  const fourFirstTracks = library.tracks
    .filter((track: Track) => playlist.tracksIds.includes(track.id))
    .slice(0, 4);

  return fourFirstTracks.map((track: Track) => ({uri: track.artwork}));
};

export const getTracksForPlaylistByIdSelector = (playlistId: string) => (
  state: RootState,
): Track[] => {
  const playlist = state.library.playlists[playlistId];
  if (!playlist) {
    return [];
  }

  const tracks = state.library.cachedTracks.filter((track) =>
    playlist.tracksIds.includes(track.id),
  );
  if (!tracks) {
    return [];
  }

  return tracks;
};

export const getTracksForPlaylistSelector = (state: RootState) => async (
  playlistId: string,
): Promise<Track[]> => {
  const playlist = state.library.playlists[playlistId];
  if (!playlist) {
    return [];
  }

  const tracks = await Promise.all(
    playlist.tracksIds.map(
      async (trackId) =>
        await getTrack(
          state.library.tracks,
          state.library.localTracksIds,
        )(trackId),
    ),
  );

  if (!tracks) {
    return [];
  }

  return tracks.filter((x) => x != null);
};

export const getIsTrackLocalByIdSelector = (trackId: string) => (
  state: RootState,
): boolean => {
  return state.library.localTracksIds.includes(trackId);
};

export const getTracksDownloadsSelector = (
  state: RootState,
): Record<string, TrackDownload> => {
  return state.library.tracksDownloads;
};

export const hasPendingDownloadsSelector = (state: RootState): boolean => {
  return (
    Object.values(state.library.tracksDownloads).length > 0 &&
    Object.values(state.library.tracksDownloads).some(
      ({status}) => status !== 'failure' && status !== 'success',
    )
  );
};

export const getTrackIdsSelector = (state: RootState) => {
  return state.library.likedTracksIds;
};

export const getTracksSelector = (state: RootState) => {
  return state.library.tracks;
};

export const getLocalTracksIdsSelector = (state: RootState) => {
  return state.library.localTracksIds;
};

export const getLikedTracksIdsSelector = (state: RootState) => {
  return state.library.likedTracksIds;
};

export const getIsTrackDownloadingByIdSelector = (trackId: string) => (
  state: RootState,
): boolean => {
  return (
    Object.keys(state.library.tracksDownloads).includes(trackId) &&
    state.library.tracksDownloads[trackId].status !== 'failure' &&
    state.library.tracksDownloads[trackId].status !== 'success'
  );
};

export const getIsLibraryLoading = (state: RootState): boolean => {
  return state.library.loading;
};

export const getLibraryError = (state: RootState) => {
  return state.library.error;
};

export const getIsLocalTrackSelector = (state: RootState) => (
  trackId: string,
): boolean => {
  return !!state.library.localTracksIds.includes(trackId);
};

export const getIsLikedTrackSelector = (state: RootState) => (
  trackId: string,
): boolean => {
  return !!state.library.likedTracksIds.includes(trackId);
};

export const getIsTrackInCacheSelector = (state: RootState) => (
  trackId: string,
): boolean => {
  return !!state.library.cachedTracks.find((track) => track.id === trackId);
};

export const getCachedTrackSelector = (state: RootState) => (
  trackId: string,
): Track | null => {
  return state.library.cachedTracks.find((track) => track.id === trackId);
};

export const getIsDownloadingTrackSelector = (state: RootState) => (
  trackId: string,
): boolean => {
  const {tracksDownloads} = state.library;
  return (
    Object.keys(tracksDownloads).includes(trackId) &&
    tracksDownloads[trackId].status !== 'failure' &&
    tracksDownloads[trackId].status !== 'success'
  );
};
