import {useNavigation} from '@react-navigation/native';
import React, {useCallback, useMemo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';

import {Routes} from '../../constants';
import {Button} from '../../core/components/Button';
import {ListItem} from '../../core/components/ListItem';
import {TextInput} from '../../core/components/TextInput';
import {useModal} from '../../core/hooks/useModal';
import {useTheme} from '../../core/hooks/useTheme';
import {deletePlaylist, updatePlaylistName} from '../actions';
import {LibraryPlaylist} from '../types';

export type UseLibraryPlaylistAPI = {
  showSharePlaylistBottomSheet: (playlist: LibraryPlaylist) => void;
  showPlaylistMoreModal: (playlist: LibraryPlaylist) => void;
  showPlaylistDeleteModal: (playlist: LibraryPlaylist) => void;
  showPlaylistRenameModal: (playlist: LibraryPlaylist) => void;
};

export const useLibraryPlaylist = (): UseLibraryPlaylistAPI => {
  const {theme} = useTheme();
  const {navigate} = useNavigation();
  const dispatch = useDispatch();

  const {
    showWithContent: showModal,
    hideCurrent: hideCurrentModal,
  } = useModal();

  const onSharePlaylistPress = useCallback((playlist: LibraryPlaylist) => {},
  []);

  const onSpotifyInfosPress = useCallback(
    (playlist: LibraryPlaylist) => {
      if (playlist.origin !== 'spotify') {
        return;
      }

      hideCurrentModal();
      navigate(Routes.IMPORT_SPOTIFY, {
        playlistId: playlist.originalId,
      });
    },
    [hideCurrentModal, navigate],
  );

  const onRenamePlaylistPress = useCallback(
    (playlist: LibraryPlaylist) => {
      showModal({
        key: 'library:playlist-item.rename-action',
        title: `Rename playlist "${playlist.name}"`,
        children: (
          <View>
            <TextInput
              title={'New playlist name:'}
              placeholder={playlist.name}
              value={''}
              onValueChange={() => undefined}
            />
            <View style={styles.buttonsContainer}>
              <Button
                text={'Cancel'}
                backgroundColor={theme.colors.backgrounds[4]}
                color={theme.colors.text}
                fluid
                onPress={hideCurrentModal}
              />
              <Button
                text={'Rename'}
                backgroundColor={theme.colors.backgrounds[4]}
                color={theme.colors.text}
                fluid
                onPress={() => {
                  dispatch(updatePlaylistName(playlist.id, 'text'));
                  hideCurrentModal();
                }}
              />
            </View>
          </View>
        ),
      });
    },
    [
      dispatch,
      hideCurrentModal,
      showModal,
      theme.colors.backgrounds,
      theme.colors.text,
    ],
  );

  const onDeletePlaylistPress = useCallback(
    (playlist: LibraryPlaylist) => {
      showModal({
        key: 'library:playlist-item.delete-action',
        title: `Delete playlist "${playlist.name}" ?`,
        children: (
          <View>
            <Text>
              {`Are you sure you want to delete the playlist named "${playlist.name}", which contains ${playlist.tracksIds.length} tracks?` +
                '\n\nThis is not reversible and will NOT de-synchronize existing local tracks from this playlist.'}
            </Text>
            <View style={styles.buttonsContainer}>
              <Button
                text={'Cancel'}
                backgroundColor={theme.colors.backgrounds[4]}
                color={theme.colors.text}
                fluid
                onPress={hideCurrentModal}
              />
              <Button
                text={'Delete'}
                backgroundColor={theme.colors.backgrounds[4]}
                color={theme.colors.danger}
                fluid
                onPress={() => {
                  dispatch(deletePlaylist(playlist.id));
                  hideCurrentModal();
                }}
              />
            </View>
          </View>
        ),
      });
    },
    [
      dispatch,
      hideCurrentModal,
      showModal,
      theme.colors.backgrounds,
      theme.colors.danger,
      theme.colors.text,
    ],
  );

  const onPlaylistMorePress = useCallback(
    (playlist: LibraryPlaylist) => {
      // const _onSharePlaylistPress = onSharePlaylistPress.bind(null, playlist);
      const _onSpotifyInfosPress = onSpotifyInfosPress.bind(null, playlist);
      const _onRenamePlaylistPress = onRenamePlaylistPress.bind(null, playlist);
      const _onDeletePlaylistPress = onDeletePlaylistPress.bind(null, playlist);

      showModal({
        key: 'library:playlist-item.more-action',
        title: playlist.name,
        children: (
          <View>
            {/* <ListItem
              title={`Share "${playlist.name}"`}
              onItemPress={_onSharePlaylistPress}
              iconName={'share-all-outline'}
            />
            <Divider /> */}
            {playlist.origin === 'spotify' && (
              <ListItem
                title={'Open in Spotify Importer...'}
                onItemPress={_onSpotifyInfosPress}
                iconName={'open-in-app'}
              />
            )}
            <ListItem
              title={'Rename playlist...'}
              onItemPress={_onRenamePlaylistPress}
              iconName={'playlist-edit'}
            />
            <ListItem
              title={'Delete playlist...'}
              onItemPress={_onDeletePlaylistPress}
              iconName={'playlist-minus'}
            />
          </View>
        ),
      });
    },
    [
      onDeletePlaylistPress,
      onRenamePlaylistPress,
      // onSharePlaylistPress,
      onSpotifyInfosPress,
      showModal,
    ],
  );

  return useMemo(
    (): UseLibraryPlaylistAPI =>
      ({
        showSharePlaylistBottomSheet: onSharePlaylistPress,
        showPlaylistMoreModal: onPlaylistMorePress,
        showPlaylistDeleteModal: onDeletePlaylistPress,
        showPlaylistRenameModal: onRenamePlaylistPress,
      } as const),
    [
      onDeletePlaylistPress,
      onPlaylistMorePress,
      onRenamePlaylistPress,
      onSharePlaylistPress,
    ],
  );
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
    marginTop: 16,
  },
});
