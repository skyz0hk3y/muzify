import React, {
  FC,
  useCallback,
  useContext,
  useLayoutEffect,
  useRef,
} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  RefreshControl,
  ListRenderItemInfo,
  FlatList,
} from 'react-native';
import {Track} from 'react-native-track-player';
import {useDispatch, useSelector} from 'react-redux';

import {useTranslation} from '../../i18n';

import * as PlayerActions from '../../audio-player/actions';
import * as LibraryActions from '../actions';

import {TrackMoreContext} from '../../audio-player/TrackMoreContext';
import {TrackItem} from '../../audio-player/components/TrackItem';
import {Button} from '../../core/components/Button';

import {
  getIsDownloadingTrackSelector,
  getIsLibraryLoading,
  getIsLikedTrackSelector,
  getIsLocalTrackSelector,
  getPlaylistByIdSelector,
  getTrackIdsSelector,
  getTracksForPlaylistByIdSelector,
} from '../selectors';
import {
  getCurrentTrackSelector,
  getIsCurrentTrackSelector,
  getIsTrackLoading,
} from '../../audio-player/selectors';
import {ScreenTopAction} from '../../core/components/ScreenTopAction';
import {useTheme} from '../../core/hooks/useTheme';
import {
  GeneratedPlaylistId,
  SCREEN_FLATLIST_MARGIN_BOTTOM,
  TRACK_ITEM_HEIGHT,
} from '../../constants';
import {useAudioPlayer} from '../../audio-player/hooks/useAudioPlayer';

export type LibraryPlaylistDetailsScreenProps = {
  // Navigation
  route: {
    params: {
      playlistId: string;
    };
  };
};

export const LibraryPlaylistDetailsScreen: FC<LibraryPlaylistDetailsScreenProps> = ({
  // Navigation
  route: {
    params: {playlistId},
  },
}) => {
  const {theme} = useTheme();
  const {t} = useTranslation('library');
  const {openTrackMoreForTrack} = useContext(TrackMoreContext);

  const {playTrack} = useAudioPlayer();

  const flatlistRef = useRef<FlatList<Track>>(null);
  const isTrackLoading = useSelector(getIsTrackLoading);
  const currentTrack = useSelector(getCurrentTrackSelector);
  const likedTracksIds = useSelector(getTrackIdsSelector);
  const playlist = useSelector(getPlaylistByIdSelector(playlistId));
  // const libraryError = useSelector(getLibraryError);
  const libraryLoading = useSelector(getIsLibraryLoading);
  const libraryTracks = useSelector(
    getTracksForPlaylistByIdSelector(playlistId),
  );

  const dispatch = useDispatch();
  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isLikedTrack = useSelector(getIsLikedTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);

  const shouldDisablePlayButton =
    !likedTracksIds || playlist.tracksIds.length === 0 || libraryLoading;

  const getIconName = useCallback(
    (trackId: string) => (isLocalTrack(trackId) ? 'heart' : 'heart-outline'),
    [isLocalTrack],
  );

  const getIconColor = useCallback(
    (trackId: string) =>
      isLocalTrack(trackId) ? 'red' : theme.colors.textMuted,
    [isLocalTrack, theme.colors.textMuted],
  );

  const onTrackFavoriteToggle = useCallback(
    (trackId: string) => {
      if (!isLikedTrack(trackId)) {
        dispatch(LibraryActions.addLikedTrack(trackId));
      } else {
        dispatch(LibraryActions.removeLikedTrack(trackId));
      }
    },
    [dispatch, isLikedTrack],
  );

  const onPlayCurrentPlaylistPress = useCallback(async () => {
    await dispatch(PlayerActions.setPlaylistId(playlist.id));
    await playTrack(playlist.tracksIds[0], playlist.id, libraryTracks);
  }, [dispatch, libraryTracks, playTrack, playlist.id, playlist.tracksIds]);

  const onTrackSelect = useCallback(
    async (trackId: string) => {
      await dispatch(PlayerActions.setPlaylistId(playlist.id));
      await playTrack(trackId, playlist.id, libraryTracks);
    },
    [dispatch, libraryTracks, playTrack, playlist.id],
  );

  const onTrackLongPress = useCallback(
    (track: Track) => {
      openTrackMoreForTrack(track);
    },
    [openTrackMoreForTrack],
  );

  const onPlaylistRefresh = useCallback(() => {
    switch (playlistId) {
      case GeneratedPlaylistId.MOST_REPEATED_TRACKS:
      case GeneratedPlaylistId.MOST_PLAYED_TRACKS:
      case GeneratedPlaylistId.LIKED_TRACKS: {
        dispatch(LibraryActions.refreshLikedTracks());
        break;
      }
      default: {
        dispatch(LibraryActions.syncPlaylistTracks(playlist.id));
        break;
      }
    }
  }, [dispatch, playlist.id, playlistId]);

  // const onSyncToggle = (shouldSync: boolean) => {
  //   if (!syncLocalTracks && shouldSync) {
  //     console.log('will dispatch syncPlaylistTracks async action creator');
  //     dispatch(LibraryActions.syncPlaylistTracks(playlist.id));
  //   } else {
  //     // TODO: Do we "un-sync" here?
  //   }

  //   setSyncLocalTracks(shouldSync);
  // };

  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<Track>) => {
      return (
        <TrackItem
          key={item.id}
          disabled={isTrackLoading}
          track={{
            id: item.id,
            videoId: item.id,
            thumbnail: item.artwork,
            title: item.title,
            artist: item.artist,
            url: '',
            author: {
              name: item.artist,
            },
          }}
          isLoading={false}
          isLocal={isLocalTrack(item.id)}
          isCurrentTrack={isCurrentTrack(item.id)}
          isDownloading={isDownloadingTrack(item.id)}
          actionIconName={getIconName(item.id)}
          actionColor={getIconColor(item.id)}
          onItemPress={onTrackSelect.bind(null, item.id)}
          onItemLongPress={onTrackLongPress.bind(null, item)}
          onActionPress={onTrackFavoriteToggle.bind(null, item.id)}
        />
      );
    },
    [
      getIconColor,
      getIconName,
      isCurrentTrack,
      isDownloadingTrack,
      isLocalTrack,
      isTrackLoading,
      onTrackFavoriteToggle,
      onTrackLongPress,
      onTrackSelect,
    ],
  );

  // useEffect(() => {
  //   if (flatlistRef.current && currentTrack && currentTrack?.id && isPlaying) {
  //     const currentTrackIdx = libraryTracks.findIndex(
  //       (tr) => tr.id === currentTrack.id,
  //     );
  //     if (currentTrackIdx >= 1 && currentTrackIdx <= libraryTracks.length - 1) {
  //       flatlistRef.current?.scrollToIndex({
  //         index: currentTrackIdx,
  //         viewPosition: 0,
  //         viewOffset: TRACK_ITEM_HEIGHT - (16 + 4),
  //         animated: true,
  //       });
  //     }
  //   }
  // }, [currentTrack, isPlaying, libraryTracks]);

  useLayoutEffect(() => {
    // Fetch tracks on mount
    onPlaylistRefresh();
  }, [onPlaylistRefresh]);

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <ScreenTopAction>
              <Button
                disabled={shouldDisablePlayButton}
                style={styles.playLibraryButton}
                text={t('library:playLibraryButton', {
                  tracksCount: libraryTracks.length,
                })}
                backgroundColor={theme.colors.primary}
                onPress={onPlayCurrentPlaylistPress}
              />
              {/* <View style={styles.synchronizeContainer}>
                <Text
                  style={[styles.synchronizeText, {color: theme.colors.text}]}>
                  {t('library:sync-playlist.hint')}
                </Text>
                <Switch
                  value={syncLocalTracks}
                  onValueChange={onSyncToggle}
                  thumbColor={theme.colors.primary}
                  trackColor={{
                    false: theme.colors.backgrounds[3],
                    true: '#076575',
                  }}
                />
              </View> */}
            </ScreenTopAction>
            <FlatList
              ref={flatlistRef}
              getItemLayout={(data, index) => ({
                length: TRACK_ITEM_HEIGHT || 0,
                offset: TRACK_ITEM_HEIGHT * index || 0,
                index,
              })}
              refreshing={libraryLoading}
              data={libraryTracks}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              scrollsToTop={false}
              refreshControl={
                <RefreshControl
                  refreshing={libraryLoading}
                  onRefresh={onPlaylistRefresh}
                  progressBackgroundColor={theme.colors.backgrounds[3]}
                  colors={[theme.colors.primary]}
                  title={'Synchronize playlist...'}
                  tintColor={theme.colors.primary}
                />
              }
              contentInsetAdjustmentBehavior="automatic"
              contentContainerStyle={styles.scrollView}
              style={[
                styles.scrollViewContent,
                currentTrack && {
                  marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM,
                },
              ]}
            />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    // paddingTop: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  scrollView: {
    // paddingBottom: 72,
    paddingVertical: 8,
  },
  scrollViewContent: {},
  playLibraryButton: {
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.16,
    shadowRadius: 4,
  },
  textError: {
    color: 'red',
    marginHorizontal: 16,
    textAlign: 'center',
  },
  synchronizeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 8,
    paddingTop: 8,
  },
  synchronizeText: {
    fontWeight: 'bold',
  },
});
