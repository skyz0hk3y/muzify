import {useNavigation} from '@react-navigation/core';
import React, {FC, useCallback} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  ListRenderItemInfo,
  FlatList,
} from 'react-native';
import {useSelector, useStore} from 'react-redux';

import {Button} from '../../core/components/Button';
import {getCurrentPlaylistIdSelector} from '../../audio-player/selectors';
import {
  Routes,
  SCREEN_FLATLIST_MARGIN_BOTTOM,
  SPACING_HALF_UNIT,
  TRACK_ITEM_HEIGHT,
} from '../../constants';
import {ScreenTopAction} from '../../core/components/ScreenTopAction';
import {RootState} from '../../store';
import {useTheme} from '../../core/hooks/useTheme';
import {useTranslation} from '../../i18n';

import {getPlaylistsSelector} from '../selectors';
import {PlaylistItem} from '../components/PlaylistItem';
import {LibraryPlaylist} from '../types';
import {SpotifyImportButton} from '../../importer/components/SpotifyImportButton';
import {useLibraryPlaylist} from '../hooks/usePlaylistMoreModal';
import {MaterialDesignIconNameType} from '../../core/types';

export type LibraryEntryScreenProps = {};

export const LibraryEntryScreen: FC<LibraryEntryScreenProps> = () => {
  const {t} = useTranslation('library');
  const {theme} = useTheme();
  const {navigate} = useNavigation();
  const {showPlaylistMoreModal} = useLibraryPlaylist();

  const {getState} = useStore();
  const state: RootState = getState();
  const {audioPlayer} = state;

  const playlists = useSelector(getPlaylistsSelector);
  const currentPlaylistId = useSelector(getCurrentPlaylistIdSelector);

  const onSpotifyImportButtonPress = () => {
    navigate(Routes.IMPORT_SPOTIFY);
  };

  const onPlaylistPress = useCallback(
    (playlist: LibraryPlaylist) => {
      navigate(Routes.LIBRARY_PLAYLIST_DETAILS, {
        playlistId: playlist.id,
        playlistTitle: playlist.name,
      });
    },
    [navigate],
  );

  const getPlaylistActionIconName = (
    playlist: LibraryPlaylist,
  ): MaterialDesignIconNameType => {
    switch (playlist.origin) {
      case 'generated':
        return undefined;
      default:
        return 'dots-vertical';
    }
  };

  const renderItem = ({item}: ListRenderItemInfo<LibraryPlaylist>) => {
    return (
      <PlaylistItem
        key={item.id}
        playlist={item}
        isCurrentPlaylist={item.id === currentPlaylistId}
        isLoading={false}
        actionIconName={getPlaylistActionIconName(item)}
        onItemPress={onPlaylistPress.bind(null, item)}
        onItemLongPress={showPlaylistMoreModal.bind(null, item)}
        onActionPress={showPlaylistMoreModal.bind(null, item)}
      />
    );
  };

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
          testID={'library-tab'}
        >
          <View style={styles.content}>
            <ScreenTopAction>
              <View style={styles.actionsContainer}>
                <Button
                  fluid
                  style={[styles.actionButton]}
                  text={t('library:createPlaylistButton')}
                  backgroundColor={theme.colors.primary}
                  onPress={() => navigate(Routes.LIBRARY_CREATE_PLAYLIST)}
                />
                <View style={{width: SPACING_HALF_UNIT}} />
                <SpotifyImportButton
                  style={styles.actionButton}
                  text={t('importer:import-button')}
                  onPress={onSpotifyImportButtonPress}
                />
                <View style={{width: SPACING_HALF_UNIT}} />
              </View>
            </ScreenTopAction>
            <View style={styles.section}>
              <FlatList
                data={playlists}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                getItemLayout={(data, index) => ({
                  length: TRACK_ITEM_HEIGHT || 0,
                  offset: TRACK_ITEM_HEIGHT * index || 0,
                  index,
                })}
                scrollsToTop={false}
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollView}
                style={[
                  styles.scrollViewContent,
                  audioPlayer?.currentTrack && {
                    marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM * 2,
                  },
                ]}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
  },
  actionsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: '100%',
  },
  actionButton: {
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.16,
    shadowRadius: 4,
    // padding: 16,
  },
  section: {
    flex: 1,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },

  scrollView: {
    paddingVertical: 8,
  },
  scrollViewContent: {},
});
