import React, {FC, useCallback, useLayoutEffect, useMemo} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  ListRenderItemInfo,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import NoResultsAsset from '../../assets/no-results-2x.png';

import {getCurrentTrackSelector} from '../../audio-player/selectors';
import {
  SCREEN_FLATLIST_MARGIN_BOTTOM,
  TRACK_ITEM_HEIGHT,
} from '../../constants';
import {EmptyState} from '../../core/components/EmptyState';
import {useTheme} from '../../core/hooks/useTheme';
import {MaterialDesignIconNameType} from '../../core/types';
import {reattachDownloads} from '../actions';
import {TrackDownloadItem} from '../components/TrackDownloadItem';
import {getTracksDownloadsSelector, getTracksSelector} from '../selectors';
import {TrackDownload} from '../types';

// import {useTranslation} from '../../i18n';

export type TracksDownloadsScreenProps = {};

export const TracksDownloadsScreen: FC<TracksDownloadsScreenProps> = () => {
  const {theme} = useTheme();
  const dispatch = useDispatch();

  const localTracks = useSelector(getTracksSelector);
  const tracksDownloads = useSelector(getTracksDownloadsSelector);

  const tracksDownloadsKeys = useMemo(
    () => Object.keys(tracksDownloads).reverse(),
    [tracksDownloads],
  );

  const getIconForStatus = useCallback(
    (status: TrackDownload['status']): MaterialDesignIconNameType => {
      switch (status) {
        case 'in-progress':
          return 'progress-download';
        case 'success':
          return 'check-circle-outline';
        case 'failure':
          return 'close-circle-outline';
        case 'started':
        case 'pending':
        default:
          return 'pause-circle-outline';
      }
    },
    [],
  );

  const getIconColorForStatus = useCallback(
    (status: TrackDownload['status']): string => {
      switch (status) {
        case 'in-progress':
          return theme.colors.primary;
        case 'success':
          return theme.colors.success;
        case 'failure':
          return theme.colors.danger;
        case 'pending':
          return theme.colors.textMuted;
        case 'started':
        default:
          return theme.colors.text;
      }
    },
    [
      theme.colors.danger,
      theme.colors.primary,
      theme.colors.success,
      theme.colors.text,
      theme.colors.textMuted,
    ],
  );

  const renderTrackDownloadItem = useMemo(
    () => ({item: downloadId}: ListRenderItemInfo<string>) => {
      const track = localTracks.find((t) => t.id === downloadId);
      if (!track) {
        return null;
      }

      const downloadInfos = tracksDownloads[downloadId];

      return (
        <TrackDownloadItem
          key={`track-download-item-${downloadId}`}
          testID={`track-download-item-${downloadId}`}
          track={track}
          downloadInfos={downloadInfos}
          onItemPress={() => {}}
          actionIconName={getIconForStatus(downloadInfos.status)}
          actionColor={getIconColorForStatus(downloadInfos.status)}
        />
      );
    },
    [getIconColorForStatus, getIconForStatus, localTracks, tracksDownloads],
  );

  useLayoutEffect(() => {
    dispatch<any>(reattachDownloads());
  }, [dispatch]);

  return (
    <SafeAreaView>
      <View
        testID="library-tracks-downloads"
        style={[
          styles.container,
          {backgroundColor: theme.colors.backgrounds[2]},
        ]}
      >
        <View style={styles.content}>
          <View style={styles.section}>
            {tracksDownloadsKeys.length >= 1 ? (
              <FlatList
                data={tracksDownloadsKeys}
                renderItem={renderTrackDownloadItem}
                keyExtractor={(item) => item}
                getItemLayout={(_data, index) => ({
                  length: TRACK_ITEM_HEIGHT || 0,
                  offset: TRACK_ITEM_HEIGHT * index || 0,
                  index,
                })}
                scrollsToTop={true}
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollViewContent}
              />
            ) : (
              <EmptyState
                source={NoResultsAsset}
                title={'Aucun téléchargement en cours'}
                subtitle={
                  'Pressez le bouton "♥" à côté d\'un titre afin de le télécharger et l\'ajouter à vos titres favoris !'
                }
              />
            )}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    paddingVertical: 8,
  },
  scrollViewContent: {
    paddingTop: 8,
  },
  section: {},
});
