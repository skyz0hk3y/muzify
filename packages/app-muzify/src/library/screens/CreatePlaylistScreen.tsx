import {useNavigation} from '@react-navigation/core';
import React, {FC, useCallback, useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  ListRenderItemInfo,
} from 'react-native';
import {Track} from 'react-native-track-player';
import UUIDGenerator from 'react-native-uuid-generator';
import {useDispatch, useSelector} from 'react-redux';
import {TrackItem} from '../../audio-player/components/TrackItem';
import {
  getCurrentTrackSelector,
  getIsCurrentTrackSelector,
  getIsTrackLoading,
} from '../../audio-player/selectors';
import {TrackMoreContext} from '../../audio-player/TrackMoreContext';
import {
  SCREEN_FLATLIST_MARGIN_BOTTOM,
  TRACK_ITEM_HEIGHT,
} from '../../constants';

import {TextInput} from '../../core/components/TextInput';
import {useTheme} from '../../core/hooks/useTheme';
import {getIsOfflineSelector} from '../../core/selector';
import {useTranslation} from '../../i18n';
import {
  getIsDownloadingTrackSelector,
  getIsLibraryLoading,
  getIsLocalTrackSelector,
  getLocalTracksIdsSelector,
  getTracksDownloadsSelector,
} from '../selectors';

export type CreatePlaylistScreenProps = {
  route: {
    params: {
      tracks?: Track[];
    };
  };
};

export const CreatePlaylistScreen: FC<CreatePlaylistScreenProps> = ({
  route,
}) => {
  const {theme} = useTheme();
  const {t} = useTranslation('library');
  const {openTrackMoreForTrack} = useContext(TrackMoreContext);
  const {setParams} = useNavigation();
  const [playlistTitle, setPlaylistTitle] = useState<string>('');

  const isTrackLoading = useSelector(getIsTrackLoading);
  const currentTrack = useSelector(getCurrentTrackSelector);
  const localTracksIds = useSelector(getLocalTracksIdsSelector);
  const tracksDownloads = useSelector(getTracksDownloadsSelector);
  // const libraryError = useSelector(getLibraryError);
  const libraryLoading = useSelector(getIsLibraryLoading);

  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);

  const generatePlaylistId = useCallback(async () => {
    const id = await UUIDGenerator.getRandomUUID();
    setParams({
      playlistId: id,
    });
  }, [setParams]);

  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<Track>) => {
      // const getIconName = (trackId: string) =>
      //   localTracksIds.includes(trackId) ? 'heart' : 'heart-outline';

      // const getIconColor = (trackId: string) =>
      //   localTracksIds.includes(trackId) ? 'red' : 'white';

      const onTrackLongPress = (track: Track) => {
        openTrackMoreForTrack(track);
      };

      return (
        <TrackItem
          key={item.id}
          disabled={isTrackLoading}
          track={{
            id: item.id,
            videoId: item.id,
            thumbnail: item.artwork,
            title: item.title,
            artist: item.artist,
            url: '',
            author: {
              name: item.artist,
            },
          }}
          isLoading={false}
          isLocal={isLocalTrack(item.id)}
          isCurrentTrack={isCurrentTrack(item.id)}
          isDownloading={isDownloadingTrack(item.id)}
          // actionIconName={getIconName(item.id)}
          // actionColor={getIconColor(item.id)}
          // onItemPress={onTrackSelect.bind(null, item.id)}
          onItemLongPress={onTrackLongPress.bind(null, item)}
          // onActionPress={onRemoveFavoriteTrack.bind(null, item.id)}
        />
      );
    },
    [
      isCurrentTrack,
      isDownloadingTrack,
      isLocalTrack,
      isTrackLoading,
      openTrackMoreForTrack,
    ],
  );

  useEffect(() => {
    generatePlaylistId();
  }, [generatePlaylistId]);

  useEffect(() => {
    if (playlistTitle.trim() !== '') {
      setParams({
        playlistTitle: playlistTitle,
      });
    }
  }, [playlistTitle, setParams]);

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <TextInput
              onValueChange={setPlaylistTitle}
              onClearTextPress={() => setPlaylistTitle('')}
              value={playlistTitle}
              placeholder={'i.e. Summers song'}
              title={t('library:create-playlist-form.playlist-name')}
              // hintText={'test'}
              // errorText={'Value should be a valid playlist name'}
            />
            {route.params?.tracks && (
              <FlatList
                getItemLayout={(data, index) => ({
                  length: TRACK_ITEM_HEIGHT,
                  offset: 0,
                  index,
                })}
                refreshing={libraryLoading}
                data={route.params.tracks}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                scrollsToTop={false}
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollView}
                style={[
                  styles.scrollViewContent,
                  currentTrack && {
                    marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM,
                  },
                ]}
              />
            )}
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#A7A7A7',
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 16,
  },
  scrollView: {
    // paddingBottom: 72,
    paddingVertical: 8,
  },
  scrollViewContent: {},
});
