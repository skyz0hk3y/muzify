// import {Track} from 'react-native-track-player';

import {GeneratedPlaylistId} from '../constants';
import {getUniqueListBy} from '../core/helpers/uniqListByKey';
import {
  LibraryActions,
  LibraryActionsType,
  LibraryPlaylist,
  LibraryStateType,
  MODULE_NAME,
  TrackDownload,
} from './types';

import PlaylistCoverLikedTracksImage from '../assets/playlist-cover-liked-tracks.jpg';
import PlaylistCoverBestLoopsImage from '../assets/playlist-cover-best-loops.jpg';
import PlaylistCoverMostPlayedTracks from '../assets/spotify-logo.png';

export const getLibraryPersistConfig = (storage: any) => ({
  key: MODULE_NAME,
  storage: storage,
  blacklist: ['loading', 'error', 'cachedTracks'],
  whitelist: [
    'tracks',
    'likedTracksIds',
    'localTracksIds',
    'playlists',
    'tracksDownloads',
  ],
});

const INITIAL_STATE: LibraryStateType = {
  // TODO(refactor): This is too generic/inflexible, switch to requests based loading/error
  loading: false,
  error: null,

  // Tracks
  tracks: [],
  likedTracksIds: [],
  localTracksIds: [],

  // Local cache
  cachedTracks: [],

  // Downloads manager
  tracksDownloads: {},

  // Playlists
  playlists: {
    [GeneratedPlaylistId.LIKED_TRACKS]: {
      id: GeneratedPlaylistId.LIKED_TRACKS,
      name: 'Liked tracks',
      artwork: PlaylistCoverLikedTracksImage,
      createdAtUnix: Date.now(),
      origin: 'generated',
      tracksIds: [],
    },
    [GeneratedPlaylistId.MOST_PLAYED_TRACKS]: {
      id: GeneratedPlaylistId.MOST_PLAYED_TRACKS,
      name: 'Most played tracks',
      artwork: PlaylistCoverMostPlayedTracks,
      createdAtUnix: Date.now(),
      origin: 'generated',
      tracksIds: [],
    },
    [GeneratedPlaylistId.MOST_REPEATED_TRACKS]: {
      id: GeneratedPlaylistId.MOST_REPEATED_TRACKS,
      name: 'Your best loops!',
      artwork: PlaylistCoverBestLoopsImage,
      createdAtUnix: Date.now(),
      origin: 'generated',
      tracksIds: [],
    },
  },
};

export const libraryReducer = (
  state = INITIAL_STATE,
  action: LibraryActionsType,
): LibraryStateType => {
  switch (action.type) {
    case LibraryActions.SET_LOADING: {
      return {
        ...state,
        loading: action.payload,
      };
    }
    case LibraryActions.SET_ERROR: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case LibraryActions.ADD_TRACKS: {
      return {
        ...state,
        tracks: getUniqueListBy(
          [...action.payload.tracks, ...state.tracks],
          'id',
        ),
      };
    }
    case LibraryActions.SET_LIKED_TRACKS: {
      return {
        ...state,
        tracks: getUniqueListBy(action.payload.tracks, 'id'),
      };
    }
    case LibraryActions.SET_LIKED_TRACK_IDS: {
      return {
        ...state,
        likedTracksIds: action.payload.likedTracksIds,
        playlists: {
          [GeneratedPlaylistId.LIKED_TRACKS]: {
            ...state.playlists[GeneratedPlaylistId.LIKED_TRACKS],
            tracksIds: action.payload.likedTracksIds,
          },
        },
      };
    }
    case LibraryActions.ADD_LOCAL_TRACK_ID: {
      return {
        ...state,
        localTracksIds: [...state.localTracksIds, action.payload],
      };
    }
    case LibraryActions.SET_LOCAL_TRACKS_IDS: {
      return {
        ...state,
        localTracksIds: action.payload,
      };
    }
    case LibraryActions.ADD_LIKED_TRACK: {
      return {
        ...state,
        likedTracksIds: [
          ...new Set([...state.likedTracksIds, action.payload.trackId]),
        ],
      };
    }
    case LibraryActions.REMOVE_LIKED_TRACK: {
      return {
        ...state,
        likedTracksIds: [
          ...state.likedTracksIds.filter((id) => id !== action.payload.trackId),
        ],
      };
    }
    case LibraryActions.CLEAR_TRACK_DOWNLOADS: {
      return {
        ...state,
        tracksDownloads: {},
      };
    }
    case LibraryActions.TRACK_DOWNLOAD_PENDING: {
      const {payload} = action;

      return {
        ...state,
        tracksDownloads: {
          ...state.tracksDownloads,
          [payload.trackId]: {
            trackId: payload.trackId,
            status: 'pending',
            progress: 0,
            startedAtUnix: undefined,
            endedAtUnix: undefined,
            expectedBytes: 0,
            writtenBytes: 0,
            totalBytes: 0,
          },
        },
      };
    }
    case LibraryActions.TRACK_DOWNLOAD_START: {
      const {payload} = action;
      const track: TrackDownload = state.tracksDownloads[payload.trackId];

      if (!track) {
        return state;
      }

      return {
        ...state,
        tracksDownloads: {
          ...state.tracksDownloads,
          [payload.trackId]: {
            trackId: payload.trackId,
            status: 'started',
            progress: 0,
            startedAtUnix: Date.now(),
            endedAtUnix: undefined,
            expectedBytes: payload.expectedBytes,
            writtenBytes: 0,
            totalBytes: 0,
          },
        },
      };
    }
    case LibraryActions.TRACK_DOWNLOAD_PROGRESS: {
      const {payload} = action;
      const track: TrackDownload = state.tracksDownloads[payload.trackId];

      if (!track) {
        return state;
      }

      return {
        ...state,
        tracksDownloads: {
          ...state.tracksDownloads,
          [payload.trackId]: {
            trackId: payload.trackId,
            progress: payload.progress * 100,
            status: 'in-progress',
            startedAtUnix: track.startedAtUnix,
            endedAtUnix: track.endedAtUnix,
            expectedBytes: track.expectedBytes,
            writtenBytes: payload.writtenBytes,
            totalBytes: payload.totalBytes,
          },
        },
      };
    }
    case LibraryActions.TRACK_DOWNLOAD_SUCCESS: {
      const {payload} = action;
      const track: TrackDownload = state.tracksDownloads[payload.trackId];

      if (!track) {
        return state;
      }

      return {
        ...state,
        tracksDownloads: {
          ...state.tracksDownloads,
          [payload.trackId]: {
            trackId: payload.trackId,
            progress: 100,
            status: 'success',
            startedAtUnix: track.startedAtUnix,
            endedAtUnix: Date.now(),
            // The following is not a bug, its intended
            expectedBytes: track.expectedBytes,
            writtenBytes: track.expectedBytes,
            totalBytes: track.expectedBytes,
          },
        },
      };
    }
    case LibraryActions.TRACK_DOWNLOAD_FAILURE: {
      const {payload} = action;
      const track: TrackDownload = state.tracksDownloads[payload.trackId];

      if (!track) {
        return state;
      }

      return {
        ...state,
        tracksDownloads: {
          ...state.tracksDownloads,
          [payload.trackId]: {
            trackId: payload.trackId,
            progress: track.progress,
            status: 'failure',
            startedAtUnix: track.startedAtUnix,
            endedAtUnix: Date.now(),
            error: payload.error,
            expectedBytes: track.expectedBytes,
            writtenBytes: track.writtenBytes,
            totalBytes: track.totalBytes,
          },
        },
      };
    }
    case LibraryActions.TRACK_DOWNLOAD_CANCEL: {
      const {payload} = action;
      const track: TrackDownload = state.tracksDownloads[payload.trackId];

      if (!track) {
        return state;
      }

      const newTracksDownloads = state.tracksDownloads;
      delete newTracksDownloads[payload.trackId];

      return {
        ...state,
        tracksDownloads: {
          ...newTracksDownloads,
        },
      };
    }
    case LibraryActions.SET_INITIAL_PLAYLISTS_STATE: {
      const {payload} = action;
      return {
        ...state,
        playlists: {
          ...state.playlists,
          ...payload.playlists,
        },
      };
    }
    case LibraryActions.CREATE_PLAYLIST: {
      const {payload} = action;
      return {
        ...state,
        playlists: {
          ...state.playlists,
          [payload.playlistId]: {
            id: payload.playlistId,
            originalId: payload.originalId,
            name: payload.playlist.name || payload.playlistId,
            origin: payload.playlist.origin || 'user',
            tracksIds: payload.playlist.tracksIds || [],
            artwork: payload.playlist.artwork,
            createdAtUnix: payload.playlist.createdAtUnix || Date.now(),
          },
        },
      };
    }
    case LibraryActions.UPDATE_PLAYLIST_NAME: {
      const {payload} = action;
      const playlist: LibraryPlaylist = state.playlists[payload.playlistId];

      if (!playlist) {
        return state;
      }

      return {
        ...state,
        playlists: {
          ...state.playlists,
          [playlist.id]: {
            ...playlist,
            name: payload.newName,
          },
        },
      };
    }
    case LibraryActions.DELETE_PLAYLIST: {
      const {payload} = action;
      const playlist: LibraryPlaylist = state.playlists[payload.playlistId];

      if (!playlist) {
        return state;
      }

      const newPlaylists = state.playlists;
      delete newPlaylists[payload.playlistId];

      return {
        ...state,
        playlists: {
          ...newPlaylists,
        },
      };
    }
    case LibraryActions.ADD_TRACK_TO_PLAYLIST: {
      const {payload} = action;
      const playlist: LibraryPlaylist = state.playlists[payload.playlistId];

      if (!playlist) {
        return state;
      }

      return {
        ...state,
        playlists: {
          ...state.playlists,
          [playlist.id]: {
            ...playlist,
            tracksIds: [...new Set([payload.trackId, ...playlist.tracksIds])],
          },
        },
      };
    }
    case LibraryActions.REMOVE_TRACK_FROM_PLAYLIST: {
      const {payload} = action;
      const playlist: LibraryPlaylist = state.playlists[payload.playlistId];

      if (!playlist) {
        return state;
      }

      return {
        ...state,
        playlists: {
          ...state.playlists,
          [playlist.id]: {
            ...playlist,
            tracksIds: [
              ...playlist.tracksIds.filter((id) => id !== payload.trackId),
            ],
          },
        },
      };
    }
    case LibraryActions.ADD_TRACKS_TO_LOCAL_CACHE: {
      const {payload} = action;

      return {
        ...state,
        // Ensure cache never has duplicates...
        cachedTracks: getUniqueListBy(
          [...state.cachedTracks, ...payload],
          'id',
        ),
      };
    }
    default:
      return state;
  }
};
