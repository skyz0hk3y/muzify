import React, {FC, useMemo} from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';
import RNTrackPlayer from 'react-native-track-player';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {MaterialDesignIconNameType} from '../../../../../packages/app-muzify/src/core/types';

import {TouchablePlatform} from '../../core/components/TouchablePlatform';
import {IconButton} from '../../core/components/IconButton';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';
import {getHumanSize} from '../../core/helpers/getHumanSize';
import {useTheme} from '../../core/hooks/useTheme';

import {TrackCover} from '../../audio-player/components/TrackCover';

import {TrackDownload} from '../types';

export type TrackDownloadItemProps = {
  testID?: string;
  track: RNTrackPlayer.Track;
  downloadInfos: TrackDownload;
  disabled?: boolean;
  isLoading?: boolean;
  isLocal?: boolean;
  isCurrentTrack?: boolean;
  isDownloading?: boolean;
  actionColor?: string;
  actionIconName?: MaterialDesignIconNameType;
  onItemPress?: (event: GestureResponderEvent) => void;
  onItemLongPress?: (event: GestureResponderEvent) => void;
  onActionPress?: (event: GestureResponderEvent) => void;
};

export const TrackDownloadItem: FC<TrackDownloadItemProps> = ({
  testID = 'track-download-item',
  track,
  downloadInfos: {status, progress, writtenBytes, totalBytes},
  disabled,
  actionIconName,
  actionColor,
  onItemPress,
  onItemLongPress,
  onActionPress,
}) => {
  const {theme} = useTheme();

  return (
    <View key={track.id}>
      <View style={styles.wrapper}>
        <TouchablePlatform
          testID={testID}
          disabled={disabled}
          onPress={onItemPress}
          onLongPress={onItemLongPress}
          useForeground
          background={TouchableNativeFeedback.Ripple(
            theme.colors.primary,
            false,
          )}
        >
          <View style={styles.itemWrapper}>
            <TrackCover
              size={'small'}
              source={{uri: track?.thumbnail || track?.artwork}}
              showLocalBadge={status === 'success'}
              isDownloading={status !== 'failure' && status !== 'success'}
            />
            <View style={styles.textsContainer}>
              <Text
                style={[styles.artistNameText, {color: theme.colors.textMuted}]}
                numberOfLines={1}
              >
                {
                  sanitizeTrack({
                    ...track,
                    artist: track?.author?.name || track?.artist,
                  }).artist
                }
              </Text>
              <Text
                style={{
                  ...styles.trackTitleText,
                  // ...(isLoading && loadingStyle),
                  color: theme.colors.text,
                }}
                numberOfLines={2}
              >
                {
                  sanitizeTrack({
                    ...track,
                    artist: track?.author?.name || track?.artist,
                  }).title
                }
              </Text>
              <Text
                style={{
                  ...styles.trackProgressText,
                  // ...(isLoading && loadingStyle),
                  color: theme.colors.text,
                }}
                numberOfLines={1}
              >
                {`${Math.floor(progress)}% - (${getHumanSize(
                  writtenBytes,
                )} / ${getHumanSize(totalBytes)})`}
              </Text>
            </View>
            {actionIconName && (
              <View style={styles.actions}>
                <IconButton
                  size={'normal'}
                  disabled={disabled || typeof onActionPress === 'undefined'}
                  color={actionColor || theme.colors.text}
                  style={styles.actionButton}
                  onPress={onActionPress}
                >
                  <Icon
                    size={24}
                    name={actionIconName}
                    color={actionColor || theme.colors.text}
                  />
                </IconButton>
              </View>
            )}
          </View>
        </TouchablePlatform>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    // height: TRACK_ITEM_HEIGHT,
  },
  itemWrapper: {
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',

    // minHeight: TRACK_ITEM_HEIGHT,

    paddingVertical: 8,
    paddingLeft: 16,
    paddingRight: 0,
  },
  actions: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionButton: {
    marginHorizontal: 8,
  },
  textsContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  artistNameText: {
    marginLeft: 16,

    fontSize: 12,
    lineHeight: 12,
    fontWeight: '700',
    color: '#666666',
  },
  trackTitleText: {
    marginLeft: 16,

    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
    color: '#ffffff',
  },
  trackProgressText: {
    marginLeft: 16,

    fontSize: 10,
    lineHeight: 16,
    fontWeight: '600',
    color: '#ffffff',
    opacity: 0.6,
  },
});
