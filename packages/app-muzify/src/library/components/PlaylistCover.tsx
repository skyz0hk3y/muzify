import React, {FC} from 'react';
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  ImageStyle,
  ImageURISource,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '../../core/hooks/useTheme';
import {MaterialDesignIconNameType} from '../../core/types';

export type PlaylistCoverProps = {
  sources: ImageURISource[];
  size?:
    | 'small'
    | 'normal'
    | 'large'
    | 'x-large'
    | 'xx-large'
    | 'xxx-large'
    | 'full';
  elevation?: number;
  showLocalBadge?: boolean;
  showSpotifyBadge?: boolean;
  isDownloading?: boolean;
};

const sizesMap = {
  small: 44,
  normal: 56,
  large: 64,
  'x-large': 72,
  'xx-large': 96,
  'xxx-large': 120,
  full: 156,
};

export const PlaylistCover: FC<PlaylistCoverProps> = ({
  sources,
  size = 'normal',
  elevation = 6,
  showLocalBadge = false,
  showSpotifyBadge = false,
  isDownloading = false,
}) => {
  const {theme} = useTheme();

  const sizeStyle: StyleProp<ImageStyle> = {
    minWidth: sizesMap[size],
    width: sizesMap[size],
    maxWidth: sizesMap[size],
    minHeight: sizesMap[size],
    height: sizesMap[size],
    maxHeight: sizesMap[size],
  };

  const elevationStyle: StyleProp<ViewStyle> = {
    shadowColor: '#000',
    shadowOffset: {width: 0, height: elevation},
    shadowOpacity: 0.24,
    shadowRadius: 8,
    elevation,
  };

  const downloadingStyle: StyleProp<ViewStyle> = {
    ...sizeStyle,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  };

  return (
    <View style={[styles.container, elevationStyle]}>
      {sources.length <= 1 && (
        <ImageBackground
          style={[styles.cover, sizeStyle]}
          source={sources[0]}
          width={sizesMap[size]}
          height={sizesMap[size]}
          resizeMode={'cover'}
          blurRadius={isDownloading ? 4 : 0}
        >
          {isDownloading && (
            <View style={downloadingStyle}>
              <ActivityIndicator
                color={theme.colors.text}
                size={sizesMap[size] * 0.7}
              />
            </View>
          )}
        </ImageBackground>
      )}
      {sources.length > 1 && sources.length <= 3 && (
        <View style={[styles.coverWrapper, sizeStyle]}>
          <Image
            source={sources[0]}
            style={[styles.coverHalfLeft]}
            width={sizesMap[size] / 2}
            height={sizesMap[size]}
            resizeMode={'cover'}
          />
          <Image
            source={sources[1]}
            style={[styles.coverHalfRight]}
            width={sizesMap[size] / 2}
            height={sizesMap[size]}
            resizeMode={'cover'}
          />
        </View>
      )}
      {sources.length >= 4 && (
        <View style={[styles.coverWrapper, sizeStyle]}>
          <Image
            source={sources[0]}
            style={[styles.coverHalfTopLeft]}
            width={sizesMap[size] / 2}
            height={sizesMap[size] / 2}
            resizeMode={'cover'}
          />
          <Image
            source={sources[1]}
            style={[styles.coverHalfTopRight]}
            width={sizesMap[size] / 2}
            height={sizesMap[size] / 2}
            resizeMode={'cover'}
          />
          <Image
            source={sources[2]}
            style={[styles.coverHalfBottomLeft]}
            width={sizesMap[size] / 2}
            height={sizesMap[size] / 2}
            resizeMode={'cover'}
          />
          <Image
            source={sources[3]}
            style={[styles.coverHalfBottomRight]}
            width={sizesMap[size] / 2}
            height={sizesMap[size] / 2}
            resizeMode={'cover'}
          />
        </View>
      )}
      {!isDownloading && showLocalBadge && (
        <View style={[styles.badge, {backgroundColor: theme.colors.primary}]}>
          <Icon
            name={'download-circle-outline' as MaterialDesignIconNameType}
            color={'#ffffff'}
            size={12}
          />
        </View>
      )}
      {showSpotifyBadge && (
        <View style={[styles.badge, styles.spotifyBadge]}>
          <Icon name={'spotify'} color={'white'} size={12} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  cover: {
    backgroundColor: '#555555',
    borderRadius: 8,
    overflow: 'hidden',
  },
  coverWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#555555',
    overflow: 'hidden',
    borderRadius: 8,
  },
  coverHalfLeft: {
    borderBottomLeftRadius: 8,
    borderTopLeftRadius: 8,
  },
  coverHalfRight: {
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8,
  },
  coverHalfTopLeft: {
    borderTopLeftRadius: 8,
  },
  coverHalfTopRight: {
    borderTopRightRadius: 8,
  },
  coverHalfBottomLeft: {
    borderBottomLeftRadius: 8,
  },
  coverHalfBottomRight: {
    borderBottomRightRadius: 8,
  },
  badge: {
    position: 'absolute',
    top: -4,
    left: -4,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    // elevation: 1,
  },
  spotifyBadge: {
    backgroundColor: '#000000',
  },
});
