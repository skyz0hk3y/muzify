import React, {FC, useMemo} from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {useTranslation} from '../../i18n';

import {TouchablePlatform} from '../../core/components/TouchablePlatform';
import {IconButton} from '../../core/components/IconButton';

import {LibraryPlaylist} from '../types';

import {PlaylistCover} from './PlaylistCover';
import {useSelector} from 'react-redux';
import {getPlaylistCoverSources} from '../selectors';
import {useTheme} from '../../core/hooks/useTheme';
import {MaterialDesignIconNameType} from '../../../../../packages/app-muzify/src/core/types';

export type PlaylistItemProps = {
  playlist: LibraryPlaylist;
  disabled?: boolean;
  isCurrentPlaylist?: boolean;
  isLoading?: boolean;
  isLocal?: boolean;
  isDownloading?: boolean;
  actionColor?: string;
  actionIconName?: MaterialDesignIconNameType;
  onItemPress?: (event: GestureResponderEvent) => void;
  onItemLongPress?: (event: GestureResponderEvent) => void;
  onActionPress?: (event: GestureResponderEvent) => void;
};

export const PlaylistItem: FC<PlaylistItemProps> = ({
  playlist,
  disabled,
  actionIconName,
  actionColor,
  isCurrentPlaylist = false,
  isLoading = false,
  isLocal = false,
  isDownloading = false,
  onItemPress,
  onItemLongPress,
  onActionPress,
}) => {
  const {theme} = useTheme();
  const {t} = useTranslation();
  const sources = useSelector(getPlaylistCoverSources(playlist.id));

  const memoizedSources = useMemo(() => {
    return sources?.[0]?.uri != null
      ? sources
      : playlist.artwork != null && typeof playlist.artwork === 'string' // URI
      ? [{uri: playlist.artwork}]
      : playlist.artwork != null && typeof playlist.artwork === 'number' // required images
      ? [playlist.artwork]
      : [require('../../assets/playlist-liked-tracks.png')];
  }, [playlist.artwork, sources]);

  const loadingStyle = useMemo(
    () => ({
      opacity: 0.7,
    }),
    [],
  );

  const itemStyle = useMemo(
    () => ({
      // ...(isLocal && {backgroundColor: 'rgba(0, 255, 0, 0.1)'}),
      ...(isCurrentPlaylist && {backgroundColor: theme.colors.backgrounds[3]}),
    }),
    [isCurrentPlaylist, theme.colors.backgrounds],
  );

  return (
    <View key={playlist.id}>
      <View style={styles.wrapper}>
        <TouchablePlatform
          disabled={disabled}
          onPress={onItemPress}
          onLongPress={onItemLongPress}
          useForeground
          background={TouchableNativeFeedback.Ripple(
            theme.colors.primary,
            false,
          )}
        >
          <View style={[styles.itemWrapper, itemStyle]}>
            <PlaylistCover
              sources={memoizedSources}
              showLocalBadge={playlist.origin === 'generated'}
              showSpotifyBadge={playlist.origin === 'spotify'}
              isDownloading={isDownloading}
            />
            <View style={styles.textsContainer}>
              <View style={styles.playlistMetaWrapper}>
                <Text
                  style={[
                    styles.playlistTracksCount,
                    {color: theme.colors.textMuted},
                  ]}
                  numberOfLines={1}
                >
                  {t('library:captions.x-tracks', {
                    tracksCount: playlist.tracksIds.length,
                  })}
                </Text>
              </View>
              <Text
                numberOfLines={1}
                style={{
                  ...styles.playlistName,
                  ...(isLoading && loadingStyle),
                  color: theme.colors.text,
                }}
              >
                {playlist.name}
              </Text>
            </View>
            {actionIconName && (
              <View style={styles.actions}>
                <IconButton
                  size={'normal'}
                  disabled={disabled || typeof onActionPress === 'undefined'}
                  color={actionColor || theme.colors.text}
                  style={styles.actionButton}
                  onPress={onActionPress}
                >
                  <Icon
                    size={24}
                    name={actionIconName}
                    color={actionColor || theme.colors.text}
                  />
                </IconButton>
              </View>
            )}
          </View>
        </TouchablePlatform>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    minHeight: 64,
    maxHeight: 80,
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 64,

    paddingVertical: 8,
    paddingLeft: 16,
    paddingRight: 0,
  },
  actions: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',

    paddingVertical: 8,
  },
  actionButton: {
    marginHorizontal: 8,
  },
  textsContainer: {
    flex: 1,
    marginHorizontal: 16,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    minHeight: 64,
    maxHeight: 80,
  },
  playlistMetaWrapper: {
    flexDirection: 'row',
    alignItems: 'center',

    width: '100%',
    marginBottom: 8,
  },
  playlistTracksCount: {
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '700',
    opacity: 0.6,
  },
  playlistName: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
  },
});
