import React, {
  FC,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {Track} from 'react-native-track-player';
import {useSelector} from 'react-redux';
import {TrackItemHorizontal} from '../../audio-player/components/TrackItemHorizontal';
import {getTrack} from '../../audio-player/helpers/getTrack';
import {useCurrentTrackAutoScroll} from '../../audio-player/hooks/useCurrentTrackAutoScroll';
import {
  getCurrentPlaylistIdSelector,
  getIsCurrentPlaylistSelector,
  getIsCurrentTrackSelector,
} from '../../audio-player/selectors';
import {TRACK_ITEM_HORIZONTAL_WIDTH} from '../../constants';
import {
  getIsDownloadingTrackSelector,
  getIsLocalTrackSelector,
  getLocalTracksIdsSelector,
  getPlaylistByIdSelector,
  getTracksSelector,
} from '../../library/selectors';

import {TrackDownload} from '../../library/types';

export const MAX_TRACKS_LIST_SIZE_BEFORE_NO_ANIMATION = 25;

export type PlaylistTracksListHorizontalProps = {
  id: string;
  playlistId: string;
  showLabels?: boolean;
  tracksDisabled?: boolean;
  onTrackSelect?: (trackId: string, playlistId: string) => void | Promise<void>;
  onTrackMorePress?: (track: Track) => void | Promise<void>;
};

export const PlaylistTracksListHorizontal: FC<PlaylistTracksListHorizontalProps> = ({
  id,
  playlistId,
  showLabels,
  tracksDisabled = false,
  onTrackSelect = () => undefined,
  onTrackMorePress = () => undefined,
}) => {
  const flatlistRef = useRef<FlatList<Track>>(null);
  const [tracks, setTracks] = useState<Track[]>([]);

  const libraryTracks = useSelector(getTracksSelector);
  const playlist = useSelector(getPlaylistByIdSelector(playlistId));
  const localTracksIds = useSelector(getLocalTracksIdsSelector);

  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isCurrentPlaylist = useSelector(getIsCurrentPlaylistSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);

  const renderItem = useMemo(
    () => ({item}) => {
      const onItemPress = () => onTrackSelect(item.id, playlist.id);
      const onTrackMoreModalPress = () => onTrackMorePress(item);
      return (
        <TrackItemHorizontal
          key={`${id}-tracks-${item.id}`}
          track={item}
          disabled={tracksDisabled}
          isCurrentTrack={
            isCurrentPlaylist(playlist.id) && isCurrentTrack(item.id)
          }
          isDownloading={isDownloadingTrack(item.id)}
          isLocal={isLocalTrack(item.id)}
          onItemPress={onItemPress}
          onItemLongPress={onTrackMoreModalPress}
          showLabels={showLabels}
        />
      );
    },
    [
      id,
      isCurrentPlaylist,
      isCurrentTrack,
      isDownloadingTrack,
      isLocalTrack,
      onTrackMorePress,
      onTrackSelect,
      playlist.id,
      showLabels,
      tracksDisabled,
    ],
  );

  useEffect(() => {
    const getTracks = () => {
      playlist.tracksIds.forEach(async (trackId) => {
        const t = await getTrack(libraryTracks, localTracksIds)(trackId);
        if (t) {
          setTracks((prev) => [...prev, t]);
        }
      });
    };

    if (tracks.length === 0) {
      getTracks();
    }
  }, [libraryTracks, localTracksIds, playlist, tracks]);

  // Handles auto-scroll when tracks changes
  useCurrentTrackAutoScroll(tracks, playlistId, (index, animated) => {
    if (flatlistRef.current) {
      // alert('HEY,Playlist,' + index + ',animated=' + animated);
      flatlistRef.current.scrollToIndex({
        animated,
        index,
        viewPosition: 0.5,
      });
    }
  });

  return (
    <View style={styles.container}>
      <FlatList
        ref={flatlistRef}
        style={styles.flatlist}
        contentContainerStyle={styles.contentContainer}
        horizontal
        contentInsetAdjustmentBehavior={'automatic'}
        nestedScrollEnabled={true}
        maxToRenderPerBatch={4}
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        onScrollToIndexFailed={(_info) => undefined}
        getItemLayout={(_, index) => ({
          index,
          length: TRACK_ITEM_HORIZONTAL_WIDTH, // 8px margins / 2
          offset: 0,
        })}
        keyExtractor={(item, index) => `${item.id}-${index}`}
        data={tracks}
        renderItem={renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  flatlist: {
    // paddingHorizontal: 8,
  },
  contentContainer: {
    paddingHorizontal: 8,
  },
});
