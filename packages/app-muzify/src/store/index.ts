import MMKVStorage from 'react-native-mmkv-storage';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {persistReducer, persistStore} from 'redux-persist';

import {
  audioPlayerReducer,
  getAudioPlayerPersistConfig,
} from '../audio-player/reducer';
import {commonReducer, getCommonPersistConfig} from '../core/reducer';
import {geniusReducer, getGeniusPersistConfig} from '../genius/reducer';
import {getLibraryPersistConfig, libraryReducer} from '../library/reducer';
import {getSearchPersistConfig, searchReducer} from '../search/reducer';
import {getSettingsPersistConfig, settingsReducer} from '../settings/reducer';

const middlewares = [thunkMiddleware];

if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

export const persistStorage = new MMKVStorage.Loader().initialize();

const appReducer = combineReducers({
  audioPlayer: persistReducer(
    getAudioPlayerPersistConfig(persistStorage),
    audioPlayerReducer,
  ),
  common: persistReducer(getCommonPersistConfig(persistStorage), commonReducer),
  genius: persistReducer(getGeniusPersistConfig(persistStorage), geniusReducer),
  library: persistReducer(
    getLibraryPersistConfig(persistStorage),
    libraryReducer,
  ),
  search: persistReducer(getSearchPersistConfig(persistStorage), searchReducer),
  settings: persistReducer(
    getSettingsPersistConfig(persistStorage),
    settingsReducer,
  ),
});

// Data persistence:
const persistenceReducer = persistReducer(
  {
    key: 'root',
    storage: persistStorage,
    blacklist: [
      'audioPlayer',
      'common',
      'genius',
      'library',
      'search',
      'settings',
    ],
  },
  appReducer,
);

const RESET_STORES_TO_INITIAL_STATES = 'root/resetStoresToInitialStates';

export const resetStoresToInitialStates = () => ({
  type: RESET_STORES_TO_INITIAL_STATES,
});

const rootReducer = (state, action) => {
  if (action.type === RESET_STORES_TO_INITIAL_STATES) {
    state = undefined;
  }
  return persistenceReducer(state, action);
};

export const store = createStore(rootReducer, applyMiddleware(...middlewares));

export const persistor = persistStore(store);
export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
