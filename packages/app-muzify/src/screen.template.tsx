import React, {FC} from 'react';
import {StyleSheet, View, Text, SafeAreaView} from 'react-native';

// import {useTranslation} from '../../i18n';

export type ScreenTemplateProps = {};

export const ScreenTemplate: FC<ScreenTemplateProps> = () => {
  // const {t} = useTranslation('core');
  return (
    <>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.screenTitle}>Template screen</Text>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#222222',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#A7A7A7',
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 16,
  },
});
