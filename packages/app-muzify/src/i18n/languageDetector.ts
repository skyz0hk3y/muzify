import {LanguageDetectorModule} from 'i18next';
import * as RNLocalize from 'react-native-localize';

const languageDetector: LanguageDetectorModule = {
  type: 'languageDetector',
  detect: () => {
    const detectedLng = RNLocalize.findBestAvailableLanguage(['en', 'fr']);
    return detectedLng?.languageTag;
  },
  init: () => {},
  cacheUserLanguage: () => {},
};

export default languageDetector;
