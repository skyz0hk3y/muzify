import i18next, {LanguageDetectorModule, ThirdPartyModule} from 'i18next';
import {initReactI18next} from 'react-i18next';

import {I18NOptions} from './types';
import languageDetector from './languageDetector';
import translations from './translations';

/**
 * Create a i18next instance
 * @param options.assetsPath The path from where translations should be fetched. Defaults to getAssetsPath('assets/translations/{{lng}}/{{ns}}.json'). There is no need to specify this throughout the app. It is here to allow others apps depending on this package to set their own path (e.g. the Game Launcher)
 * @param options.availableLanguages list of available languages
 * @param options.defaultLanguage default language code
 * @param options.initialLanguage initial language code
 * @param options.namespaces List of namespaces
 * @param options.defaultNamespace default namespace
 * @param options.preload list of languages to preload
 * @param options.useSuspense if true, i18n will trigger suspense each time a new file has to be loaded (default `false`)
 */
export const getI18n = ({
  // assetsPath = 'assets/translations/{{lng}}/{{ns}}.json',
  availableLanguages,
  defaultLanguage,
  initialLanguage,
  namespaces,
  defaultNamespace,
  preload = [],
  useSuspense = false,
}: I18NOptions) => {
  if (i18next.isInitialized) {
    return i18next;
  }

  i18next
    .use<ThirdPartyModule>(initReactI18next)
    .use<LanguageDetectorModule>(languageDetector)
    .init({
      debug: false,
      preload,
      fallbackLng: defaultLanguage,
      lng: initialLanguage,
      supportedLngs: availableLanguages,
      load: 'all',
      ns: namespaces,
      defaultNS: defaultNamespace,
      lowerCaseLng: true,
      resources: {
        ...translations,
      },
      interpolation: {
        // React already protects us from XSS...
        escapeValue: false,
      },
      react: {
        useSuspense,
        transSupportBasicHtmlNodes: true,
        transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'],
      },
    });

  return i18next;
};
