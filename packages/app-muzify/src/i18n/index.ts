import i18next from 'i18next';
import {getI18n} from './i18n';

/* Helpers */
export {I18nextProvider, useTranslation, Trans} from 'react-i18next';

/* Types */
export type I18n = typeof i18next;

export const i18n = getI18n({
  assetsPath: 'translations/{{lng}}/{{ns}}.json',
  availableLanguages: ['en', 'fr'],
  defaultLanguage: 'en',
  namespaces: ['core', 'audioPlayer', 'library', 'search', 'settings'],
  defaultNamespace: 'core',
  waitForTranslations: false,
  useSuspense: true,
});
