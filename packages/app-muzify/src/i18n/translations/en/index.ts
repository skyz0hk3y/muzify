import audioPlayer from './audioPlayer.json';
import core from './core.json';
import library from './library.json';
import search from './search.json';
import settings from './settings.json';
import importer from './importer.json';

export default {
  get core() {
    return core;
  },
  get audioPlayer() {
    return audioPlayer;
  },
  get library() {
    return library;
  },
  get search() {
    return search;
  },
  get settings() {
    return settings;
  },
  get importer() {
    return importer;
  },
};
