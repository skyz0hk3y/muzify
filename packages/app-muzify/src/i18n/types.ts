export type I18NOptions = {
  /**
   * The path from where translations should be fetched. Defaults to
   * getAssetsPath('assets/translations/{{lng}}/{{ns}}.json').
   *
   * There is no need to specify this throughout the app. It is here to allow
   * others apps depending on this package to set their own path.
   * (e.g. the Game Launcher)
   */
  assetsPath?: string;
  /** A list of 2 char iso code languages */
  availableLanguages: string[];
  /** The default language (2 char iso code) */
  defaultLanguage: string;
  /** The initial language (2 char iso code) */
  initialLanguage?: string;
  /** List of supported namespaces */
  namespaces: string[];
  /** Default namespace */
  defaultNamespace: string;
  /** A list of 2 char iso code languages to preload */
  preload?: string[];
  /**
   * If true, i18n will trigger suspense each time a new file has to be loaded.
   * Defaults to false.
   */
  useSuspense?: boolean;
  /**
   * If true, components using translated string will not be rendered until the
   * initial translations have been loaded. Defaults to false.
   */
  waitForTranslations?: boolean;
};
