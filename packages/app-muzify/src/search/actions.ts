import {Track} from 'react-native-track-player';
import {Dispatch} from 'redux';

import {API_BASE_URL} from '../constants';
import {getLocalPathForTrack} from '../core/helpers/getLocalPathForTrack';
import {RootState} from '../store';

import {
  SearchActions,
  SetSearchTermsActionType,
  SetSearchErrorActionType,
  SetLoadingActionType,
  SetResultsActionType,
  ClearSearchResultsActionType,
  AddOrIncrementRecentSearchQueryActionType,
  DeleteRecentSearchQueryActionType,
  RecentSearchQuery,
  SetRecentSearchQueriesActionType,
} from './types';

/* Actions */

export const setSearchTerms = (
  searchTerms: string,
): SetSearchTermsActionType => ({
  type: SearchActions.SET_SEARCH_TERMS,
  payload: searchTerms,
});

export const setSearchLoading = (isLoading: boolean): SetLoadingActionType => ({
  type: SearchActions.SET_LOADING,
  payload: isLoading,
});

export const setSearchError = (
  error: null | {
    code: string;
    message: string;
  },
): SetSearchErrorActionType => ({
  type: SearchActions.SET_SEARCH_ERROR,
  payload: error,
});

export const setSearchResults = (results: {
  playlists: Record<string, any>[];
  videos: Record<string, any>[];
  channels: Record<string, any>[];
}): SetResultsActionType => ({
  type: SearchActions.SET_RESULTS,
  payload: results,
});

export const clearSearchResults = (): ClearSearchResultsActionType => ({
  type: SearchActions.CLEAR_SEARCH_RESULTS,
  payload: undefined,
});

export const setRecentSearchQueries = (
  recentSearchQueries: RecentSearchQuery[],
): SetRecentSearchQueriesActionType => ({
  type: SearchActions.SET_RECENT_SEARCH_QUERIES,
  payload: {
    recentSearchQueries,
  },
});

export const addOrIncrementRecentSearchQuery = (
  searchTerms: string,
): AddOrIncrementRecentSearchQueryActionType => ({
  type: SearchActions.ADD_OR_INCREMENT_RECENT_SEARCH_QUERY,
  payload: {
    searchTerms,
  },
});

export const deleteRecentSearchQuerySync = (
  searchTerms: string,
): DeleteRecentSearchQueryActionType => ({
  type: SearchActions.DELETE_RECENT_SEARCH_QUERY,
  payload: {
    searchTerms,
  },
});

/* Actions creators */

type SearchActionDispatchType =
  | SetLoadingActionType
  | SetSearchErrorActionType
  | SetResultsActionType
  | AddOrIncrementRecentSearchQueryActionType;

export const search = (searchTerms: string) => {
  return async (
    dispatch: Dispatch<SearchActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {common, library} = state;

      dispatch(setSearchLoading(true));
      dispatch(setSearchError(null));
      dispatch(addOrIncrementRecentSearchQuery(searchTerms));

      if (common.networkState.isConnected === false) {
        const results = library.tracks
          .filter((track: Track) => {
            return (
              track.title
                .toLocaleLowerCase()
                .includes(searchTerms.toLocaleLowerCase()) ||
              track.artist
                .toLocaleLowerCase()
                .includes(searchTerms.toLocaleLowerCase())
            );
          })
          .map((track: Track) => ({
            ...track,
            videoId: track.id,
            url: 'file:///' + getLocalPathForTrack(track),
          }));

        dispatch(
          setSearchResults({
            playlists: [],
            videos: results || [],
            channels: [],
          }),
        );

        return;
      }

      const query = encodeURIComponent(searchTerms);
      const res = await fetch(`${API_BASE_URL}/search/music?q=${query}`);
      const json = await res.json();

      if (json.error) {
        dispatch(setSearchError(json.error));
        return;
      }

      dispatch(
        setSearchResults({
          playlists: json.data.results.playlists,
          videos: json.data.results.videos,
          channels: json.data.results.channels,
        }),
      );
    } catch (err) {
      dispatch(setSearchError(err));
    } finally {
      dispatch(setSearchLoading(false));
    }
  };
};

export const deleteRecentSearchQuery = (searchTerms: string) => {
  return async (dispatch: Dispatch<DeleteRecentSearchQueryActionType>) => {
    dispatch(deleteRecentSearchQuerySync(searchTerms));
  };
};
