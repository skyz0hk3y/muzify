export const MODULE_NAME = 'search';

export enum SearchActions {
  SET_SEARCH_TERMS = 'search/setSearchTerms',
  SET_SEARCH_ERROR = 'search/setError',
  SET_LOADING = 'search/setLoading',
  SET_RESULTS = 'search/setResults',
  CLEAR_SEARCH_RESULTS = 'search/clearSearchResults',
  SET_RECENT_SEARCH_QUERIES = 'search/setRecentSearchQueries',
  ADD_OR_INCREMENT_RECENT_SEARCH_QUERY = 'search/addOrIncrementRecentSearchQuery',
  DELETE_RECENT_SEARCH_QUERY = 'search/deleteRecentSearchQuery',
}

export type SetSearchTermsActionType = {
  type: SearchActions.SET_SEARCH_TERMS;
  payload: string;
};

export type SetSearchErrorActionType = {
  type: SearchActions.SET_SEARCH_ERROR;
  payload: null | {
    code: string;
    message: string;
  };
};

export type SetLoadingActionType = {
  type: SearchActions.SET_LOADING;
  payload: boolean;
};

export type SetResultsActionType = {
  type: SearchActions.SET_RESULTS;
  payload: {
    playlists: Record<string, any>[];
    videos: Record<string, any>[];
    channels: Record<string, any>[];
  };
};

export type ClearSearchResultsActionType = {
  type: SearchActions.CLEAR_SEARCH_RESULTS;
  payload: undefined;
};

export type SetRecentSearchQueriesActionType = {
  type: SearchActions.SET_RECENT_SEARCH_QUERIES;
  payload: {
    recentSearchQueries: RecentSearchQuery[];
  };
};

export type AddOrIncrementRecentSearchQueryActionType = {
  type: SearchActions.ADD_OR_INCREMENT_RECENT_SEARCH_QUERY;
  payload: {
    searchTerms: string;
  };
};

export type DeleteRecentSearchQueryActionType = {
  type: SearchActions.DELETE_RECENT_SEARCH_QUERY;
  payload: {
    searchTerms: string;
  };
};

export type SearchActionsType =
  | SetSearchTermsActionType
  | SetSearchErrorActionType
  | SetLoadingActionType
  | SetResultsActionType
  | ClearSearchResultsActionType
  | SetRecentSearchQueriesActionType
  | AddOrIncrementRecentSearchQueryActionType
  | DeleteRecentSearchQueryActionType;

export type RecentSearchQuery = {
  searchTerms: string;
  searchCount: number;
};

export type SearchResultsType = {
  playlists: Record<string, any>[];
  videos: Record<string, any>[];
  channels: Record<string, any>[];
};

export type SearchStateType = {
  searchTerms: string;
  searchError: null | {
    code: string;
    message: string;
  };
  loading: boolean;
  results: SearchResultsType;
  recentSearchQueries: RecentSearchQuery[];
};
