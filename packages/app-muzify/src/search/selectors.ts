import {RootState} from '../store';
import {RecentSearchQuery, SearchResultsType} from './types';

export const getSearchTermsSelector = (state: RootState): string => {
  return state.search.searchTerms;
};

export const getSearchErrorSelector = (state: RootState) => {
  return state.search.searchError;
};

export const getIsSearchLoadingSelector = (state: RootState): boolean => {
  return state.search.loading;
};

export const getSearchResultsSelector = (
  state: RootState,
): SearchResultsType => {
  return state.search.results;
};

export const getRecentSearchQueriesSelector = (state: RootState) =>
  state.search.recentSearchQueries.sort(
    (a: RecentSearchQuery, b: RecentSearchQuery) => {
      return b.searchCount - a.searchCount;
    },
  );

export const getRecentSearchQueriesForSearchTermsSelector = (
  state: RootState,
) =>
  state.search.recentSearchQueries.filter(
    (recentSearchQuery: RecentSearchQuery) => {
      return recentSearchQuery.searchTerms
        .toLowerCase()
        .includes(state.search.searchTerms.toLowerCase());
    },
  );
