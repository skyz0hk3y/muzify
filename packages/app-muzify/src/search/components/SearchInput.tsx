import {useNavigation} from '@react-navigation/core';
import React, {FC, useMemo} from 'react';
import {Platform, StyleSheet, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector} from 'react-redux';
import {MaterialDesignIconNameType} from '../../../../../packages/app-muzify/src/core/types';
import {Routes} from '../../constants';

import {IconButton} from '../../core/components/IconButton';
import {useTheme} from '../../core/hooks/useTheme';
import {hasPendingDownloadsSelector} from '../../library/selectors';

export type SearchInputProps = {
  autoFocus?: boolean;
  value?: string;
  placeholder?: string;
  iconName?: MaterialDesignIconNameType;
  onSearchSubmit?: () => void;
  onValueChange?: (value: string) => void;
  onClearTextPress?: () => void;
};

export const SearchInput: FC<SearchInputProps> = ({
  autoFocus = false,
  value = '',
  placeholder = '',
  iconName = 'magnify',
  onSearchSubmit = () => {},
  onValueChange = () => {},
  onClearTextPress = () => {},
}) => {
  const {theme} = useTheme();
  const {navigate} = useNavigation();

  const hasPendingDownloads = useSelector(hasPendingDownloadsSelector);

  return (
    <View
      style={[styles.container, {borderBottomColor: theme.colors.textMuted}]}
    >
      <IconButton size={'normal'} disabled style={styles.searchIcon}>
        <Icon name={iconName} size={32} color={theme.colors.text} />
      </IconButton>
      <TextInput
        testID="search-input"
        style={[styles.input, {color: theme.colors.text}]}
        placeholder={placeholder}
        placeholderTextColor={theme.colors.textMuted}
        value={value}
        onEndEditing={onSearchSubmit}
        onChangeText={onValueChange}
        // clearButtonMode={'while-editing'}
        returnKeyType={'search'}
        keyboardAppearance={'dark'}
        autoFocus={autoFocus}
      />
      <View style={styles.iconsRow}>
        <IconButton
          style={styles.iconMR8}
          onPress={() => navigate(Routes.LIBRARY_TRACKS_DOWNLOADS)}
        >
          <Icon
            name={
              hasPendingDownloads
                ? 'download-circle'
                : 'download-circle-outline'
            }
            size={24}
            color={
              hasPendingDownloads ? theme.colors.primary : theme.colors.text
            }
          />
        </IconButton>
        {value.trim() !== '' ? (
          <IconButton
            size={'normal'}
            onPress={onClearTextPress}
            style={styles.clearButton}
            testID={'search-input-clear-button'}
          >
            <Icon name="close" size={24} color={theme.colors.text} />
          </IconButton>
        ) : (
          <IconButton
            style={styles.iconMR8}
            onPress={() => navigate(Routes.SETTINGS)}
          >
            <Icon name={'cog'} size={24} color={theme.colors.text} />
          </IconButton>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? 56 - 7 : 64,
    borderBottomWidth: Platform.OS === 'ios' ? 0.3 : 0,
    paddingHorizontal: 0,
    // paddingBottom: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 44,
    paddingHorizontal: 16,
    flex: 1,
    fontSize: 20,
  },
  searchIcon: {
    marginTop: -8,
    marginLeft: 8,
    width: 32,
    minWidth: 32,
  },
  clearButton: {
    marginTop: -2,
    marginRight: 8,
    width: 32,
    minWidth: 32,
  },
  iconML8: {
    marginLeft: 8,
  },
  iconMR8: {
    marginRight: 8,
  },
  iconsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
