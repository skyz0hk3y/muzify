import React, {FC} from 'react';
import {ListItem} from '../../core/components/ListItem';
import {useTheme} from '../../core/hooks/useTheme';

export const RECENT_SEARCH_ITEM_HEIGHT = 44;

export type RecentSearchItemProps = {
  searchTerms: string;
  testID?: string;
  onPress: (searchTerms: string) => void;
  onClear: (searchTerms: string) => void;
};

export const RecentSearchItem: FC<RecentSearchItemProps> = ({
  searchTerms,
  testID = 'recent-search-item',
  onPress,
  onClear,
}) => {
  const {theme} = useTheme();

  const onItemPress = () => {
    onPress && onPress(searchTerms);
  };

  const onActionPress = () => {
    onClear && onClear(searchTerms);
  };

  return (
    <ListItem
      key={`${testID}-${searchTerms}`}
      testID={`${testID}-${searchTerms}`}
      iconName={'history'}
      actionIconName={'close'}
      iconColor={theme.colors.text}
      actionColor={theme.colors.text}
      title={searchTerms}
      onItemPress={onItemPress}
      onActionPress={onActionPress}
      style={{height: RECENT_SEARCH_ITEM_HEIGHT}}
    />
  );
};
