import {
  MODULE_NAME,
  RecentSearchQuery,
  SearchActions,
  SearchActionsType,
  SearchStateType,
} from './types';

const INITIAL_STATE: SearchStateType = {
  searchTerms: '',
  searchError: null,
  loading: false,
  recentSearchQueries: [],
  results: {
    playlists: [],
    videos: [],
    channels: [],
  },
};

export const getSearchPersistConfig = (storage: any) => ({
  key: MODULE_NAME,
  storage: storage,
  whitelist: ['recentSearchQueries'],
});

export const searchReducer = (
  state = INITIAL_STATE,
  action: SearchActionsType,
): SearchStateType => {
  switch (action.type) {
    case SearchActions.SET_SEARCH_TERMS: {
      return {
        ...state,
        searchTerms: action.payload,
      };
    }
    case SearchActions.SET_SEARCH_ERROR: {
      return {
        ...state,
        searchError: action.payload,
      };
    }
    case SearchActions.SET_LOADING: {
      return {
        ...state,
        loading: action.payload,
      };
    }
    case SearchActions.SET_RESULTS: {
      return {
        ...state,
        results: {
          ...action.payload,
        },
      };
    }
    case SearchActions.CLEAR_SEARCH_RESULTS: {
      return {
        ...state,
        results: {
          ...INITIAL_STATE.results,
        },
      };
    }
    case SearchActions.SET_RECENT_SEARCH_QUERIES: {
      return {
        ...state,
        recentSearchQueries: [...new Set(action.payload.recentSearchQueries)],
      };
    }
    case SearchActions.ADD_OR_INCREMENT_RECENT_SEARCH_QUERY: {
      const {searchTerms} = action.payload;

      const recentSearchQuery = state.recentSearchQueries.find(
        (item) => item.searchTerms === searchTerms.trim(),
      );

      let searchQuery: RecentSearchQuery = {
        searchTerms,
        searchCount: 1,
      };

      if (recentSearchQuery) {
        searchQuery = {
          searchTerms: recentSearchQuery.searchTerms,
          searchCount: recentSearchQuery.searchCount + 1,
        };
      }

      return {
        ...state,
        recentSearchQueries: [
          ...state.recentSearchQueries.filter(
            (item) => item.searchTerms !== searchTerms.trim(),
          ),
          searchQuery,
        ],
      };
    }
    case SearchActions.DELETE_RECENT_SEARCH_QUERY: {
      const {searchTerms} = action.payload;

      const recentSearchQuery = state.recentSearchQueries.find(
        (item) => item.searchTerms === searchTerms.trim(),
      );

      if (!recentSearchQuery) {
        return state;
      }

      return {
        ...state,
        recentSearchQueries: [
          ...state.recentSearchQueries.filter(
            (item) => item.searchTerms !== searchTerms.trim(),
          ),
        ],
      };
    }
    default:
      return state;
  }
};
