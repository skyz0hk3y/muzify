import React, {
  FC,
  useEffect,
  useCallback,
  useState,
  useContext,
  useMemo,
} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  RefreshControl,
  Keyboard,
  ListRenderItem,
  FlatList,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import EmptyStateSearch from '../../assets/empty-state-search-2x.png';
import NoSearchResults from '../../assets/no-results-2x.png';

import {useTranslation} from '../../i18n';
import {API_BASE_URL, SCREEN_FLATLIST_MARGIN_BOTTOM} from '../../constants';

import * as PlayerActions from '../../audio-player/actions';
import * as SearchActions from '../actions';
import * as LibraryActions from '../../library/actions';

import {TrackItem} from '../../audio-player/components/TrackItem';
import {SearchInput} from '../components/SearchInput';
import {EmptyState} from '../../core/components/EmptyState';
import {
  getRecentSearchQueriesSelector,
  getRecentSearchQueriesForSearchTermsSelector,
  getSearchTermsSelector,
  // getSearchErrorSelector,
  getIsSearchLoadingSelector,
  getSearchResultsSelector,
} from '../selectors';
import {RecentSearchItem} from '../components/RecentSearchItem';
import {RecentSearchQuery} from '../types';
import {TrackMoreContext} from '../../audio-player/TrackMoreContext';
import {getIsOfflineSelector} from '../../core/selector';
import {
  getCurrentTrackSelector,
  getIsCurrentTrackSelector,
  getIsTrackLoading,
} from '../../audio-player/selectors';
import {
  getLocalTracksIdsSelector,
  getTracksDownloadsSelector,
  getTrackIdsSelector,
  getPlaylistsSelector,
  getIsLocalTrackSelector,
  getIsDownloadingTrackSelector,
} from '../../library/selectors';
import {ScreenTopAction} from '../../core/components/ScreenTopAction';
import {useTheme} from '../../core/hooks/useTheme';
import {useAudioPlayer} from '../../audio-player/hooks/useAudioPlayer';
import {Track} from 'react-native-track-player';
import {getStreamingUrlForTrackId} from '../../core/helpers/getStreamingUrlForTrackId';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';

const searchTrack = async (query: string): Promise<any> => {
  const res = await fetch(
    `${API_BASE_URL}/search/music?q=${encodeURIComponent(query)}`,
  );
  const json = await res.json();

  if (json.error) {
    return Promise.reject(json.error);
  }

  return Promise.resolve(json.data);
};

export type SearchTabProps = {
  // React Navigation
  focusSearch: boolean;
  route: {
    params: {
      videoId?: string;
      playlistId?: string;
      focusSearchInput: boolean;
    };
  };
};

export const SearchTab: FC<SearchTabProps> = (props) => {
  const {
    // React Navigation
    focusSearch,
    route,
  } = props;

  const {theme} = useTheme();
  const {t} = useTranslation('search');
  const {openTrackMoreForTrack} = useContext(TrackMoreContext);
  const {playTrack} = useAudioPlayer();

  const [shouldFocusInput, setShouldFocusInput] = useState(false);
  const [isKeyboardShown, setIsKeyboardShown] = useState(false);

  const currentTrack = useSelector(getCurrentTrackSelector);
  const isTrackLoading = useSelector(getIsTrackLoading);
  const likedTracksIds = useSelector(getTrackIdsSelector);
  const playlists = useSelector(getPlaylistsSelector);
  const searchTerms = useSelector(getSearchTermsSelector);
  const searchLoading = useSelector(getIsSearchLoadingSelector);
  const searchResults = useSelector(getSearchResultsSelector);
  const recentSearchQueries = useSelector(getRecentSearchQueriesSelector);
  const recentSearchQueriesForSearchTerms = useSelector(
    getRecentSearchQueriesForSearchTermsSelector,
  );

  const dispatch = useDispatch();
  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);

  const normalizedSearchResults = useMemo(
    () => [
      ...searchResults.playlists?.map((item) => ({
        ...item,
        id: item.listId,
        type: 'playlist',
      })),
      ...searchResults.videos?.map((item) => ({
        ...item,
        id: item.videoId,
        type: 'music',
      })),
    ],
    [searchResults.playlists, searchResults.videos],
  );

  const resultsShown =
    searchResults.playlists.length > 0 || searchResults.videos.length > 0;

  const canShowEmptyState = !searchLoading && !isKeyboardShown;

  const showEmptyState =
    recentSearchQueries.length === 0 &&
    canShowEmptyState &&
    searchTerms.trim() === '' &&
    normalizedSearchResults.length === 0;

  const showNoResults =
    canShowEmptyState &&
    searchTerms.trim() !== '' &&
    normalizedSearchResults.length === 0;

  const showRecentSearches =
    recentSearchQueries.length > 0 &&
    // !isKeyboardShown &&
    !searchLoading &&
    searchTerms.trim() === '' &&
    normalizedSearchResults.length === 0;

  const showRecentSearchesForSearchTerms =
    recentSearchQueriesForSearchTerms.length > 0 &&
    isKeyboardShown &&
    !searchLoading &&
    searchTerms.trim() !== '' &&
    normalizedSearchResults.length === 0;

  const onTrackSelect = useCallback(
    async (trackId: string) => {
      await dispatch(PlayerActions.setPlaylistId(null));
      await playTrack(
        trackId,
        null,
        normalizedSearchResults.filter((tr) => tr.type === 'music') as Track[],
      );
    },
    [dispatch, playTrack, normalizedSearchResults],
  );

  const onPlaylistSelect = useCallback(
    async (listId: string) => {
      await dispatch(PlayerActions.playPlaylist(listId));
    },
    [dispatch],
  );

  const onPlaylistImport = useCallback(
    (playlistId: string) => {
      // console.log('onPlaylistImport -> item:', item);
      dispatch(LibraryActions.importRemotePlaylist(playlistId));
    },
    [dispatch],
  );

  const onSearchEnd = useCallback(async () => {
    if (searchTerms.trim() !== '') {
      setShouldFocusInput(false);
      await dispatch(SearchActions.search(searchTerms));
    }
  }, [dispatch, searchTerms]);

  const onClearSearchTerms = useCallback(() => {
    dispatch(SearchActions.setSearchTerms(''));
    dispatch(SearchActions.clearSearchResults());
    setShouldFocusInput(true);
  }, [dispatch]);

  const onTrackFavoriteToggle = useCallback(
    (trackId: string) => {
      if (!likedTracksIds.includes(trackId)) {
        dispatch(LibraryActions.addLikedTrack(trackId));
      } else {
        dispatch(LibraryActions.removeLikedTrack(trackId));
      }
    },
    [dispatch, likedTracksIds],
  );

  const onRecentSearchQueryPress = useCallback(
    async (terms: string) => {
      dispatch(SearchActions.setSearchTerms(terms));
      await dispatch(SearchActions.search(terms));
      setShouldFocusInput(false);
    },
    [dispatch],
  );

  const onRecentSearchQueryClearPress = useCallback(
    (terms: string) => {
      dispatch(SearchActions.deleteRecentSearchQuery(terms));
    },
    [dispatch],
  );

  const onSearchInputValueChange = useCallback(
    (value: string) => {
      dispatch(SearchActions.setSearchTerms(value));
    },
    [dispatch],
  );

  const getTrackIcon = useCallback(
    (trackId: string) => {
      return likedTracksIds.includes(trackId) ? 'heart' : 'heart-outline';
    },
    [likedTracksIds],
  );

  const getTrackIconColor = useCallback(
    (trackId: string) => {
      return likedTracksIds.includes(trackId) ? 'red' : theme.colors.textMuted;
    },
    [likedTracksIds, theme.colors.textMuted],
  );

  const getPlaylistIcon = useCallback(
    (playlistId: string) => {
      return playlists.find((p) => p.id === playlistId) != null
        ? 'playlist-remove'
        : 'playlist-star';
    },
    [playlists],
  );

  const getPlaylistIconColor = useCallback(
    (playlistId: string) => {
      return playlists.find((p) => p.id === playlistId) != null
        ? 'red'
        : theme.colors.textMuted;
    },
    [playlists, theme.colors.textMuted],
  );

  const renderItem: ListRenderItem<Record<string, any>> = useMemo(
    () => ({item}: Record<string, any>) => {
      if (searchLoading) {
        return null;
      }

      if (item.listId) {
        return (
          <TrackItem
            key={item.listId}
            track={item}
            disabled={isTrackLoading}
            onItemPress={onPlaylistSelect.bind(null, item.listId)}
            actionIconName={getPlaylistIcon(item.listId)}
            actionColor={getPlaylistIconColor(item.listId)}
            onActionPress={onPlaylistImport.bind(null, item.listId)}
          />
        );
      }

      if (item.videoId) {
        const itemAsTrack: Track = sanitizeTrack({
          id: item.videoId,
          artist: item.author.name,
          title: item.title,
          artwork: item.image,
          duration: item.duration.seconds,
          description: item.description,
          url: getStreamingUrlForTrackId(item.videoId),
        });

        return (
          <TrackItem
            key={item?.videoId}
            track={itemAsTrack}
            disabled={isTrackLoading}
            isLocal={isLocalTrack(item.videoId)}
            isCurrentTrack={isCurrentTrack(item.videoId)}
            isDownloading={isDownloadingTrack(item.videoId)}
            onItemPress={onTrackSelect.bind(null, item?.videoId)}
            onItemLongPress={openTrackMoreForTrack?.bind(null, item)}
            actionColor={getTrackIconColor(item?.videoId)}
            actionIconName={getTrackIcon(item?.videoId)}
            onActionPress={onTrackFavoriteToggle.bind(null, item?.videoId)}
          />
        );
      }

      return null;
    },
    [
      getPlaylistIcon,
      getPlaylistIconColor,
      getTrackIcon,
      getTrackIconColor,
      isCurrentTrack,
      isDownloadingTrack,
      isLocalTrack,
      isTrackLoading,
      onPlaylistImport,
      onPlaylistSelect,
      onTrackFavoriteToggle,
      onTrackSelect,
      openTrackMoreForTrack,
      searchLoading,
    ],
  );

  const renderRecentSearchQueryItem: ListRenderItem<RecentSearchQuery> = useMemo(
    () => ({item}) => {
      return (
        <RecentSearchItem
          testID={'recent-search'}
          searchTerms={item.searchTerms}
          onPress={onRecentSearchQueryPress}
          onClear={onRecentSearchQueryClearPress}
        />
      );
    },
    [onRecentSearchQueryClearPress, onRecentSearchQueryPress],
  );

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => setIsKeyboardShown(true));
    Keyboard.addListener('keyboardDidHide', () => setIsKeyboardShown(false));

    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  });

  useEffect(() => {
    const reactToRouteParams = async () => {
      const {playlistId, videoId} = route.params;

      if (playlistId) {
        const {results} = await searchTrack(playlistId);
        await onPlaylistSelect(results.playlists[0].listId);
      } else if (videoId) {
        await onTrackSelect(videoId);
      }
    };

    reactToRouteParams();
  }, [onPlaylistSelect, onTrackSelect, route.params]);

  return (
    <>
      <SafeAreaView style={{backgroundColor: theme.colors.backgrounds[0]}}>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
          testID="search-tab"
        >
          <ScreenTopAction
            backgroundColor={theme.colors.backgrounds[0]}
            noMargins
          >
            <SearchInput
              value={searchTerms}
              placeholder={t('search:inputPlaceholder')}
              onSearchSubmit={onSearchEnd}
              onValueChange={onSearchInputValueChange}
              onClearTextPress={onClearSearchTerms}
              autoFocus={
                shouldFocusInput || route.params.focusSearchInput || focusSearch
              }
            />
          </ScreenTopAction>

          {showNoResults && (
            <EmptyState
              source={NoSearchResults}
              title={t('search:no-results.title')}
              subtitle={t('search:no-results.subtitle')}
            />
          )}
          {showEmptyState && (
            <EmptyState
              source={EmptyStateSearch}
              title={t('search:empty-state.title')}
              subtitle={t('search:empty-state.subtitle')}
            />
          )}
          {showRecentSearches && (
            <>
              <FlatList
                data={recentSearchQueries}
                keyExtractor={(item) => item.searchTerms}
                renderItem={renderRecentSearchQueryItem}
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollView}
                style={[
                  styles.scrollViewContent,
                  currentTrack && {
                    marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM,
                  },
                ]}
              />
            </>
          )}
          {showRecentSearchesForSearchTerms && (
            <>
              <FlatList
                data={recentSearchQueriesForSearchTerms}
                keyExtractor={(item) => item.searchTerms}
                renderItem={renderRecentSearchQueryItem}
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollView}
                style={[
                  styles.scrollViewContent,
                  currentTrack && {
                    marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM,
                  },
                ]}
              />
            </>
          )}
          {(resultsShown || searchLoading) && (
            <>
              <FlatList
                data={normalizedSearchResults}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                refreshControl={
                  <RefreshControl
                    refreshing={searchLoading}
                    onRefresh={onSearchEnd}
                    progressBackgroundColor={theme.colors.backgrounds[3]}
                    colors={[theme.colors.primary]}
                  />
                }
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollView}
                style={[
                  styles.scrollViewContent,
                  currentTrack && {
                    marginBottom: SCREEN_FLATLIST_MARGIN_BOTTOM,
                  },
                ]}
              />
            </>
          )}
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    paddingTop: 16,
    paddingBottom: 8,
    paddingHorizontal: 16,
  },
  scrollView: {
    // paddingBottom: 72,
    paddingVertical: 8,
  },
  scrollViewContent: {},
  errorText: {
    marginTop: 8,
    color: 'red',
  },
});
