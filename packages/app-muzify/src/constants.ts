import {Platform} from 'react-native';
import TrackPlayer, {MetadataOptions} from 'react-native-track-player';
import {PLATFORM_BOTTOM_HEIGHT} from './core/components/BottomTabBar';

export const DONATION_BTC_ADDRESS =
  'bc1q3td4qznfvravr7uqjhqu47j00mtnhyd4wagp0k';
export const DONATION_BTC_COLOR = '#f7931a';

export const DONATION_LIBERAPAY_HANDLE = 'SkyzohKey';
export const DONATION_LIBERAPAY_URL = 'https://liberapay.com';
export const DONATION_LIBERAPAY_COLOR = '#f6c915';

// export const API_BASE_URL = 'http://127.0.0.1:3000';
// export const API_BASE_URL = __DEV__
//   ? 'http://192.168.0.140:3000'
//   : 'https://muzify.eu.openode.io';
export const API_BASE_URL = 'https://muzify.eu.openode.io';

export const YOUTUBE_ID_REGEX = /(?:youtube\.com\/(?:[^/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?/\s]{11})/gi;
export const YOUTUBE_PLAYLIST_ID_REGEX = /^https:\/\/(?:www|m)\.youtube\.com\/(?:playlist|watch)\?(?:.+&|)list=([a-zA-Z0-9_-]{12,})(?:(&.*)|$)/gi;
export const SPOTIFY_PLAYLIST_ID_REGEX = /^https:\/\/open.spotify.com\/playlist\/([a-zA-Z0-9_-]{20,25})\??/gi;

export const SCREEN_FLATLIST_MARGIN_BOTTOM = Platform.select({
  ios: PLATFORM_BOTTOM_HEIGHT - PLATFORM_BOTTOM_HEIGHT / 3,
  android: PLATFORM_BOTTOM_HEIGHT,
  default: PLATFORM_BOTTOM_HEIGHT,
});

export const SPACING_UNIT = 8;
export const SPACING_HALF_UNIT = SPACING_UNIT / 2;

export const TRACK_ITEM_HEIGHT = 64;
export const TRACK_ITEM_HORIZONTAL_WIDTH = 112;

export const TrackPlayerEvents = {
  REMOTE_PLAY: 'remote-play',
  REMOTE_PLAY_ID: 'remote-play-id',
  REMOTE_PLAY_SEARCH: 'remote-play-search',
  REMOTE_PAUSE: 'remote-pause',
  REMOTE_STOP: 'remote-stop',
  REMOTE_SKIP: 'remote-skip',
  REMOTE_NEXT: 'remote-next',
  REMOTE_PREVIOUS: 'remote-previous',
  REMOTE_SEEK: 'remote-seek',
  REMOTE_SET_RATING: 'remote-set-rating',
  REMOTE_JUMP_FORWARD: 'remote-jump-forward',
  REMOTE_JUMP_BACKWARD: 'remote-jump-backward',
  REMOTE_DUCK: 'remote-duck',
  REMOTE_LIKE: 'remote-like',
  REMOTE_DISLIKE: 'remote-dislike',
  REMOTE_BOOKMARK: 'remote-bookmark',
  PLAYBACK_STATE: 'playback-state',
  PLAYBACK_TRACK_CHANGED: 'playback-track-changed',
  PLAYBACK_QUEUE_ENDED: 'playback-queue-ended',
  PLAYBACK_ERROR: 'playback-error',
};

export const TRACK_PLAYER_CONTROLS_OPTS: MetadataOptions = {
  stopWithApp: false,
  ratingType: TrackPlayer.RATING_HEART,
  capabilities: [
    TrackPlayer.CAPABILITY_PLAY,
    TrackPlayer.CAPABILITY_PLAY_FROM_ID,
    TrackPlayer.CAPABILITY_PAUSE,
    TrackPlayer.CAPABILITY_STOP,
    TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
    TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
    TrackPlayer.CAPABILITY_SEEK_TO,
  ],
  compactCapabilities: [
    TrackPlayer.CAPABILITY_PLAY,
    TrackPlayer.CAPABILITY_PAUSE,
    TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
    TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
    // TrackPlayer.CAPABILITY_LIKE,
  ],
};

export enum Routes {
  DONATE = 'DonateScreen',
  GENIUS = 'HomeTab',
  HOME = 'MainScreen',
  IMPORT_SPOTIFY = 'SpotifyImportScreen',
  LIBRARY = 'LibraryTab',
  LIBRARY_HOME = 'LibraryEntryScreen',
  LIBRARY_CREATE_PLAYLIST = 'CreatePlaylistScreen',
  LIBRARY_PLAYLIST_DETAILS = 'PlaylistDetailsScreen',
  LIBRARY_TRACKS_DOWNLOADS = 'TracksDownloadsScreen',
  SEARCH = 'SearchTab',
  SETTINGS = 'SettingsScreen',
  SETTINGS_APPEARANCE = 'SettingsAppearanceScreen',
  SETTINGS_DOWNLOADS = 'SettingsDownloadsScreen',
  SETTINGS_HOME = 'SettingsEntryScreen',
  SETTINGS_STATS = 'SettingsStatsScreen',
  SETTINGS_STREAMING = 'SettingsStreamingScreen',
}

export enum GeneratedPlaylistId {
  LIKED_TRACKS = 'liked-tracks',
  MOST_PLAYED_TRACKS = 'most-played-tracks',
  MOST_REPEATED_TRACKS = 'most-repeated-tracks',
}
