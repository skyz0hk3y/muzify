declare module 'react-native-dialogs' {
  const actionPositive = 'positive';

  function showPicker(
    title: string,
    text: string | null,
    opts: {
      items: (null | ({id: string; label: string} & Record<string, any>))[];
      neutralText?: string;
      positiveText?: string;
    },
  ): {
    selectedItem: {
      id: string;
      label: string;
    } & Record<string, any>;
  };

  function alert(
    title: string,
    text: string | null,
    opts: {
      positiveText?: string;
      positiveColor?: string;
      neutralText?: string;
      neutralColor?: string;
    },
  );

  function prompt(
    title: string,
    text: string | null,
  ): {
    action: typeof actionPositive;
    text: string;
  };
}
