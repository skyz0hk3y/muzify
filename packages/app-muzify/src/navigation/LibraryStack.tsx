import React, {FC} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {CreatePlaylistScreen} from '../library/screens/CreatePlaylistScreen';
import {LibraryEntryScreen} from '../library/screens/EntryScreen';
import {LibraryPlaylistDetailsScreen} from '../library/screens/PlaylistDetailsScreen';
import {Routes} from '../constants';

const Library = createStackNavigator();

export type LibraryStackProps = {};

export const LibraryStack: FC<LibraryStackProps> = () => (
  <Library.Navigator initialRouteName={Routes.LIBRARY_HOME}>
    <Library.Screen
      name={Routes.LIBRARY_HOME}
      component={LibraryEntryScreen}
      options={{
        headerShown: false,
      }}
    />
    <Library.Screen
      name={Routes.LIBRARY_CREATE_PLAYLIST}
      component={CreatePlaylistScreen}
      options={{
        headerShown: false,
      }}
    />
    <Library.Screen
      name={Routes.LIBRARY_PLAYLIST_DETAILS}
      component={LibraryPlaylistDetailsScreen}
      options={{
        headerShown: false,
      }}
    />
  </Library.Navigator>
);
