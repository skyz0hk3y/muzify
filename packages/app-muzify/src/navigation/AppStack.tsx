import React, {FC, useContext} from 'react';
import {Image, Platform, StyleSheet, View} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {
  createStackNavigator,
  HeaderHeightContext,
} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// ts-fixme: wtf?
import MuzifyLogoImage from '../assets/ic_launcher_round.png';

import {useTranslation} from '../i18n';

import {IconButton} from '../core/components/IconButton';
import {SpotifyImportScreen} from '../importer/screens/SpotifyImportScreen';

import {MainStack} from './MainStack';
import {SettingsStack} from './SettingsStack';
import {useTheme} from '../core/hooks/useTheme';
import {useDispatch, useSelector} from 'react-redux';
import {clearTrackDownloads, createPlaylist} from '../library/actions';
import {NoInternetBanner} from '../core/components/NoInternetBanner';
import {getIsOfflineSelector} from '../core/selector';
import {UpdaterBanner} from '../core/components/UpdaterBanner';
import {Routes} from '../constants';
import {hasPendingDownloadsSelector} from '../library/selectors';
import {TracksDownloadsScreen} from '../library/screens/TracksDownloadsScreen';
import {StackHeaderOptions} from '@react-navigation/stack/lib/typescript/src/types';
import {MaterialDesignIconNameType} from '../core/types';

const App = createStackNavigator();

export type AppStackProps = {};

type Route = {
  route: RouteProp<Record<string, any | undefined>, ''> & {
    state: {routes: any[]; index: number};
    name: string;
  };
};

const getActiveRoute = (
  route: RouteProp<Record<string, any | undefined>, ''> & {
    state: {routes: any[]; index: number};
    name: string;
  },
): Route => {
  if (route.state) {
    return getActiveRoute(route.state.routes[route.state.index]);
  }

  return {route};
};
const getActiveRouteName = (
  route: RouteProp<Record<string, any | undefined>, ''> & {
    state: {routes: any[]; index: number};
    name: string;
  },
): string => {
  const {route: activeRoute} = getActiveRoute(route);
  return activeRoute.name;
};

export const AppStack: FC<AppStackProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('core');

  const dispatch = useDispatch();
  const isOffline = useSelector(getIsOfflineSelector);
  const hasPendingDownloads = useSelector(hasPendingDownloadsSelector);

  const headerHeight = useContext(HeaderHeightContext);
  const headerRealHeight = headerHeight || Platform.OS === 'ios' ? 96 : 64;

  const getHeaderTitle = (route: any) => {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "MainScreen" as that's the first screen inside the navigator
    const routeName = getActiveRouteName(route) ?? Routes.HOME;
    const activeRoute = getActiveRoute(route);

    switch (routeName) {
      case Routes.IMPORT_SPOTIFY: {
        return t('core:navigation.importer.spotify-import');
      }
      case Routes.LIBRARY:
      case Routes.LIBRARY_HOME: {
        return t('core:navigation.library');
      }
      case Routes.LIBRARY_CREATE_PLAYLIST: {
        return t('library:createPlaylistButton').replace('...', '');
      }
      case Routes.LIBRARY_PLAYLIST_DETAILS: {
        if (activeRoute.route.params?.playlistTitle) {
          return activeRoute.route.params.playlistTitle;
        }
        return t('core:navigation.library-screens.playlist-details');
      }
      case Routes.LIBRARY_TRACKS_DOWNLOADS: {
        // TODO(i18n): Move & localize this
        return 'Gestionnaire de téléchargements';
      }
      case Routes.SEARCH: {
        return t('core:navigation.search');
      }
      default: {
        return 'Muzify';
      }
    }
  };

  const getHeaderShown = (route: any) => {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "MainScreen" as that's the first screen inside the navigator
    const routeName = getActiveRouteName(route) ?? Routes.HOME;

    switch (routeName) {
      case Routes.SEARCH:
      case Routes.IMPORT_SPOTIFY:
        return false;
      default:
        return true;
    }
  };

  const getHeaderLeftIcon = (
    route: any,
    navigation: any,
    tintColor?: string,
  ) => {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "MainScreen" as that's the first screen inside the navigator
    const routeName = getActiveRouteName(route) ?? Routes.HOME;

    switch (routeName) {
      case Routes.IMPORT_SPOTIFY:
      case Routes.LIBRARY_TRACKS_DOWNLOADS: {
        return (
          <IconButton
            style={styles.iconML8}
            onPress={() => navigation.goBack()}
          >
            <Icon
              name={'close' as MaterialDesignIconNameType}
              size={24}
              color={tintColor}
            />
          </IconButton>
        );
      }
      case Routes.LIBRARY_CREATE_PLAYLIST:
      case Routes.LIBRARY_PLAYLIST_DETAILS: {
        return (
          <IconButton
            style={styles.iconML8}
            onPress={() => navigation.navigate(Routes.LIBRARY_HOME)}
          >
            <Icon
              name={'arrow-left' as MaterialDesignIconNameType}
              size={24}
              color={tintColor}
            />
          </IconButton>
        );
      }
      default: {
        return (
          <View style={styles.logoWrapper}>
            <Image
              source={MuzifyLogoImage}
              fadeDuration={0}
              style={styles.logo}
            />
          </View>
        );
      }
    }
  };

  const getHeaderRightIcon = (
    route: any,
    navigation: any,
    tintColor?: string,
  ) => {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "MainScreen" as that's the first screen inside the navigator
    const routeName = getActiveRouteName(route) ?? Routes.HOME;
    const activeRoute = getActiveRoute(route);

    const onCreatePlaylistPress = () => {
      const {params} = activeRoute.route;

      if (params?.playlistId && params?.playlistTitle) {
        dispatch(
          createPlaylist(params.playlistId, {
            id: params.playlistId,
            name: params.playlistTitle || 'Unnamed playlist',
            createdAtUnix: Date.now(),
            origin: 'user',
            tracksIds:
              params?.tracks &&
              params.tracks.every((tr: unknown) => typeof tr === 'string')
                ? params.tracks
                : [],
          }),
        );
      }

      navigation.navigate(Routes.LIBRARY_HOME);
    };

    const onClearTrackDownloads = () => {
      dispatch(clearTrackDownloads());
    };

    switch (routeName) {
      case Routes.LIBRARY_CREATE_PLAYLIST: {
        return (
          <IconButton style={styles.iconMR8} onPress={onCreatePlaylistPress}>
            <Icon
              name={'check' as MaterialDesignIconNameType}
              size={24}
              color={tintColor}
            />
          </IconButton>
        );
      }
      case Routes.LIBRARY_PLAYLIST_DETAILS: {
        return null;
      }
      case Routes.LIBRARY_TRACKS_DOWNLOADS: {
        return (
          <IconButton style={styles.iconMR8} onPress={onClearTrackDownloads}>
            <Icon
              name={'delete-forever' as MaterialDesignIconNameType}
              size={24}
              color={tintColor}
            />
          </IconButton>
        );
      }
      default: {
        return (
          <View style={styles.iconsRow}>
            <IconButton
              style={styles.iconMR8}
              onPress={() =>
                navigation.navigate(Routes.LIBRARY_TRACKS_DOWNLOADS)
              }
            >
              <Icon
                name={
                  (hasPendingDownloads
                    ? 'download-circle'
                    : 'download-circle-outline') as MaterialDesignIconNameType
                }
                size={24}
                color={hasPendingDownloads ? theme.colors.primary : tintColor}
              />
            </IconButton>
            <IconButton
              style={styles.iconMR8}
              onPress={() => navigation.navigate(Routes.SETTINGS)}
            >
              <Icon
                name={'cog' as MaterialDesignIconNameType}
                size={24}
                color={tintColor}
              />
            </IconButton>
          </View>
        );
      }
    }
  };

  const getCommonOptions = ({navigation, route}) =>
    ({
      title: getHeaderTitle(route),
      // headerTitle: getHeaderTitle(route),
      headerShown: getHeaderShown(route),
      headerLeft: ({tintColor}) =>
        getHeaderLeftIcon(route, navigation, tintColor),
      headerRight: ({tintColor}) =>
        getHeaderRightIcon(route, navigation, tintColor),
    } as StackHeaderOptions);

  return (
    <>
      <UpdaterBanner />
      <NoInternetBanner isOffline={isOffline} />
      <App.Navigator
        screenOptions={{
          headerTintColor: theme.colors.text,
          headerStyle: {
            height: headerRealHeight,
            maxHeight: headerRealHeight,
            backgroundColor: theme.colors.backgrounds[0],
            borderBottomColor: theme.colors.textMuted,
          },
        }}
      >
        {/* Header for these route is manager upper in the code */}
        <App.Screen
          name={Routes.HOME}
          component={MainStack}
          options={getCommonOptions}
        />
        <App.Screen
          name={Routes.IMPORT_SPOTIFY}
          component={SpotifyImportScreen}
          options={getCommonOptions}
        />
        <App.Screen
          name={Routes.LIBRARY_TRACKS_DOWNLOADS}
          component={TracksDownloadsScreen}
          options={getCommonOptions}
        />
        {/* App settings headers are manager by the view themselves or by the stack */}
        <App.Screen
          name={Routes.SETTINGS}
          component={SettingsStack}
          options={() => ({
            headerShown: false,
          })}
        />
      </App.Navigator>
    </>
  );
};

const styles = StyleSheet.create({
  logoWrapper: {
    width: 44,
    height: 44,
    marginLeft: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 40,
    height: 40,
  },
  iconML8: {
    marginLeft: 8,
  },
  iconMR8: {
    marginRight: 8,
  },
  iconsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
