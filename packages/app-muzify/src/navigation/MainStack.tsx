import React, {FC, useEffect, useMemo, useState} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {default as Icon} from 'react-native-vector-icons/MaterialCommunityIcons';

import {useTranslation} from '../i18n';

import {default as BottomTabBar} from '../core/components/BottomTabBar';

import {LibraryStack} from './LibraryStack';
import {HomeTab} from '../genius/screens/HomeTab';
import {SearchTab} from '../search/screens/SearchTab';
import {DeepLinkHandler} from '../deep-linking/components/DeepLinkHandler';
import {Routes} from '../constants';
import {Keyboard} from 'react-native';

const BottomTab = createBottomTabNavigator();

export type MainStackProps = {};

export const MainStack: FC<MainStackProps> = () => {
  const [searchFocused, setSearchFocused] = useState(false);
  const [shouldFocusSearchInput, focusSearchInput] = useState(false);
  const {t} = useTranslation('core');

  const onSearchTabPress = () => {
    if (searchFocused) {
      focusSearchInput(true);
    }
  };

  const onSearchTabFocus = () => {
    setSearchFocused(true);
  };

  const onSearchTabBlur = () => {
    setSearchFocused(false);
    focusSearchInput(false);
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => setSearchFocused(true));
    Keyboard.addListener('keyboardDidHide', () => setSearchFocused(false));

    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  });

  const SearchTabWrapper = useMemo(() => {
    return (props: any) => (
      <SearchTab {...props} focusSearch={shouldFocusSearchInput} />
    );
  }, [shouldFocusSearchInput]);

  return (
    <>
      <DeepLinkHandler />
      <BottomTab.Navigator
        tabBar={({navigation, ...props}) => (
          <BottomTabBar navigation={navigation} {...props} />
        )}
        initialRouteName={Routes.GENIUS}
      >
        <BottomTab.Screen
          options={{
            // unmountOnBlur: true,
            tabBarLabel: t('core:navigation.home'),
            tabBarIcon: ({color, size}) => (
              <Icon name="home" color={color} size={size} />
            ),
          }}
          name={Routes.GENIUS}
          component={HomeTab}
        />
        <BottomTab.Screen
          initialParams={{
            focusSearchInput: searchFocused,
          }}
          listeners={{
            tabPress: onSearchTabPress,
            focus: onSearchTabFocus,
            blur: onSearchTabBlur,
          }}
          options={() => ({
            // unmountOnBlur: true,
            tabBarLabel: t('core:navigation.search'),
            tabBarIcon: ({color, size}) => (
              <Icon
                name="magnify"
                color={color}
                size={size}
                testID="search-tab-icon"
              />
            ),
          })}
          name={Routes.SEARCH}
          component={SearchTabWrapper}
        />
        <BottomTab.Screen
          options={{
            // unmountOnBlur: true,
            tabBarLabel: t('core:navigation.library'),
            tabBarIcon: ({color, size}) => (
              <Icon name="bookshelf" color={color} size={size} />
            ),
          }}
          name={Routes.LIBRARY}
          component={LibraryStack}
        />
      </BottomTab.Navigator>
    </>
  );
};
