import React, {FC, useContext} from 'react';
import {Platform, StyleSheet} from 'react-native';
import {
  createStackNavigator,
  HeaderHeightContext,
} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {useTranslation} from '../i18n';

import {SettingsEntryScreen} from '../settings/screens/EntryScreen';
import {SettingsAppearanceScreen} from '../settings/screens/AppearanceScreen';
import {SettingsDownloadsScreen} from '../settings/screens/DownloadsScreen';
import {SettingsStatsScreen} from '../settings/screens/StatsScreen';
import {SettingsStreamingScreen} from '../settings/screens/StreamingScreen';
import {IconButtonBis as IconButton} from '../core/components/IconButtonBis';
import {useTheme} from '../core/hooks/useTheme';
import {DonateScreen} from '../settings/screens/DonateScreen';
import {Routes} from '../constants';

const Settings = createStackNavigator();

export type SettingsStackProps = {};

export const SettingsStack: FC<SettingsStackProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('settings');
  const headerHeight = useContext(HeaderHeightContext);
  const headerRealHeight = headerHeight || Platform.OS === 'ios' ? 96 : 64;

  return (
    <Settings.Navigator
      initialRouteName={Routes.SETTINGS_HOME}
      screenOptions={{
        headerTintColor: theme.colors.text,
        headerStyle: {
          height: headerRealHeight,
          maxHeight: headerRealHeight,
          backgroundColor: theme.colors.backgrounds[0],
        },
      }}
    >
      <Settings.Screen
        name={Routes.SETTINGS_HOME}
        component={SettingsEntryScreen}
        options={({navigation}) => ({
          title: t('core:navigation.settings.entry'),
          // headerTransparent: true,
          // headerBackground: HeaderBackground,
          headerLeft: ({tintColor}) => (
            <IconButton
              style={styles.iconML8}
              onPress={() => navigation.goBack()}
            >
              <Icon name={'arrow-left'} size={24} color={tintColor} />
            </IconButton>
          ),
        })}
      />
      <Settings.Screen
        name={Routes.SETTINGS_APPEARANCE}
        component={SettingsAppearanceScreen}
        options={({navigation}) => ({
          title: t('core:navigation.settings.appearance'),
          // headerTransparent: true,
          // headerBackground: HeaderBackground,
          headerLeft: ({tintColor}) => (
            <IconButton
              style={styles.iconML8}
              onPress={() => navigation.goBack()}
            >
              <Icon name={'arrow-left'} size={24} color={tintColor} />
            </IconButton>
          ),
        })}
      />
      <Settings.Screen
        name={Routes.SETTINGS_DOWNLOADS}
        component={SettingsDownloadsScreen}
        options={({navigation}) => ({
          title: t('core:navigation.settings.downloads'),
          // headerTransparent: true,
          // headerBackground: HeaderBackground,
          headerLeft: ({tintColor}) => (
            <IconButton
              style={styles.iconML8}
              onPress={() => navigation.goBack()}
            >
              <Icon name={'arrow-left'} size={24} color={tintColor} />
            </IconButton>
          ),
        })}
      />
      <Settings.Screen
        name={Routes.SETTINGS_STATS}
        component={SettingsStatsScreen}
        options={({navigation}) => ({
          title: t('core:navigation.settings.stats'),
          // headerTransparent: true,
          // headerBackground: HeaderBackground,
          headerLeft: ({tintColor}) => (
            <IconButton
              style={styles.iconML8}
              onPress={() => navigation.goBack()}
            >
              <Icon name={'arrow-left'} size={24} color={tintColor} />
            </IconButton>
          ),
        })}
      />
      <Settings.Screen
        name={Routes.SETTINGS_STREAMING}
        component={SettingsStreamingScreen}
        options={({navigation}) => ({
          title: t('core:navigation.settings.streaming'),
          // headerTransparent: true,
          // headerBackground: HeaderBackground,
          headerLeft: ({tintColor}) => (
            <IconButton
              style={styles.iconML8}
              onPress={() => navigation.goBack()}
            >
              <Icon name={'arrow-left'} size={24} color={tintColor} />
            </IconButton>
          ),
        })}
      />
      <Settings.Screen
        name={Routes.DONATE}
        component={DonateScreen}
        options={({navigation}) => ({
          title: t('settings:modules.donate.meta.title'),
          // headerTransparent: true,
          // headerBackground: HeaderBackground,
          headerLeft: ({tintColor}) => (
            <IconButton
              style={styles.iconML8}
              onPress={() => navigation.goBack()}
            >
              <Icon name={'arrow-left'} size={24} color={tintColor} />
            </IconButton>
          ),
        })}
      />
    </Settings.Navigator>
  );
};

const styles = StyleSheet.create({
  iconML8: {
    marginLeft: 8,
  },
  iconMR8: {
    marginRight: 8,
  },
});
