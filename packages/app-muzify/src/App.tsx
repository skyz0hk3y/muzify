import React, {useEffect, useLayoutEffect, useMemo, useState} from 'react';
import {BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import {NavigationContainer} from '@react-navigation/native';
import {Provider, useDispatch, useSelector} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {StatusBar, View, LogBox, StyleSheet} from 'react-native';
import {Track} from 'react-native-track-player';
import {useNetInfo} from '@react-native-community/netinfo';
import SplashScreen from 'react-native-splash-screen';

import {API_BASE_URL} from './constants';
import {i18n, I18nextProvider} from './i18n';
import {persistor, store} from './store';

import {getThemeNameSelector} from './core/selector';
import {
  checkForAppUpdates,
  fetchInitialNetworkStatus,
  fetchInstalledVersionInfos,
  setNetworkState,
} from './core/actions';
import {useTheme} from './core/hooks/useTheme';
import {reattachDownloads} from './library/actions';
import {ThemeProvider} from './core/providers/ThemeProvider';

import {AppStack} from './navigation/AppStack';
import {TrackMoreContext} from './audio-player/TrackMoreContext';

LogBox.ignoreLogs([
  'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
  "Accessing the 'state' property of the 'route' object is not supported.",
]);

const AppInit = ({persistor: _persistor}) => {
  const [initialStateReady, setInitialStateReady] = useState(false);

  const {isConnected, isInternetReachable, type} = useNetInfo();
  const dispatch = useDispatch();

  useEffect(() => {
    const performInitSequence = async () => {
      if (persistor.getState().bootstrapped && !initialStateReady) {
        dispatch(
          setNetworkState({
            connectionType: type,
            isConnected: type !== 'none' && type !== 'unknown',
            isInternetReachable: isInternetReachable || false,
          }),
        );

        // Network state
        dispatch(fetchInitialNetworkStatus());
        dispatch(checkForAppUpdates());

        // Updates state
        dispatch(fetchInstalledVersionInfos());

        // Reattach background downloads
        dispatch(reattachDownloads());

        SplashScreen.hide();
        setInitialStateReady(true);
      }
    };

    if (!initialStateReady) {
      performInitSequence();
      const splashScreenTimeout = setTimeout(() => {
        SplashScreen.hide();
      }, 3000); // Always hide after 3s max.

      return () => clearTimeout(splashScreenTimeout);
    } else {
      console.log('[APP] App is ready!');
      console.log(`[APP] API base URL is set to: ${API_BASE_URL}`);

      return () => undefined;
    }
  }, [dispatch, initialStateReady, isInternetReachable, type]);

  return null;
};

export const App = () => {
  const {theme} = useTheme();
  const currentThemeName = useSelector(getThemeNameSelector);

  // eslint-disable-next-line no-spaced-func
  const [openTrackMoreForTrack, setOpenTrackMoreForTrack] = useState<
    (track: Track) => void
  >(() => {});
  const [trackMoreTrack, setTrackMoreTrack] = useState<Track | null>(null);

  // TODO(refactor): Move this in a Provider (see ModalProvider for example)
  const trackMoreCtxValue = useMemo(
    () => ({
      openTrackMoreForTrack: (track?: Track) => {
        if (!track) {
          setTrackMoreTrack(null);
          return;
        }
        setTrackMoreTrack(track);
        if (openTrackMoreForTrack) {
          openTrackMoreForTrack(track);
        }
      },
      setOpenTrackMoreForTrack,
      track: trackMoreTrack,
    }),
    [openTrackMoreForTrack, trackMoreTrack],
  );

  return (
    <Provider store={store}>
      <ThemeProvider>
        <PersistGate persistor={persistor} loading={null}>
          <AppInit persistor={persistor} />
          <View
            style={[
              styles.appWrapper,
              {backgroundColor: theme.colors.backgrounds[0]},
            ]}
          >
            <StatusBar
              key={currentThemeName}
              barStyle={
                currentThemeName === 'dark' ? 'light-content' : 'dark-content'
              }
              backgroundColor={
                currentThemeName === 'dark'
                  ? theme.colors.backgrounds[0]
                  : 'white'
              }
            />
            <I18nextProvider i18n={i18n}>
              <>
                <NavigationContainer
                  theme={{
                    dark: false,
                    colors: {
                      primary: theme.colors.primary,
                      background: theme.colors.backgrounds[0],
                      notification: theme.colors.backgrounds[0],
                      border: theme.colors.textMuted,
                      card: theme.colors.backgrounds[0],
                      text: theme.colors.text,
                    },
                  }}
                >
                  <BottomSheetModalProvider>
                    <TrackMoreContext.Provider value={trackMoreCtxValue}>
                      <AppStack />
                    </TrackMoreContext.Provider>
                  </BottomSheetModalProvider>
                </NavigationContainer>
              </>
            </I18nextProvider>
          </View>
        </PersistGate>
      </ThemeProvider>
    </Provider>
  );
};

const styles = StyleSheet.create({
  appWrapper: {
    flex: 1,
  },
});
