import {AppTheme} from '../types';

export const LightTheme: AppTheme = {
  name: 'light',
  colors: {
    primary: '#00adcc',
    danger: '#ff0000',
    info: '#2196F3',
    success: '#4CAF50',
    warning: 'orange',
    text: '#000000',
    textMuted: 'rgba(0, 0, 0, .5)',
    backgrounds: {
      0: '#ffffff',
      1: '#fafafa',
      2: '#f7f7f7',
      3: '#d9d9d9',
      4: '#e5e5e5',
    },
    selected: 'rgba(0, 0, 0, .2)',
  },
};
