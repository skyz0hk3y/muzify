import {AppTheme} from '../types';

export const DarkTheme: AppTheme = {
  name: 'dark',
  colors: {
    primary: '#00adcc',
    danger: '#ff0000',
    info: '#2196F3',
    success: '#4CAF50',
    warning: 'orange',
    text: '#ffffff',
    textMuted: '#999999',
    backgrounds: {
      0: '#121212',
      1: '#171717',
      2: '#222222',
      3: '#333333',
      4: '#666666',
    },
    selected: 'rgba(255, 255, 255, 0.2)',
  },
};
