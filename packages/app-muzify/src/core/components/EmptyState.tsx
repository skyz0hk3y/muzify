import React, {FC} from 'react';
import {Image, View, Text, ImageSourcePropType, StyleSheet} from 'react-native';
import {useTheme} from '../hooks/useTheme';

export type EmptyStateProps = {
  testID?: string;
  source: ImageSourcePropType;
  title: string;
  subtitle?: string;
  button?: React.ReactElement;
};

export const EmptyState: FC<EmptyStateProps> = ({
  testID,
  source,
  title,
  subtitle,
  button,
}) => {
  const {theme} = useTheme();
  return (
    <View style={styles.container} testID={testID}>
      <Image source={source} style={styles.image} resizeMode={'contain'} />
      <Text style={[styles.title, {color: theme.colors.text}]}>{title}</Text>
      {subtitle && (
        <Text style={[styles.subtitle, {color: theme.colors.textMuted}]}>
          {subtitle}
        </Text>
      )}
      {button}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 16,
  },
  image: {
    height: 200,
    width: '100%',
    marginBottom: 24,
  },
  title: {
    fontSize: 24,
    fontWeight: '900',
    textAlign: 'center',
    color: '#ffffff',
    marginBottom: 16,
  },
  subtitle: {
    fontSize: 16,
    textAlign: 'center',
    color: '#A7A7A7',
  },
  button: {
    marginTop: 24,
  },
});
