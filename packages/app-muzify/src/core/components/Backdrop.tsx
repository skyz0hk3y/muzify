import React, {useCallback, useMemo, useRef} from 'react';
import {Dimensions, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import Animated, {
  and,
  call,
  cond,
  eq,
  Extrapolate,
  neq,
  not,
  set,
} from 'react-native-reanimated';

import {useValue} from 'react-native-redash';
import {BottomSheetDefaultBackdropProps} from '@gorhom/bottom-sheet/lib/typescript/components/bottomSheetBackdrop/types';
import {useBottomSheet} from '@gorhom/bottom-sheet';

const {width: WINDOW_WIDTH, height: WINDOW_HEIGHT} = Dimensions.get('window');

const {
  interpolate: interpolateV1,
  interpolateNode: interpolateV2,
} = require('react-native-reanimated');

const interpolate = interpolateV2 || interpolateV1;

const AnimatedTouchableWithoutFeedback = Animated.createAnimatedComponent(
  TouchableWithoutFeedback,
);

export const Backdrop = ({
  animatedIndex,
  animatedPosition,
  opacity = 0.6,
  appearsOnIndex = 1,
  disappearsOnIndex = 1,
  enableTouchThrough = false,
  closeOnPress = true,
  style,
}: BottomSheetDefaultBackdropProps) => {
  //#region hooks
  const {close} = useBottomSheet();
  //#endregion

  //#region variables
  const containerRef = useRef<Animated.View>(null);
  const pointerEvents = useMemo(() => (enableTouchThrough ? 'none' : 'auto'), [
    enableTouchThrough,
  ]);
  //#endregion

  //#region animation variables
  const isTouchable = useValue(closeOnPress !== undefined ? 1 : 0);
  const animatedOpacity = useMemo(
    () =>
      interpolate(animatedIndex, {
        inputRange: [disappearsOnIndex, appearsOnIndex],
        outputRange: [0, opacity],
        extrapolate: Extrapolate.CLAMP,
      }),
    [animatedIndex, opacity, appearsOnIndex, disappearsOnIndex],
  );
  //#endregion

  //#region callbacks
  const handleOnPress = useCallback(() => {
    close();
  }, [close]);
  //#endregion

  //#region styles
  const buttonStyle = useMemo(
    () => [
      style,
      {
        top: cond(eq(animatedIndex, disappearsOnIndex), WINDOW_HEIGHT, 0),
      },
    ],
    [disappearsOnIndex, style, animatedIndex],
  );
  const containerStyle = useMemo(
    () => [
      styles.container,
      style,
      {
        opacity: animatedOpacity,
      },
    ],
    [style, animatedOpacity],
  );
  //#endregion
  return closeOnPress ? (
    <>
      <AnimatedTouchableWithoutFeedback
        accessible={true}
        accessibilityRole="button"
        accessibilityLabel="Bottom Sheet backdrop"
        accessibilityHint="Tap to close the Bottom Sheet"
        onPress={handleOnPress}
        style={buttonStyle}
      >
        <Animated.View ref={containerRef} style={containerStyle} />
      </AnimatedTouchableWithoutFeedback>
      <Animated.Code>
        {() =>
          cond(
            and(eq(animatedPosition, disappearsOnIndex), isTouchable),
            [
              set(isTouchable, 0),
              call([], () => {
                // @ts-ignore
                containerRef.current.setNativeProps({
                  pointerEvents: 'none',
                });
              }),
            ],
            cond(
              and(neq(animatedPosition, disappearsOnIndex), not(isTouchable)),
              [
                set(isTouchable, 1),
                call([], () => {
                  // @ts-ignore
                  containerRef.current.setNativeProps({
                    pointerEvents: 'auto',
                  });
                }),
              ],
            ),
          )
        }
      </Animated.Code>
    </>
  ) : (
    <Animated.View pointerEvents={pointerEvents} style={containerStyle} />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
  },
});
