import React, {FC} from 'react';
import {View, Text, StyleSheet, GestureResponderEvent} from 'react-native';

import {TouchableNativeFeedback} from 'react-native-gesture-handler';
import {useTheme} from '../hooks/useTheme';

export type IconButtonBisProps = {
  size?: 'small' | 'normal' | 'large';
  backgroundColor?: string;
  color?: string;
  disabled?: boolean;
  style?: Record<string, any>;
  onPress?: (event: GestureResponderEvent) => void;
};

export const IconButtonBis: FC<IconButtonBisProps> = ({
  children,
  size = 'normal',
  backgroundColor = 'transparent',
  color = '#ffffff',
  disabled = false,
  style = {},
  onPress = () => null,
}) => {
  const {theme} = useTheme();

  const sizesMap = {
    small: 32,
    normal: 44,
    large: 64,
  };

  const sizeStyle = {
    borderRadius: sizesMap[size],
    minHeight: sizesMap[size],
    height: sizesMap[size],
    maxHeight: sizesMap[size],
    minWidth: sizesMap[size],
    width: sizesMap[size],
    maxWidth: sizesMap[size],
  };

  const bgColorStyle = {
    backgroundColor,
  };

  const colorStyle = {
    color,
  };

  return (
    <View
      style={{
        ...style,
        ...styles.circleWrapper,
        ...sizeStyle,
      }}
    >
      <TouchableNativeFeedback
        onPress={onPress}
        disabled={disabled}
        background={TouchableNativeFeedback.Ripple(
          theme.colors.backgrounds[3],
          false,
        )}
      >
        <View
          style={{
            ...styles.iconWrapper,
            ...bgColorStyle,
            ...sizeStyle,
          }}
        >
          <Text style={colorStyle}>{children}</Text>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  circleWrapper: {
    overflow: 'hidden',
  },
  iconWrapper: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
