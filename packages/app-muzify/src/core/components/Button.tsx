import React, {FC} from 'react';
import {
  View,
  Text,
  StyleSheet,
  GestureResponderEvent,
  StyleProp,
  ViewStyle,
  TextStyle,
} from 'react-native';

import {TouchablePlatform} from './TouchablePlatform';

export type ButtonProps = {
  allCaps?: boolean;
  text?: string;
  size?: 'small' | 'normal' | 'large';
  backgroundColor?: string;
  color?: string;
  disabled?: boolean;
  fluid?: boolean;
  style?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  onPress?: (event: GestureResponderEvent) => void;
};

export const Button: FC<ButtonProps> = ({
  allCaps = true,
  children,
  text,
  size = 'normal',
  backgroundColor = 'transparent',
  color = '#ffffff',
  disabled = false,
  fluid = false,
  style,
  containerStyle,
  onPress = () => null,
}) => {
  const sizesMap = {
    small: 32,
    normal: 44,
    large: 64,
  };

  const sizeStyle: StyleProp<ViewStyle> = {
    borderRadius: sizesMap[size],
    minHeight: sizesMap[size],
    height: sizesMap[size],
    maxHeight: sizesMap[size],
    minWidth: 150,
  };

  const bgColorStyle: StyleProp<ViewStyle> = {
    backgroundColor: !disabled ? backgroundColor : '#333333',
  };

  const textStyle: StyleProp<TextStyle> = {
    color: !disabled ? color : '#999999',
    fontSize: sizesMap[size] * 0.3,
    textTransform: allCaps ? 'uppercase' : 'none',
  };

  const layoutStyle: StyleProp<ViewStyle> = {
    flex: fluid ? 1 : 0,
    marginHorizontal: fluid ? 4 : 0,
  };

  return (
    <View
      style={[
        {
          ...styles.circleWrapper,
          ...sizeStyle,
          ...layoutStyle,
        },
        containerStyle,
      ]}
    >
      <TouchablePlatform
        onPress={onPress}
        disabled={disabled}
        style={{backgroundColor}}
      >
        <View
          style={[
            {
              ...styles.wrapper,
              ...bgColorStyle,
              ...sizeStyle,
            },
            style,
          ]}
        >
          {text && <Text style={[styles.text, textStyle]}>{text}</Text>}
          {!text && <>{children}</>}
        </View>
      </TouchablePlatform>
    </View>
  );
};

const styles = StyleSheet.create({
  circleWrapper: {
    overflow: 'hidden',
    elevation: 3,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
});
