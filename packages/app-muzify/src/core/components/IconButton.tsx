import React, {FC} from 'react';
import {
  View,
  Text,
  StyleSheet,
  GestureResponderEvent,
  TouchableNativeFeedback,
} from 'react-native';
import {useTheme} from '../hooks/useTheme';

import {TouchablePlatform} from './TouchablePlatform';

export type IconButtonProps = {
  size?: 'small' | 'normal' | 'large';
  backgroundColor?: string;
  color?: string;
  disabled?: boolean;
  style?: Record<string, any>;
  testID?: string;
  onPress?: (event: GestureResponderEvent) => void;
};

export const IconButton: FC<IconButtonProps> = ({
  children,
  size = 'normal',
  backgroundColor = 'transparent',
  color = '#ffffff',
  disabled = false,
  style = {},
  testID,
  onPress = () => null,
}) => {
  const {theme} = useTheme();

  const sizesMap = {
    small: 32,
    normal: 44,
    large: 64,
  };

  const sizeStyle = {
    borderRadius: sizesMap[size],
    minHeight: sizesMap[size],
    height: sizesMap[size],
    maxHeight: sizesMap[size],
    minWidth: sizesMap[size],
    width: sizesMap[size],
    maxWidth: sizesMap[size],
  };

  const bgColorStyle = {
    backgroundColor,
  };

  const colorStyle = {
    color,
  };

  return (
    <View
      style={{
        ...style,
        ...styles.circleWrapper,
        ...sizeStyle,
      }}
    >
      <TouchablePlatform
        testID={testID}
        onPress={onPress}
        disabled={disabled}
        useForeground
        background={TouchableNativeFeedback.Ripple(
          theme.colors.backgrounds[3],
          false,
        )}
      >
        <View
          style={{
            ...styles.iconWrapper,
            ...bgColorStyle,
            ...sizeStyle,
          }}
        >
          <Text style={colorStyle}>{children}</Text>
        </View>
      </TouchablePlatform>
    </View>
  );
};

const styles = StyleSheet.create({
  circleWrapper: {
    overflow: 'hidden',
  },
  iconWrapper: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
