import React, {FC} from 'react';
import {View, StyleSheet} from 'react-native';
import {useTheme} from '../hooks/useTheme';

export const Divider: FC = () => {
  const {theme} = useTheme();
  return (
    <View
      style={[styles.divider, {backgroundColor: theme.colors.backgrounds[3]}]}
    />
  );
};

const styles = StyleSheet.create({
  divider: {
    width: '100%',
    height: 1,
    marginVertical: 8,
    marginHorizontal: 16,
  },
});
