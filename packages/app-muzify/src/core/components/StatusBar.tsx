import React from 'react';
import {
  Platform,
  View,
  StatusBar as OriginalStatusBar,
  StyleSheet,
} from 'react-native';
import {useTheme} from '../hooks/useTheme';

export const StatusBar = () => {
  const {theme} = useTheme();
  return (
    <>
      <OriginalStatusBar
        animated
        translucent={Platform.OS === 'ios'}
        barStyle="light-content"
        backgroundColor={theme.colors.backgrounds[1]}
      />
      {Platform.OS === 'ios' && (
        <View
          style={[
            styles.statusBarBackground,
            {backgroundColor: theme.colors.backgrounds[1]},
          ]}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  statusBarBackground: {
    width: '100%',
    height: 100, // For all devices, even X, XS Max
    position: 'absolute',
    top: 0,
    left: 0,
  },
});
