import React, {FC} from 'react';
import {StyleSheet, ViewStyle} from 'react-native';
import Animated from 'react-native-reanimated';
import {useTheme} from '../hooks/useTheme';

export type ScreenTopActionProps = {
  backgroundColor?: string;
  noMargins?: boolean;
  style?: ViewStyle;
};

export const ScreenTopAction: FC<ScreenTopActionProps> = ({
  backgroundColor,
  children,
  noMargins,
  style,
}) => {
  const {theme} = useTheme();
  const containerStyleOverrides = {
    backgroundColor: backgroundColor || theme.colors.backgrounds[4],
    ...(noMargins && {paddingVertical: 0, paddingHorizontal: 0}),
  };

  return (
    <Animated.View style={[styles.container, style, containerStyleOverrides]}>
      {children}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 8,
    paddingHorizontal: 8,
    elevation: 4,
    zIndex: 100,
  },
});
