import {useNavigation} from '@react-navigation/native';
import React, {FC} from 'react';
import {StyleSheet, View, Text} from 'react-native';
// import {CastButton} from 'react-native-google-cast';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Routes} from '../../constants';
import {IconButton} from './IconButton';

export type HeaderBarProps = {
  title?: string;
  showLogoIcon?: boolean;
};

export const HeaderBar: FC<HeaderBarProps> = ({
  title = 'Muzify',
  showLogoIcon = true,
}) => {
  const navigation = useNavigation();

  const onSettingsPress = () => {
    navigation.navigate(Routes.SETTINGS);
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        {showLogoIcon && (
          <Icon name={'music-circle-outline'} size={32} color={'#00adcc'} />
        )}
        <Text style={styles.logo}>{title}</Text>
      </View>
      {/* <CastButton /> */}
      <IconButton size={'normal'} onPress={onSettingsPress}>
        <Icon name={'cog'} size={24} color={'#ffffff'} />
      </IconButton>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',

    height: 64,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  logoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    color: '#00adcc',
    flex: 1,
  },
  logo: {
    marginLeft: 8,
    fontSize: 24,
    fontWeight: '700',
    color: '#ffffff',
  },
  actionButton: {
    width: 44,
    height: 44,
    borderRadius: 44,

    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 24,
    fontWeight: '600',
  },
});
