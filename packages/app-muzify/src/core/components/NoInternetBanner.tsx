import React, {FC} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Trans, useTranslation} from '../../i18n';
import {useTheme} from '../hooks/useTheme';

export type NoInternetBannerProps = {
  isOffline: boolean;
};

export const NoInternetBanner: FC<NoInternetBannerProps> = ({isOffline}) => {
  const {theme} = useTheme();
  const {i18n} = useTranslation('core');

  if (!isOffline) {
    return null;
  }

  return (
    <View
      style={[styles.container, {backgroundColor: theme.colors.backgrounds[0]}]}
    >
      <Text style={[styles.text, {color: theme.colors.danger}]}>
        <Trans i18nKey={'core:network.you-are-offline'} i18n={i18n}>
          <Text style={[styles.textBold, {color: theme.colors.danger}]}>
            You are currently offline.
          </Text>{' '}
          Only local tracks are available.
        </Trans>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 32,
    paddingVertical: 8,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'red',
    textAlign: 'center',
  },
  textBold: {
    fontWeight: 'bold',
    color: 'red',
  },
});
