import React, {FC} from 'react';
import {
  Platform,
  TouchableNativeFeedback,
  TouchableNativeFeedbackProps,
  TouchableHighlight,
  TouchableHighlightProps,
  StyleSheet,
} from 'react-native';
import {
  TouchableNativeFeedback as TouchableNativeFeedbackGestureHandler,
  TouchableHighlight as TouchableHighlightGestureHandler,
} from 'react-native-gesture-handler';

import {useTheme} from '../hooks/useTheme';

export type TouchablePlatformProps = {withNativeResponder?: boolean} & (
  | TouchableHighlightProps
  | TouchableNativeFeedbackProps
);

export const TouchablePlatform: FC<TouchablePlatformProps> = ({
  children,
  withNativeResponder = false,
  style = {},
  ...props
}) => {
  const {theme} = useTheme();

  const commonTouchableHighlightProps = {
    activeOpacity: 0.6,
    underlayColor: theme.colors.backgrounds[3],
  };

  const commonTouchableNativeFeedpackProps = {
    useForeground: false,
    background: TouchableNativeFeedback.Ripple(
      theme.colors.backgrounds[3],
      false,
    ),
  };

  if (withNativeResponder) {
    if (Platform.OS === 'ios') {
      return (
        <TouchableHighlightGestureHandler
          // Weirdly enough, this doesn't respects React Native counterpart types
          {...(props as unknown)}
          {...commonTouchableHighlightProps}
          style={[styles.flex, style]}
        >
          {children}
        </TouchableHighlightGestureHandler>
      );
    }

    return (
      <TouchableNativeFeedbackGestureHandler
        {...props}
        {...commonTouchableNativeFeedpackProps}
        style={[styles.container, style]}
      >
        {children}
      </TouchableNativeFeedbackGestureHandler>
    );
  }

  if (Platform.OS === 'ios') {
    return (
      <TouchableHighlight
        {...props}
        {...commonTouchableHighlightProps}
        style={[styles.flex, style]}
      >
        {children}
      </TouchableHighlight>
    );
  }

  return (
    <TouchableNativeFeedback
      {...props}
      {...commonTouchableNativeFeedpackProps}
      style={[styles.flex, style]}
    >
      {children}
    </TouchableNativeFeedback>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    maxWidth: '100%',
    minWidth: '100%',
    width: '100%',
  },
});
