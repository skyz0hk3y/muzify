import React, {FC, PropsWithChildren} from 'react';
import {
  useWindowDimensions,
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  View,
  ViewProps,
} from 'react-native';

import {useTheme} from '../hooks/useTheme';
import {Caption} from './Caption';

export const MODAL_WIDTH_MARGIN_PX = 16;

export type ModalProps = PropsWithChildren<{
  style?: StyleProp<ViewProps>;
  title: string;
  visible?: boolean;
  onCloseRequest?: (e: GestureResponderEvent) => void;
}>;

export type ModalOverlayProps = {
  color?: string;
  opacity?: number;
  style?: StyleProp<ViewProps>;
  visible?: boolean;
  onCloseRequest?: (e: GestureResponderEvent) => void;
};

export const Modal: FC<ModalProps> = ({
  children,
  style: styleOverrides = {},
  title,
  visible = false,
}) => {
  const {theme} = useTheme();
  const {width: deviceWidth} = useWindowDimensions();

  if (!visible) {
    return null;
  }

  return (
    <View
      style={[
        {backgroundColor: theme.colors.backgrounds[3]},
        styles.modal,
        styleOverrides,
        {width: Math.min(450, deviceWidth) - MODAL_WIDTH_MARGIN_PX * 2},
      ]}
    >
      <Caption numberOfLines={2}>{title}</Caption>
      {children}
    </View>
  );
};

export const ModalOverlay: FC<ModalOverlayProps> = ({
  color = 'rgba(0, 0, 0, 1)',
  style: styleOverrides = {},
  visible = false,
  onCloseRequest = () => undefined,
}) => {
  const {width, height} = useWindowDimensions();

  if (!visible) {
    return null;
  }

  return (
    <View
      onStartShouldSetResponderCapture={() => true}
      onResponderRelease={onCloseRequest}
      style={[
        {backgroundColor: color},
        styles.overlay,
        styleOverrides,
        {width, height},
      ]}
    />
  );
};

const styles = StyleSheet.create({
  modal: {
    position: 'relative',
    marginVertical: 8,
    marginHorizontal: MODAL_WIDTH_MARGIN_PX,
    paddingHorizontal: MODAL_WIDTH_MARGIN_PX,
    paddingTop: 8,
    paddingBottom: 16,
    borderRadius: 8,
    elevation: 6,
    zIndex: 9000,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 8000,
  },
});
