import React, {FC, ReactNode, useState} from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TouchableNativeFeedback,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {TouchablePlatform} from './TouchablePlatform';
import {IconButton} from './IconButton';
import {Switch} from 'react-native-gesture-handler';
import {useTheme} from '../hooks/useTheme';
import {MaterialDesignIconNameType} from '../types';

export type ListItemProps = {
  iconColor?: string;
  iconName?: MaterialDesignIconNameType;
  textColor?: string;
  textMutedColor?: string;
  title: string;
  testID?: string;
  subtitle?: string;
  actionColor?: string;
  actionIconName?: MaterialDesignIconNameType;
  actionComponent?: ReactNode;
  actionIsSwitch?: boolean;
  actionSwitchChecked?: boolean;
  disabled?: boolean;
  withNativeResponder?: boolean;
  style?: StyleProp<ViewStyle>;
  onItemPress?: (event: GestureResponderEvent) => void;
  onItemLongPress?: (event: GestureResponderEvent) => void;
  onActionPress?: (event: GestureResponderEvent) => void;
  onActionSwitchChanged?: ((checked: boolean) => void | undefined) | undefined;
};

export const ListItem: FC<ListItemProps> = ({
  iconColor,
  iconName,
  textColor,
  textMutedColor,
  title,
  testID = 'list-item',
  subtitle,
  actionIconName,
  actionComponent,
  actionColor,
  actionIsSwitch,
  actionSwitchChecked,
  disabled = false,
  style,
  withNativeResponder = false,
  onItemPress,
  onItemLongPress,
  onActionPress,
  onActionSwitchChanged,
}) => {
  const {theme} = useTheme();
  const [containerWidth, setContainerWidth] = useState(0);

  return (
    <View style={style} testID={testID}>
      <View
        style={styles.wrapper}
        onLayout={(event) => {
          const {width} = event.nativeEvent.layout;
          setContainerWidth(width);
        }}
      >
        <TouchablePlatform
          testID={`${testID}-touchable`}
          disabled={disabled}
          onPress={onItemPress}
          onLongPress={onItemLongPress}
          withNativeResponder={withNativeResponder}
          style={[styles.container, {width: containerWidth}]}
          background={TouchableNativeFeedback.Ripple(
            theme.colors.backgrounds[3],
            false,
          )}
        >
          <View style={styles.itemWrapper}>
            {iconName && (
              <IconButton
                disabled
                size={'normal'}
                style={styles.iconButton}
                testID={`${testID}-icon`}
              >
                <Icon
                  name={iconName}
                  color={iconColor || theme.colors.text}
                  size={24}
                />
              </IconButton>
            )}
            <View style={styles.textsContainer}>
              <Text
                style={[styles.title, {color: textColor || theme.colors.text}]}
                testID={`${testID}-title`}
                numberOfLines={1}
              >
                {title}
              </Text>
              {subtitle && (
                <Text
                  style={[
                    styles.subtitle,
                    {color: textMutedColor || theme.colors.textMuted},
                  ]}
                  testID={`${testID}-subtitle`}
                  numberOfLines={2}
                >
                  {subtitle}
                </Text>
              )}
            </View>
            {actionIsSwitch && (
              <View style={styles.actions}>
                <Switch
                  onValueChange={onActionSwitchChanged}
                  value={actionSwitchChecked}
                  thumbColor={theme.colors.primary}
                  trackColor={{
                    false: theme.colors.backgrounds[3],
                    true: '#076575', // TODO(refactor): Move this to the theme
                  }}
                  style={styles.MR8}
                  testID={`${testID}-action-switch`}
                />
              </View>
            )}
            {actionComponent ? (
              <View
                style={styles.actions}
                testID={`${testID}-action-container`}
              >
                {actionComponent}
              </View>
            ) : (
              !actionIsSwitch &&
              actionIconName && (
                <View style={styles.actions}>
                  <IconButton
                    size={'normal'}
                    color={actionColor || theme.colors.text}
                    style={styles.actionButton}
                    testID={`${testID}-action-button`}
                    onPress={onActionPress}
                    disabled={!onActionPress}
                  >
                    <Icon
                      size={24}
                      name={actionIconName}
                      color={actionColor || theme.colors.text}
                    />
                  </IconButton>
                </View>
              )
            )}
          </View>
        </TouchablePlatform>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 56,
    maxHeight: 64,
  },
  container: {
    flex: 1,
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 8,
  },
  iconButton: {
    // marginHorizontal: 8,
  },
  actions: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionButton: {
    // marginHorizontal: 8,
  },
  textsContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    marginHorizontal: 8,
    paddingLeft: 8,
  },
  title: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
  },
  subtitle: {
    marginTop: 4,
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '600',
  },
  MR8: {
    marginRight: 8,
  },
});
