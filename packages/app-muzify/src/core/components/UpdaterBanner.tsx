import React, {VFC} from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {useTranslation} from '../../i18n';
import {
  getIsUpdateAvailableSelector,
  getNewVersionNameSelector,
  getUpdateDownloadProgressSelector,
} from '../selector';
import {checkForAppUpdates} from '../actions';

import {Button} from './Button';

export type UpdaterBannerProps = {};

export const UpdaterBanner: VFC<UpdaterBannerProps> = () => {
  const {t} = useTranslation('settings');
  const dispatch = useDispatch();
  const newVersionName = useSelector(getNewVersionNameSelector);
  const isUpdatesAvailable = useSelector(getIsUpdateAvailableSelector);
  const updateDownloadProgress = useSelector(getUpdateDownloadProgressSelector);

  const updateAvailableMessage =
    'settings:modules.updates.check-for-updates.title.updates-available';

  const onUpdateNowPress = () => {
    dispatch(
      checkForAppUpdates(true, {
        meta: {
          cancel: t('core:captions.cancel'),
          updateNow: t('settings:captions.update-now'),
        },
        noUpdates: {
          title: '',
          subtitle: '',
        },
        success: {
          title: t(updateAvailableMessage),
          subtitle: t(
            'settings:modules.updates.check-for-updates.do-u-want-to-update',
          ),
          releaseNotesTitle: t('settings:captions.release-notes'),
        },
      }),
    );
  };

  if (!newVersionName || !isUpdatesAvailable) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.copy}>
        {`${t(updateAvailableMessage)} (v${newVersionName})`}
      </Text>
      {updateDownloadProgress ? (
        <ActivityIndicator
          style={styles.updateButton}
          size={24}
          color={'#ffffff'}
        />
      ) : (
        <Button
          style={styles.updateButton}
          text={t('settings:captions.update-now')}
          onPress={onUpdateNowPress}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

    minHeight: 48,
    height: 48,
    paddingLeft: 16,
    paddingRight: 4,
    paddingVertical: 4,

    backgroundColor: '#2196F3',
  },
  copy: {
    flex: 1,
    marginRight: 16,

    color: '#ffffff',
    fontSize: 14,
    fontWeight: 'bold',
  },
  updateButton: {
    minWidth: 88,
    height: 40,
    minHeight: 40,
    margin: 0,

    borderRadius: 6,
    backgroundColor: '#52aaf1',
  },
});
