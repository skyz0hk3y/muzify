import React, {FC} from 'react';
import {StyleSheet, Text, TextInput as RNTextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {MaterialDesignIconNameType} from '../../../../../packages/app-muzify/src/core/types';

import {IconButton} from '../../core/components/IconButton';
import {useTheme} from '../../core/hooks/useTheme';

export type TextInputProps = {
  autoFocus?: boolean;
  value?: string;
  title?: string;
  errorText?: string;
  hintText?: string;
  placeholder?: string;
  iconName?: MaterialDesignIconNameType;
  onSearchSubmit?: () => void;
  onValueChange?: (value: string) => void;
  onClearTextPress?: () => void;
};

export const TextInput: FC<TextInputProps> = ({
  autoFocus = false,
  value = '',
  title,
  errorText,
  hintText,
  placeholder = '',
  iconName,
  onSearchSubmit = () => {},
  onValueChange = () => {},
  onClearTextPress = () => {},
}) => {
  const {theme} = useTheme();

  const primaryColor = errorText ? theme.colors.danger : theme.colors.text;
  const secondaryColor = errorText
    ? theme.colors.danger
    : theme.colors.textMuted;

  return (
    <View style={styles.wrapper}>
      {title && (
        <Text style={[styles.inputTitle, {color: primaryColor}]}>{title}</Text>
      )}
      <View style={[styles.container, {borderBottomColor: primaryColor}]}>
        {iconName && (
          <IconButton size={'normal'} disabled style={styles.searchIcon}>
            <Icon name={iconName} size={24} color={primaryColor} />
          </IconButton>
        )}
        <RNTextInput
          style={[styles.input, {color: theme.colors.text}]}
          placeholder={placeholder}
          placeholderTextColor={secondaryColor}
          value={value}
          onEndEditing={onSearchSubmit}
          onChangeText={onValueChange}
          // clearButtonMode={'while-editing'}
          // returnKeyType={'search'}
          keyboardAppearance={'dark'}
          autoFocus={autoFocus}
        />
        {value.trim() !== '' && (
          <IconButton
            size={'normal'}
            onPress={onClearTextPress}
            style={styles.clearButton}
          >
            <Icon name="close" size={24} color={primaryColor} />
          </IconButton>
        )}
        {errorText && (
          <IconButton disabled size={'normal'} style={styles.clearButton}>
            <Icon name="alert-circle-outline" size={24} color={primaryColor} />
          </IconButton>
        )}
      </View>
      {!errorText && hintText && (
        <Text style={[styles.underText, {color: theme.colors.text}]}>
          {hintText}
        </Text>
      )}
      {errorText && (
        <Text style={[styles.underText, {color: theme.colors.danger}]}>
          {errorText}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    marginVertical: 8,
  },
  container: {
    height: 44,
    borderBottomWidth: 1,
    paddingHorizontal: 0,
    // paddingBottom: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputTitle: {
    fontWeight: 'bold',
  },
  input: {
    height: 44,
    paddingHorizontal: 8,
    flex: 1,
  },
  underText: {
    marginTop: 8,
    fontSize: 12,
    fontWeight: 'bold',
  },
  searchIcon: {
    marginTop: -2,
    marginLeft: 8,
    width: 32,
    minWidth: 32,
  },
  clearButton: {
    marginTop: -2,
    marginRight: 0,
    width: 32,
    minWidth: 32,
  },
});
