import React, {FC} from 'react';
import {StyleProp, StyleSheet, Text, TextStyle} from 'react-native';
import {useTheme} from '../hooks/useTheme';

export type CaptionProps = {
  numberOfLines?: number;
  style?: StyleProp<TextStyle>;
};

export const Caption: FC<CaptionProps> = ({numberOfLines, style, children}) => {
  const {theme} = useTheme();
  return (
    <Text
      style={[styles.caption, {color: theme.colors.text}, style]}
      numberOfLines={numberOfLines}
    >
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  caption: {
    fontSize: 24,
    fontWeight: '700',
    textTransform: 'capitalize',
    color: '#f7f7f7',
    marginTop: 8,
    marginBottom: 8,
  },
});
