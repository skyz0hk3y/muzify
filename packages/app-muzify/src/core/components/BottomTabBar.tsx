import React, {
  FC,
  useRef,
  useCallback,
  useMemo,
  useEffect,
  useContext,
  useState,
  useLayoutEffect,
} from 'react';
import {
  BackHandler,
  NativeEventSubscription,
  Platform,
  StyleSheet,
  View,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {Track, useTrackPlayerProgress} from 'react-native-track-player';

import {BottomSheetModal} from '@gorhom/bottom-sheet';

import {
  BottomTabBar as OriginalBottomTabBar,
  BottomTabBarProps as OriginalBottomTabBarProps,
} from '@react-navigation/bottom-tabs';

import * as PlayerActions from '../../audio-player/actions';

import {default as ModalPlayer} from '../../audio-player/components/ModalPlayer';
import {default as ModalTrackMore} from '../../audio-player/components/ModalTrackMore';
import {MiniPlayer} from '../../audio-player/components/MiniPlayer';
import {TrackMoreContext} from '../../audio-player/TrackMoreContext';
import {useTheme} from '../hooks/useTheme';
import {HeaderHeightContext} from '@react-navigation/stack';
import {Backdrop} from './Backdrop';
import {useAudioPlayer} from '../../audio-player/hooks/useAudioPlayer';

export const PLATFORM_BOTTOM_HEIGHT = Platform.OS === 'ios' ? 96 : 64;

export type BottomTabBarProps = OriginalBottomTabBarProps & {
  currentTrack: Track | null;
  isPlaying: boolean;
  trackLoading: boolean;
  canPlayNext: boolean;
  canPlayPrevious: boolean;
  setIsPlaying: typeof PlayerActions.setIsPlaying;
  seekToSeconds: typeof PlayerActions.seekToSeconds;
};

export const BottomTabBar: FC<BottomTabBarProps> = ({
  navigation,
  currentTrack,
  ...props
}) => {
  const headerHeight = useContext(HeaderHeightContext);
  const {theme} = useTheme();
  const {setOpenTrackMoreForTrack, track} = useContext(TrackMoreContext);
  const {isPlaying, seekToSeconds} = useAudioPlayer();

  const backHandler = useRef<NativeEventSubscription | null>(null);
  const bottomSheetModalRef = useRef<BottomSheetModal>(null);
  const trackMoreModalRef = useRef<BottomSheetModal>(null);
  const snapPoints = useMemo(() => [0, 480, 650], []);
  const trackMoreSnapPoints = useMemo(() => [0, 440], []);

  const [playerModalOpen, setPlayerModalOpen] = useState(false);
  const [bottomSheetSize, setBottomSheetSize] = useState(snapPoints[1]);

  const dispatch = useDispatch();

  const closeModal = useCallback((): boolean => {
    bottomSheetModalRef.current?.dismiss();
    backHandler.current?.remove();
    setPlayerModalOpen(false);
    return true;
  }, []);

  const onModalOpenPress = useCallback(() => {
    bottomSheetModalRef.current?.present();
    backHandler.current = BackHandler.addEventListener(
      'hardwareBackPress',
      closeModal,
    );
    setPlayerModalOpen(true);
  }, [closeModal]);

  const closeTrackMoreModal = useCallback((): boolean => {
    trackMoreModalRef.current?.dismiss();
    backHandler.current?.remove();

    if (playerModalOpen) {
      onModalOpenPress();
    }

    return true;
  }, [onModalOpenPress, playerModalOpen]);

  const onTrackMoreModalOpenPress = useCallback(() => {
    if (playerModalOpen) {
      bottomSheetModalRef.current?.dismiss();
    }

    trackMoreModalRef.current?.present();
    backHandler.current = BackHandler.addEventListener(
      'hardwareBackPress',
      closeTrackMoreModal,
    );
  }, [closeTrackMoreModal, playerModalOpen, bottomSheetModalRef]);

  const focusedOptions =
    props.descriptors[props.state.routes[props.state.index].key].options;

  const {bufferedPosition, duration, position} = useTrackPlayerProgress();
  const bufferingProgress = Math.floor((bufferedPosition / duration) * 100);

  const onSeekbarValueChanged = async (newPosition: number) => {
    // @deprecated
    // dispatch(PlayerActions.seekToSeconds(newPosition));

    await seekToSeconds(newPosition);
  };

  const onTogglePlayPausePress = () => {
    dispatch(PlayerActions.setIsPlaying(!isPlaying));
  };

  const onBottomSheetSizeChange = (snapPointsIndex: number) => {
    setBottomSheetSize(snapPoints[snapPointsIndex] || snapPoints[1]);
  };

  useEffect(() => {
    if (!currentTrack) {
      bottomSheetModalRef.current?.dismiss();
    }
  }, [currentTrack, onTrackMoreModalOpenPress, setOpenTrackMoreForTrack]);

  useLayoutEffect(() => {
    setOpenTrackMoreForTrack(() => {
      return onTrackMoreModalOpenPress;
    });
  }, [onTrackMoreModalOpenPress, playerModalOpen, setOpenTrackMoreForTrack]);

  useEffect(() => {
    return () => {
      backHandler.current?.remove();
      BackHandler.removeEventListener('hardwareBackPress', closeModal);
      BackHandler.removeEventListener('hardwareBackPress', closeTrackMoreModal);
    };
  });

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <>
      <View style={styles.wrapper}>
        <MiniPlayer
          style={[
            styles.miniPlayer,
            {bottom: headerHeight || PLATFORM_BOTTOM_HEIGHT},
          ]}
          bufferingProgress={bufferingProgress}
          trackPosition={position}
          trackDuration={duration}
          onTogglePlayPausePress={onTogglePlayPausePress}
          onSeekbarValueChanged={onSeekbarValueChanged}
          onPress={onModalOpenPress}
        />
        <OriginalBottomTabBar
          {...props}
          navigation={navigation}
          inactiveBackgroundColor={theme.colors.backgrounds[1]}
          activeBackgroundColor={theme.colors.backgrounds[1]}
          inactiveTintColor={theme.colors.textMuted}
          activeTintColor={theme.colors.text}
          style={[
            styles.bottomTabBar,
            {
              height: headerHeight || PLATFORM_BOTTOM_HEIGHT,
              minHeight: headerHeight || PLATFORM_BOTTOM_HEIGHT,
              borderTopColor: theme.colors.backgrounds[3],
            },
          ]}
          labelStyle={styles.bottomTabBarLabels}
        />
      </View>
      <BottomSheetModal
        ref={bottomSheetModalRef}
        index={1}
        animationDuration={240}
        onDismiss={closeModal}
        onChange={onBottomSheetSizeChange}
        snapPoints={snapPoints}
        backdropComponent={(backdropProps) => (
          <Backdrop {...backdropProps} closeOnPress={true} />
        )}
        backgroundComponent={null}
        handleComponent={null}
      >
        <View style={styles.modalPlayerWrapper}>
          <ModalPlayer
            bufferingProgress={bufferingProgress}
            position={position}
            duration={duration}
            onPositionChangeRequest={onSeekbarValueChanged}
            bottomSheetSize={bottomSheetSize}
          />
        </View>
      </BottomSheetModal>
      {track && (
        <BottomSheetModal
          ref={trackMoreModalRef}
          index={1}
          animationDuration={240}
          onDismiss={closeTrackMoreModal}
          snapPoints={trackMoreSnapPoints}
          animateOnMount
          backdropComponent={(backdropProps) => (
            <Backdrop {...backdropProps} closeOnPress={true} />
          )}
          backgroundComponent={null}
          handleComponent={null}
        >
          <View style={styles.modalPlayerWrapper}>
            <ModalTrackMore navigation={navigation} track={track} />
          </View>
        </BottomSheetModal>
      )}
    </>
  );
};

export default BottomTabBar;

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'column-reverse',
  },
  modalPlayerWrapper: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    overflow: 'hidden',
  },
  miniPlayer: {
    position: 'absolute',
    left: 0,
    right: 0,
  },
  bottomTabBar: {
    borderTopWidth: 1,
  },
  bottomTabBarLabels: {
    top: -8,
  },
});
