import {Dispatch} from 'redux';
import {Alert, NativeModules} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import semverCompare from 'semver-compare';
import {
  UpdateAPK,
  getInstalledVersionName,
  getInstalledVersionCode,
  getInstalledLastUpdateTime,
} from 'rn-update-apk';

import {
  CommonActions,
  SetThemeActionType,
  SetNetworkStateActionType,
  NetworkStateConnectionType,
  SetUpdatesStateActionType,
  CheckForUpdatesRequestActionType,
  CommonActionsType,
  CheckForUpdatesSuccessActionType,
  CheckForUpdatesFailureActionType,
} from './types';
import {getUpdateCheckUrl} from './helpers/updates';
import {RootState} from '../store';

/* Actions */
export const setTheme = (themeName: string): SetThemeActionType => ({
  type: CommonActions.SET_THEME,
  payload: {
    themeName,
  },
});

export const setNetworkState = (payload: {
  isConnected?: boolean;
  isInternetReachable?: boolean;
  isConnectionExpensive?: boolean;
  connectionType?: NetworkStateConnectionType;
}): SetNetworkStateActionType => ({
  type: CommonActions.SET_NETWORK_STATE,
  payload: {
    ...payload,
  },
});

export const setUpdatesState = (payload: {
  lastCheckDate?: Date;
  isUpgradeAvailable?: boolean;
  currentVersionName?: string;
  currentVersionCode?: string;
  newVersionName?: string | null;
  apkDownloadProgress?: number | null;
}): SetUpdatesStateActionType => ({
  type: CommonActions.SET_UPDATES_STATE,
  payload: {
    ...payload,
  },
});

export const checkForUpdates = {
  request: (): CheckForUpdatesRequestActionType => ({
    type: CommonActions.CHECK_FOR_UPDATES_REQUEST,
    payload: undefined,
  }),
  success: (payload: {
    isUpgradeAvailable: boolean;
    newVersionName?: string;
  }): CheckForUpdatesSuccessActionType => ({
    type: CommonActions.CHECK_FOR_UPDATES_SUCCESS,
    payload: {
      isUpgradeAvailable: payload.isUpgradeAvailable,
      newVersionName: payload.newVersionName,
      lastCheckDate: new Date(+Date.now()),
    },
  }),
  failure: (payload: {
    error: {
      code: string;
      key: string;
      message: string;
    };
  }): CheckForUpdatesFailureActionType => ({
    type: CommonActions.CHECK_FOR_UPDATES_FAILURE,
    payload: {
      error: payload.error,
      lastCheckDate: new Date(+getInstalledLastUpdateTime()),
    },
  }),
};

/* Actions creators */
export const fetchInitialNetworkStatus = () => {
  return async (dispatch: Dispatch<SetNetworkStateActionType>) => {
    const netInfo = await NetInfo.fetch();
    dispatch(
      setNetworkState({
        isConnected: netInfo.isConnected || undefined,
        isInternetReachable: netInfo.isInternetReachable || undefined,
        isConnectionExpensive: netInfo.details?.isConnectionExpensive,
        connectionType: netInfo.type,
      }),
    );
  };
};

export const fetchInstalledVersionInfos = () => {
  return async (dispatch: Dispatch<SetUpdatesStateActionType>) => {
    dispatch(
      setUpdatesState({
        lastCheckDate: new Date(+getInstalledLastUpdateTime()),
        currentVersionName: getInstalledVersionName(),
        currentVersionCode: getInstalledVersionCode(),
      }),
    );
  };
};

export type CheckForUpdatesI18NKeys = {
  meta: {
    cancel: string;
    updateNow: string;
  };
  success: {
    title: string;
    subtitle: string;
    releaseNotesTitle: string;
  };
  noUpdates: {
    title: string;
    subtitle: string;
  };
};

export const checkForAppUpdates = (
  shouldInteruptUser?: boolean,
  i18nKeys?: CheckForUpdatesI18NKeys,
) => {
  return async (
    dispatch: Dispatch<CommonActionsType>,
    getState: () => RootState,
  ) => {
    dispatch(checkForUpdates.request());

    const updater = new UpdateAPK({
      apkVersionUrl: getUpdateCheckUrl(),
      //apkVersionOptions is optional, you should use it if you need to pass options to fetch request
      apkVersionOptions: {
        method: 'GET',
        headers: {},
      },

      //apkOptions is optional
      //Complements or replaces the DownloadFileOptions (from react-native-fs) to download the new APK
      //By default the following options are already set: fromUrl, toFile, begin, progress, background and progressDivider
      //You should use it if you need to pass additional information (for example: headers) to download the new APK
      apkOptions: {
        headers: {},
      },

      // The name of this 'fileProviderAuthority' is defined in AndroidManifest.xml. THEY MUST MATCH.
      // By default other modules like rn-fetch-blob define one (conveniently named the same as below)
      // but if you don't match the names you will get an odd-looking XML exception:
      // "Attempt to invoke virtual method 'android.content.res.XmlResourceParser ....' on a null object reference"
      fileProviderAuthority: 'com.muzify.provider',

      // This callback is called if there is a new version but it is not a forceUpdate.
      needUpdateApp: async (needUpdate: (isUpdate: boolean) => void) => {
        const changelogReq = await fetch(getUpdateCheckUrl());
        const changelog = await changelogReq.json();
        if (changelog.versionName == null) {
          if (shouldInteruptUser) {
            Alert.alert('No update available', 'No new version released yet');
          }

          dispatch(
            checkForUpdates.success({
              isUpgradeAvailable: false,
              newVersionName: null,
            }),
          );

          return;
        }

        const state = getState();
        const {
          common: {
            updates: {currentVersionName},
          },
        } = state;

        // Returns 1 if a is greater than b, -1 either, 0 if equals
        const isReallyAnUpdate =
          semverCompare(changelog.versionName, currentVersionName) !== 1;

        // Avoid asking user to downgrade...
        if (isReallyAnUpdate) {
          if (shouldInteruptUser) {
            Alert.alert('No update available', 'No new version released yet');
          }

          dispatch(
            checkForUpdates.success({
              isUpgradeAvailable: false,
              newVersionName: null,
            }),
          );

          return;
        }

        // We have a real update here!
        dispatch(
          checkForUpdates.success({
            isUpgradeAvailable: true,
            newVersionName: changelog.versionName,
          }),
        );

        if (shouldInteruptUser) {
          // First let check if we have permission, and ask if not
          if (NativeModules.Intents.shouldAllowUnknownSourceFromApp()) {
            NativeModules.Intents.intentAllowUnknownSourceFromApp();
          }

          Alert.alert(
            `${i18nKeys?.success.title} (v${changelog.versionName})`,
            `${i18nKeys?.success.subtitle}` +
              `\n\n# ${i18nKeys?.success.releaseNotesTitle}:` +
              `\n\n${changelog.whatsNew}`,
            [
              {
                style: 'cancel',
                text: i18nKeys?.meta.cancel,
                onPress: () => {},
              },
              {
                style: 'default',
                text: i18nKeys?.meta.updateNow,
                onPress: () => {
                  dispatch(setUpdatesState({apkDownloadProgress: 0}));
                  needUpdate(true);
                },
              },
            ],
          );
        }
      },

      // This will be called before the download/update where you defined forceUpdate: true in the version JSON
      forceUpdateApp: () => {
        // console.log('forceUpdateApp callback called');
      },

      // Called if the current version appears to be the most recent available
      notNeedUpdateApp: () => {
        // console.log(
        //   'notNeedUpdateApp callback called',
        //   NativeModules.RNUpdateAPK.versionCode,
        // );
        dispatch(
          checkForUpdates.success({
            isUpgradeAvailable: false,
            newVersionName: null,
          }),
        );

        if (shouldInteruptUser) {
          Alert.alert('No update available', 'No new version released yet');
        }
      },

      // This is passed to react-native-fs as a callback
      downloadApkStart: () => {
        // console.log('downloadApkStart callback called');
        dispatch(setUpdatesState({apkDownloadProgress: 10}));
      },

      // Called with 0-99 for progress during the download
      downloadApkProgress: (progress: number) => {
        dispatch(setUpdatesState({apkDownloadProgress: progress - 10}));
      },

      // This is called prior to the update. If you throw it will abort the update
      downloadApkEnd: () => {
        // This could be an opportunity to check the APK signature thumbprints,
        // If they mismatch your update will fail, the user will have to uninstall first.

        // If you implement SHAsums on the file you could detect tampering here as well

        // Finally for APK25+ you should check REQUEST_INSTALL_PACKAGES permission
        // prior to the attempt at some point, and provide guidance about "unknown sources" etc
        // console.log('downloadApkEnd callback called');

        dispatch(
          setUpdatesState({
            lastCheckDate: new Date(+getInstalledLastUpdateTime()),
            apkDownloadProgress: 100,
          }),
        );
      },

      // This is called if the fetch of the version or the APK fails, so should be generic
      onError: (err: Error) => {
        // console.log('onError callback called', err);
        if (shouldInteruptUser) {
          Alert.alert('There was an error', err.message);
        }

        dispatch(
          checkForUpdates.failure({
            error: {
              code: err.name,
              key: 'errors.update-check-error-unknown',
              message: err.message,
            },
          }),
        );
      },
    });

    updater.checkUpdate();
  };
};
