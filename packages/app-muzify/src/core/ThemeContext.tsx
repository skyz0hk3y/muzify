import {createContext} from 'react';

import {DarkTheme} from './themes';
import {AppTheme} from './types';

export type ThemeContextType = {
  theme: AppTheme;
};

export const defaultTheme = DarkTheme;

export const ThemeContext = createContext({
  theme: defaultTheme,
});
