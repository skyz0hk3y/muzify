import React, {createContext} from 'react';
import {GestureResponderEvent} from 'react-native';

export type ModalDTO = {
  id: number;
  key: string;
  title: string;
  children: React.ReactChild;
  onClose?: (e: GestureResponderEvent) => void;
};

export type ModalContextType = {
  // Getters
  modals: ModalDTO[];
  visibleModalIndex: number;

  // Setters
  setModals: (modals: ModalDTO[]) => void;
  setVisibleModalIndex: (modelIndex: number) => void;

  // Utils
  dismissCurrentModal: () => void;
};

export const defaultContext: ModalContextType = {
  // Getters
  modals: [],
  visibleModalIndex: -1,

  // Setters
  setModals: () => undefined,
  setVisibleModalIndex: () => undefined,

  // Utils
  dismissCurrentModal: () => undefined,
};

export const ModalContext = createContext(defaultContext);
