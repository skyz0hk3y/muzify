import DeviceInfo from 'react-native-device-info';
import {fetch} from 'cross-fetch';

import {API_BASE_URL} from '../../constants';

export const getUpdateCheckUrl = () => {
  const abis = DeviceInfo.supportedAbisSync();

  // FIXME: Maybe we can do better than using 0 index ?
  return `${API_BASE_URL}/updates/check?abi_code=${abis[0]}`;
};

export const getChangelogForVersion = async (versionName: string) => {
  const res = await fetch(
    `${API_BASE_URL}/updates/changelog?version=${versionName}`,
  );
  const json = await res.json();

  if (json.error) {
    console.log(json.error);
    return json;
  }

  return json.data.changelog;
};
