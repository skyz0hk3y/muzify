import {Platform} from 'react-native';
import {Track} from 'react-native-track-player';
import {default as FileSystem} from 'react-native-fs';
import {default as slugify} from '@sindresorhus/slugify';

import {sanitizeTrack} from './sanitizeTrack';
import {RootState} from '../../store';

export const getLocalPath = () => {
  return Platform.OS === 'android'
    ? `${FileSystem.ExternalStorageDirectoryPath}`
    : FileSystem.LibraryDirectoryPath;
};

export const getAppFolderName = () => 'Muzify';

export const getAudioFileExtension = () => {
  // require is important so it can run in headless service too
  const {store} = require('../../store');
  const state: RootState = store.getState();

  return state.settings.downloads.fileFormat;
};

export const getLocalPathForTrack = (track: Track) => {
  // require is important so it can run in headless service too
  const {store} = require('../../store');
  const state: RootState = store.getState();

  const sanitizedTrack = sanitizeTrack(track);
  const artistSlug = slugify(sanitizedTrack.artist);
  const titleSlug = slugify(sanitizedTrack.title);

  const base = state.settings.downloads.destinationFolderPath;
  const ext = getAudioFileExtension();

  if (state.settings.downloads.flatFolderStructure) {
    return `${base}/${titleSlug}.${ext}`;
  }

  return `${base}/${artistSlug}/${titleSlug}.${ext}`;
};
