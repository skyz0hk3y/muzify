export function getUniqueListBy<T extends Record<string, unknown>>(
  arr: T[],
  key: string,
): T[] {
  return [...new Map(arr.map((item) => [item[key], item])).values()];
}
