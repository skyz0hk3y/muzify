import {Track} from 'react-native-track-player';

export const getTrackArtist = (track: Track): string => {
  if (!track) {
    return '';
  }

  if (!track.artist) {
    return '<unknown artist>';
  }

  const sanitizedArtist = track.artist
    .replace(/VEVO/gi, '')
    .replace(/ - Topic/gi, '')
    .replace(/Music/, '')
    .replace(/ Offici(e|a)l/gi, '')
    .trim();

  return sanitizedArtist;
};

export const getTrackTitle = (track: Track): string => {
  if (!track) {
    return '';
  }

  if (!track.title) {
    return '<unknown title>';
  }

  const condensedArtist = track.artist.replace(/\s+/gi, '');

  const sanitizedTitle = track.title
    // .replace(/ - /, '')
    .replace(track.artist.replace(/(et|and)/gi, '&'), '')
    .replace(new RegExp(`\b${track.artist}\b`), '')
    .replace(new RegExp(`\b${track.artist.toUpperCase()}\b`), '')
    .replace(new RegExp(`\b${track.artist.toLowerCase()}\b`), '')
    .replace(new RegExp(`\b${condensedArtist}\b`), '')
    .replace(new RegExp(`\b${condensedArtist}VEVO\b`), '')
    .replace(new RegExp(`\b${condensedArtist}Officiel\b`), '')
    .replace(/\(?Clip? ?Officiel?\)?/gi, '')
    .replace(/\(?Audio\)?/gi, '')
    .replace(/VEVO/gi, '')
    .replace(' Officiel)', '')
    .replace(/Ft. /gi, 'feat. ')
    .replace(/Feat. /gi, 'feat. ')
    .replace(/Featuring /gi, 'feat. ')
    .replace(/.* - (.*)/gi, '$1')
    .replace(/.*"(.*)"(.*)/g, '$1$2')
    .replace(/\(.*\)?/gi, '')
    .replace(/\[.*\]?/gi, '')
    .replace(/^\s+?-\s+?/, '')
    .replace(/^, /, '')
    .replace(/ \|.*$/gi, '')
    .replace(/\)$/gi, '')
    .trim();

  return sanitizedTitle;
};

export const sanitizeTrack = (track: Track): Track => {
  const defaults: Partial<Track> = {
    title: '<unknown title>',
    artist: '<unknown artist>',
  };

  const sanitizedArtistTrack = {
    ...defaults,
    ...track,
    artist: getTrackArtist({...defaults, ...track}),
  };

  const sanitizedTrack = {
    ...sanitizedArtistTrack,
    title: getTrackTitle(sanitizedArtistTrack),
  };

  return sanitizedTrack;
};
