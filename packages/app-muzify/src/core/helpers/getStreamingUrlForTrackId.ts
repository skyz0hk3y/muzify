import {Platform} from 'react-native';

import {API_BASE_URL} from '../../constants';
import {RootState} from '../../store';

export const getTrackMetaUrlForTrackId = (trackId: string) => {
  const id = encodeURIComponent(trackId);
  return `${API_BASE_URL}/stream/${id}`;
};

export const getStreamingUrlForTrackId = (trackId: string) => {
  // require is important so it can run In headless service too
  const {store} = require('../../store');
  const {
    settings: {streaming},
  }: RootState = store.getState();

  const id = encodeURIComponent(trackId);
  const baseUrl = `${API_BASE_URL}/stream/${id}`;
  const format = Platform.OS === 'ios' ? 'mp4' : 'webm';
  const quality = streaming.audioQuality;

  return `${baseUrl}/audio?format=${format}&quality=${quality}`;
};
