import {RootState} from '../store';
import {DarkTheme, LightTheme} from './themes';
import {AppTheme} from './types';

export const getThemeNameSelector = (state: RootState): 'dark' | 'light' => {
  return state.common.themeName;
};
export const getThemeSelector = (state: RootState): AppTheme => {
  return state.common.themeName === 'dark' ? DarkTheme : LightTheme;
};

export const getIsOfflineSelector = (state: RootState): boolean => {
  return state.common.networkState.isConnected === false;
};

export const getCurrentVersionSelector = (state: RootState) => {
  return state.common.updates.currentVersionName;
};

export const getNewVersionNameSelector = (state: RootState) => {
  return state.common.updates.newVersionName;
};

export const getIsUpdateAvailableSelector = (state: RootState) => {
  return state.common.updates.isUpgradeAvailable;
};

export const getLastUpdateCheckSelector = (state: RootState) => {
  return state.common.updates.lastCheckDate?.toLocaleString();
};

export const getUpdateDownloadProgressSelector = (state: RootState) => {
  return state.common.updates.apkDownloadProgress;
};

export const isCheckingForUpdatesSelector = (state: RootState): boolean => {
  return state.common.updates.isCheckingForUpdates;
};
