import {useCallback, useMemo} from 'react';
import {lighten as _lighten, darken as _darken, toHex} from '@hbis/color';

type ColorString = string;
type HslAmount = number;
type ColorConversionFunction = (
  color: ColorString,
  amount: HslAmount,
) => ColorString;

export type UseColorHelpersHook = () => {
  lighten: ColorConversionFunction;
  darken: ColorConversionFunction;
};

const shade: ColorConversionFunction = (
  color: ColorString,
  percent: HslAmount,
) => {
  var R = parseInt(color.substring(1, 3), 16);
  var G = parseInt(color.substring(3, 5), 16);
  var B = parseInt(color.substring(5, 7), 16);

  R = parseInt(`${(R * (100 + percent)) / 100}`, 10);
  G = parseInt(`${(G * (100 + percent)) / 100}`, 10);
  B = parseInt(`${(B * (100 + percent)) / 100}`, 10);

  R = R < 255 ? R : 255;
  G = G < 255 ? G : 255;
  B = B < 255 ? B : 255;

  var RR = R.toString(16).length === 1 ? '0' + R.toString(16) : R.toString(16);
  var GG = G.toString(16).length === 1 ? '0' + G.toString(16) : G.toString(16);
  var BB = B.toString(16).length === 1 ? '0' + B.toString(16) : B.toString(16);

  return '#' + RR + GG + BB;
};

export const useColorHelpers: UseColorHelpersHook = () => {
  const lighten = useCallback((colorHex, amount) => {
    return shade(colorHex, amount);
  }, []);

  const darken = useCallback((colorHex, amount) => {
    return shade(colorHex, amount * -1);
  }, []);

  return useMemo(
    () => ({
      lighten,
      darken,
    }),
    [darken, lighten],
  );
};
