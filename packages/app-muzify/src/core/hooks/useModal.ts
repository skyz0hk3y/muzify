import {useCallback, useContext, useMemo, useRef} from 'react';

import {ModalContext, ModalDTO} from '../ModalContext';

export type UseModalAPI = {
  visibleModalIndex: number;
  showWithContent: (modal: Omit<ModalDTO, 'id'>) => number;
  hideAtIndex: (modalIndex: number) => boolean;
  hideCurrent: () => boolean;
  isVisibleAtIndex: (modalIndex: number) => boolean;
  dismissCurrentModal: () => void;
};

export const useModal = (): UseModalAPI => {
  const {
    modals,
    visibleModalIndex,
    setModals,
    setVisibleModalIndex,
    dismissCurrentModal: _dismissCurrentModal,
  } = useContext(ModalContext);

  const dismissCurrentModal = useRef(null);
  dismissCurrentModal.current = _dismissCurrentModal;

  const showWithContent = useCallback(
    (modal: Omit<ModalDTO, 'id'>) => {
      const modalIndex = modals.length;
      setModals([
        ...modals,
        {
          ...modal,
          id: modalIndex,
        },
      ]);
      setVisibleModalIndex(modalIndex);
      return modalIndex;
    },
    [modals, setModals, setVisibleModalIndex],
  );

  const hideAtIndex = useCallback(
    (modalIndex: number) => {
      if (modals[modalIndex] == null) {
        // TODO(logs): Add something here?
        return false;
      }

      setModals([
        ...modals.filter((modal: ModalDTO) => modal.id !== modalIndex),
      ]);
      const previousModalIndex = modalIndex - 1;
      setVisibleModalIndex(previousModalIndex >= -1 ? previousModalIndex : -1);

      return true;
    },
    [modals, setModals, setVisibleModalIndex],
  );

  const hideCurrent = useCallback(() => {
    dismissCurrentModal.current?.();
    return true;
  }, []);

  const isVisibleAtIndex = useCallback(
    (modalIndex: number) =>
      modals[modalIndex] != null ? !!modals[modalIndex] : false,
    [modals],
  );

  return useMemo(
    (): UseModalAPI =>
      ({
        visibleModalIndex,
        dismissCurrentModal: dismissCurrentModal.current,
        hideAtIndex,
        hideCurrent,
        isVisibleAtIndex,
        showWithContent,
      } as const),
    [
      visibleModalIndex,
      dismissCurrentModal,
      hideAtIndex,
      hideCurrent,
      isVisibleAtIndex,
      showWithContent,
    ],
  );
};
