import {
  CommonActions,
  CommonActionsType,
  CommonStateType,
  MODULE_NAME,
} from './types';

const INITIAL_STATE: CommonStateType = {
  themeName: 'dark',
  networkState: {
    isConnected: true,
    isInternetReachable: false,
    isConnectionExpensive: false,
    connectionType: 'unknown',
  },
  updates: {
    isCheckingForUpdates: false,
    lastCheckDate: null,
    isUpgradeAvailable: false,
    currentVersionName: '',
    currentVersionCode: '',
    newVersionName: null,
    apkDownloadProgress: null,
    error: null,
  },
};

export const getCommonPersistConfig = (storage: any) => ({
  key: MODULE_NAME,
  storage: storage,
  blacklist: ['networkState'],
});

export const commonReducer = (
  state = INITIAL_STATE,
  action: CommonActionsType,
): CommonStateType => {
  switch (action.type) {
    case CommonActions.SET_THEME: {
      return {
        ...state,
        themeName: action.payload.themeName as 'dark' | 'light',
      };
    }
    case CommonActions.SET_NETWORK_STATE: {
      return {
        ...state,
        networkState: {
          ...state.networkState,
          isConnected: action.payload.isConnected || false,
          isInternetReachable: action.payload.isInternetReachable || false,
          isConnectionExpensive: action.payload.isConnectionExpensive || false,
          connectionType: action.payload.connectionType || 'unknown',
        },
      };
    }
    case CommonActions.CHECK_FOR_UPDATES_REQUEST: {
      return {
        ...state,
        updates: {
          ...state.updates,
          isCheckingForUpdates: true,
          // Reset some stuff
          isUpgradeAvailable: false,
          newVersionName: null,
          apkDownloadProgress: null,
          lastCheckDate: new Date(Date.now()),
          error: null,
        },
      };
    }
    case CommonActions.CHECK_FOR_UPDATES_SUCCESS: {
      const {
        isUpgradeAvailable,
        newVersionName,
        lastCheckDate,
      } = action.payload;
      return {
        ...state,
        updates: {
          ...state.updates,
          isUpgradeAvailable,
          lastCheckDate,
          newVersionName: newVersionName || null,
          isCheckingForUpdates: false,
          error: null,
        },
      };
    }
    case CommonActions.CHECK_FOR_UPDATES_FAILURE: {
      return {
        ...state,
        updates: {
          ...state.updates,
          isCheckingForUpdates: false,
          // reset some stuff
          isUpgradeAvailable: false,
          newVersionName: null,
          apkDownloadProgress: null,
          lastCheckDate: new Date(Date.now()),
          error: action.payload.error,
        },
      };
    }
    case CommonActions.SET_UPDATES_STATE: {
      return {
        ...state,
        updates: {
          ...state.updates,
          ...action.payload,
        },
      };
    }
    default:
      return state;
  }
};
