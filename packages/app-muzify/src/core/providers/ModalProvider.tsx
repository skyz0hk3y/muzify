import React, {
  FC,
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {BackHandler, StyleSheet, View} from 'react-native';

import {Modal, ModalOverlay} from '../components/Modal';
import {ModalContext, ModalDTO} from '../ModalContext';

export const ModalProvider: FC = ({children}) => {
  const [modals, setModals] = useState<ModalDTO[]>([]);
  const [visibleModalIndex, setVisibleModalIndex] = useState(-1);

  const currentModal: ModalDTO | null = useMemo(
    () => modals?.[visibleModalIndex] || null,
    [modals, visibleModalIndex],
  );

  const opacityLevel = useMemo(() => 0.4 * (visibleModalIndex + 1), [
    visibleModalIndex,
  ]);

  const handleCloseRequest = useCallback(() => {
    if (currentModal == null) {
      return;
    }

    setModals([
      ...modals.filter((modal: ModalDTO) => modal.id !== visibleModalIndex),
    ]);
    const previousModalIndex = visibleModalIndex - 1;
    setVisibleModalIndex(previousModalIndex >= -1 ? previousModalIndex : -1);
  }, [modals, currentModal, visibleModalIndex]);

  const ctxValue = useMemo(
    () => ({
      // Getters
      modals,
      visibleModalIndex,
      // Setters
      setModals,
      setVisibleModalIndex,
      // Utils
      dismissCurrentModal: handleCloseRequest,
    }),
    [handleCloseRequest, modals, visibleModalIndex],
  );

  useEffect(() => {
    const _onBackHandlerBackPress = () => {
      if (currentModal) {
        handleCloseRequest();
        return true;
      }
      return false;
    };

    BackHandler.addEventListener('hardwareBackPress', _onBackHandlerBackPress);

    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        _onBackHandlerBackPress,
      );
    };
  });

  // Hot path optimization
  if (!currentModal) {
    return (
      <ModalContext.Provider value={ctxValue}>{children}</ModalContext.Provider>
    );
  }

  return (
    <ModalContext.Provider value={ctxValue}>
      {children}
      <View style={styles.wrapper}>
        <ModalOverlay
          onCloseRequest={handleCloseRequest}
          visible={true}
          color={`rgba(0,0,0,${opacityLevel})`}
        />
        <Modal
          onCloseRequest={handleCloseRequest}
          key={currentModal.key}
          title={currentModal.title}
          visible={true}
        >
          {React.cloneElement(currentModal.children as ReactElement<any>, {
            key: currentModal.key,
          })}
        </Modal>
      </View>
    </ModalContext.Provider>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',

    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 7000,
  },
});
