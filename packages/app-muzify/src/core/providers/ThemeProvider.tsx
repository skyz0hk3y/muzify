import React, {FC} from 'react';
import {useSelector} from 'react-redux';
import {getThemeSelector} from '../selector';
import {ThemeContext} from '../ThemeContext';

export const ThemeProvider: FC = ({children}) => {
  const currentTheme = useSelector(getThemeSelector);
  return (
    <ThemeContext.Provider value={{theme: currentTheme}}>
      {children}
    </ThemeContext.Provider>
  );
};
