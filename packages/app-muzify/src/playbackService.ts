import TrackPlayer from 'react-native-track-player';

import {
  setCurrentTrack,
  setError,
  setIsBuffering,
  setIsPlaying,
  setIsPlayingSync,
  setTrackLoading,
} from './audio-player/actions';
import {TRACK_PLAYER_CONTROLS_OPTS} from './constants';
import {incrementTrackPlaysCount} from './genius/actions';
import {RootState, store as appStore} from './store';

type ReduxStore = typeof appStore;

export const playbackService = async (store: ReduxStore) => {
  // BOOT SEQUENCE

  console.log('[BG] initializing playbackService');

  // Init TrackPlayer
  await TrackPlayer.setupPlayer();

  console.log('[BG] playbackService initialized');

  // END BOOT SEQUENCE

  TrackPlayer.addEventListener('playback-queue-ended', async (event: any) => {
    console.log('[BG] Playback queue ended. Ev:', event);

    const state: RootState = store.getState();
    const {
      audioPlayer: {queue, currentTrackIndex, canPlayNext, loadingTrack},
    } = state;

    // Play the track in order of the queue, or restart queue.
    if (!loadingTrack && queue.length > 1 && canPlayNext) {
      let skipToId: string = null;

      const nextTrack = queue[currentTrackIndex + 1];
      if (nextTrack) {
        skipToId = nextTrack.id;
      } else {
        skipToId = queue[0].id;
      }

      await TrackPlayer.reset();
      // Make sure to set this again to avoid loosing notification features
      await TrackPlayer.updateOptions(TRACK_PLAYER_CONTROLS_OPTS);
      await TrackPlayer.add(queue);
      await TrackPlayer.skip(skipToId);
      await TrackPlayer.play();
    }

    // FIXME(hack): This was making the whole queue fucked. Commented ATM.
    // NOTE: This can now be fixed as we manage the queue ourselves in RN side.
    // Loop current queue if on repeat-queue mode
    //
    // if (audioPlayer.repeatMode === 'repeat-queue') {
    //   TrackPlayer.stop().then(() => {
    //     if (audioPlayer.currentPlaylist) {
    //       console.log('[BG] Rewinding queue to the start...', event);
    //       store.dispatch<any>(
    //         playFromLibraryPlaylist(
    //           audioPlayer.currentPlaylist.tracksIds[0],
    //           audioPlayer.currentPlaylist.id,
    //         ),
    //       );
    //     } else if (!audioPlayer.currentPlaylist && audioPlayer.currentTrack) {
    //       console.log('[BG] Repeating current track...', event);
    //       store.dispatch<any>(playTrack(audioPlayer.currentTrack.id));
    //     }
    //   });
    // }

    if (event.error) {
      store.dispatch(setError(event.error));
      store.dispatch(setIsPlayingSync(false));
      store.dispatch(setIsBuffering(false));
    }
  });

  TrackPlayer.addEventListener(
    'playback-track-changed',
    (event: {track: string; nextTrack: string; position: string}) => {
      console.log('[BG] Track changed. Ev:', event);

      TrackPlayer.getTrack(event.nextTrack)
        .then((track) => {
          if (track != null) {
            console.log('[BG] Track changed to track=', track);
            store.dispatch(setCurrentTrack(track));
            store.dispatch<any>(incrementTrackPlaysCount(track.id));
          }
        })
        .catch((_err) => undefined);

      // // Don't process if next/current track is undefined.
      // if (!event.nextTrack || event.nextTrack === event.track) {
      //   return;
      // }

      // const {audioPlayer} = store.getState();

      // if (
      //   audioPlayer.currentTrack &&
      //   event.nextTrack !== audioPlayer.currentTrack.id &&
      //   event.track !== audioPlayer.currentTrack.id
      // ) {
      //   return;
      // }

      // if (audioPlayer.repeatMode !== 'repeat-current') {
      // store.dispatch(setTrackLoading(true));
      // TrackPlayer.getTrack(event.nextTrack)
      //   .then((track) => {
      //     store.dispatch(setCurrentTrack(track));
      //     store.dispatch<any>(incrementTrackPlaysCount(track.id));
      //   })
      //   .finally(() => {
      //     store.dispatch<any>(updatePlaybackControls());
      //     store.dispatch(setTrackLoading(false));
      //   });
      // // }

      // FIXME(hack): REPEAT CURRENT TRACK MODE -> use ExoPlayer API instead
      //
      // if (
      //   audioPlayer.repeatMode === 'repeat-current' &&
      //   audioPlayer.repeatTrackId != null &&
      //   event.nextTrack !== audioPlayer.repeatTrackId
      // ) {
      //   store.dispatch(setTrackLoading(true));

      //   TrackPlayer.pause().then(() => TrackPlayer.seekTo(0));

      //   TrackPlayer.getTrack(audioPlayer.repeatTrackId)
      //     .then((repeatTrack) => {
      //       store.dispatch(setCurrentTrack(repeatTrack));
      //       return TrackPlayer.skip(repeatTrack.id);
      //     })
      //     .then(() => TrackPlayer.play())
      //     .finally(() => {
      //       if (!audioPlayer.repeatTrackId) {
      //         return;
      //       }
      //       store.dispatch<any>(
      //         incrementTrackRepeatsCount(audioPlayer.repeatTrackId),
      //       );
      //     })
      //     .catch(() => {
      //       if (!audioPlayer.repeatTrackId) {
      //         return;
      //       }

      //       console.log(
      //         '[BG] Repeating track failed, retrying with redux logic... Ev:',
      //         event,
      //       );
      //       if (audioPlayer.currentPlaylist) {
      //         store.dispatch<any>(
      //           playFromLibraryPlaylist(
      //             audioPlayer.repeatTrackId,
      //             audioPlayer.currentPlaylist.id,
      //           ),
      //         );
      //       } else {
      //         store.dispatch<any>(playTrack(audioPlayer.repeatTrackId));
      //       }

      //       store.dispatch<any>(
      //         incrementTrackRepeatsCount(audioPlayer.repeatTrackId),
      //       );
      //     })
      //     .finally(() => {
      //       store.dispatch<any>(updatePlaybackControls());
      //       store.dispatch(setTrackLoading(false));
      //     });
      // }
    },
  );

  TrackPlayer.addEventListener('playback-state', (event: any) => {
    console.log('[BG] Playback state. Ev:', event);

    switch (event.state) {
      case TrackPlayer.STATE_BUFFERING: {
        store.dispatch(setIsPlayingSync(true));
        store.dispatch(setTrackLoading(true));
        break;
      }
      case TrackPlayer.STATE_PLAYING: {
        store.dispatch(setIsPlayingSync(true));
        store.dispatch(setTrackLoading(false));
        break;
      }
      case TrackPlayer.STATE_PAUSED: {
        store.dispatch(setIsPlayingSync(false));
        store.dispatch(setTrackLoading(false));
        break;
      }
      case TrackPlayer.STATE_STOPPED: {
        store.dispatch(setIsPlayingSync(false));
        store.dispatch(setIsBuffering(false));
        store.dispatch(setTrackLoading(false));
        break;
      }
      default: {
        break;
      }
    }
  });

  TrackPlayer.addEventListener('playback-error', (event: any) => {
    console.log('[BG] Playback error. Ev:', event);

    // TODO(ux): Display an in UI error?
    try {
      store.dispatch(
        setError({
          code: 'playback-error',
          message: JSON.stringify(event),
        }),
      );
    } catch (err) {}
  });

  TrackPlayer.addEventListener('remote-play', async (event: any) => {
    console.log('[BG] Remote play. Ev:', event);

    TrackPlayer.play()
      .then(() => store.dispatch(setIsPlayingSync(true)))
      .catch(() => store.dispatch<any>(setIsPlaying(true)));
  });

  TrackPlayer.addEventListener('remote-pause', async (event: any) => {
    console.log('[BG] Remote pause. Ev:', event);

    TrackPlayer.pause()
      .then(() => store.dispatch(setIsPlayingSync(false)))
      .catch(() => store.dispatch<any>(setIsPlaying(false)));
  });

  TrackPlayer.addEventListener('remote-stop', async (event: any) => {
    console.log('[BG] Remote stop. Ev:', event);

    TrackPlayer.stop()
      .then(() => TrackPlayer.destroy())
      .then(() => store.dispatch(setIsPlayingSync(false)));
  });

  TrackPlayer.addEventListener('remote-next', async (event: any) => {
    console.log('[BG] Remote next. Ev:', event);
    const state = store.getState();
    const {canPlayNext} = state.audioPlayer;

    if (canPlayNext) {
      TrackPlayer.skipToNext();
    }
  });

  TrackPlayer.addEventListener('remote-previous', async (event: any) => {
    console.log('[BG] Remote previous. Ev:', event);
    const state = store.getState();
    const {canPlayPrevious} = state.audioPlayer;

    // TODO: Seek back to 0 if position < 10.
    if (canPlayPrevious) {
      TrackPlayer.skipToPrevious();
    } else {
      TrackPlayer.seekTo(0);
    }
  });

  TrackPlayer.addEventListener('remote-seek', async (event: any) => {
    console.log('[BG] Remote seek. Ev:', event);

    TrackPlayer.seekTo(event.position);
  });

  TrackPlayer.addEventListener('remote-duck', async (event: any) => {
    console.log('[BG] Remote duck. Ev:', event);

    store.dispatch<any>(setIsPlaying(!event.paused));
  });
};
