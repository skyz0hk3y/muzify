import type {RootState} from '../store';

export const getAppearanceSettings = (state: RootState) =>
  state.settings.appearance;

export const getDownloadsSettings = (state: RootState) =>
  state.settings.downloads;

export const getStreamingSettings = (state: RootState) =>
  state.settings.streaming;
