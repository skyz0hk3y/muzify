import React, {FC, useCallback} from 'react';
import {StyleSheet, View, Text, SafeAreaView, Linking} from 'react-native';
import {
  DONATION_BTC_ADDRESS,
  DONATION_BTC_COLOR,
  DONATION_LIBERAPAY_COLOR,
  DONATION_LIBERAPAY_HANDLE,
  DONATION_LIBERAPAY_URL,
} from '../../constants';
import {Button} from '../../core/components/Button';
import {Caption} from '../../core/components/Caption';
import {TouchablePlatform} from '../../core/components/TouchablePlatform';
import {useTheme} from '../../core/hooks/useTheme';

import {useTranslation} from '../../i18n';

const BTC_DONATE_URI = `bitcoin:${DONATION_BTC_ADDRESS}?message=Donation%20to%20Muzify&time=${Date.now()}`;
const BTC_BLOCKCHAIN_URL = `https://www.blockchain.com/btc/address/${DONATION_BTC_ADDRESS}`;

export type DonateScreenProps = {};

export const DonateScreen: FC<DonateScreenProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('core');

  const onDonateInBtcPressed = useCallback(async () => {
    if (await Linking.canOpenURL(BTC_DONATE_URI)) {
      await Linking.openURL(BTC_DONATE_URI);
    } else {
      await Linking.openURL(BTC_BLOCKCHAIN_URL);
    }
  }, []);

  const onDonateViaLiberaPayPressed = useCallback(async () => {
    const donationUrl = `${DONATION_LIBERAPAY_URL}/${DONATION_LIBERAPAY_HANDLE}/donate`;
    if (await Linking.canOpenURL(donationUrl)) {
      await Linking.openURL(donationUrl);
    }
  }, []);

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <Text style={[styles.subtitle, {color: theme.colors.text}]}>
              {t('settings:modules.donate.meta.subtitle')}
            </Text>
            <View style={styles.paymentProviders}>
              <Button
                containerStyle={styles.donationButton}
                fluid
                allCaps={false}
                text={'Donate in Bitcoin (BTC/XBT)'}
                onPress={onDonateInBtcPressed}
                backgroundColor={DONATION_BTC_COLOR}
                color={'#000000'}
                size={'large'}
              />
              <Button
                containerStyle={styles.donationButton}
                fluid
                allCaps={false}
                text={'Donate via LiberaPay (PayPal)'}
                onPress={onDonateViaLiberaPayPressed}
                backgroundColor={DONATION_LIBERAPAY_COLOR}
                color={'#000000'}
                size={'large'}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    width: '100%',
    alignItems: 'flex-start',
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  caption: {
    fontSize: 16,
    lineHeight: 16,
    fontWeight: '700',
    textTransform: 'uppercase',
  },
  subtitle: {
    fontSize: 20,
  },
  paymentProviders: {
    marginVertical: 16,
    flex: 1,
    width: '100%',
  },
  donationButton: {
    marginVertical: 8,
  },
});
