import React, {FC} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  SafeAreaView,
  Platform,
  Alert,
  ActivityIndicator,
  Linking,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useNetInfo} from '@react-native-community/netinfo';

import {Routes} from '../../constants';
import {useTranslation} from '../../i18n';

import {getChangelogForVersion} from '../../core/helpers/updates';
import {checkForAppUpdates, setNetworkState} from '../../core/actions';
import {ListItem} from '../../core/components/ListItem';
import {Divider} from '../../core/components/Divider';
import {Caption} from '../../core/components/Caption';
import {
  getCurrentVersionSelector,
  getIsOfflineSelector,
  getIsUpdateAvailableSelector,
  getLastUpdateCheckSelector,
  getNewVersionNameSelector,
  getUpdateDownloadProgressSelector,
  isCheckingForUpdatesSelector,
} from '../../core/selector';
import {IconButton} from '../../core/components/IconButton';
import {useTheme} from '../../core/hooks/useTheme';

export type SettingsEntryScreenProps = {};

export const SettingsEntryScreen: FC<SettingsEntryScreenProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('core');
  const {navigate} = useNavigation();

  const dispatch = useDispatch();
  const isOffline = useSelector(getIsOfflineSelector);
  const {isInternetReachable, type: connectionType} = useNetInfo();
  const currentVersion = useSelector(getCurrentVersionSelector);
  const newVersionName = useSelector(getNewVersionNameSelector);
  const lastUpdateCheck = useSelector(getLastUpdateCheckSelector);
  const isUpdateAvailable = useSelector(getIsUpdateAvailableSelector);
  const isCheckingForUpdates = useSelector(isCheckingForUpdatesSelector);
  const updateDownloadProgress = useSelector(getUpdateDownloadProgressSelector);

  const showUpdateAvailable =
    isUpdateAvailable &&
    !newVersionName?.includes('undefined') &&
    !updateDownloadProgress;

  const onOfflineModeToggle = () => {
    if (isOffline) {
      dispatch(
        setNetworkState({
          connectionType,
          isConnected:
            connectionType !== 'none' && connectionType !== 'unknown',
          isInternetReachable: isInternetReachable || false,
        }),
      );
    } else {
      dispatch(
        setNetworkState({
          isConnected: false,
          connectionType: 'none',
        }),
      );
    }
  };

  const onCheckUpdatePress = () => {
    dispatch(
      checkForAppUpdates(true, {
        meta: {
          cancel: t('core:captions.cancel'),
          updateNow: t('settings:captions.update-now'),
        },
        success: {
          title: t(
            'settings:modules.updates.check-for-updates.title.updates-available',
          ),
          subtitle: t(
            'settings:modules.updates.check-for-updates.do-u-want-to-update',
          ),
          releaseNotesTitle: t('settings:captions.release-notes'),
        },
        noUpdates: {
          title: '',
          subtitle: '',
        },
      }),
    );
  };

  const onReleaseNotesPress = async () => {
    const releaseNotes = await getChangelogForVersion(currentVersion);

    if (releaseNotes.error) {
      Alert.alert(
        `${t(
          'settings:modules.updates.release-notes.title',
        )} (v${currentVersion}${__DEV__ ? '-dev' : ''})`,
        releaseNotes.error.message,
      );

      return;
    }

    Alert.alert(
      `${t(
        'settings:modules.updates.release-notes.title',
      )} (v${currentVersion})`,
      releaseNotes,
    );
  };

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <ScrollView
            contentContainerStyle={styles.content}
            style={styles.scrollView}
          >
            <Caption style={styles.caption}>
              {t('settings:captions.general')}
            </Caption>
            {/*<ListItem
              onItemPress={() => navigate(Routes.SETTINGS_STATS)}
              iconName={'chart-donut'}
              title={t('settings:modules.stats.meta.title')}
              subtitle={t('settings:modules.stats.meta.subtitle')}
              actionIconName={'chevron-right'}
            />*/}
            <ListItem
              onItemPress={onOfflineModeToggle}
              iconName={'flash-circle'}
              actionIsSwitch
              actionSwitchChecked={isOffline}
              onActionSwitchChanged={onOfflineModeToggle}
              title={'Offline mode'}
            />
            <ListItem
              onItemPress={() => navigate(Routes.SETTINGS_APPEARANCE)}
              iconName={'theme-light-dark'}
              title={t('settings:modules.appearance.meta.title')}
              subtitle={t('settings:modules.appearance.meta.subtitle')}
              actionIconName={'chevron-right'}
            />
            <Divider />
            <Caption style={styles.caption}>
              {t('settings:captions.preferences')}
            </Caption>
            <ListItem
              onItemPress={() => navigate(Routes.SETTINGS_STREAMING)}
              iconName={'cloud-sync'}
              title={t('settings:modules.streaming.meta.title')}
              subtitle={t('settings:modules.streaming.meta.subtitle')}
              actionIconName={'chevron-right'}
            />
            <ListItem
              onItemPress={() => navigate(Routes.SETTINGS_DOWNLOADS)}
              iconName={'download'}
              title={t('settings:modules.downloads.meta.title')}
              subtitle={t('settings:modules.downloads.meta.subtitle')}
              actionIconName={'chevron-right'}
            />
            <Divider />
            <Caption style={styles.caption}>
              {t('settings:captions.updates')}
            </Caption>
            {Platform.OS === 'android' && (
              <ListItem
                onItemPress={onCheckUpdatePress}
                disabled={isCheckingForUpdates}
                iconName={'update'}
                actionIconName={
                  showUpdateAvailable ? 'cellphone-arrow-down' : undefined
                }
                actionComponent={
                  isCheckingForUpdates ? (
                    <IconButton disabled size={'normal'}>
                      <ActivityIndicator size={24} color={theme.colors.text} />
                    </IconButton>
                  ) : undefined
                }
                actionColor={showUpdateAvailable ? 'lime' : 'white'}
                title={
                  showUpdateAvailable
                    ? t(
                        'settings:modules.updates.check-for-updates.title.updates-available',
                      )
                    : t(
                        'settings:modules.updates.check-for-updates.title.default',
                      )
                }
                subtitle={t(
                  'settings:modules.updates.check-for-updates.subtitle',
                  {
                    time: lastUpdateCheck,
                  },
                )}
              />
            )}
            <ListItem
              onItemPress={onReleaseNotesPress}
              iconName={'delta'}
              title={t('settings:modules.updates.release-notes.title')}
              subtitle={t('settings:modules.updates.release-notes.subtitle', {
                version: currentVersion,
              })}
            />
            <Divider />
            <Caption style={styles.caption}>
              {t('settings:captions.contribute')}
            </Caption>
            <ListItem
              onItemPress={() => navigate(Routes.DONATE)}
              iconName={'hand-heart'}
              title={t('settings:modules.donate.meta.title')}
              subtitle={t('settings:modules.donate.meta.subtitle')}
            />
            <ListItem
              onItemPress={() =>
                Linking.openURL('https://gitlab.com/skyz0hk3y/muzify')
              }
              iconName={'dev-to'}
              title={'Code source'}
              subtitle={
                'Muzify est une application gratuite et open-source sous license libre GPLv3+'
              }
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#222222',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingVertical: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 16,
  },
});
