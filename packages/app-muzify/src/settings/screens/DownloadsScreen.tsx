import React, {FC, useCallback} from 'react';
import {StyleSheet, View, SafeAreaView, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import DialogAndroid from 'react-native-dialogs';

// import {RootState} from '../../store';
import {useTranslation} from '../../i18n';

import {ListItem} from '../../core/components/ListItem';
// import {Divider} from '../../Common/components/Divider';
import {Caption} from '../../core/components/Caption';
import {getDownloadsSettings} from '../selectors';
import {
  chooseDestinationFolder,
  setFileFormat,
  // setFlatFolderStructure,
} from '../actions';
import {useTheme} from '../../core/hooks/useTheme';
import {Divider} from '../../core/components/Divider';
import {
  setLikedTracks,
  setLikedTracksIds,
  setLocalTracksIds,
} from '../../library/actions';
import {setCurrentTrack, setRepeatTrackId} from '../../audio-player/actions';
import MMKVStorage from 'react-native-mmkv-storage';
import {persistStorage, resetStoresToInitialStates, store} from '../../store';

export type SettingsDownloadsScreenProps = {};

export const SettingsDownloadsScreen: FC<SettingsDownloadsScreenProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('core');
  const dispatch = useDispatch();
  const downloadSettings = useSelector(getDownloadsSettings);

  const onChooseFolderPress = () => {
    dispatch(chooseDestinationFolder());
  };

  const onChooseFormatPress = async () => {
    if (Platform.OS === 'ios') {
      return;
    }

    const {selectedItem} = await DialogAndroid.showPicker(
      t('settings:modules.downloads.file-format.title'),
      null,
      {
        items: [
          Platform.OS === 'android'
            ? {
                id: 'webm',
                label: 'WebM/Opus (.webm)',
                fileFormat: 'webm',
              }
            : null,
          {id: 'mp4', label: 'MP4 (.mp4)', fileFormat: 'mp4'},
        ],
      },
    );

    if (selectedItem) {
      dispatch(setFileFormat(selectedItem.fileFormat));
    }
  };

  // const onToggleFlatFolderPress = () => {
  //   dispatch(setFlatFolderStructure(!downloadSettings.flatFolderStructure));
  // };

  const onClearDownloadedTracksPress = useCallback(() => {
    dispatch(setLikedTracks([]));
    dispatch(setLikedTracksIds([]));
    dispatch(setLocalTracksIds([]));
    dispatch(setCurrentTrack(null));
    dispatch(setRepeatTrackId(null));
  }, [dispatch]);

  const onClearEverythingPress = useCallback(() => {
    const success = persistStorage.clearStore();
    if (success) {
      dispatch(resetStoresToInitialStates());
      console.log('[INFO] Cleaned the whole MMKV persist store.');
    }
  }, [dispatch]);

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <Caption style={styles.caption}>
              {t('settings:captions.storage')}
            </Caption>
            <ListItem
              onItemPress={onChooseFolderPress}
              iconName={'folder-music'}
              title={t('settings:modules.downloads.destination-folder.title')}
              subtitle={downloadSettings.destinationFolderPath}
              actionIconName={'open-in-new'}
            />
            <ListItem
              onItemPress={onChooseFormatPress}
              iconName={'paperclip'}
              title={t('settings:modules.downloads.file-format.title')}
              subtitle={downloadSettings.fileFormat}
            />

            {/* <Divider />
            <Caption style={styles.caption}>
              {t('settings:captions.behavior')}
            </Caption>
            <ListItem
              onItemPress={onToggleFlatFolderPress}
              iconName={'file-cabinet'}
              actionIsSwitch
              actionSwitchChecked={downloadSettings.flatFolderStructure}
              onActionSwitchChanged={onToggleFlatFolderPress}
              title={t('settings:modules.downloads.flat-folder.title')}
              subtitle={t('settings:modules.downloads.flat-folder.subtitle')}
            /> */}
            <Divider />
            <Caption style={styles.caption}>DANGER ZONE</Caption>
            <ListItem
              onItemPress={onClearDownloadedTracksPress}
              iconName={'trash-can-outline'}
              title={'Clear downloaded tracks'}
            />
            <ListItem
              onItemPress={onClearEverythingPress}
              iconName={'trash-can'}
              title={'Clear everything'}
            />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#222222',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingVertical: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 16,
  },
});
