import React, {FC} from 'react';
import {StyleSheet, View, SafeAreaView} from 'react-native';

import {useTranslation} from '../../i18n';

import {Caption} from '../../core/components/Caption';
import {ListItem} from '../../core/components/ListItem';
import {useTheme} from '../../core/hooks/useTheme';
import {useSelector} from 'react-redux';
import {getStreamingSettings} from '../selectors';

export type SettingsStreamingScreenProps = {};

export const SettingsStreamingScreen: FC<SettingsStreamingScreenProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('core');

  const {audioQuality} = useSelector(getStreamingSettings);

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <Caption style={styles.caption}>
              {t('settings:modules.streaming.meta.title')}
            </Caption>
            <ListItem
              onItemPress={() => {}}
              iconName={'quality-high'}
              title={t('settings:modules.streaming.audio-quality.title')}
              subtitle={audioQuality}
            />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#222222',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingVertical: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#A7A7A7',
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 16,
  },
});
