import React, {FC} from 'react';
import {StyleSheet, View, SafeAreaView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {useTranslation} from '../../i18n';

import {setTheme} from '../../core/actions';
import {Caption} from '../../core/components/Caption';
import {ListItem} from '../../core/components/ListItem';
import {useTheme} from '../../core/hooks/useTheme';
import {getThemeNameSelector} from '../../core/selector';

export type SettingsAppearanceScreenProps = {};

export const SettingsAppearanceScreen: FC<SettingsAppearanceScreenProps> = () => {
  const {theme} = useTheme();
  const {t} = useTranslation('core');
  const dispatch = useDispatch();
  const themeName = useSelector(getThemeNameSelector);

  const onUseDarkModePress = async () => {
    await dispatch(setTheme(themeName === 'dark' ? 'light' : 'dark'));
  };

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <Caption style={styles.caption}>
              {t('settings:captions.theme')}
            </Caption>
            <ListItem
              onItemPress={onUseDarkModePress}
              iconName={'theme-light-dark'}
              actionIsSwitch
              actionSwitchChecked={themeName === 'dark'}
              onActionSwitchChanged={onUseDarkModePress}
              title={t('settings:modules.appearance.dark-mode.title')}
            />
            {/* <ListItem
              onItemPress={() => {}}
              iconName={'palette'}
              title={t('settings:modules.appearance.primary-color.title')}
            /> */}
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingVertical: 8,
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#A7A7A7',
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 16,
  },
});
