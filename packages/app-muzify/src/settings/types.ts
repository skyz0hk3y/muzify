export const MODULE_NAME = 'settings';

export enum SettingsActions {
  SET_FLAT_FOLDER_STRUCTURE = 'settings/downloads/setFlatFolderStructure',
  SET_DESTINATION_FOLDER_PATH = 'settings/downloads/setDestinationFolderPath',
  SET_FILE_FORMAT = 'settings/downloads/setFileFormat',
}

export type SetFlatFolderStructureActionType = {
  type: SettingsActions.SET_FLAT_FOLDER_STRUCTURE;
  payload: boolean;
};

export type SetDestinationFolderPathActionType = {
  type: SettingsActions.SET_DESTINATION_FOLDER_PATH;
  payload: {
    destinationFolderPath: string;
  };
};

export type SetFileFormatActionType = {
  type: SettingsActions.SET_FILE_FORMAT;
  payload: {
    fileFormat: 'mp4' | 'webm';
  };
};

export type SettingsActionsType =
  | SetFlatFolderStructureActionType
  | SetDestinationFolderPathActionType
  | SetFileFormatActionType;

export type SettingsStateType = {
  appearance: {
    darkMode: boolean;
    primaryColor: string;
  };
  streaming: {
    // audioCodec: 'mp4' | 'webm';
    audioQuality: 'low' | 'medium';
  };
  downloads: {
    destinationFolderPath: string;
    fileFormat: 'mp4' | 'webm';
    flatFolderStructure: boolean;
  };
};
