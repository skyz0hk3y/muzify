import {Dispatch} from 'redux';
// import {default as FileSelector} from 'react-native-file-selector'; // install
// import {default as FileSystem} from 'react-native-fs';

import {
  SetDestinationFolderPathActionType,
  SetFlatFolderStructureActionType,
  SetFileFormatActionType,
  SettingsActions,
} from './types';

/* Actions */

export const setFlatFolderStructure = (
  enabled: boolean,
): SetFlatFolderStructureActionType => ({
  type: SettingsActions.SET_FLAT_FOLDER_STRUCTURE,
  payload: enabled,
});

export const setDestinationFolderPath = (
  destinationFolderPath: string,
): SetDestinationFolderPathActionType => ({
  type: SettingsActions.SET_DESTINATION_FOLDER_PATH,
  payload: {
    destinationFolderPath,
  },
});

export const setFileFormat = (
  fileFormat: 'mp4' | 'webm',
): SetFileFormatActionType => ({
  type: SettingsActions.SET_FILE_FORMAT,
  payload: {
    fileFormat,
  },
});

/* Actions creator */

export const chooseDestinationFolder = () => {
  return async (_dispatch: Dispatch<SetDestinationFolderPathActionType>) => {
    // FileSelector.Show({
    //   title: 'Choose a folder',
    //   chooseFolderMode: true,
    //   closeMenu: false,
    //   path: getLocalPath(),
    //   onDone: async (newDestinationFolderPath: string) => {
    //     dispatch(setDestinationFolderPath(newDestinationFolderPath));
    //   },
    // });
  };
};
