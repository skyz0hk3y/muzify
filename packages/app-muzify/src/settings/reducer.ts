import {Platform} from 'react-native';
import {getLocalPath} from '../core/helpers/getLocalPathForTrack';

import {
  MODULE_NAME,
  SettingsActions,
  SettingsActionsType,
  SettingsStateType,
} from './types';

const INITIAL_STATE: SettingsStateType = {
  appearance: {
    darkMode: true,
    primaryColor: '#00adcc',
  },
  streaming: {
    audioQuality: 'medium',
  },
  downloads: {
    destinationFolderPath: `${getLocalPath()}/Music/Muzify`,
    fileFormat: Platform.OS === 'ios' ? 'mp4' : 'webm',
    flatFolderStructure: false,
  },
};

export const getSettingsPersistConfig = (storage: any) => ({
  key: MODULE_NAME,
  storage: storage,
  whitelist: ['appearance', 'streaming', 'downloads'],
});

export const settingsReducer = (
  state = INITIAL_STATE,
  action: SettingsActionsType,
): SettingsStateType => {
  switch (action.type) {
    case SettingsActions.SET_FLAT_FOLDER_STRUCTURE: {
      return {
        ...state,
        downloads: {
          ...state.downloads,
          flatFolderStructure: action.payload,
        },
      };
    }
    case SettingsActions.SET_DESTINATION_FOLDER_PATH: {
      return {
        ...state,
        downloads: {
          ...state.downloads,
          destinationFolderPath: action.payload.destinationFolderPath,
        },
      };
    }
    case SettingsActions.SET_FILE_FORMAT: {
      return {
        ...state,
        downloads: {
          ...state.downloads,
          fileFormat: action.payload.fileFormat,
        },
      };
    }
    default:
      return state;
  }
};
