import {Track, PITCH_ALGORITHM_MUSIC} from 'react-native-track-player';
import {API_BASE_URL} from '../../constants';
import {getLocalPathForTrack} from '../../core/helpers/getLocalPathForTrack';
import {getStreamingUrlForTrackId} from '../../core/helpers/getStreamingUrlForTrackId';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';
import {AudioMeta} from '../../core/types';

const fetchAudioMeta = async (videoId: string): Promise<AudioMeta> => {
  const res = await fetch(
    `${API_BASE_URL}/stream/${encodeURIComponent(videoId)}`,
  );
  const json = await res.json();

  if (json.error) {
    return Promise.reject(json.error);
  }

  return Promise.resolve(json.data);
};

/**
 * An helper function that either returns the local track, if possible.
 * If not a local track, it fetches it from the API, or return null.
 *
 * @param {Track[]} localTracks An array of locally stored tracks.
 * @param {string[]} localTracksIds An array of locally stored tracks ids.
 * @returns {Function} Returns a function that given a `trackId` either returns a Track for that trackId, or null.
 */
export const getTrack = (
  localTracks: Track[],
  localTracksIds: string[],
) => async (trackId: string): Promise<null | Track> => {
  try {
    let track: Partial<Track> = {
      id: trackId,
      rating: false,
      pitchAlgorithm: PITCH_ALGORITHM_MUSIC,
    };

    const isLocalTrack = localTracksIds.includes(trackId);
    const localTrack = localTracks.find((t) => t.id === trackId);
    if (isLocalTrack && localTrack) {
      // Track exists locally on user device, play from there.
      track = {
        ...track,
        ...localTrack,
        id: trackId,
        url: 'file:///' + getLocalPathForTrack(localTrack),
      };
    } else {
      // Track isnt local, play from network, if possible.
      const trackData = await fetchAudioMeta(trackId);
      track = {
        ...track,
        id: trackId,
        title: trackData.title,
        artist: trackData.author.name,
        duration: trackData.duration?.seconds || 0,
        artwork: trackData.cover.url,
        url: getStreamingUrlForTrackId(trackId),
      };
    }

    return track as Track;
  } catch (err) {
    return null;
  }
};
