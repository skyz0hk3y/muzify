import {Track} from 'react-native-track-player';
import {RootState} from '../store';
import {AudioPlayerRepeatMode} from './types';

export const getCurrentTrackIndexSelector = (state: RootState): number => {
  return state.audioPlayer.currentTrackIndex;
};

export const getCurrentTrackSelector = (state: RootState): Track | null => {
  return state.audioPlayer.currentTrack;
};

export const getQueueSelector = (state: RootState): Track[] => {
  return state.audioPlayer.queue;
};

export const getTrackFromQueueSelector = (trackIndexInQueue: number) => (
  state: RootState,
): Track | null => {
  return state.audioPlayer.queue[trackIndexInQueue];
};

export const getIsTrackInQueueSelector = (state: RootState) => (
  trackId: string,
): boolean => {
  return !!state.audioPlayer.queue.find((track) => track.id === trackId);
};

export const getIsCurrentPlaylistSelector = (state: RootState) => (
  playlistId: string,
): boolean => {
  return state.audioPlayer.playlistId === playlistId;
};

export const getCanPlayNextSelector = (state: RootState) => {
  return state.audioPlayer.canPlayNext;
};

export const getCanPlayPreviousSelector = (state: RootState) => {
  return state.audioPlayer.canPlayPrevious;
};

export const getIsCurrentTrackSelector = (state: RootState) => (
  trackId: string,
): boolean => {
  return state.audioPlayer.currentTrack?.id === trackId;
};

export const getCurrentPlaylistIdSelector = (state: RootState): string | null =>
  state.audioPlayer.playlistId;

export const getIsTrackLoading = (state: RootState) => {
  return state.audioPlayer.loadingTrack;
};

export const getCurrentTrackDuration = (state: RootState): number => {
  return state.audioPlayer.duration;
};

export const getCurrentTrackPosition = (state: RootState): number => {
  return state.audioPlayer.position;
};

export const getIsPlayingSelector = (state: RootState): boolean => {
  return state.audioPlayer.isPlaying;
};

export const getRepeatModeSelector = (
  state: RootState,
): AudioPlayerRepeatMode => {
  return state.audioPlayer.repeatMode;
};
