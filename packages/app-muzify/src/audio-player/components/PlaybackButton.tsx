import React, {FC} from 'react';
import {Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export type PlaybackButtonProps = {
  isPlaying: boolean;
  iconSize?: number;
};

export const PlaybackButton: FC<PlaybackButtonProps> = ({
  isPlaying,
  iconSize = 24,
}) => {
  if (!isPlaying) {
    return (
      <Text style={styles.text}>
        <Icon size={iconSize} name="play" />
      </Text>
    );
  }

  return (
    <Text style={styles.text}>
      <Icon size={iconSize} name="pause" />
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#ffffff',
  },
});
