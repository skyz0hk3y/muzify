import React, {FC, useState} from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {IconButton} from '../../core/components/IconButton';
import {IconButtonBis} from '../../core/components/IconButtonBis';
import {useTheme} from '../../core/hooks/useTheme';
import {PlaybackButton} from './PlaybackButton';

export type PlaybackControlsProps = {
  size?: 'small' | 'large';
  style?: StyleProp<ViewStyle>;
  canPlayNext: boolean;
  canPlayPrevious: boolean;
  isPlaying: boolean;
  withNativeResponder?: boolean;
  iconColor?: string;
  iconMutedColor?: string;
  onSkipToNextPress: (event: GestureResponderEvent) => void;
  onSkipToPreviousPress: (event: GestureResponderEvent) => void;
  onTogglePlayPausePress: (event: GestureResponderEvent) => void;
};

export const PlaybackControls: FC<PlaybackControlsProps> = ({
  size = 'small',
  style = {},
  canPlayNext,
  canPlayPrevious,
  isPlaying,
  withNativeResponder = false,
  iconColor,
  iconMutedColor,
  onSkipToNextPress,
  onSkipToPreviousPress,
  onTogglePlayPausePress,
}) => {
  const {theme} = useTheme();
  const [lastPlayPausePress, setLastPlayPausePress] = useState(0);
  const [lastSkipNextPress, setLastSkipNextPress] = useState(0);
  const [lastSkipPreviousPress, setLastSkipPreviousPress] = useState(0);

  const IconButtonComponent = withNativeResponder ? IconButtonBis : IconButton;

  const skipButtonsSize = size === 'small' ? 'small' : 'normal';
  const playPauseButtonSize = size === 'small' ? 'normal' : 'large';
  const iconSize = size === 'small' ? 20 : 32;
  const playPauseIconSize = size === 'small' ? 24 : 36;

  const onSkipToNext = (event: GestureResponderEvent) => {
    const delta = new Date().getTime() - lastSkipNextPress;
    if (delta < 200) {
      return;
    }

    setLastSkipNextPress(new Date().getTime());
    onSkipToNextPress(event);
  };

  const onSkipToPrevious = (event: GestureResponderEvent) => {
    const delta = new Date().getTime() - lastSkipPreviousPress;
    if (delta < 200) {
      return;
    }

    setLastSkipPreviousPress(new Date().getTime());
    onSkipToPreviousPress(event);
  };

  const onTogglePlayPause = (event: GestureResponderEvent) => {
    const delta = new Date().getTime() - lastPlayPausePress;
    if (delta < 200) {
      return;
    }

    setLastPlayPausePress(new Date().getTime());
    onTogglePlayPausePress(event);
  };

  return (
    <View style={[styles.container, style]}>
      <IconButtonComponent
        size={skipButtonsSize}
        disabled={!canPlayPrevious}
        onPress={onSkipToPrevious}
        style={styles.prevButton}
      >
        <Icon
          size={iconSize}
          name="skip-previous"
          color={
            canPlayPrevious
              ? iconColor || theme.colors.text
              : iconMutedColor || theme.colors.textMuted
          }
        />
      </IconButtonComponent>
      <IconButtonComponent
        size={playPauseButtonSize}
        backgroundColor={theme.colors.primary}
        style={styles.playButton}
        onPress={onTogglePlayPause}
      >
        <PlaybackButton isPlaying={isPlaying} iconSize={playPauseIconSize} />
      </IconButtonComponent>
      <IconButtonComponent
        size={skipButtonsSize}
        disabled={!canPlayNext}
        onPress={onSkipToNext}
        style={styles.prevButton}
      >
        <Icon
          size={iconSize}
          name="skip-next"
          color={
            canPlayNext
              ? iconColor || theme.colors.text
              : iconMutedColor || theme.colors.textMuted
          }
        />
      </IconButtonComponent>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  prevButton: {
    marginLeft: 0,
  },
  playButton: {
    marginHorizontal: 8,
  },
  nextButton: {
    marginRight: 0,
  },
});
