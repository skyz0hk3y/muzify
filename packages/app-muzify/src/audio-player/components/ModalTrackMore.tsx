import {BottomTabNavigationEventMap} from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import {NavigationHelpers, ParamListBase} from '@react-navigation/native';
import React, {FC, useContext, useState} from 'react';
import {
  Alert,
  ImageBackground,
  Platform,
  Share,
  StyleSheet,
  Text,
  ToastAndroid,
  View,
} from 'react-native';
import {Track} from 'react-native-track-player';
import {useDispatch, useStore} from 'react-redux';
import DialogAndroid from 'react-native-dialogs';
import {default as RNFS} from 'react-native-fs';

import * as LibraryActions from '../../library/actions';

import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';
import {RootState} from '../../store';
import {i18n, useTranslation} from '../../i18n';

import {ListItem} from '../../core/components/ListItem';

import {TrackCover} from './TrackCover';
import {getLocalPathForTrack} from '../../core/helpers/getLocalPathForTrack';
import {getHumanSize} from '../../core/helpers/getHumanSize';

import {TrackMoreContext} from '../TrackMoreContext';
import {Routes} from '../../constants';

const DATE_LOCALES: Record<string, string> = {
  en: 'en-US',
  fr: 'fr-FR',
};

export const DIVIDER_HEIGHT = 16;

export type ModalTrackMoreProps = {
  navigation: NavigationHelpers<ParamListBase, BottomTabNavigationEventMap>;
  track: Track;
};

export const ModalTrackMore: FC<ModalTrackMoreProps> = ({
  navigation,
  track,
}) => {
  const {openTrackMoreForTrack} = useContext(TrackMoreContext);
  const {getState} = useStore();
  const {t} = useTranslation();

  const hqTrackCoverUrl =
    track?.artwork?.replace('hqdefault', 'hq720') || track?.thumbnail;
  const [trackCoverUrl, setTrackCoverUrl] = useState(hqTrackCoverUrl);
  const setLowQualityTrackCover = () => setTrackCoverUrl(track.artwork);

  const dispatch = useDispatch();

  const isLocalTrack = (trackId: string) => {
    const {
      library: {localTracksIds},
    }: RootState = getState();
    return localTracksIds.includes(trackId);
  };

  const onSharePress = async () => {
    const url = `https://youtu.be/${track.id}`;
    const safeTrack = sanitizeTrack(track);
    await Share.share({
      message: `${safeTrack.artist} - ${safeTrack.title}\n${url}`,
      url,
    });
  };

  const onAddToPlaylistPress = async () => {
    const {library}: RootState = getState();

    if (Platform.OS === 'ios') {
      Alert.alert('Not implemented yet...');
      // Alert.prompt(
      //   'Add to new playlist',
      //   'Enter a playlist name',
      //   createPlaylistCallback,
      // );

      return;
    }

    const {selectedItem} = await DialogAndroid.showPicker(
      t('core:captions.add-to-playlist').replace('...', ''),
      null,
      {
        items: [
          {
            label: t('core:captions.create-new-playlist'),
            id: 'create-playlist',
          },
          ...Object.values(library.playlists)
            .filter((playlist) => playlist.origin !== 'generated')
            .map((playlist) => ({
              label: playlist.name,
              id: playlist.id,
            })),
        ],
      },
    );
    if (selectedItem) {
      if (selectedItem.id === 'create-playlist') {
        openTrackMoreForTrack(undefined);
        navigation.navigate(Routes.LIBRARY, {
          screen: Routes.LIBRARY_CREATE_PLAYLIST,
          params: {
            tracks: [track],
          },
        });
      } else {
        dispatch(LibraryActions.addTrackToPlaylist(selectedItem.id, track.id));
        const message = t('library:playlists.add-track.success', {
          trackTitle: sanitizeTrack(track).title,
          trackArtist: sanitizeTrack(track).artist,
          playlistName: selectedItem.label,
        });

        ToastAndroid.showWithGravityAndOffset(
          message,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
          0,
          200,
        );
      }
    }
  };

  const onFileInfoPress = async () => {
    const localPath = getLocalPathForTrack(track);
    const stat = await RNFS.stat(localPath);
    const sanitizedTrack = sanitizeTrack(track);
    const title = `${sanitizedTrack.artist} - ${sanitizedTrack.title}`;
    const addedDate = new Date(stat.ctime).toLocaleString(
      DATE_LOCALES[i18n.language] || DATE_LOCALES.en,
    );
    const size = getHumanSize(parseInt(stat.size, 10));

    const details =
      `YouTube ID: ${track.id}\n\n` +
      `${t('library:trackDetails.addedOn')} ${addedDate}\n\n` +
      `${t('library:trackDetails.fileSize')} ${size}\n\n` +
      `${t('library:trackDetails.filePath')} ${localPath}`;

    Alert.alert(title, details, [
      {
        text: t('library:trackDetails.deleteTrackButton'),
        style: 'destructive',
        onPress: () => dispatch(LibraryActions.removeLikedTrack(track.id)),
      },
      {
        text: 'Ok',
        style: 'default',
      },
    ]);
  };

  return (
    <ImageBackground
      loadingIndicatorSource={{uri: track.artwork}}
      source={{uri: trackCoverUrl}}
      onError={setLowQualityTrackCover}
      fadeDuration={140}
      // source={{uri: track.artwork}}
      style={styles.backgroundWrapper}
      blurRadius={8}
      resizeMode={'cover'}
    >
      <View style={styles.swipeHandle} />
      <View style={styles.container}>
        <TrackCover
          key={`${track.id}-artwork-xx-large`}
          source={{uri: trackCoverUrl}}
          size={'xx-large'}
          elevation={12}
        />
        <Text numberOfLines={1} style={styles.trackArtist}>
          {
            sanitizeTrack({
              ...track,
              artist: track?.author?.name || track?.artist,
            }).artist
          }
        </Text>
        <Text numberOfLines={1} style={styles.trackTitle}>
          {
            sanitizeTrack({
              ...track,
              artist: track?.author?.name || track?.artist,
            }).title
          }
        </Text>
        <View style={{height: DIVIDER_HEIGHT}} />
        <ListItem
          withNativeResponder
          onItemPress={onSharePress}
          iconName={'share-variant'}
          iconColor={'#ffffff'}
          textColor={'#ffffff'}
          title={t('core:captions.share-track')}
        />
        <ListItem
          withNativeResponder
          onItemPress={onAddToPlaylistPress}
          iconName={'playlist-plus'}
          iconColor={'#ffffff'}
          textColor={'#ffffff'}
          title={t('core:captions.add-to-playlist')}
        />
        {isLocalTrack(track.id) && (
          <ListItem
            withNativeResponder
            onItemPress={onFileInfoPress}
            iconName={'information-outline'}
            iconColor={'#ffffff'}
            textColor={'#ffffff'}
            title={t('core:captions.local-track-file-info')}
          />
        )}
      </View>
    </ImageBackground>
  );
};

export default ModalTrackMore;

const styles = StyleSheet.create({
  backgroundWrapper: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: '#333333',
  },
  swipeHandle: {
    position: 'absolute',
    top: 16,
    width: 24,
    height: 4,

    alignSelf: 'center',

    backgroundColor: '#ffffff',
    borderRadius: 8,
    zIndex: 100,
  },
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flex: 1,
    padding: 16,
    paddingVertical: 64,
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  header: {
    position: 'absolute',
    top: 24,
    right: 16,
    left: 16,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  trackTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: '600',
    marginHorizontal: 24,
    marginTop: 4,
  },
  trackArtist: {
    color: 'rgba(255,255,255,.48)',
    fontSize: 18,
    fontWeight: '700',
    marginTop: 24,
  },
  playbackSlider: {
    marginVertical: 16,
    marginHorizontal: 16,
  },
  playbackControls: {},
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    marginTop: 44,
    alignItems: 'center',
    width: '100%',
  },
});
