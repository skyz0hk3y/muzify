import React, {FC} from 'react';
import {
  StyleProp,
  StyleSheet,
  View,
  Text,
  ViewStyle,
  Platform,
} from 'react-native';
import Slider from 'react-native-smooth-slider';
import {useColorHelpers} from '../../core/hooks/useColorHelpers';
import {useTheme} from '../../core/hooks/useTheme';

export type PlaybackSliderProps = {
  showHints?: boolean;
  bufferingProgress?: number;
  duration?: number;
  position?: number;
  style?: StyleProp<ViewStyle>;
  onPositionChangeRequest?: (newPosition: number) => void;
};

const secondsToTime = (initialSeconds: number) => {
  const secureSeconds = parseInt(`${initialSeconds}`, 10);
  const hours = Math.floor(secureSeconds / 3600);
  const minutes = Math.floor((secureSeconds - hours * 3600) / 60);
  const seconds = secureSeconds - hours * 3600 - minutes * 60;

  const hoursText = hours > 0 ? (hours < 10 ? `0${hours}:` : `${hours}:`) : '';
  const minutesText = minutes < 10 ? `0${minutes}` : minutes;
  const secondsText = seconds < 10 ? `0${seconds}` : seconds;

  return `${hoursText}${minutesText}:${secondsText}`;
};

export const PlaybackSlider: FC<PlaybackSliderProps> = ({
  showHints = false,
  bufferingProgress,
  duration = 100,
  position = 0,
  style = {},
  onPositionChangeRequest,
}) => {
  const {theme} = useTheme();
  const {lighten, darken} = useColorHelpers();

  const onSliderValueChanged = (value: number) => {
    onPositionChangeRequest?.(Platform.OS === 'ios' ? value / 2 : value);
  };

  return (
    <View style={[styles.container, style]}>
      {bufferingProgress != null && (
        <View
          style={{
            ...styles.bufferBar,
            width: `${bufferingProgress}%`,
          }}
        />
      )}
      <Slider
        animateTransitions={true}
        useNativeDriver={true}
        minimumTrackTintColor={theme.colors.primary}
        minimumValue={0}
        maximumValue={duration || 100}
        moveVelocityThreshold={240}
        value={
          position && position >= 0
            ? Platform.OS === 'ios'
              ? position * 2
              : position
            : 0 || 0
        }
        trackClickable
        trackStyle={styles.trackStyle}
        thumbTouchSize={{width: 44, height: 44}}
        thumbTintColor={lighten(theme.colors.primary, 33)}
        thumbStyle={styles.thumbStyle}
        onValueChange={onSliderValueChanged}
        style={styles.positionBar}
      />
      {showHints && (
        <View style={styles.hints}>
          <Text style={styles.textHint}>
            {position ? secondsToTime(position) : '00:00'}
          </Text>
          <Text style={styles.textHint}>
            {duration ? secondsToTime(duration) : '00:00'}
          </Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 4,
    maxHeight: 4,
    width: '100%',
    backgroundColor: '#555555',
  },
  bufferBar: {
    position: 'relative',
    top: 0,
    left: 0,
    height: 4,
    backgroundColor: '#888888',
  },
  positionBar: {
    position: 'relative',
    top: -4,
    left: 0,
    height: 4,
    backgroundColor: 'transparent',
  },
  trackStyle: {
    backgroundColor: 'transparent',
  },
  thumbStyle: {
    width: 20,
    height: 20,
    borderRadius: 20,
  },
  hints: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  textHint: {
    color: '#aaa',
  },
});
