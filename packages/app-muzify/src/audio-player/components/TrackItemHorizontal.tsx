import React, {FC, useMemo} from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';
import {Track} from 'react-native-track-player';

import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';
import {TouchablePlatform} from '../../core/components/TouchablePlatform';
import {TrackCover, TrackCoverSize} from './TrackCover';
import {useTheme} from '../../core/hooks/useTheme';
import {TRACK_ITEM_HORIZONTAL_WIDTH} from '../../constants';

export type TrackItemHorizontalProps = {
  track: Track;
  disabled?: boolean;
  isLoading?: boolean;
  isLocal?: boolean;
  isCurrentTrack?: boolean;
  isDownloading?: boolean;
  showLabels?: boolean;
  coverSize?: TrackCoverSize;
  onItemPress?: (event: GestureResponderEvent) => void;
  onItemLongPress?: (event: GestureResponderEvent) => void;
};

export const TrackItemHorizontal: FC<TrackItemHorizontalProps> = ({
  track,
  disabled,
  isLoading = false,
  isLocal = false,
  isCurrentTrack = false,
  isDownloading = false,
  showLabels = true,
  coverSize = 'xx-large',
  onItemPress,
  onItemLongPress,
}) => {
  const {theme} = useTheme();

  const loadingStyle = {
    ...(isLoading && {opacity: 0.7}),
  };

  const itemStyle = {
    // ...(isLocal && {backgroundColor: 'rgba(0, 255, 0, 0.1)'}),
    ...(isCurrentTrack && {backgroundColor: theme.colors.backgrounds[4]}),
  };

  const sanitizedTrack = useMemo(
    () =>
      sanitizeTrack({
        ...track,
        artist: track?.author?.name || track?.artist || track?.artistName,
      }),
    [track],
  );

  return (
    <View key={track.id || track.videoId} style={styles.wrapper}>
      <View>
        <TouchablePlatform
          disabled={disabled}
          onPress={onItemPress}
          onLongPress={onItemLongPress}
          useForeground
          background={TouchableNativeFeedback.Ripple(
            theme.colors.primary,
            false,
          )}
        >
          <View style={[styles.itemWrapper, itemStyle]}>
            <TrackCover
              size={coverSize}
              source={{uri: track?.thumbnail || track?.artwork}}
              showLocalBadge={isLocal}
              isDownloading={isDownloading}
            />
            {showLabels && (
              <>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.artistNameText,
                    {color: theme.colors.textMuted},
                  ]}
                >
                  {sanitizedTrack.artist}
                </Text>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.trackTitleText,
                    loadingStyle,
                    {color: theme.colors.text},
                  ]}
                >
                  {sanitizedTrack.title}
                </Text>
              </>
            )}
          </View>
        </TouchablePlatform>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginRight: 4,
    minHeight: 96,
    borderRadius: 8,
    overflow: 'hidden',
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    minHeight: 96,
    maxWidth: TRACK_ITEM_HORIZONTAL_WIDTH,
    padding: 8,
    borderRadius: 8,
  },
  // textsContainer: {
  //   flex: 1,
  //   flexDirection: 'column',
  //   alignItems: 'flex-start',
  //   justifyContent: 'center',
  //   minHeight: 64,
  //   maxHeight: 80,
  // },
  artistNameText: {
    width: '100%',
    marginTop: 12,
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '700',
  },
  trackTitleText: {
    width: '100%',
    marginTop: 4,
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
  },
});
