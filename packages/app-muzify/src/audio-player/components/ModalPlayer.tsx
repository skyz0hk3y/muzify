import {FC, useMemo} from 'react';
import type {AudioPlayerRepeatMode} from '../types';
import type {MaterialDesignIconNameType} from '../../core/types';

import React, {useCallback, useRef, useState} from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import {
  ActivityIndicator,
  ImageBackground,
  ImageURISource,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as LibraryActions from '../../library/actions';
import {IconButtonBis} from '../../core/components/IconButtonBis';
import {
  getIsDownloadingTrackSelector,
  getIsLocalTrackSelector,
  getLikedTracksIdsSelector,
} from '../../library/selectors';

import * as PlayerActions from '../actions';
import {useAudioPlayer} from '../hooks/useAudioPlayer';
import {
  getCurrentPlaylistIdSelector,
  getIsCurrentTrackSelector,
  getQueueSelector,
  getRepeatModeSelector,
} from '../selectors';

import {TrackCover} from './TrackCover';
import {TrackItem} from './TrackItem';
import {PlaybackControls} from './PlaybackControls';
import {PlaybackSlider} from './PlaybackSlider';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';

const YT_HQ_QUALITY_LOW = 'hqdefault';
const YT_HQ_QUALITY_HIGH = 'hq720';

export type ModalPlayerProps = {
  bufferingProgress: number;
  duration: number;
  position: number;
  bottomSheetSize: number;
  onPositionChangeRequest: (newPosition: number) => void;
};

export const ModalPlayer: FC<ModalPlayerProps> = ({
  bufferingProgress = 0,
  duration = 0,
  position = 0,
  bottomSheetSize,
  onPositionChangeRequest,
}) => {
  const {width} = useWindowDimensions();

  const {
    playTrack,
    playNextTrack,
    playPreviousTrack,
    currentTrack,
    canPlayNext,
    canPlayPrevious,
    isPlaying,
  } = useAudioPlayer();

  const [playbackControlsHeight, setPlaybackControlsHeight] = useState(120);
  const scrollViewRef = useRef<ScrollView>();

  const audioPlayerQueue = useSelector(getQueueSelector);
  const currentPlaylistId = useSelector(getCurrentPlaylistIdSelector);
  const likedTracksIds = useSelector(getLikedTracksIdsSelector);
  const repeatMode = useSelector(getRepeatModeSelector);

  const dispatch = useDispatch();
  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);

  const safeCurrentTrack = useMemo(() => sanitizeTrack(currentTrack), [
    currentTrack,
  ]);

  // Called whenever the 720p image cannot be found, and we need to fallback to
  // the default low-res image provided by the API.
  // const setLowQualityTrackCover = useCallback(
  //   () => setTrackCoverUrl(currentTrack.artwork),
  //   [currentTrack.artwork],
  // );

  const getTrackIcon = useCallback(
    (trackId: string) => {
      return likedTracksIds.includes(trackId) ? 'heart' : 'heart-outline';
    },
    [likedTracksIds],
  );

  const getTrackIconColor = useCallback(
    (trackId: string) => {
      return likedTracksIds.includes(trackId) ? 'red' : 'white';
    },
    [likedTracksIds],
  );

  const getRepeatModeIcon = useCallback(
    (mode: AudioPlayerRepeatMode): string => {
      const modeToIconMap: Record<AudioPlayerRepeatMode, string> = {
        'no-repeat': 'repeat-off',
        'repeat-current': 'repeat-once',
        'repeat-queue': 'repeat',
      };

      return modeToIconMap[mode];
    },
    [],
  );

  const getRepeatModeIconColor = useCallback(
    (mode: AudioPlayerRepeatMode): string => {
      const modeToIconColorMap: Record<AudioPlayerRepeatMode, string> = {
        'no-repeat': '#888888',
        'repeat-current': '#00adcc',
        'repeat-queue': '#00adcc',
      };

      return modeToIconColorMap[mode];
    },
    [],
  );

  const onTogglePlayPausePress = useCallback(() => {
    dispatch(PlayerActions.setIsPlaying(!isPlaying));
  }, [dispatch, isPlaying]);

  const onTrackFavoriteToggle = useCallback(
    (trackId: string) => {
      if (!likedTracksIds.includes(trackId)) {
        dispatch(LibraryActions.addLikedTrack(trackId));
      } else {
        dispatch(LibraryActions.removeLikedTrack(trackId));
      }
    },
    [dispatch, likedTracksIds],
  );

  const onRepeatModeCyclePress = useCallback(() => {
    const modeToNextModeMap: Record<
      AudioPlayerRepeatMode,
      AudioPlayerRepeatMode
    > = {
      'no-repeat': 'repeat-current',
      'repeat-current': 'repeat-queue',
      'repeat-queue': 'no-repeat',
    };

    const nextMode = modeToNextModeMap[repeatMode];

    if (nextMode !== 'repeat-current') {
      dispatch(PlayerActions.setRepeatMode(nextMode));
    } else {
      dispatch(PlayerActions.setRepeatMode(nextMode, currentTrack.id));
    }
  }, [repeatMode, dispatch, currentTrack.id]);

  const onShowPlayerPress = useCallback(() => {
    scrollViewRef?.current.scrollTo({x: 0, y: 0, animated: true});
  }, []);

  const onShowQueuePress = useCallback(() => {
    scrollViewRef?.current.scrollToEnd({animated: true});
  }, []);

  const onTrackSelect = useCallback(
    async (trackId: string) => {
      await playTrack(
        trackId,
        currentPlaylistId || 'main-queue',
        audioPlayerQueue,
      );
    },
    [audioPlayerQueue, currentPlaylistId, playTrack],
  );

  if (!currentTrack) {
    return (
      <View style={styles.modalWrapper}>
        <ActivityIndicator />
      </View>
    );
  }

  return (
    <ImageBackground
      key={`${currentTrack.id}-artwork`}
      loadingIndicatorSource={{uri: currentTrack.artwork}}
      source={
        currentTrack && currentTrack.artwork
          ? {
              uri: currentTrack.artwork.replace(
                YT_HQ_QUALITY_LOW,
                YT_HQ_QUALITY_HIGH,
              ),
            }
          : undefined
      }
      // onError={setLowQualityTrackCover}
      fadeDuration={140}
      style={styles.modalWrapper}
      blurRadius={8}
      resizeMode={'cover'}
    >
      <View style={styles.swipeHandle} />
      <View style={styles.container}>
        <ScrollView
          ref={scrollViewRef}
          pagingEnabled
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.pager}
        >
          <View
            testID="modal-player-entry"
            style={[
              styles.playerVerticalSection,
              styles.audioPlayerPage,
              {
                width,
                minWidth: width,
                maxWidth: width,
                height: bottomSheetSize - playbackControlsHeight,
              },
            ]}
          >
            <View style={styles.header}>
              <IconButtonBis size={'normal'} onPress={onShowQueuePress}>
                <Icon
                  name={'playlist-music-outline' as MaterialDesignIconNameType}
                  color={'#ffffff'}
                  size={24}
                />
              </IconButtonBis>
            </View>
            <View style={styles.trackCover}>
              {currentTrack && currentTrack.artwork ? (
                <TrackCover
                  key={`${currentTrack.id}-artwork`}
                  source={{
                    uri: currentTrack.artwork.replace(
                      YT_HQ_QUALITY_LOW,
                      YT_HQ_QUALITY_HIGH,
                    ),
                  }}
                  size={'full'}
                  elevation={12}
                />
              ) : (
                <ActivityIndicator color={'#ffffff'} />
              )}
            </View>
            <Text numberOfLines={1} style={styles.trackArtist}>
              {safeCurrentTrack.artist || 'Loading...'}
            </Text>
            <Text numberOfLines={1} style={styles.trackTitle}>
              {safeCurrentTrack.title || 'Loading...'}
            </Text>
          </View>
          <View
            testID="modal-player-queue"
            style={[
              styles.playerVerticalSection,
              styles.currentQueuePage,
              {
                width,
                minWidth: width,
                maxWidth: width,
                height: bottomSheetSize - playbackControlsHeight,
              },
            ]}
          >
            <View style={styles.header}>
              <IconButtonBis size={'normal'} onPress={onShowPlayerPress}>
                <Icon
                  name={'disc-player' as MaterialDesignIconNameType}
                  color={'#ffffff'}
                  size={24}
                />
              </IconButtonBis>
            </View>
            <ScrollView
              contentContainerStyle={[
                styles.playerVerticalSection,
                //   styles.currentQueuePage,
                {
                  width,
                  minWidth: width,
                  maxWidth: width,
                },
              ]}
            >
              {audioPlayerQueue.map((queueTrack) => (
                <TrackItem
                  track={queueTrack}
                  key={queueTrack.id}
                  actionColor={'#ffffff'}
                  textColor={'#ffffff'}
                  isCurrentTrack={isCurrentTrack(queueTrack.id)}
                  isDownloading={isDownloadingTrack(queueTrack.id)}
                  isLocal={isLocalTrack(queueTrack.id)}
                  onItemPress={onTrackSelect.bind(
                    null,
                    queueTrack.id,
                    queueTrack,
                  )}
                />
              ))}
            </ScrollView>
          </View>
        </ScrollView>
        <View
          style={styles.playbackControls}
          onLayout={(event) =>
            setPlaybackControlsHeight(event.nativeEvent.layout.height)
          }
        >
          <PlaybackSlider
            showHints
            style={[
              styles.playbackSlider,
              {width: width - 64}, // To account for padding (32px)
            ]}
            bufferingProgress={bufferingProgress}
            duration={duration}
            position={position}
            onPositionChangeRequest={onPositionChangeRequest}
          />
          <View style={styles.actions}>
            <IconButtonBis
              disabled // Unless fixed (playbackService commented code)
              size={'normal'}
              onPress={onRepeatModeCyclePress}
            >
              <Icon
                name={getRepeatModeIcon(repeatMode)}
                color={getRepeatModeIconColor(repeatMode)}
                size={24}
              />
            </IconButtonBis>
            <PlaybackControls
              withNativeResponder
              size={'large'}
              canPlayNext={canPlayNext}
              canPlayPrevious={canPlayPrevious}
              isPlaying={isPlaying}
              iconColor={'#ffffff'}
              iconMutedColor={'#999999'}
              onSkipToNextPress={playNextTrack}
              onSkipToPreviousPress={playPreviousTrack}
              onTogglePlayPausePress={onTogglePlayPausePress}
            />
            <IconButtonBis
              size={'normal'}
              onPress={onTrackFavoriteToggle.bind(null, currentTrack.id)}
            >
              <Icon
                name={getTrackIcon(currentTrack.id)}
                color={getTrackIconColor(currentTrack.id)}
                size={24}
              />
            </IconButtonBis>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

export default ModalPlayer;

const styles = StyleSheet.create({
  modalWrapper: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: '#333333',
  },
  swipeHandle: {
    position: 'absolute',
    top: 16,
    width: 24,
    height: 4,

    alignSelf: 'center',

    backgroundColor: '#ffffff',
    borderRadius: 8,
    zIndex: 100,
  },
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  header: {
    position: 'absolute',
    top: 8,
    right: 16,
    left: 8,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  trackCover: {
    marginTop: 40,
  },
  trackTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: '600',
    marginHorizontal: 24,
    marginTop: 4,
  },
  trackArtist: {
    color: 'rgba(255,255,255,.48)',
    fontSize: 18,
    fontWeight: '700',
    marginTop: 24,
  },
  playbackControls: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginHorizontal: 32,
  },
  playbackSlider: {
    marginTop: 32,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 16,
    paddingBottom: 32,
    alignItems: 'center',
    width: '90%',
  },
  playerVerticalSection: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  pager: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  audioPlayerPage: {
    flex: 0.3,
    paddingHorizontal: 24,
    justifyContent: 'center',
    // flex: 1,
  },
  currentQueuePage: {
    flex: 1,
    // padding: 16,
    paddingTop: 48,
  },
});
