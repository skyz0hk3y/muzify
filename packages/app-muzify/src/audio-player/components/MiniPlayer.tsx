import React, {FC, useMemo} from 'react';
import {StyleSheet, View, Text, GestureResponderEvent} from 'react-native';
import {useSelector} from 'react-redux';

import {TrackCover} from './TrackCover';
import {TouchablePlatform} from '../../core/components/TouchablePlatform';
import {PlaybackControls} from './PlaybackControls';
import {PlaybackSlider} from './PlaybackSlider';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';
import {useTheme} from '../../core/hooks/useTheme';
import {getIsTrackLoading} from '../selectors';
import {useAudioPlayer} from '../hooks/useAudioPlayer';

const noop = () => null;

export type MiniPlayerProps = {
  style?: Record<string, any>;
  bufferingProgress?: number;
  trackDuration?: number;
  trackPosition?: number;
  onTogglePlayPausePress?: (event: GestureResponderEvent) => void;
  onPress?: (event: GestureResponderEvent) => void;
  onSeekbarValueChanged?: (position: number) => void;
};

export const MiniPlayer: FC<MiniPlayerProps> = ({
  style = {},
  bufferingProgress = 0,
  trackDuration = 0,
  trackPosition = 0,
  onPress = noop,
  onTogglePlayPausePress = noop,
  onSeekbarValueChanged = noop,
}) => {
  const {theme} = useTheme();
  const isTrackLoading = useSelector(getIsTrackLoading);

  const {
    playNextTrack,
    playPreviousTrack,
    isPlaying,
    canPlayNext,
    canPlayPrevious,
    currentTrack,
  } = useAudioPlayer();

  const safeTrack = useMemo(() => sanitizeTrack(currentTrack), [currentTrack]);

  return (
    <View
      style={[
        styles.playerBottomBar,
        style,
        {backgroundColor: theme.colors.backgrounds[1]},
      ]}
    >
      <PlaybackSlider
        style={styles.playerProgress}
        bufferingProgress={bufferingProgress}
        duration={trackDuration}
        position={trackPosition}
        onPositionChangeRequest={onSeekbarValueChanged}
      />
      <TouchablePlatform onPress={onPress}>
        <View style={styles.playerAudioItem}>
          {safeTrack && (
            <TrackCover
              key={`${safeTrack.id}-artwork`}
              source={{uri: safeTrack.artwork}}
              size={'small'}
              isDownloading={isTrackLoading}
            />
          )}
          <View style={styles.playerTextView}>
            <Text
              key={`${safeTrack.id}-artist`}
              style={[
                styles.playerAudioArtist,
                {
                  color: theme.colors.textMuted,
                },
              ]}
              numberOfLines={1}
            >
              {safeTrack.artist}
            </Text>
            <Text
              key={`${currentTrack?.id || 'unknown'}-title`}
              style={[
                styles.playerAudioTitle,
                {
                  color: isTrackLoading
                    ? theme.colors.textMuted
                    : theme.colors.text,
                },
              ]}
              numberOfLines={1}
            >
              {safeTrack.title}
            </Text>
          </View>
          <PlaybackControls
            canPlayNext={canPlayNext}
            canPlayPrevious={canPlayPrevious}
            isPlaying={isPlaying}
            onSkipToNextPress={playNextTrack}
            onSkipToPreviousPress={playPreviousTrack}
            onTogglePlayPausePress={onTogglePlayPausePress}
          />
        </View>
      </TouchablePlatform>
    </View>
  );
};

const styles = StyleSheet.create({
  loader: {
    width: 44,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  playerBottomBar: {
    flexBasis: 0.2,
    height: 64,
  },
  playerProgress: {
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
  },
  playerAudioItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  playerAudioCover: {
    width: 44,
    maxWidth: 44,
    height: 44,
    maxHeight: 44,
    backgroundColor: '#555555',
    borderRadius: 8,
  },
  playerAudioArtist: {
    marginLeft: 12,
    marginRight: 12,
    flex: 1,
    fontSize: 12,
    fontWeight: '700',
    color: '#666666',
  },
  playerAudioTitle: {
    marginLeft: 12,
    marginRight: 12,
    flex: 1,
    fontSize: 16,
    lineHeight: 18,
    fontWeight: '600',
    color: '#ffffff',
  },
  playerTextView: {
    flexDirection: 'column',
    flex: 1,
    minHeight: 44,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  playerActions: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  playBtn: {
    width: 44,
    height: 44,

    backgroundColor: '#00adcc',
    borderRadius: 44,

    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 24,
    fontWeight: '600',
  },
  playerActionButton: {
    width: 32,
    height: 32,

    backgroundColor: '#333333',
    borderRadius: 44,
    color: '#00adcc',

    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 24,
    fontWeight: '600',
  },
  playerActionButtonPrev: {
    marginLeft: 0,
  },
  playerActionButtonNext: {
    marginRight: 0,
  },
  playerActionButtonText: {
    color: '#ffffff',
  },
});
