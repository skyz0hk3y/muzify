import React, {FC, useCallback, useEffect, useRef} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {Track} from 'react-native-track-player';
import {useSelector} from 'react-redux';
import {TRACK_ITEM_HORIZONTAL_WIDTH} from '../../constants';
import {
  getIsDownloadingTrackSelector,
  getIsLocalTrackSelector,
} from '../../library/selectors';

import {useCurrentTrackAutoScroll} from '../hooks/useCurrentTrackAutoScroll';
import {
  getIsCurrentPlaylistSelector,
  getIsCurrentTrackSelector,
} from '../selectors';

import {TrackItemHorizontal} from './TrackItemHorizontal';

export type TracksListHorizontalProps = {
  id: string;
  tracks: Track[];
  showLabels?: boolean;
  tracksDisabled?: boolean;
  onTrackSelect?: (trackId: string, tracks: Track[]) => void | Promise<void>;
  onTrackMorePress?: (track: Track) => void | Promise<void>;
};

export const TracksListHorizontal: FC<TracksListHorizontalProps> = ({
  id,
  tracks,
  showLabels,
  tracksDisabled = false,
  onTrackSelect = () => undefined,
  onTrackMorePress = () => undefined,
}) => {
  const flatlistRef = useRef<FlatList<Track>>(null);
  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isCurrentPlaylist = useSelector(getIsCurrentPlaylistSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);

  const renderItem = useCallback(
    ({item}) => (
      <TrackItemHorizontal
        key={`${id}-tracks-${item.id}`}
        track={item}
        disabled={tracksDisabled}
        isCurrentTrack={isCurrentPlaylist(id) && isCurrentTrack(item.id)}
        isDownloading={isDownloadingTrack(item.id)}
        isLocal={isLocalTrack(item.id)}
        onItemPress={() => onTrackSelect(item.id, tracks)}
        onItemLongPress={() => onTrackMorePress(item)}
        showLabels={showLabels}
      />
    ),
    [
      id,
      isCurrentPlaylist,
      isCurrentTrack,
      isDownloadingTrack,
      isLocalTrack,
      onTrackMorePress,
      onTrackSelect,
      showLabels,
      tracks,
      tracksDisabled,
    ],
  );

  // Handles auto-scroll when tracks changes
  useCurrentTrackAutoScroll(tracks, id, (index, animated) => {
    if (flatlistRef.current) {
      // alert('HEY,Playlist,' + index + ',animated=' + animated);
      flatlistRef.current.scrollToIndex({
        animated,
        index,
        viewPosition: 0.5,
      });
    }
  });

  return (
    <View style={styles.container}>
      <FlatList
        ref={flatlistRef}
        style={styles.flatlist}
        contentContainerStyle={styles.contentContainer}
        horizontal
        contentInsetAdjustmentBehavior={'automatic'}
        nestedScrollEnabled={true}
        maxToRenderPerBatch={4}
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        onScrollToIndexFailed={(_info) => null}
        getItemLayout={(_, index) => ({
          index,
          length: TRACK_ITEM_HORIZONTAL_WIDTH, // 8px margins / 2
          offset: 0,
        })}
        keyExtractor={(item, index) => `${item.id}-${index}`}
        data={tracks}
        renderItem={renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  flatlist: {
    // paddingHorizontal: 8,
  },
  contentContainer: {
    paddingHorizontal: 8,
  },
});
