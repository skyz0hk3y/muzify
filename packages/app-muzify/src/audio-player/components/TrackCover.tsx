import React, {FC, useEffect, useMemo, useState} from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  ImageStyle,
  ImageURISource,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '../../core/hooks/useTheme';
import {MaterialDesignIconNameType} from '../../core/types';

export type TrackCoverSize =
  | 'small'
  | 'normal'
  | 'large'
  | 'x-large'
  | 'xx-large'
  | 'xxx-large'
  | 'full';

export type TrackCoverProps = {
  source: ImageURISource;
  size?: TrackCoverSize;
  elevation?: number;
  showLocalBadge?: boolean;
  isDownloading?: boolean;
};

const sizesMap = {
  small: 44,
  normal: 56,
  large: 64,
  'x-large': 72,
  'xx-large': 96,
  'xxx-large': 120,
  full: 156,
};

export const TrackCover: FC<TrackCoverProps> = ({
  source,
  size = 'normal',
  elevation = 4,
  showLocalBadge = false,
  isDownloading = false,
}) => {
  const {theme} = useTheme();
  const [loaded, setLoaded] = useState(false);
  const [trackCoverUrl, setTrackCoverUrl] = useState(
    sizesMap[size] >= 96
      ? source.uri?.replace('hqdefault', 'hq720')
      : source.uri,
  );

  const setLowQualityTrackCover = () => {
    setTrackCoverUrl(source.uri);
  };

  const sizeStyle: StyleProp<ImageStyle> = useMemo(
    () => ({
      minWidth: sizesMap[size],
      width: sizesMap[size],
      maxWidth: sizesMap[size],
      minHeight: sizesMap[size],
      height: sizesMap[size],
      maxHeight: sizesMap[size],
    }),
    [size],
  );

  const elevationStyle: StyleProp<ViewStyle> = {
    shadowColor: '#000',
    shadowOffset: {width: 0, height: elevation},
    shadowOpacity: 0.24,
    shadowRadius: 8,
    elevation,
  };

  const downloadingStyle: StyleProp<ViewStyle> = {
    ...sizeStyle,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  };

  const imageStyles = useMemo(() => [styles.cover, sizeStyle], [sizeStyle]);

  const onImageLoaded = () => {
    setLoaded(true);
  };

  useEffect(() => {
    if (!loaded && size && source?.uri) {
      setTrackCoverUrl(
        sizesMap[size] >= 96
          ? source.uri?.replace('hqdefault', 'hq720')
          : source.uri,
      );
    }
  }, [loaded, size, source]);

  return (
    <View style={{...styles.container, ...elevationStyle}}>
      <ImageBackground
        key={source.uri}
        style={imageStyles}
        source={{uri: trackCoverUrl}}
        onError={setLowQualityTrackCover}
        onLoadEnd={onImageLoaded}
        width={sizesMap[size]}
        height={sizesMap[size]}
        resizeMode={'cover'}
        blurRadius={isDownloading ? 4 : 0}
      >
        {isDownloading && (
          <View style={downloadingStyle}>
            <ActivityIndicator color={'#ffffff'} size={sizesMap[size] * 0.7} />
          </View>
        )}
      </ImageBackground>
      {!isDownloading && showLocalBadge && (
        <View
          style={[styles.localBadge, {backgroundColor: theme.colors.primary}]}
        >
          <Icon
            name={'download-circle-outline' as MaterialDesignIconNameType}
            color={'#ffffff'}
            size={10}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    backgroundColor: '#555555',
    borderRadius: 8,
  },
  cover: {
    overflow: 'hidden',
    borderRadius: 8,
  },
  localBadge: {
    position: 'absolute',
    top: -4,
    left: -4,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#00adcc',
    borderRadius: 20,
    // elevation: 1,
  },
});
