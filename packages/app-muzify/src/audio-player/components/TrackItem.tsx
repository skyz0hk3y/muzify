import React, {FC, useMemo} from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';
import {Track} from 'react-native-track-player';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {TouchablePlatform} from '../../core/components/TouchablePlatform';
import {IconButtonBis as IconButton} from '../../core/components/IconButtonBis';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';

import {TrackCover} from './TrackCover';
import {useTheme} from '../../core/hooks/useTheme';
import {TRACK_ITEM_HEIGHT} from '../../constants';
import {MaterialDesignIconNameType} from '../../../../../packages/app-muzify/src/core/types';

export type TrackItemProps = {
  track: Track;
  disabled?: boolean;
  isLoading?: boolean;
  isLocal?: boolean;
  isCurrentTrack?: boolean;
  isDownloading?: boolean;
  textColor?: string;
  actionColor?: string;
  actionIconName?: MaterialDesignIconNameType;
  onItemPress?: (event: GestureResponderEvent) => void;
  onItemLongPress?: (event: GestureResponderEvent) => void;
  onActionPress?: (event: GestureResponderEvent) => void;
};

export const TrackItem: FC<TrackItemProps> = ({
  track,
  disabled,
  actionIconName,
  actionColor,
  textColor,
  isLoading = false,
  isLocal = false,
  isCurrentTrack = false,
  isDownloading = false,
  onItemPress,
  onItemLongPress,
  onActionPress,
}) => {
  const {theme} = useTheme();

  const safeTrack = useMemo(() => sanitizeTrack(track), [track]);

  const itemStyle = useMemo(
    () => ({
      // ...(isLocal && {backgroundColor: 'rgba(0, 255, 0, 0.1)'}),
      ...(isCurrentTrack && {
        backgroundColor: theme.colors.selected,
        opacity: 0.8,
      }),
    }),
    [isCurrentTrack, theme.colors.selected],
  );

  const trackTitleStyle = useMemo(
    () => ({
      ...(isLoading && {
        opacity: 0.68,
      }),
    }),
    [isLoading],
  );

  const touchableHitSlop = useMemo(
    () => ({bottom: 0, left: 0, right: actionIconName ? -56 : 0, top: 0}),
    [actionIconName],
  );

  return (
    <View style={styles.wrapper} key={track.videoId}>
      <TouchablePlatform
        withNativeResponder
        disabled={disabled}
        onPress={onItemPress}
        onLongPress={onItemLongPress}
        useForeground
        background={TouchableNativeFeedback.Ripple(theme.colors.primary, false)}
        // This avoids to trigger parent touchable on click on action icon
        hitSlop={touchableHitSlop}
      >
        <View style={[styles.itemWrapper, itemStyle]}>
          <TrackCover
            size={'small'}
            source={{uri: track?.thumbnail || track?.artwork}}
            showLocalBadge={isLocal}
            isDownloading={isDownloading}
          />
          <View style={styles.textsContainer}>
            <Text
              style={[
                styles.artistNameText,
                {color: textColor || theme.colors.textMuted},
              ]}
              numberOfLines={1}
            >
              {safeTrack.artist}
            </Text>
            <Text
              style={[
                styles.trackTitleText,
                trackTitleStyle,
                {color: textColor || theme.colors.text},
              ]}
              numberOfLines={2}
            >
              {safeTrack.title}
            </Text>
          </View>
          {actionIconName && (
            <View style={styles.actions}>
              <IconButton
                size={'normal'}
                disabled={disabled || typeof onActionPress === 'undefined'}
                color={actionColor || theme.colors.text}
                style={styles.actionButton}
                onPress={(event) => {
                  if (event) {
                    event.stopPropagation();
                    event.preventDefault();
                  }
                  onActionPress(event);
                }}
              >
                <Icon
                  size={24}
                  name={actionIconName}
                  color={actionColor || theme.colors.text}
                />
              </IconButton>
            </View>
          )}
        </View>
      </TouchablePlatform>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    height: TRACK_ITEM_HEIGHT,
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',

    minHeight: TRACK_ITEM_HEIGHT,

    paddingVertical: 0,
    paddingLeft: 16,
    paddingRight: 0,
  },
  actions: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionButton: {
    marginHorizontal: 8,
  },
  textsContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  artistNameText: {
    marginLeft: 16,
    marginBottom: 8,

    fontSize: 12,
    lineHeight: 12,
    fontWeight: '700',
    color: '#666666',
  },
  trackTitleText: {
    marginLeft: 16,

    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600',
    color: '#ffffff',
  },
});
