import {getUniqueListBy} from '../core/helpers/uniqListByKey';
import {
  AudioPlayerActions,
  AudioPlayerActionsType,
  AudioPlayerStateType,
  MODULE_NAME,
} from './types';

export const getAudioPlayerPersistConfig = (storage: any) => ({
  key: MODULE_NAME,
  storage: storage,
  whitelist: [
    'queue',
    'playlistId',
    'currentPlaylist',
    'currentTrack',
    'currentTrackIndex',
    'position',
    'duration',
  ],
  blacklist: ['repeatMode', 'repeatTrackId'], // Unless fixed (playbackService commented code)
});

const INITIAL_STATE: AudioPlayerStateType = {
  error: null,
  queue: [],

  currentTrackIndex: 0,
  currentTrack: null,
  currentPlaylist: null,

  playlistId: null,
  loadingTrack: false,

  position: 0,
  duration: 250, // default to average music duration

  isPlaying: false,
  isBuffering: false,

  canPlayNext: false,
  canPlayPrevious: false,

  repeatMode: 'no-repeat',
  repeatTrackId: undefined,
};

export const audioPlayerReducer = (
  state = INITIAL_STATE,
  action: AudioPlayerActionsType,
): AudioPlayerStateType => {
  switch (action.type) {
    case AudioPlayerActions.SET_QUEUE: {
      const {tracks, nextTrackIndex} = action.payload;

      if (nextTrackIndex != null) {
        return {
          ...state,
          queue: getUniqueListBy(tracks, 'id'),
          currentTrackIndex: nextTrackIndex,
        };
      }

      return {
        ...state,
        queue: getUniqueListBy(tracks, 'id'),
        currentTrackIndex: null,
      };
    }
    case AudioPlayerActions.SET_CURRENT_TRACK_IDX_QUEUE: {
      return {
        ...state,
        currentTrackIndex: action.payload,
      };
    }
    case AudioPlayerActions.PLAY_FROM_PLAYLIST: {
      return {
        ...state,
        playlistId: action.payload.playlistId,
      };
    }
    case AudioPlayerActions.SET_PLAYLIST_ID:
      return {
        ...state,
        playlistId: action.payload,
      };
    case AudioPlayerActions.SET_CURRENT_TRACK:
      return {
        ...state,
        currentTrack: action.payload.track,
      };
    case AudioPlayerActions.SET_POSITION:
      return {
        ...state,
        position: action.payload,
      };
    case AudioPlayerActions.SET_DURATION:
      return {
        ...state,
        duration: action.payload,
      };
    case AudioPlayerActions.SET_TRACK_LOADING:
      return {
        ...state,
        loadingTrack: action.payload,
      };
    case AudioPlayerActions.SET_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case AudioPlayerActions.SET_IS_PLAYING:
      return {
        ...state,
        isPlaying: action.payload,
      };
    case AudioPlayerActions.SET_IS_BUFFERING:
      return {
        ...state,
        isBuffering: action.payload,
      };
    case AudioPlayerActions.SET_CAN_PLAY_NEXT:
      return {
        ...state,
        canPlayNext: action.payload,
      };
    case AudioPlayerActions.SET_CAN_PLAY_PREVIOUS:
      return {
        ...state,
        canPlayPrevious: action.payload,
      };
    case AudioPlayerActions.SET_REPEAT_MODE:
      return {
        ...state,
        repeatMode: action.payload.mode,
        repeatTrackId:
          action.payload.mode === 'repeat-current'
            ? action.payload.trackId
            : undefined,
      };
    case AudioPlayerActions.SET_REPEAT_TRACK_ID:
      return {
        ...state,
        repeatTrackId: action.payload.trackId,
      };
    default:
      return state;
  }
};
