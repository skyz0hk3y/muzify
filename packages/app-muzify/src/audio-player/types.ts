import {Track} from 'react-native-track-player';
import {LibraryPlaylist} from '../library/types';

export const MODULE_NAME = 'audioPlayer';

export type TrackMeta = {
  videoId: string;
  title: string;
  duration: number;
  author: {
    id: string;
    name: string;
    avatar: {
      url: string;
      width: number;
      height: number;
    };
  };
  cover: {
    url: string;
    width: number;
    height: number;
  };
};

export type AudioPlayerRepeatMode =
  | 'no-repeat'
  | 'repeat-current'
  | 'repeat-queue';

/* State Type */
export type AudioPlayerStateType = {
  playlistId: string | null;

  queue: Track[];
  currentTrackIndex: number;

  currentPlaylist: LibraryPlaylist | null;
  currentTrack: Track | null;
  loadingTrack: boolean;

  position: number;
  duration: number;

  error: null | {
    code: string;
    message: string;
  };

  isPlaying: boolean;
  isBuffering: boolean;
  canPlayNext: boolean;
  canPlayPrevious: boolean;
  repeatMode: AudioPlayerRepeatMode;
  repeatTrackId?: string;
};

/* Actions Types */

export enum AudioPlayerActions {
  SET_QUEUE = 'audioPlayer/setQueue',
  SET_CURRENT_TRACK_IDX_QUEUE = 'audioPlayer/setCurrentTrackIndex',
  SET_PLAYLIST_ID = 'audioPlayer/setPlaylistId',
  SET_CURRENT_TRACK = 'audioPlayer/setCurrentTrack',
  SET_POSITION = 'audioPlayer/setPosition',
  SET_DURATION = 'audioPlayer/setDuration',
  SET_TRACK_LOADING = 'audioPlayer/setTrackLoading',
  SET_ERROR = 'audioPlayer/setError',
  SET_IS_PLAYING = 'audioPlayer/setIsPlaying',
  SET_IS_BUFFERING = 'audioPlayer/setIsBuffering',
  SET_CAN_PLAY_NEXT = 'audioPlayer/setCanPlayNext',
  SET_CAN_PLAY_PREVIOUS = 'audioPlayer/setCanPlayPrevious',
  SET_REPEAT_MODE = 'audioPlayer/setRepeatMode',
  SET_REPEAT_TRACK_ID = 'audioPlayer/setRepeatTrackId',
  PLAY_FROM_PLAYLIST = 'audioPlayer/playFromPlaylist',
}

export type AudioPlayerSetQueueActionType = {
  type: AudioPlayerActions.SET_QUEUE;
  payload: {
    tracks: Track[];
    nextTrackIndex?: number;
  };
};

export type AudioPlayerSetCurrentTrackIndexActionType = {
  type: AudioPlayerActions.SET_CURRENT_TRACK_IDX_QUEUE;
  payload: number;
};

export type PlayFromPlaylistActionType = {
  type: AudioPlayerActions.PLAY_FROM_PLAYLIST;
  payload: {
    playlistId: string;
    trackId: string;
  };
};

export type SetPlaylistIdType = {
  type: AudioPlayerActions.SET_PLAYLIST_ID;
  payload: string | null;
};

export type SetCurrentTrackActionType = {
  type: AudioPlayerActions.SET_CURRENT_TRACK;
  payload: {
    track: Track | null;
  };
};

export type SetPositionActionType = {
  type: AudioPlayerActions.SET_POSITION;
  payload: number;
};
export type SetDurationActionType = {
  type: AudioPlayerActions.SET_DURATION;
  payload: number;
};

export type SetTrackLoadingActionType = {
  type: AudioPlayerActions.SET_TRACK_LOADING;
  payload: boolean;
};

export type SetErrorActionType = {
  type: AudioPlayerActions.SET_ERROR;
  payload: null | {
    code: string;
    message: string;
  };
};

export type SetIsPlayingActionType = {
  type: AudioPlayerActions.SET_IS_PLAYING;
  payload: boolean;
};

export type SetIsBufferingActionType = {
  type: AudioPlayerActions.SET_IS_BUFFERING;
  payload: boolean;
};

export type SetCanPlayNextActionType = {
  type: AudioPlayerActions.SET_CAN_PLAY_NEXT;
  payload: boolean;
};

export type SetCanPlayPreviousActionType = {
  type: AudioPlayerActions.SET_CAN_PLAY_PREVIOUS;
  payload: boolean;
};

export type SetRepeatModeActionType = {
  type: AudioPlayerActions.SET_REPEAT_MODE;
  payload: {
    mode: AudioPlayerRepeatMode;
    trackId?: string;
  };
};

export type SetRepeatTrackIdActionType = {
  type: AudioPlayerActions.SET_REPEAT_TRACK_ID;
  payload: {
    trackId: string;
  };
};

export type AudioPlayerActionsType =
  | AudioPlayerSetQueueActionType
  | AudioPlayerSetCurrentTrackIndexActionType
  | PlayFromPlaylistActionType
  | SetPlaylistIdType
  | SetCurrentTrackActionType
  | SetPositionActionType
  | SetDurationActionType
  | SetTrackLoadingActionType
  | SetErrorActionType
  | SetIsPlayingActionType
  | SetIsBufferingActionType
  | SetCanPlayNextActionType
  | SetCanPlayPreviousActionType
  | SetRepeatModeActionType
  | SetRepeatTrackIdActionType;
