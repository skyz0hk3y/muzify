import {Dispatch} from 'redux';
import {default as TrackPlayer, Track} from 'react-native-track-player';

import {
  AudioPlayerActions,
  AudioPlayerRepeatMode,
  AudioPlayerSetCurrentTrackIndexActionType,
  AudioPlayerSetQueueActionType,
  SetCanPlayNextActionType,
  SetCanPlayPreviousActionType,
  SetCurrentTrackActionType,
  SetDurationActionType,
  SetErrorActionType,
  SetIsBufferingActionType,
  SetIsPlayingActionType,
  SetPlaylistIdType,
  SetPositionActionType,
  SetRepeatModeActionType,
  SetRepeatTrackIdActionType,
  SetTrackLoadingActionType,
} from './types';

import {API_BASE_URL, TRACK_PLAYER_CONTROLS_OPTS} from '../constants';
import {AudioMeta} from '../core/types';
import {getLocalPathForTrack} from '../core/helpers/getLocalPathForTrack';
import {sanitizeTrack} from '../core/helpers/sanitizeTrack';
import {RootState} from '../store';
import {getStreamingUrlForTrackId} from '../core/helpers/getStreamingUrlForTrackId';
import {getTrack} from './helpers/getTrack';

// const LIBRARY_FOLDER = `${BackgroundDownloader.directories.documents}/Library`;
// const LOCAL_EXT = Platform.OS === 'android' ? 'webm' : 'mp4';

/* Actions */

export const setQueue = (
  tracks: Track[],
  nextTrackIndex: number = 0,
): AudioPlayerSetQueueActionType => ({
  type: AudioPlayerActions.SET_QUEUE,
  payload: {
    tracks,
    nextTrackIndex,
  },
});

export const setCurrentTrackIndex = (
  nextTrackIndex: number,
): AudioPlayerSetCurrentTrackIndexActionType => ({
  type: AudioPlayerActions.SET_CURRENT_TRACK_IDX_QUEUE,
  payload: nextTrackIndex,
});

export const setPlaylistId = (playlistId: string): SetPlaylistIdType => ({
  type: AudioPlayerActions.SET_PLAYLIST_ID,
  payload: playlistId,
});

export const setCurrentTrack = (
  track: Track | null,
): SetCurrentTrackActionType => ({
  type: AudioPlayerActions.SET_CURRENT_TRACK,
  payload: {
    track,
  },
});

export const setPosition = (position: number): SetPositionActionType => ({
  type: AudioPlayerActions.SET_POSITION,
  payload: position,
});

export const setDuration = (duration: number): SetDurationActionType => ({
  type: AudioPlayerActions.SET_DURATION,
  payload: duration,
});

export const setTrackLoading = (
  isLoading: boolean,
): SetTrackLoadingActionType => ({
  type: AudioPlayerActions.SET_TRACK_LOADING,
  payload: isLoading,
});

export const setError = (
  error: null | {code: string; message: string},
): SetErrorActionType => ({
  type: AudioPlayerActions.SET_ERROR,
  payload: error,
});

export const setIsPlayingSync = (
  isPlaying: boolean,
): SetIsPlayingActionType => ({
  type: AudioPlayerActions.SET_IS_PLAYING,
  payload: isPlaying,
});

export const setIsBuffering = (
  isBuffering: boolean,
): SetIsBufferingActionType => ({
  type: AudioPlayerActions.SET_IS_BUFFERING,
  payload: isBuffering,
});

export const setCanPlayNext = (
  canPlayNext: boolean,
): SetCanPlayNextActionType => ({
  type: AudioPlayerActions.SET_CAN_PLAY_NEXT,
  payload: canPlayNext,
});

export const setCanPlayPrevious = (
  canPlayPrevious: boolean,
): SetCanPlayPreviousActionType => ({
  type: AudioPlayerActions.SET_CAN_PLAY_PREVIOUS,
  payload: canPlayPrevious,
});

export const setRepeatModeSync = (
  mode: AudioPlayerRepeatMode,
  trackId?: string,
): SetRepeatModeActionType => ({
  type: AudioPlayerActions.SET_REPEAT_MODE,
  payload: {
    mode,
    trackId,
  },
});

export const setRepeatTrackId = (
  trackId: string,
): SetRepeatTrackIdActionType => ({
  type: AudioPlayerActions.SET_REPEAT_TRACK_ID,
  payload: {
    trackId,
  },
});

/* Actions creators */

type SetIsPlayingActionDispatchType =
  | SetIsPlayingActionType
  | SetErrorActionType
  | SetTrackLoadingActionType;

export const setIsPlaying = (isPlaying: boolean) => {
  return async (
    dispatch: Dispatch<SetIsPlayingActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {currentTrack, currentPlaylist} = state.audioPlayer;
      const {tracks} = state.library;

      if (!currentTrack) {
        return;
      }

      const playerCurrentState = await TrackPlayer.getState();
      // FIRST, decide if we need to play/pause.
      if (
        isPlaying === true &&
        playerCurrentState !== TrackPlayer.STATE_PLAYING
      ) {
        await TrackPlayer.play();
      } else if (
        isPlaying === false &&
        playerCurrentState === TrackPlayer.STATE_PLAYING
      ) {
        await TrackPlayer.pause();
      }

      const playerCurrentTrack = await TrackPlayer.getCurrentTrack();
      const playerCurrentQueue = await TrackPlayer.getQueue();

      dispatch(setIsPlayingSync(isPlaying));

      // If no current track, or state is "invalid", reset
      if (
        playerCurrentTrack == null ||
        (playerCurrentState !== TrackPlayer.STATE_PLAYING &&
          playerCurrentState !== TrackPlayer.STATE_PAUSED)
      ) {
        await TrackPlayer.reset();

        // If currentTrack is associated to currentPlaylist, add playlist to queue, skip to track
        if (
          currentPlaylist != null &&
          currentPlaylist.tracksIds.includes(currentTrack.id) &&
          playerCurrentQueue.length !== currentPlaylist.tracksIds.length
        ) {
          const currentPlaylistTracks: Track[] = currentPlaylist.tracksIds
            .map(
              (trackId) => tracks.find((t: Track) => t.id === trackId) || null,
            )
            .filter((x) => !!x) as Track[];

          await TrackPlayer.add(currentPlaylistTracks);
          await TrackPlayer.skip(currentTrack.id);
        } else {
          await TrackPlayer.add(currentTrack);
        }
      }

      // Finally, decide if we need to play/pause.
      if (
        isPlaying === true &&
        playerCurrentState !== TrackPlayer.STATE_PLAYING
      ) {
        await TrackPlayer.play();
      } else if (
        isPlaying === false &&
        playerCurrentState === TrackPlayer.STATE_PLAYING
      ) {
        await TrackPlayer.pause();
      }
    } catch (err) {
      dispatch(setError(err.message));
    } finally {
      dispatch(setTrackLoading(false));
      dispatch<any>(updatePlaybackControls());
    }
  };
};

export const updatePlaybackControls = () => {
  return async (
    dispatch: Dispatch<SetCanPlayNextActionType | SetCanPlayPreviousActionType>,
    getState: () => RootState,
  ) => {
    const {audioPlayer} = getState();

    const trackId = await TrackPlayer.getCurrentTrack();
    const queue = await TrackPlayer.getQueue();

    const isInRepeatQueueMode = audioPlayer.repeatMode === 'repeat-queue';
    const trackIdx = queue.findIndex((t: Track) => t.id === trackId);
    const canPlayNext =
      isInRepeatQueueMode || (queue.length > 1 && trackIdx < queue.length - 1);
    const canPlayPrevious =
      isInRepeatQueueMode || (queue.length > 1 && trackIdx !== 0);

    dispatch(setCanPlayNext(canPlayNext));
    dispatch(setCanPlayPrevious(canPlayPrevious));
  };
};

export const skipToNext = () => {
  return async (
    dispatch: Dispatch<
      | SetTrackLoadingActionType
      | SetRepeatTrackIdActionType
      | SetCurrentTrackActionType
      | SetIsPlayingActionDispatchType
    >,
    getState: () => RootState,
  ) => {
    const state = getState();
    const {canPlayNext, repeatMode, currentTrack} = state.audioPlayer;

    if (canPlayNext) {
      const queue = await TrackPlayer.getQueue();
      const trackId = await TrackPlayer.getCurrentTrack();

      if (repeatMode === 'repeat-current' && currentTrack) {
        const currentTrackIdx = queue.findIndex(
          (t: Track) => t.id === currentTrack.id,
        );

        const nextTrack = queue[currentTrackIdx + 1];
        dispatch(setRepeatTrackId(nextTrack.id));
        dispatch(setCurrentTrack(nextTrack));
        await TrackPlayer.skipToNext();
      } else if (
        repeatMode === 'repeat-queue' &&
        currentTrack != null &&
        queue[queue.length - 1].id === currentTrack.id
      ) {
        const firstTrackId = queue[0].id;
        dispatch(setCurrentTrack(queue[0]));
        await TrackPlayer.skip(firstTrackId);
      } else {
        const nextTrackIdx = queue.findIndex((t) => t.id === trackId) + 1;
        const nextTrack = queue[nextTrackIdx];
        dispatch(setCurrentTrack(nextTrack));
        await TrackPlayer.skipToNext();
      }

      dispatch<any>(updatePlaybackControls());
    }
  };
};

export const skipToPrevious = () => {
  return async (
    dispatch: Dispatch<
      | SetTrackLoadingActionType
      | SetRepeatTrackIdActionType
      | SetCurrentTrackActionType
      | SetIsPlayingActionDispatchType
    >,
    getState: () => RootState,
  ) => {
    const state = getState();
    const {canPlayPrevious, repeatMode, currentTrack} = state.audioPlayer;

    const queue = await TrackPlayer.getQueue();
    const trackId = await TrackPlayer.getCurrentTrack();

    if (canPlayPrevious) {
      if (repeatMode === 'repeat-current' && currentTrack) {
        const currentTrackIdx = queue.findIndex(
          (t: Track) => t.id === currentTrack.id,
        );

        const prevTrack = queue[currentTrackIdx - 1];
        dispatch(setRepeatTrackId(prevTrack.id));
        dispatch(setCurrentTrack(prevTrack));
        await TrackPlayer.skipToPrevious();
      } else if (
        repeatMode === 'repeat-queue' &&
        currentTrack != null &&
        queue[0].id === currentTrack.id
      ) {
        const lastTrackId = queue[queue.length - 1].id;
        dispatch(setCurrentTrack(queue[queue.length - 1]));
        await TrackPlayer.skip(lastTrackId);
      } else {
        const prevTrackIdx = queue.findIndex((t) => t.id === trackId) + 1;
        const prevTrack = queue[prevTrackIdx];
        dispatch(setCurrentTrack(prevTrack));
        await TrackPlayer.skipToPrevious();
      }
    } else {
      await TrackPlayer.seekTo(0);
    }

    // dispatch<any>(setIsPlaying(true));
    dispatch<any>(updatePlaybackControls());
  };
};

export const seekToSeconds = (seconds: number) => {
  return TrackPlayer.seekTo(seconds);
};

// TODO: Move this logic into the action.
const fetchAudioMeta = async (videoId: string): Promise<AudioMeta> => {
  const res = await fetch(
    `${API_BASE_URL}/stream/${encodeURIComponent(videoId)}`,
  );
  const json = await res.json();

  if (json.error) {
    return Promise.reject(json.error);
  }

  return Promise.resolve<AudioMeta>(json.data);
};

type PlayTrackActionDispatchType =
  | SetTrackLoadingActionType
  | SetErrorActionType
  | SetCurrentTrackActionType
  | any; // TODO: Fix this type

export const playTrack = (trackId: string) => {
  return async (
    dispatch: Dispatch<PlayTrackActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {repeatMode} = state.audioPlayer;
      const {localTracksIds, tracks} = state.library;

      const isLocalTrack = (tId: string) => localTracksIds.includes(tId);

      dispatch(setTrackLoading(true));
      dispatch(setError(null));
      dispatch(setIsPlaying(false));

      let track: Track;
      if (isLocalTrack(trackId)) {
        const localTrack = tracks.find((t: Track) => t.id === trackId);

        // FIXME: This should validate file exists on disk too!
        if (!localTrack) {
          // TODO: Try to re-sync the track from API before throwing, if not offline
          throw new Error(
            'Cannot find local track but id is in localTracksIds. trackId=' +
              trackId,
          );
        }

        track = {
          ...localTrack,
          url: 'file:///' + getLocalPathForTrack(localTrack),
        };
      } else {
        const meta = await fetchAudioMeta(trackId);

        // Adds a track to the queue
        track = {
          id: meta.videoId,
          url: getStreamingUrlForTrackId(trackId),
          title: meta.title,
          artist: meta.author.name,
          artwork: meta.cover.url,
          rating: false,
          pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_MUSIC,
        };
      }

      if (repeatMode === 'repeat-current') {
        dispatch(setRepeatTrackId(track.id));
      }

      dispatch(setCurrentTrack(track));

      await TrackPlayer.stop();
      await TrackPlayer.add(sanitizeTrack(track));
      await TrackPlayer.skip(track.id);
      await TrackPlayer.play();
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: 'rate-limited',
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setTrackLoading(false));
      dispatch(updatePlaybackControls());
    }
  };
};

export const playTrackFromLibrary = (trackId: string) => {
  return async (
    dispatch: Dispatch<PlayMultipleTracksActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {repeatMode} = state.audioPlayer;
      const {localTracksIds, tracks} = state.library;
      const trackInLibrary = tracks.find((t: Track) => t.id === trackId);

      if (!localTracksIds.includes(trackId) || trackInLibrary == null) {
        dispatch<any>(playTrack(trackId));
        return;
      }

      dispatch(setTrackLoading(true));
      dispatch(setError(null));

      const localTracks: Track[] = tracks.map((track) => ({
        ...track,
        url: 'file:///' + getLocalPathForTrack(track),
      }));

      const track: Track = {
        ...trackInLibrary,
        url: 'file:///' + getLocalPathForTrack(trackInLibrary),
      };

      if (repeatMode === 'repeat-current') {
        dispatch(setRepeatTrackId(trackId));
      }

      dispatch(setCurrentTrack(track));

      try {
        await TrackPlayer.stop();
        await TrackPlayer.skip(trackId);
        await TrackPlayer.play();
      } catch (e) {
        const queue = await TrackPlayer.getQueue();
        const trackInQueue = queue.find((t: Track) => t.id === trackId) != null;
        if (!trackInQueue) {
          await TrackPlayer.reset();
          await TrackPlayer.updateOptions(TRACK_PLAYER_CONTROLS_OPTS);
          await TrackPlayer.add(localTracks);
          await TrackPlayer.skip(trackId);
          await TrackPlayer.play();
        }
      }
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: err.code,
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setTrackLoading(false));
      dispatch(updatePlaybackControls());
    }
  };
};

type PlayMultipleTracksActionDispatchType =
  | SetTrackLoadingActionType
  | SetErrorActionType
  | SetCurrentTrackActionType
  | SetCanPlayPreviousActionType
  | any; // TODO: Fix this type.

export const playMultipleTracks = (
  likedTracksIds: string[],
  prevTrackIds: string[] = [],
) => {
  return async (
    dispatch: Dispatch<PlayMultipleTracksActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {repeatMode} = state.audioPlayer;
      const {localTracksIds, tracks} = state.library;

      const isLocalTrack = (trackId: string) =>
        localTracksIds.includes(trackId);

      dispatch(setTrackLoading(true));
      dispatch(setError(null));
      dispatch(setIsPlaying(false));

      const allTrackIds = [...prevTrackIds, ...likedTracksIds];

      const toPlayTracks: Track[] = (await Promise.all(
        allTrackIds
          .map(
            async (trackId): Promise<Track> => {
              let track: Track;
              if (isLocalTrack(trackId)) {
                const localTrack = tracks.find((t: Track) => t.id === trackId);
                if (!localTrack) {
                  return Promise.resolve(null);
                }

                track = {
                  ...localTrack,
                  id: trackId,
                  url: 'file:///' + getLocalPathForTrack(localTrack as Track),
                };
              } else {
                const meta = await fetchAudioMeta(trackId);

                // Adds a track to the queue
                track = {
                  id: meta.videoId,
                  url: getStreamingUrlForTrackId(trackId),
                  title: meta.title,
                  artist: meta.author.name,
                  artwork: meta.cover.url,
                  rating: false,
                  pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_MUSIC,
                };
              }

              return Promise.resolve(sanitizeTrack(track));
            },
          )
          .filter((x) => x != null),
      )) as Track[];

      if (repeatMode === 'repeat-current') {
        dispatch(setRepeatTrackId(likedTracksIds[0]));
      }

      const currTrack = toPlayTracks.find(
        (t: Track) => t.id === likedTracksIds[0],
      );
      if (currTrack) {
        dispatch(setCurrentTrack(currTrack));

        await TrackPlayer.stop();
        await TrackPlayer.add(toPlayTracks);
        await TrackPlayer.skip(likedTracksIds[0]);
        await TrackPlayer.play();
      }
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: err.code,
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setTrackLoading(false));
      dispatch(updatePlaybackControls());
    }
  };
};

export const playFromLibraryPlaylist = (
  trackId: string,
  playlistId: string,
) => {
  // FIXME(typescript): Better type than Dispatch<any>
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    dispatch(setError(null));
    dispatch(setTrackLoading(true));

    try {
      const state = getState();
      const {playlistId: currentPlaylistId, repeatMode} = state.audioPlayer;
      const {localTracksIds, tracks, playlists} = state.library;

      // In case repeat mode is set to same track:
      if (repeatMode === 'repeat-current') {
        dispatch(setRepeatTrackId(trackId));
      }

      const track = await getTrack(tracks, localTracksIds)(trackId);
      if (!track) {
        // Track does not exist locally AND on YouTube, inform the user. NOW.
        dispatch(setTrackLoading(false));
        dispatch(
          setError({
            code: 'not-found',
            message: 'Cannot find the requested track.',
          }),
        );
        return;
      }

      // Set current track before doing anything else.
      dispatch(setCurrentTrack(track));

      if (playlistId === currentPlaylistId) {
        // Early return if we are in same playlist as prev track.
        await TrackPlayer.stop();
        await TrackPlayer.skip(trackId);
        await TrackPlayer.play();

        dispatch(updatePlaybackControls());
        return;
      }

      const playlistsArray = Object.values(playlists);
      const playlist = playlistsArray.find((p) => p.id === playlistId);

      if (!playlist) {
        // Playlist does not exists, inform the user. NOW.
        dispatch(
          setError({
            code: 'not-found',
            message: 'Playlist does not exists.',
          }),
        );
        return;
      }

      if (!playlist.tracksIds.includes(trackId)) {
        // Track is not in playlist, inform the user. NOW.
        // NOTE: Should we still play track if it exists locally?
        if (localTracksIds.includes(trackId)) {
          dispatch(playTrackFromLibrary(trackId));
          return;
        }

        dispatch(
          setError({
            code: 'not-found',
            message: 'Cannot find track in current playlist.',
          }),
        );
        return;
      }

      // In case playlistId is different from last time:
      dispatch(setPlaylistId(playlistId));

      // 1. Get playlist tracks.
      const playlistTracks: Track[] = await Promise.all<Track>(
        playlist.tracksIds.map(async (id: string) => {
          const t = await getTrack(tracks, localTracksIds)(id);
          if (t) {
            return Promise.resolve(t);
          }
          return Promise.resolve(null);
        }),
      );

      // 2.2. Reset player, add playlist tracks, skip to track.
      await TrackPlayer.reset();
      await TrackPlayer.updateOptions(TRACK_PLAYER_CONTROLS_OPTS);
      await TrackPlayer.add(playlistTracks);
      await TrackPlayer.skip(track.id);
      await TrackPlayer.play();
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: err.code,
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setTrackLoading(false));
      dispatch(updatePlaybackControls());
    }
  };
};

export const playTrackFromTrackslist = (trackId: string, tracks: Track[]) => {
  return async (
    dispatch: Dispatch<PlayMultipleTracksActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {repeatMode} = state.audioPlayer;

      dispatch(setTrackLoading(true));
      dispatch(setError(null));
      dispatch(setIsPlaying(false));

      if (repeatMode === 'repeat-current') {
        dispatch(setRepeatTrackId(trackId));
      }

      const toPlayTracks = tracks.map((t) => {
        return sanitizeTrack({
          ...t,
          id: t.id,
          url: 'file:///' + getLocalPathForTrack(t),
        });
      });

      const currTrack = toPlayTracks.find((t) => t.id === trackId);
      if (currTrack) {
        dispatch(setCurrentTrack(currTrack));

        await TrackPlayer.stop();
        await TrackPlayer.add(currTrack);
        await TrackPlayer.play();
      }
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: err.code,
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setTrackLoading(false));
      dispatch(updatePlaybackControls());
    }
  };
};

// TODO: Move this logic into the action.
const fetchPlaylist = async (listId: string): Promise<Record<string, any>> => {
  const res = await fetch(
    `${API_BASE_URL}/playlist/${encodeURIComponent(listId)}`,
  );
  const json = await res.json();

  if (json.error) {
    return Promise.reject(json.error);
  }

  return Promise.resolve(json.data.results);
};

type PlayPlaylistActionDispatchType =
  | SetTrackLoadingActionType
  | SetErrorActionType
  | SetCurrentTrackActionType
  | any; // TODO: Fix this type.

export const playPlaylist = (listId: string) => {
  return async (
    dispatch: Dispatch<PlayPlaylistActionDispatchType>,
    getState: () => RootState,
  ) => {
    try {
      const state = getState();
      const {loadingTrack, repeatMode} = state.audioPlayer;

      if (loadingTrack) {
        return;
      }

      dispatch(setTrackLoading(true));
      dispatch(setError(null));
      dispatch(setIsPlaying(false));

      const playlistResults = await fetchPlaylist(listId);
      const playlistTracks = playlistResults.items
        .filter((track: Record<string, any>) => track.isPlayable === true)
        .map((track: Record<string, any>) =>
          sanitizeTrack({
            id: track.id,
            url: getStreamingUrlForTrackId(track.id),
            title: track.title,
            artist: track.author.name,
            artwork: track.bestThumbnail?.url,
            rating: false,
            pitchAlgorithm: TrackPlayer.PITCH_ALGORITHM_MUSIC,
          }),
        );

      if (repeatMode === 'repeat-current') {
        dispatch(setRepeatTrackId(playlistTracks[0].id));
      }

      dispatch(setCurrentTrack(playlistTracks[0]));

      await TrackPlayer.reset();
      await TrackPlayer.updateOptions(TRACK_PLAYER_CONTROLS_OPTS);
      await TrackPlayer.add(playlistTracks);
      await TrackPlayer.skip(playlistTracks[0].id);
      await TrackPlayer.play();
    } catch (err) {
      if (err.message.indexOf('Status code: 429') !== -1) {
        dispatch(
          setError({
            code: 'rate-limited',
            message:
              'Trop de requêtes, merci de réessayer dans quelques heures...',
          }),
        );
      } else {
        dispatch(setError(err));
      }
    } finally {
      dispatch(setTrackLoading(false));
      dispatch(updatePlaybackControls());
    }
  };
};

export const setRepeatMode = (
  mode: AudioPlayerRepeatMode,
  trackId?: string,
) => {
  return async (dispatch: Dispatch<SetRepeatModeActionType>) => {
    dispatch(setRepeatModeSync(mode, trackId));
  };
};
