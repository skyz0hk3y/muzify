import {useCallback, useEffect, useMemo} from 'react';
import TrackPlayer, {Track} from 'react-native-track-player';
import {useDispatch, useSelector} from 'react-redux';

import {TRACK_PLAYER_CONTROLS_OPTS} from '../../constants';

import {getLocalPathForTrack} from '../../core/helpers/getLocalPathForTrack';
import {
  getStreamingUrlForTrackId,
  getTrackMetaUrlForTrackId,
} from '../../core/helpers/getStreamingUrlForTrackId';
import {sanitizeTrack} from '../../core/helpers/sanitizeTrack';
import {
  getCachedTrackSelector,
  getIsLocalTrackSelector,
  getIsTrackInCacheSelector,
  getLocalTracksIdsSelector,
  getTracksSelector,
} from '../../library/selectors';

import {getTrack} from '../helpers/getTrack';
import {
  setCanPlayNext,
  setCanPlayPrevious,
  setCurrentTrack,
  setDuration,
  setError,
  setIsPlayingSync,
  setQueue,
  setTrackLoading,
  updatePlaybackControls,
} from '../actions';
import {
  getCanPlayNextSelector,
  getCanPlayPreviousSelector,
  getCurrentPlaylistIdSelector,
  getCurrentTrackIndexSelector,
  getCurrentTrackSelector,
  getIsCurrentPlaylistSelector,
  getIsPlayingSelector,
  getIsTrackInQueueSelector,
  getQueueSelector,
  getTrackFromQueueSelector,
} from '../selectors';

export type UseAudioPlayerHook = () => {
  /** Getters (Audio Player properties) */
  currentTrack: Track;
  nextTrack: Track;
  previousTrack: Track;
  canPlayNext: boolean;
  canPlayPrevious: boolean;
  isPlaying: boolean;
  isLoading: boolean;
  /** Setters (Audio Player API) */
  playTrack(
    trackId: string,
    playlistId: string | null,
    nextTracks?: Track[],
  ): Promise<void>;
  // playTrackInPlaylist(trackId: string, playlistId: string): Promise<void>;
  playNextTrack(): Promise<void>;
  playPreviousTrack(): Promise<void>;
  seekToSeconds(position: number): Promise<void>;
};

// FIXME(refactor): Extract this type elsewhere, make it generic
type APIResponse<D> = {
  error?: {
    code: string;
    key: string;
    message: string;
  };
  data: D;
};

type APIStreamInfoTrack = {
  videoId: string;
  title: string;
  duration: number; // seconds
  author: {
    id: string;
    name: string;
    avatar: {
      url: string;
      width: number; // px
      height: number; // px
    };
  };
  cover: {
    url: string;
    width: number; // px
    height: number; // px
  };
};

/**
 * A hook that helps to play audio within Muzify, should be the preferred way to
 * interact/play any track/playlist! This aims to deprecate the old broken audio
 * player ;)
 */
export const useAudioPlayer: UseAudioPlayerHook = () => {
  const dispatch = useDispatch();

  // Library state (bad, we shouldn't rely on these data)
  const localTracksIds = useSelector(getLocalTracksIdsSelector);
  const localTracks = useSelector(getTracksSelector);

  // Audio Player state
  const queue = useSelector(getQueueSelector);
  const currentTrackIndex = useSelector(getCurrentTrackIndexSelector);
  const currentTrack = useSelector(getCurrentTrackSelector);
  const nextTrack = useSelector(
    getTrackFromQueueSelector(currentTrackIndex + 1),
  );
  const previousTrack = useSelector(
    getTrackFromQueueSelector(currentTrackIndex - 1),
  );
  const canPlayNext = useSelector(getCanPlayNextSelector);
  const canPlayPrevious = useSelector(getCanPlayPreviousSelector);
  const isPlaying = useSelector(getIsPlayingSelector);

  const getCachedTrack = useSelector(getCachedTrackSelector);
  const isTrackInCache = useSelector(getIsTrackInCacheSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isCurrentPlaylist = useSelector(getIsCurrentPlaylistSelector);
  const isTrackInQueue = useSelector(getIsTrackInQueueSelector);

  const _getTrackById = useCallback(
    async (trackId: string): Promise<Track | null> => {
      try {
        let track: Track;

        if (isLocalTrack(trackId)) {
          const localTrack = await getTrack(
            localTracks,
            localTracksIds,
          )(trackId);
          track = {
            ...localTrack,
            url: getLocalPathForTrack(localTrack),
          };
        } else if (isTrackInCache(trackId)) {
          const cachedTrack = await getCachedTrack(trackId);
          track = {
            ...cachedTrack,
            url: getStreamingUrlForTrackId(trackId),
          };
        } else {
          const res = await fetch(getTrackMetaUrlForTrackId(trackId));
          if (!res.ok) {
            dispatch(
              setError({
                code: 'EUnknownError',
                message: 'Cannot contact API, we have a server issue',
              }),
            );
            return;
          }

          const {
            data,
            error,
          }: APIResponse<APIStreamInfoTrack> = await res.json();

          if (error) {
            console.log(
              '[AudioPlayer] Cannot fetch remote track, API is probably broken...',
              error,
            );
            dispatch(setError(error));
            return;
          }

          track = {
            id: data.videoId,
            artist: data.author.name,
            title: data.title,
            duration: data.duration,
            artwork: data.cover.url,
            url: getStreamingUrlForTrackId(data.videoId),
          };
        }

        // 1.2. Make sure data is always well formatted and nice to user
        track = sanitizeTrack(track);
        return track;
      } catch (err) {
        console.log(
          `[AudioPlayer] Cannot play trackId=${trackId}, error: ${err.message}`,
        );
        dispatch(setError(err));
      }
    },
    [
      dispatch,
      getCachedTrack,
      isLocalTrack,
      isTrackInCache,
      localTracks,
      localTracksIds,
    ],
  );

  /**
   * WARN: Make sure you know what you do when modifying this function as its
   * written in a way that is meant to be efficient under a certain number of
   * scenarios (i.e. it allows to play a track from its id, not having to care
   * for local/cache file presence every time its needed).
   *
   * So Please; think twice before dealing with this function as it may breaks
   * the audio player for our end-users. This is definitely not part of road map
   */
  const playTrack = useCallback(
    async (
      trackId: string,
      playlistId: string | null,
      nextTracks?: Track[],
    ) => {
      console.log(
        `[AudioPlayer] Asked to play trackId="${trackId}", nextTracksLen=${nextTracks.length}`,
      );

      // 1. Find track data based on trackId (check for local/remote track)
      const track = await _getTrackById(trackId);
      if (!track) {
        console.log(`[AudioPlayer] Cannot find requested trackId=${trackId}`);
        dispatch(
          setError({
            code: 'ENotFound',
            message: `No such track found for trackId="${trackId}", nor locally, nor in cache.`,
          }),
        );
        return;
      }

      dispatch(setTrackLoading(true));

      // 2. Pass audio URI to audio player ASAP
      const canPlayFromQueue =
        playlistId != null &&
        isCurrentPlaylist(playlistId) &&
        isTrackInQueue(trackId);

      let tracksToAdd: (Track | null)[] = [];
      let skipToId: string = null;

      if (canPlayFromQueue) {
        tracksToAdd = [track];
        skipToId = trackId;
      } else {
        tracksToAdd = [nextTracks.find((t) => t != null && t.id === trackId)];
        tracksToAdd = await Promise.all(
          tracksToAdd.map((t) =>
            t != null ? _getTrackById(t.id) : Promise.resolve(null),
          ),
        );
        skipToId = tracksToAdd.length ? tracksToAdd[0].id : null;
      }

      const shouldStopAndResetPlay = skipToId == null;

      if (shouldStopAndResetPlay) {
        await TrackPlayer.reset();
      }

      // Make sure to set this again to avoid loosing notification features
      await TrackPlayer.updateOptions(TRACK_PLAYER_CONTROLS_OPTS);

      if (tracksToAdd.length) {
        await TrackPlayer.add(tracksToAdd);
      }

      if (skipToId) {
        await TrackPlayer.skip(skipToId);
      }

      await TrackPlayer.play();
      // FROM HERE, AUDIO IS PLAYING

      // 2.2. Set current track before dealing with the queue so UI doesn't lag behind
      dispatch(setCurrentTrack(track));
      dispatch(setDuration(track.duration));
      dispatch(setTrackLoading(false));

      const playerState = await TrackPlayer.getState();

      dispatch(setIsPlayingSync(playerState === TrackPlayer.STATE_PLAYING));
      dispatch(updatePlaybackControls());

      // TODO: Move the following into a reusable `syncQueueTracks` or similar
      // 3.1. Update queue to match requested audio (only if setting new queue)
      // const isNotAPlaylist = playlistId == null;

      // TODO(perf): Benchmark this, seems slow; complexity O(n^n)
      // const isQueueEqualToPreviousCall = queue.every(
      //   (enqueuedTrack) =>
      //     enqueuedTrack != null &&
      //     nextTracks.find(
      //       (nTrack) => nTrack != null && nTrack.id === enqueuedTrack.id,
      //     ),
      // );

      const shouldGenerateAndEnqueueTracks = true;
      if (shouldGenerateAndEnqueueTracks) {
        let tracks = [];
        if (nextTracks && nextTracks.length >= 1) {
          // TODO(perf): Measure how performant this is with big playlists
          tracks = nextTracks.map(async (t) =>
            t != null ? _getTrackById(t.id) : Promise.resolve(null),
          );
          tracks = await Promise.all(tracks);
          tracks = tracks.filter((t) => t != null);
        }

        const trackIndexInQueue = tracks.findIndex(
          (t) => t != null && t.id === trackId,
        );
        const leftSlice = tracks.slice(0, trackIndexInQueue);
        const rightSlice = tracks.slice(trackIndexInQueue + 1);

        // 3.2. Load previous/next tracks into audio player
        const shouldAddPrevTracks = tracks.length >= 1 && leftSlice.length >= 1;
        const shouldAddNextTracks =
          tracks.length >= 1 && rightSlice.length >= 1;

        console.log(
          `[AudioPlayer] shouldAddNextTracks=${shouldAddNextTracks}, shouldAddPrevTracks=${shouldAddPrevTracks}`,
        );

        if (shouldAddPrevTracks) {
          await TrackPlayer.add(leftSlice, track.id);
          dispatch(setCanPlayPrevious(true));
        }

        if (shouldAddNextTracks) {
          await TrackPlayer.add(rightSlice);
          dispatch(setCanPlayNext(true));
        }

        const playerQueue = await TrackPlayer.getQueue();

        // 4. Update UI once audio plays
        dispatch(setQueue(playerQueue, trackIndexInQueue));
        dispatch(updatePlaybackControls());
      }
    },
    [_getTrackById, dispatch, isCurrentPlaylist, isTrackInQueue],
  );

  const playNextTrack = useCallback(async () => {
    await TrackPlayer.skipToNext();

    // // 1. Get next track index
    // const nextTrackIndex = currentTrackIndex + 1;
    // // 2. Get trackId for current track + 1 (next one), check existence
    // if (!queue[nextTrackIndex]) {
    //   try {
    //   } catch (_err) {
    //     dispatch(setCanPlayNext(false));
    //     dispatch(updatePlaybackControls());
    //   }
    //   return;
    // }
    // // 3. Use previously defined `playTrack` to handle the rest
    // await playTrack(queue[nextTrackIndex].id, currentPlaylistId, queue);
  }, []);

  const playPreviousTrack = useCallback(async () => {
    await TrackPlayer.skipToPrevious();

    // // 1. Get previous track index
    // const prevTrackIndex = currentTrackIndex - 1;
    // // 2. Get track for current track - 1 (previous one), check existence
    // if (!queue[prevTrackIndex]) {
    //   try {
    //   } catch (_err) {
    //     dispatch(setCanPlayPrevious(false));
    //     dispatch(updatePlaybackControls());
    //   }
    //   return;
    // }
    // // 3. Use previously defined `playTrack` to handle the rest
    // await playTrack(queue[prevTrackIndex].id, currentPlaylistId, queue);
  }, []);

  const seekToSeconds = useCallback(
    (position) => TrackPlayer.seekTo(position),
    [],
  );

  useEffect(() => {
    console.log('[AudioPlayer] Queue was updated:', {queue});
  }, [queue]);

  return useMemo(
    () => ({
      // Getters
      currentTrack,
      nextTrack,
      previousTrack,
      canPlayNext,
      canPlayPrevious,
      isPlaying,
      isLoading: true,
      // Setters
      playTrack,
      playNextTrack,
      playPreviousTrack,
      seekToSeconds,
    }),
    [
      currentTrack,
      nextTrack,
      previousTrack,
      canPlayNext,
      canPlayPrevious,
      isPlaying,
      playTrack,
      playNextTrack,
      playPreviousTrack,
      seekToSeconds,
    ],
  );
};
