import React, {useEffect, useRef, useState} from 'react';
import {FlatList} from 'react-native';
import {Track} from 'react-native-track-player';
import {useSelector} from 'react-redux';
import {MAX_TRACKS_LIST_SIZE_BEFORE_NO_ANIMATION} from '../../library/components/PlaylistTracksListHorizontal';
import {
  getCurrentPlaylistIdSelector,
  getCurrentTrackSelector,
  getIsCurrentPlaylistSelector,
  getIsCurrentTrackSelector,
} from '../selectors';

export const useCurrentTrackAutoScroll = (
  tracks: Track[],
  playlistId: string,
  scrollTo: (index: number, animated: boolean) => void,
) => {
  const scrollFn = useRef(scrollTo);
  scrollFn.current = scrollTo;

  const [lastScrollTrackId, setLastScrollTrackId] = useState<string | null>(
    null,
  );

  const currentTrack = useSelector(getCurrentTrackSelector);
  const currentPlaylistId = useSelector(getCurrentPlaylistIdSelector);

  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isCurrentPlaylist = useSelector(getIsCurrentPlaylistSelector);

  useEffect(() => {
    const tracksLength = tracks.length - 1;

    // Disable animation when list is too big to avoid paying a performance penalty
    let animated = true;
    if (tracksLength >= MAX_TRACKS_LIST_SIZE_BEFORE_NO_ANIMATION) {
      animated = false;
    }

    if (
      tracks &&
      tracks.length >= 1 &&
      currentPlaylistId &&
      isCurrentPlaylist(playlistId) &&
      currentTrack &&
      currentTrack.id !== lastScrollTrackId
      // !isCurrentTrack(lastScrollTrackId)
    ) {
      const currentTrackIdx = tracks.findIndex((t) => t.id === currentTrack.id);
      if (currentTrackIdx >= 1 && currentTrackIdx <= tracksLength) {
        if (scrollFn.current) {
          console.log(
            `[CurrentTrackAutoScroll] Will scroll to track at index: ${currentTrackIdx} from playlist: ${playlistId}`,
          );
          scrollFn.current(currentTrackIdx, animated);
        }
        setLastScrollTrackId(currentTrack.id);
      }
    }
  }, [
    currentPlaylistId,
    currentTrack,
    isCurrentPlaylist,
    isCurrentTrack,
    lastScrollTrackId,
    playlistId,
    scrollTo,
    tracks,
  ]);
};
