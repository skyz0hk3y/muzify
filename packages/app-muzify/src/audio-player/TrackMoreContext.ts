import {createContext} from 'react';
import {Track} from 'react-native-track-player';

export const TrackMoreContext = createContext({
  openTrackMoreForTrack: (_track?: Track) => {},
  setOpenTrackMoreForTrack: (_callbackFn: (track: Track) => void) => {},
  track: null as Track | null,
});
