import React, {FC} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import {Track} from 'react-native-track-player';

import {PlaylistItem} from '../../library/components/PlaylistItem';

export type Playlist = {
  details: {
    id: string;
    name: string;
    ownerName: string;
    artwork: string;
    isPublic: boolean;
  };
  tracks: Track[];
};

export type ImportProgressProps = {
  playlist: Playlist | undefined;
  createdAtUnix: number;
  importStarted: boolean;
  importCompleted: boolean;
  importProgress: number;
  onImportPress?: () => void;
};

export const ImportProgress: FC<ImportProgressProps> = ({
  playlist,
  createdAtUnix,
  importStarted,
  importCompleted,
  importProgress = 0,
  onImportPress,
}) => {
  const libraryTrackIdsFromPlaylist =
    playlist?.tracks.map((track) => track.id) || [];

  // FIXME(animation): make use of reanimated so progress is smoother ?
  // TODO(perf): also provide a "no animation"/"fast animation" mode (e2e tests)
  const progressStyle: StyleProp<ViewStyle> = {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    width: `${importProgress}%`,
    backgroundColor: '#ffffff',
    opacity: 0.2,
  };

  return (
    <>
      {importStarted && !importCompleted && <View style={progressStyle} />}
      {playlist != null && (
        <PlaylistItem
          onItemPress={onImportPress}
          actionIconName={
            importStarted
              ? 'progress-download'
              : importCompleted
              ? 'progress-check'
              : 'download-multiple'
          }
          playlist={{
            id: playlist.details.id,
            name: playlist.details.name,
            origin: 'spotify',
            tracksIds: libraryTrackIdsFromPlaylist,
            artwork: playlist.details.artwork,
            createdAtUnix,
          }}
        />
      )}
    </>
  );
};
