import React, {FC} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {Button} from '../../core/components/Button';
import {useTheme} from '../../core/hooks/useTheme';

type SpotifyImportButtonProps = {
  onPress: () => void;
  text: string;
  fluid?: boolean;
  style?: Record<string, any>;
};

const SPOTIFY_COLOR = '#1ed760';
const TEXT_COLOR = '#A9A9A9';

export const SpotifyImportButton: FC<SpotifyImportButtonProps> = ({
  text,
  fluid = false,
  style,
  onPress,
}) => {
  const {theme} = useTheme();

  return (
    <Button
      fluid={fluid}
      style={[styles.button, style]}
      backgroundColor={theme.colors.backgrounds[3]}
      color={theme.colors.text}
      onPress={onPress}
    >
      <Icon
        name={'spotify'}
        size={Platform.OS === 'ios' ? 18 : 24}
        color={SPOTIFY_COLOR}
      />
      <Text style={[styles.text, {color: theme.colors.text}]}>{text}</Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.16,
    shadowRadius: 4,
    // padding: 16,
  },
  text: {
    marginLeft: 16,
    marginRight: 4,
    color: TEXT_COLOR,
    fontWeight: '700',
    textTransform: 'uppercase',
  },
});
