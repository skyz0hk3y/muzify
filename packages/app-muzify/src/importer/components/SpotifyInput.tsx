import {useNavigation} from '@react-navigation/core';
import React, {FC, useMemo} from 'react';
import {Platform, StyleSheet, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector} from 'react-redux';

import {Routes} from '../../constants';

import {MaterialDesignIconNameType} from '../../core/types';
import {IconButton} from '../../core/components/IconButton';
import {useTheme} from '../../core/hooks/useTheme';

import {getTracksDownloadsSelector} from '../../library/selectors';

export type SpotifyInputProps = {
  autoFocus?: boolean;
  value?: string;
  placeholder?: string;
  onSearchSubmit?: () => void;
  onValueChange?: (value: string) => void;
  onClearTextPress?: () => void;
  onBackButtonPress?: () => void;
};

export const SpotifyInput: FC<SpotifyInputProps> = ({
  autoFocus = false,
  value = '',
  placeholder = '',
  onSearchSubmit = () => undefined,
  onValueChange = () => undefined,
  onClearTextPress = () => undefined,
  onBackButtonPress = () => undefined,
}) => {
  const {theme} = useTheme();
  const {navigate} = useNavigation();

  const tracksDownloads = useSelector(getTracksDownloadsSelector);
  const hasPendingDownloads = useMemo(
    () => Object.values(tracksDownloads).length > 0,
    [tracksDownloads],
  );

  return (
    <View
      style={[styles.container, {borderBottomColor: theme.colors.textMuted}]}
    >
      {value.trim() !== '' ? (
        <IconButton
          testID={'spotify-input-back-button'}
          size={'normal'}
          onPress={onBackButtonPress}
          style={styles.clearButton}
        >
          <Icon
            name={'arrow-left' as MaterialDesignIconNameType}
            size={24}
            color={theme.colors.text}
          />
        </IconButton>
      ) : (
        <IconButton
          testID={'spotify-input-spotify-icon'}
          style={styles.searchIcon}
          size={'normal'}
          disabled
        >
          <Icon
            name={'spotify' as MaterialDesignIconNameType}
            size={24}
            color={theme.colors.text}
          />
        </IconButton>
      )}
      <TextInput
        testID="spotify-input"
        returnKeyType={'search'}
        keyboardAppearance={'dark'}
        style={[styles.input, {color: theme.colors.text}]}
        value={value}
        autoFocus={autoFocus}
        placeholder={placeholder}
        placeholderTextColor={theme.colors.textMuted}
        onEndEditing={onSearchSubmit}
        onChangeText={onValueChange}
        // clearButtonMode={'while-editing'}
      />
      {value.trim() !== '' ? (
        <IconButton
          testID={'spotify-input-clear-button'}
          size={'normal'}
          onPress={onClearTextPress}
          style={styles.clearButton}
        >
          <Icon
            name={'close' as MaterialDesignIconNameType}
            size={24}
            color={theme.colors.text}
          />
        </IconButton>
      ) : (
        <View style={styles.iconsRow}>
          <IconButton
            testID={'spotify-input-track-downloads'}
            style={styles.iconMR8}
            onPress={() => navigate(Routes.LIBRARY_TRACKS_DOWNLOADS)}
          >
            <Icon
              name={
                (hasPendingDownloads
                  ? 'download-circle'
                  : 'download-circle-outline') as MaterialDesignIconNameType
              }
              size={24}
              color={
                hasPendingDownloads ? theme.colors.primary : theme.colors.text
              }
            />
          </IconButton>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? 56 - 7 : 64,
    borderBottomWidth: Platform.OS === 'ios' ? 0.3 : 0,
    paddingHorizontal: 0,
    // paddingBottom: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 44,
    paddingHorizontal: 16,
    flex: 1,
  },
  searchIcon: {
    marginTop: -2,
    marginLeft: 8,
    width: 32,
    minWidth: 32,
  },
  clearButton: {
    marginTop: -2,
    marginRight: 8,
    width: 32,
    minWidth: 32,
  },
  iconML8: {
    marginLeft: 8,
  },
  iconMR8: {
    marginRight: 8,
  },
  iconsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
