import React, {FC, useCallback, useEffect, useMemo, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  RefreshControl,
  ListRenderItem,
  FlatList,
  Keyboard,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Track} from 'react-native-track-player';
import {fetch} from 'cross-fetch';

import spotifyLogo from '../../assets/spotify-logo.png';

import {API_BASE_URL} from '../../constants';
import {useTranslation} from '../../i18n';

import * as LibraryActions from '../../library/actions';
import {TrackDownload} from '../../library/types';

import {SearchInput} from '../../search/components/SearchInput';
import {TrackItem} from '../../audio-player/components/TrackItem';
import {EmptyState} from '../../core/components/EmptyState';
import {
  getCurrentTrackSelector,
  getIsCurrentTrackSelector,
} from '../../audio-player/selectors';
import {
  getIsDownloadingTrackSelector,
  getIsLocalTrackSelector,
  getLocalTracksIdsSelector,
  getTrackIdsSelector,
  getTracksDownloadsSelector,
} from '../../library/selectors';
import {ImportProgress, Playlist} from '../components/ImportProgress';
import {ScreenTopAction} from '../../core/components/ScreenTopAction';
import {useTheme} from '../../core/hooks/useTheme';
import {SpotifyInput} from '../components/SpotifyInput';
import {useNavigation} from '@react-navigation/native';

export type SpotifyImportScreenProps = {
  // Settings
  maxConcurrentDownloads: number;
  // Navigation
  route: {
    params: {
      playlistId?: string;
    };
  };
};

export const SpotifyImportScreen: FC<SpotifyImportScreenProps> = ({
  // Settings
  // maxConcurrentDownloads = 3,
  // Navigation
  route,
}) => {
  const [loading, setLoading] = useState(false);
  const [isKeyboardShown, setIsKeyboardShown] = useState(false);
  const [importStarted, setImportStarted] = useState(false);
  const [importCompleted, setImportCompleted] = useState(false);
  const [importedTracksCount, setImportedTracksCount] = useState<number>(0);
  const [importedTracksIds, setImportedTracksIds] = useState<string[]>([]);
  const [playlistId, setPlaylistId] = useState<string>('');
  const [playlist, setPlaylist] = useState<Playlist | undefined>();

  const {theme} = useTheme();
  const {t} = useTranslation('core');
  const {goBack} = useNavigation();

  const currentTrack = useSelector(getCurrentTrackSelector);
  const localTracksIds = useSelector(getLocalTracksIdsSelector);
  const likedTracksIds = useSelector(getTrackIdsSelector);
  const tracksDownloads = useSelector(getTracksDownloadsSelector);

  const dispatch = useDispatch();
  const isCurrentTrack = useSelector(getIsCurrentTrackSelector);
  const isLocalTrack = useSelector(getIsLocalTrackSelector);
  const isDownloadingTrack = useSelector(getIsDownloadingTrackSelector);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => setIsKeyboardShown(true));
    Keyboard.addListener('keyboardDidHide', () => setIsKeyboardShown(false));

    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  });

  const notImportedTracksIds = useMemo(
    () =>
      (playlist?.tracks || [])
        .filter(
          (track: Track) =>
            !localTracksIds.includes(track.id) ||
            !importedTracksIds.includes(track.id),
          // !Object.keys(tracksDownloads).includes(track.id) === false,
        )
        // .slice(0, maxConcurrentDownloads)
        .map((track: Track) => track.id),
    [importedTracksIds, localTracksIds, playlist],
  );

  const alreadyImportedTracks = playlist?.tracks.filter((track) =>
    importedTracksIds.includes(track.id),
  );

  useEffect(() => {
    if (
      !importCompleted &&
      playlist?.tracks.every((track) => importedTracksIds.includes(track.id))
    ) {
      setImportCompleted(true);
    }
  }, [importCompleted, importedTracksIds, playlist]);

  const downloadTracksByChunks = async () => {
    if (!playlist?.tracks) {
      return;
    }

    setImportStarted(true);

    const libraryTrackIdsFromPlaylist =
      playlist.tracks.map((track) => track.id) || [];

    dispatch(
      LibraryActions.createPlaylist(`spotify:${playlist.details.id}`, {
        id: `spotify:${playlist.details.id}`,
        originalId: playlist.details.id,
        name: playlist.details.name,
        artwork: playlist.details.artwork,
        origin: 'spotify',
        tracksIds: libraryTrackIdsFromPlaylist,
      }),
    );

    for (let i = 0, l = notImportedTracksIds.length; i < l; i++) {
      await Promise.all([
        new Promise((resolve) => {
          const TIMEOUT = 60 / 2; // ms (0.5s)
          const importingTrackId = notImportedTracksIds[i];

          setTimeout(async () => {
            await dispatch(LibraryActions.addLikedTrack(importingTrackId));
            await dispatch(
              LibraryActions.addTrackToPlaylist(playlistId, importingTrackId),
            );

            setImportedTracksIds((s) => [...s, importingTrackId]);
            setImportedTracksCount((s) => s + 1);

            resolve(true);
          }, TIMEOUT);
        }),
      ]);
    }

    setImportStarted(false);
    setImportCompleted(true);
  };

  const onImportPlaylist = useCallback(async () => {
    if (playlistId.trim() === '') {
      return;
    }

    setLoading(true);

    const playlistRes = await fetch(
      `${API_BASE_URL}/playlist/spotify/${playlistId}`,
      {
        headers: new Headers({
          Accept: 'application/json',
        }),
      },
    );

    const playlistJson = await playlistRes.json();
    if (!playlistJson) {
      return;
    }

    setPlaylist({
      details: playlistJson.data.playlist,
      tracks: playlistJson.data.results,
    });
    setLoading(false);
  }, [playlistId]);

  useEffect(() => {
    const loadPlaylistFromRouteParams = async () => {
      const {params} = route;
      if (params && params.playlistId) {
        setPlaylistId(params.playlistId);
        await onImportPlaylist();
      }
    };

    loadPlaylistFromRouteParams();
  }, [onImportPlaylist, route, route.params]);

  const onTrackFavoriteToggle = async (trackId: string) => {
    if (!likedTracksIds.includes(trackId)) {
      await dispatch(LibraryActions.addLikedTrack(trackId));
    } else {
      await dispatch(LibraryActions.removeLikedTrack(trackId));
    }
  };

  const onDownloadFullPlaylist = () => {
    if (!playlist?.tracks) {
      return;
    }

    downloadTracksByChunks();
  };

  const isTrackDisabled = useCallback(
    (trackId: string) =>
      Object.keys(tracksDownloads).includes(trackId) ||
      localTracksIds.includes(trackId) ||
      isDownloadingTrack(trackId),
    [tracksDownloads, localTracksIds, isDownloadingTrack],
  );

  const getTrackIcon = (trackId: string) => {
    const downloadTracks = Object.values(tracksDownloads);
    const currTrackInDownloads = downloadTracks.find(
      (trackDownload: TrackDownload) => trackId === trackDownload.trackId,
    );

    if (isDownloadingTrack(currTrackInDownloads?.trackId as string)) {
      return 'progress-download';
    }

    if (isLocalTrack(currTrackInDownloads?.trackId as string)) {
      return 'check-circle-outline';
    }

    return 'download';
  };

  const getTrackIconColor = (trackId: string) => {
    const downloadTracks = Object.values(tracksDownloads);
    const currTrackInDownloads = downloadTracks.find(
      (trackDownload: TrackDownload) => trackId === trackDownload.trackId,
    );

    if (isDownloadingTrack(currTrackInDownloads?.trackId as string)) {
      return theme.colors.primary;
    }

    if (isLocalTrack(currTrackInDownloads?.trackId as string)) {
      return theme.colors.success;
    }

    return theme.colors.text;
  };

  const renderItem: ListRenderItem<Track> = ({item}) => {
    return (
      <TrackItem
        key={`${item.title}-${item.id}`}
        onItemPress={onTrackFavoriteToggle.bind(null, item.id)}
        actionIconName={getTrackIcon(item.id)}
        actionColor={getTrackIconColor(item.id)}
        isLocal={isLocalTrack(item.id)}
        isCurrentTrack={isCurrentTrack(item.id)}
        isDownloading={isDownloadingTrack(item.id)}
        disabled={isTrackDisabled(item.id)}
        track={{
          id: item.id,
          videoId: item.id,
          thumbnail: item.artwork,
          title: item.title,
          artist: item.artist,
          url: '',
          author: {
            name: item.artist,
          },
        }}
      />
    );
  };

  const getPlaylistCreatedAtUnix = useMemo(() => Date.now, []);

  const shouldShowImportProgress = playlist?.details != null;
  const screenTopBackgroundColor = importStarted
    ? theme.colors.primary
    : importCompleted
    ? theme.colors.success
    : theme.colors.backgrounds[1];

  const importProgress =
    (importedTracksCount / (playlist?.tracks.length || 0)) * 100;

  return (
    <>
      <SafeAreaView>
        <View
          style={[
            styles.container,
            {backgroundColor: theme.colors.backgrounds[2]},
          ]}
        >
          <View style={styles.content}>
            <ScreenTopAction
              noMargins={
                shouldShowImportProgress || importStarted || importCompleted
              }
              backgroundColor={screenTopBackgroundColor}
            >
              {!playlist?.details && (
                <SpotifyInput
                  value={playlistId}
                  placeholder={t('importer:placeholders.playlist-id')}
                  onValueChange={setPlaylistId}
                  onClearTextPress={() => setPlaylistId('')}
                  onSearchSubmit={onImportPlaylist}
                  onBackButtonPress={goBack}
                />
              )}
              {shouldShowImportProgress && (
                <ImportProgress
                  playlist={playlist}
                  importStarted={importStarted}
                  importCompleted={importCompleted}
                  importProgress={importProgress}
                  onImportPress={onDownloadFullPlaylist}
                  createdAtUnix={getPlaylistCreatedAtUnix()}
                />
              )}
            </ScreenTopAction>
            {!isKeyboardShown &&
              !loading &&
              !playlist?.details &&
              !playlist?.tracks && (
                <EmptyState
                  source={spotifyLogo}
                  title={t('importer:empty-state.title')}
                  subtitle={t('importer:empty-state.subtitle')}
                />
              )}
            {playlist?.tracks && (
              <View style={styles.section}>
                {importStarted ? (
                  <Text style={styles.caption}>
                    {t('importer:captions.import-progress', {
                      percent: Math.floor(importProgress),
                    })}
                  </Text>
                ) : (
                  <Text style={styles.caption}>
                    {t('importer:captions.imported-count', {
                      syncedFiles: alreadyImportedTracks.length,
                      totalFiles: playlist.tracks.length,
                    })}
                  </Text>
                )}
              </View>
            )}
            {playlist?.tracks && (
              <FlatList
                data={playlist?.tracks}
                renderItem={renderItem}
                keyExtractor={(item: Track) => item.id}
                refreshControl={
                  <RefreshControl
                    refreshing={loading || importStarted}
                    onRefresh={onImportPlaylist}
                    progressBackgroundColor={theme.colors.backgrounds[3]}
                    colors={[theme.colors.primary]}
                  />
                }
                contentInsetAdjustmentBehavior="automatic"
                contentContainerStyle={styles.scrollView}
                style={styles.scrollViewContent}
              />
            )}
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#222222',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  scrollView: {
    // paddingBottom: 72,
    paddingVertical: 8,
  },
  scrollViewContent: {
    flex: 1,
    height: '100%',
  },
  screenTitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#A7A7A7',
    marginTop: 8,
    marginBottom: 8,
  },
  section: {
    paddingHorizontal: 16,
  },
});
