import React, {FC, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

import {useInitialURL} from '../hooks/useInitialURL';
import {
  Routes,
  YOUTUBE_ID_REGEX,
  YOUTUBE_PLAYLIST_ID_REGEX,
  SPOTIFY_PLAYLIST_ID_REGEX,
} from '../../constants';

export type DeepLinkHandlerProps = {};

export const DeepLinkHandler: FC<DeepLinkHandlerProps> = () => {
  const {initialUrl, isProcessingInitialUrl} = useInitialURL();
  const {navigate} = useNavigation();

  useEffect(() => {
    const playDeepLink = async () => {
      if (isProcessingInitialUrl || !initialUrl) {
        return;
      }

      const matchYouTubeVideo = YOUTUBE_ID_REGEX.exec(initialUrl);
      const matchYouTubePlaylist = YOUTUBE_PLAYLIST_ID_REGEX.exec(initialUrl);
      const matchSpotifyPlaylist = SPOTIFY_PLAYLIST_ID_REGEX.exec(initialUrl);

      if (matchYouTubeVideo) {
        const [, videoId] = matchYouTubeVideo;
        const playlistId = matchYouTubePlaylist?.[0];

        if (playlistId) {
          navigate(Routes.SEARCH, {playlistId});
        } else {
          navigate(Routes.SEARCH, {videoId});
        }
      } else if (matchSpotifyPlaylist) {
        const [, playlistId] = matchSpotifyPlaylist;
        navigate(Routes.IMPORT_SPOTIFY, {playlistId});
      }
    };

    playDeepLink();
  }, [initialUrl, isProcessingInitialUrl, navigate]);

  return null;
};
