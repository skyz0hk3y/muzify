import {useState} from 'react';
import {Linking} from 'react-native';

import {useMount} from '../../core/hooks/useMount';

export const useInitialURL = () => {
  const [url, setUrl] = useState<string | null>(null);
  const [processing, setProcessing] = useState(true);

  useMount(() => {
    const getUrlAsync = async () => {
      // Get the deep link used to open the app
      const initialUrl = await Linking.getInitialURL();
      setUrl(initialUrl);
      setProcessing(false);
    };

    const setUrlAsync = async (event: {url: string}) => {
      setProcessing(true);
      setUrl(event.url);
      setProcessing(false);
    };

    getUrlAsync();
    Linking.addEventListener('url', setUrlAsync);

    return () => {
      Linking.removeEventListener('url', setUrlAsync);
    };
  });

  return {initialUrl: url, isProcessingInitialUrl: processing};
};
