import {device, by, expect, element} from 'detox';

describe('Search', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    // Start fresh
    await device.reloadReactNative();
    // Navigate to search screen
    await element(by.text('Search')).tap();
  });

  it('should navigate to Search tab when Search icon is pressed', async () => {
    await expect(element(by.id('search-tab'))).toBeVisible();
  });

  it('should focus the Search input when pressing the Search icon a second time', async () => {
    await element(by.text('Search')).tap();

    // FIXME: Check that input is focused, instead of visible
    await expect(element(by.id('search-input'))).toBeVisible();
  });

  it('should search for music when Search input is submitted from the keyboard', async () => {
    await element(by.id('search-input')).tap();
    await element(by.id('search-input')).typeText('PNL - Luz de Luna');
    await element(by.id('search-input')).tapReturnKey();

    await expect(element(by.text('Luz de Luna'))).toBeVisible();
    // FIXME: Check that input is focused, instead of visible
  });

  it('should clear the query and results when the SearchInput clear button is pressed', async () => {
    await element(by.id('search-input')).tap();
    await element(by.id('search-input')).typeText('PNL - Luz de Luna');
    await element(by.id('search-input')).tapReturnKey();

    await expect(element(by.text('Luz de Luna'))).toBeVisible();

    await element(by.id('search-input-clear-button')).tap();

    await expect(element(by.text('Luz de Luna'))).toNotExist();
  });

  it('should add an entry into the recent Search queries when a Search is submitted', async () => {
    await element(by.id('search-input')).tap();
    await element(by.id('search-input')).typeText('DA UZI - La vraie vie');
    await element(by.id('search-input')).tapReturnKey();

    await expect(element(by.text('La vraie vie'))).toBeVisible();

    await element(by.id('search-input-clear-button')).tap();

    await expect(
      element(by.id('recent-search-DA UZI - La vraie vie')),
    ).toExist();
  });

  it('should set the Search input value when a recent Search query is pressed', async () => {
    await element(by.id('recent-search-DA UZI - La vraie vie-touchable')).tap();
    await expect(element(by.text('La vraie vie'))).toBeVisible();
  });

  it('should delete a recent Search query when its clear button is pressed', async () => {
    await element(
      by.id('recent-search-DA UZI - La vraie vie-action-button'),
    ).tap();

    await expect(element(by.text('DA Uzi'))).toNotExist();
    await expect(element(by.text('La vraie vie'))).toNotExist();
  });
});
