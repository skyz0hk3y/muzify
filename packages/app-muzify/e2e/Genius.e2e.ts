import {device, by, expect, element} from 'detox';

describe('Genius', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should start on the Genius screen', async () => {
    const home = element(by.id('genius-home'));
    await expect(home).toBeVisible();
  });

  it('should show an empty state when no data is available', async () => {
    const emptyState = element(by.id('genius-empty-state'));
    await expect(emptyState).toBeVisible();
  });

  // TODO: Write tests when UI is not empty, too
});
