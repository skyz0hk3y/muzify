import {device, by, expect, element} from 'detox';

describe('Library', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    // Start fresh
    await device.reloadReactNative();
    // Navigate to search screen
    await element(by.text('Library')).tap();
  });

  it('should navigate to Library tab when Library icon is pressed', async () => {
    await expect(element(by.id('library-tab'))).toBeVisible();
  });

  it.todo('should show a button to create a playlist');
  it.todo('should show a button to import a playlist from Spotify');
  it.todo(
    'should navigate to the playlist creation screen and create a new playlist',
  );
  it.todo('should add a track to a playlist');

  it.todo('should add a track to the Liked Tracks playlist');
  it.todo('should remove a track from the Liked Tracks playlist');
});
