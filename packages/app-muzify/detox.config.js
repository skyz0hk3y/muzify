module.exports = {
  testRunner: 'jest',
  runnerConfig:
    process.env.DETOX_EXPOSE_GLOBALS === '0'
      ? 'e2eExplicitRequire/config.json'
      : 'e2e/config.json',
  specs:
    process.env.DETOX_EXPOSE_GLOBALS === '0' ? 'e2eExplicitRequire' : 'e2e',
  behavior: {
    init: {
      exposeGlobals: process.env.DETOX_EXPOSE_GLOBALS === '0' ? false : true,
    },
  },
  apps: {
    'ios.release': {
      type: 'ios.app',
      binaryPath:
        'ios/build/Build/Products/Release-iphonesimulator/muzify.app',
      build:
        'export RCT_NO_LAUNCH_PACKAGER=true && xcodebuild -project ios/muzify.xcodeproj -UseNewBuildSystem=NO -scheme muzify -configuration Release -sdk iphonesimulator -derivedDataPath ios/build -quiet',
    },
    'ios.debug': {
      type: 'ios.app',
      binaryPath: 'ios/build/Build/Products/Debug-iphonesimulator/muzify.app',
      build:
        'xcodebuild -project ios/muzify.xcodeproj -UseNewBuildSystem=NO -scheme muzify -configuration Debug -sdk iphonesimulator -derivedDataPath ios/build',
    },
    'android.debug': {
      type: 'android.apk',
      build:
        'cd android ; ./gradlew assembleDebug assembleAndroidTest -x bundleReleaseJsAndAssets -DtestBuildType=debug ; cd -',
      binaryPath: './android/app/build/outputs/apk/release/app-universal-debug.apk',
      testBinaryPath: './android/app/build/outputs/apk/androidTest/release/app-debug-androidTest.apk',
      utilBinaryPaths: ['../.detox-cache/test-butler-app.apk'],
    },
    'android.release': {
      type: 'android.apk',
      build:
        'cd android ; NODE_ENV=production ./gradlew assembleRelease assembleAndroidTest -x bundleReleaseJsAndAssets -DtestBuildType=release ; cd -',
      binaryPath: './android/app/build/outputs/apk/release/app-universal-release.apk',
      testBinaryPath: './android/app/build/outputs/apk/androidTest/release/app-release-androidTest.apk',
      utilBinaryPaths: ['../.detox-cache/test-butler-app.apk'],
    },
  },
  devices: {
    simulator: {
      type: 'ios.simulator',
      device: {
        type: 'iPhone 11',
      },
    },
    emulator: {
      type: 'android.emulator',
      device: {
        avdName: 'Pixel_4_XL_API_30',
      },
    },
    "android.attached": {
      "type": "android.attached",
      "device": {
        "adbName": "UBV0218402003604",
      }
    }
  },
  configurations: {
    'ios.sim.debug': {
      device: 'simulator',
      app: 'ios.debug',
    },
    'ios.sim.release': {
      device: 'simulator',
      app: 'ios.release',
      artifacts: {
        pathBuilder: './e2e/__setup/detox.pathbuilder.ios.js',
      },
    },
    'ios.none': {
      type: 'ios.none',
      session: {
        server: 'ws://localhost:8099',
        sessionId: 'com.wix.demo.react.native',
      },
    },
    'android.emu.debug': {
      device: 'emulator',
      app: 'android.debug',
    },
    'android.emu.release': {
      device: 'emulator',
      app: 'android.release',
      artifacts: {
        pathBuilder: './e2e/__setup/detox.pathbuilder.android.js',
      },
    },
    'android.attached.release': {
      device: 'android.attached',
      app: 'android.release',
      artifacts: {
        pathBuilder: './e2e/__setup/detox.pathbuilder.android.js',
      },
    },
    
  },
};
