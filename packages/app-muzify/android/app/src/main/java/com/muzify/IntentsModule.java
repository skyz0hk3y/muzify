package com.muzify;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

import android.util.Log;
import android.app.Activity;
import android.net.Uri;
import android.provider.Settings;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;

import java.util.Map;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class IntentsModule extends ReactContextBaseJavaModule {

  private static ReactApplicationContext reactContext;

  IntentsModule(ReactApplicationContext context) {
    super(context);
    reactContext = context;
  }

  @Override
  public String getName() {
    return "Intents";
  }

  @ReactMethod
  public boolean shouldAllowUnknownSourceFromApp() {
    try {
      boolean isNonPlayAppAllowed = Settings.Secure
      .getInt(
        reactContext.getContentResolver(),
        Settings.Secure.INSTALL_NON_MARKET_APPS
      ) == 1;
      
      return isNonPlayAppAllowed;
    } catch (Settings.SettingNotFoundException e) {
      PackageManager pm = reactContext.getPackageManager();
      return pm.canRequestPackageInstalls();
    }
  }

  @ReactMethod
  public void intentAllowUnknownSourceFromApp(@NonNull Promise promise) {
    Activity activity = getCurrentActivity();
    if (activity != null) {
      final Intent intent = new Intent(
        Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES,
        Uri.parse("package:com.muzify")
      );

      activity.startActivity(intent);

      promise.resolve(activity.getClass().getSimpleName());
    } else {
      promise.reject("NO_ACTIVITY", "No current activity");
    }
  }

  @ReactMethod
  public void getAsset(String fileName, @NonNull Promise promise) {
    try {
      InputStream inputStream = reactContext.getApplicationContext().getAssets().open(fileName);
      int size = inputStream.available();
      byte[] buffer = new byte[size];

      inputStream.read(buffer);
      inputStream.close();

      final String fileContents = new String(buffer, "UTF-8");
      promise.resolve(fileContents);
    } catch (IOException ex) {
      ex.printStackTrace();
      promise.reject("FILE_NOT_EXISTS", ex.getLocalizedMessage());
    }
  }
}
