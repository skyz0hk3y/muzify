# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /usr/local/Cellar/android-sdk/24.3.3/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

#
# Most needed rules come from React Native's proguard-rules.pro (either .aar or source) -- hence
# the lean configuration file.
###

-dontnote android.net.**
-dontnote org.apache.**

# An addition to RN's 'keep' and 'dontwarn' configs -- need to also 'dontnote' some stuff.

-dontnote com.facebook.**
-dontnote sun.misc.Unsafe
-dontnote okhttp3.**
-dontnote okio.**

-keep class com.facebook.hermes.unicode.** { *; }
-keep class com.facebook.jni.** { *; }
-keep class com.facebook.soloader.** { *; }
-keep class com.facebook.react.turbomodule.** { *; }
-keep class com.facebook.react.modules.systeminfo.ReactNativeVersion {*;}
-keep class kotlin.** { *; }
-keep class com.facebook.react.** {*;}
-keepclassmembers class com.facebook.react.** {*;}
-keep class org.reactnative.maskedview.** { *; }

# Do not strip any method/class that is annotated with @DoNotStrip
# This should really come from React Native itself. See here: https://github.com/react-native-community/upgrade-support/issues/31
-keep @com.facebook.jni.annotations.DoNotStrip class *
-keep class * {
    @com.facebook.proguard.annotations.DoNotStrip *;
    @com.facebook.common.internal.DoNotStrip *;
    @com.facebook.jni.annotations.DoNotStrip *;
}
-keepclassmembers class * {
    @com.facebook.jni.annotations.DoNotStrip *;
}

# Add any project specific keep options here: