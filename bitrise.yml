---
format_version: '8'
default_step_lib_source: https://github.com/bitrise-io/bitrise-steplib.git
project_type: react-native
trigger_map:
- push_branch: develop
  workflow: e2e-tests_android
- pull_request_source_branch: "*"
  pull_request_target_branch: develop
  workflow: e2e-tests_android
workflows:
  deploy:
    description: "not yet implemented"
    steps:
    - activate-ssh-key@4:
        run_if: '{{getenv "SSH_RSA_PRIVATE_KEY" | ne ""}}'
    - git-clone@4: {}
    - script@1:
        title: Do anything with Script step
    - yarn@0:
        inputs:
        - workdir: packages/app-muzify
        - command: install
    - install-missing-android-tools@2:
        inputs:
        - gradlew_path: "$PROJECT_LOCATION/gradlew"
    - android-build@0:
        inputs:
        - project_location: "$PROJECT_LOCATION"
    - certificate-and-profile-installer@1: {}
    - cocoapods-install@2: {}
    - xcode-archive@3:
        inputs:
        - project_path: "$BITRISE_PROJECT_PATH"
        - scheme: "$BITRISE_SCHEME"
        - export_method: "$BITRISE_EXPORT_METHOD"
        - configuration: Release
    - deploy-to-bitrise-io@1: {}
  e2e-tests_android:
    steps:
    - activate-ssh-key@4:
        run_if: '{{getenv "SSH_RSA_PRIVATE_KEY" | ne ""}}'
    - git-clone@4: {}
    - cache-pull@2:
        inputs:
        - workdir: |-
            $HOME/.gradle
            ./.gradle
    - script@1:
        title: Generate secrets files
        inputs:
        - content: |
            #!/usr/bin/env bash
            # fail if any commands fails
            set -e
            # debug log
            set -x

            sh ./packages/api-muzify/scripts/ci-generate-dotenv.sh > ./packages/app-muzify/.env
            echo "Generated .env file"

            sh ./packages/api-muzify/scripts/ci-generate-openode-token.sh > ./packages/app-muzify/.openode
            echo "Generated .openode file"

            sh ./packages/api-muzify/scripts/ci-generate-yt-cookie.sh > ./packages/app-muzify/src/youtubeCookie.ts
            echo "Generated youtubeCookie.ts file"
    - script@1:
        title: Setup Android emulator
        inputs:
        - content: "#!/bin/bash\n\n# This is the remote image we are going to run.\n#
            Docker will obtain it for us if needed.\nDOCKER_IMAGE=us-docker.pkg.dev/android-emulator-268719/images/r-google-x64:30.0.23\n\n#
            This is the forwarding port. Higher ports are preferred as to not interfere\n#
            with adb's ability to scan for emulators.\nPORT=15555\n\nAVD_NAME=\"Pixel_4_XL_API_30\"\n\nls
            /dev/kvm\ngrep -E \"(vmx|svm)\" /proc/cpuinfo | wc -l\n\nsudo apt upgrade
            -y\n\n# Install Necessary Packages for KVM\nsudo apt-get install qemu-kvm
            libvirt-daemon-system libvirt-clients bridge-utils -y\n\nsudo apt --fix-broken
            install -y\n\n# Install cmake > 8 for mmkv\nwget https://github.com/Kitware/CMake/releases/download/v3.13.5/cmake-3.13.5-Linux-x86_64.sh\necho
            \"y\" | bash cmake-3.13.5-Linux-x86_64.sh\n\n# Add Users to Groups\nsudo
            adduser `id -un` kvm\n\n# restart the kernel modules: \n# rmmod kvm kvm_intel\n#
            modprobe -a kvm kvm_intel\n\nsh ./scripts/ci-detox-create-emulator.sh\n\nadb
            start-server\n\n# start the emulator\n$ANDROID_HOME/emulator/emulator
            -avd $AVD_NAME -no-audio -no-window &\n\n# show connected virtual device\nadb
            devices\n\n"
    - yarn@0:
        inputs:
        - workdir: packages/app-muzify
        - cache_local_deps: 'yes'
        - command: install --frozen-lockfile
        title: Install dependencies
    - yarn@0:
        inputs:
        - workdir: packages/app-muzify
        - cache_local_deps: 'yes'
        - command: e2e:android-release-ci
        title: Run Detox E2E tests
    - cache-push@2:
        inputs:
        - cache_paths: |-
            $HOME/.gradle/caches/*.lock
            $HOME/.gradle/caches/*.bin
            ./.gradle/*.lock
            ./.gradle/*.bin
        is_always_run: true
    - deploy-to-bitrise-io@1:
        inputs:
        - deploy_path: "$BITRISE_SOURCE_DIR/packages/app-muzify/artifacts"
        - notify_user_groups: developers, testers
        - is_enable_public_page: 'false'
        - is_compress: 'true'
    - script@1:
        title: Cleanup emulator
        inputs:
        - content: |-
            # stop emulator
            adb -s "Pixel_4_XL_API_30" emu kill
        is_always_run: true
    description: Muzify E2E tests suite for Android using Detox/Espresso
  e2e-tests_ios:
    steps:
    - activate-ssh-key@4:
        run_if: '{{getenv "SSH_RSA_PRIVATE_KEY" | ne ""}}'
    - git-clone@4: {}
    - script@1:
        title: Generate secrets files
        inputs:
        - content: |-
            #!/usr/bin/env bash
            # fail if any commands fails
            set -e
            # debug log
            set -x

            sh ./scripts/ci-generate-dotenv.sh > ./packages/app-muzify/.env
            echo "Generated .env file"

            sh ./scripts/ci-generate-openode-token.sh > ./packages/app-muzify/.openode
            echo "Generated .openode file"

            sh ./scripts/ci-generate-yt-cookie.sh > ./packages/app-muzify/src/youtubeCookie.ts
            echo "Generated youtubeCookie.ts file"
    - script@1:
        title: Setup Android emulator
        inputs:
        - content: |
            mkdir -p /root/.android && touch /root/.android/repositories.cfg

            # See https://stackoverflow.com/a/56652468/3855007
            export QT_DEBUG_PLUGINS=1

            # echo yes | $ANDROID_HOME/tools/bin/sdkmanager --list

            # Install SDK 30 (x86_64)
            echo yes | $ANDROID_HOME/tools/bin/sdkmanager --channel=0 --verbose "system-images;android-30;google_apis_playstore;x86_64"

            # Create a "Pixel_4_XL_API_30" emulator for Detox to run tests on
            echo no | $ANDROID_HOME/tools/bin/avdmanager --verbose create avd --force --name "Pixel_4_XL_API_30" --package "system-images;android-30;google_apis_playstore;x86_64" --sdcard 200M --device 11
    - yarn@0:
        inputs:
        - workdir: packages/app-muzify
        - cache_local_deps: 'yes'
        - command: install
        title: Install dependencies
    - yarn@0:
        inputs:
        - workdir: packages/app-muzify
        - cache_local_deps: 'yes'
        - command: e2e:android-release
    - deploy-to-bitrise-io@1:
        inputs:
        - deploy_path: packages/app-muzify/artifacts
        - notify_user_groups: developers, testers
        - is_enable_public_page: 'false'
        - is_compress: 'true'
    description: Muzify E2E tests suite for Android using Detox/Espresso
    meta:
      bitrise.io:
        stack: osx-xcode-12.5.x
app:
  envs:
  - opts:
      is_expand: false
    PROJECT_LOCATION: packages/app-muzify/android
  - opts:
      is_expand: false
    MODULE: app
  - opts:
      is_expand: false
    VARIANT: ''
  - opts:
      is_expand: false
    BITRISE_PROJECT_PATH: packages/app-muzify/ios/muzify.xcworkspace
  - opts:
      is_expand: false
    BITRISE_SCHEME: muzify
  - opts:
      is_expand: false
    BITRISE_EXPORT_METHOD: development
