# Muzify

Muzify is a free Spotify like YouTube client built with React Native, Redux and
TypeScript. It is written with user-friendliness in mind and user tested by real
"Joe" users! Focus is on having a well-designed, open-source and free Spotify
alternative/importer so one does not lose its whole collection when canceling the
Spotify subscription ^^.

## Join the Community

Chat: [@muzify_app](https://t.me/muzify_community) on Telegram  
Releases: [@muzify_releases](https://t.me/muzify_app) on Telegram

## Features

- **Search YouTube** for your preferred tracks
- Like tracks you want to keep to enjoy **offline music**
- Keep your library organized with track playlists (local, YouTube, Spotify)
- **Download tracks** from YouTube, **import playlists** from Spotify
- Auto-generated playlists based on your behavior, **locally, ON YOUR DEVICE**
  (most played tracks, most repeated tracks, liked tracks, etc)
- Slick **Spotify-like UI** with light & dark modes
- No monthly fees because **it's FREE!**

## Download & Install

Muzify actually works on Android and iOS. I have plan to port it over to desktop
once it's stable enough on mobile.

### Android

- Android (armv7a): https://muzify.eu.openode.io/updates/check?abi_code=armeabi-v7a
- Android (armv8a): https://muzify.eu.openode.io/updates/check?abi_code=arm64-v8a
- Android (x86): https://muzify.eu.openode.io/updates/check?abi_code=x86
- Android (x86_64): https://muzify.eu.openode.io/updates/check?abi_code=x86_64

### iOS

- iOS: You need to compile the app yourself and to install it with a developer
  provisioning profile that is registered with Apple.

<!-- TODO: Write how to compile/run/dev the API/APP -->

## How it works?

Basically this is web-scrapping. I run [an instance](https://muzify.eu.openode.io)
of `@muzify/api-muzify` at [opeNode](https://openode.io) for around 5$/month.

This API exposes a set of "public" endpoints that the `@muzify/app-muzify` client
consume to retrieve and display data about tracks, playlists, updates, etc.

This API needs both a Redis service and a sqlite/postgres database by design so it
remains as fast as possible. In fact, YouTube puts hard rate-limit rules (_1 req/s_)
that basically limits web-scrapping **A LOT**.

In order to "bypass" this reliability issue when fetching for YouTube data,
the API makes use of redis to store/retrieve cached data about the playlists,
search results, tracks, and so on. Cache is powerful to lower pressure on web
scrapping and get faster API responses, and to avoid hitting rate limits.

This means API has a lot of metadata stored on it, and is publicly accessible.
It could make it vulnerable in case user-data were stored inside, but that's not
the case. User's data stays on user's device, so we don't have to pay for storing
their data, and thus **no sensible data can gets stolen/sold/etc because we
store no sensible data.** Only public data.

Spotify import is done by using a free Spotify API token so the API can retrieve
tracks in a playlist, and search for them on YouTube in order to "convert" them.

Concerning streaming, the API handles that so the client can hit a single endpoint
and get just what it cares for: an audio file to play. It's currently capable of
streaming `webm`/`mp4` files and to transcode/stream from `mp4` to `mp3` over-the-fly.
Beware still that transcoding is CPU intensive and not meant for receiving
thousands of requests/s. Client does not request MP3 at the moment.

The client codebase and the API codebase both does not contains analytics/crash
report tools so the end-user doesn't send any data that should be kept private.

In the future, I will develop such a "SDK" that will also get open-sourced for
everyone to profit from free, open-source, privacy conscious analytics-and-errors
reporting tool (if time permits me).

The Muzify app (the Client) has a built-in APK updater so that we can keep users
up-to-date without headaches both for us, and the end-user. API store/handle app
releases management and versioning, and also take care of returning the correct
APK given the user's device processor platform (arm, x86, etc).

## Donations

By submitting a donation you do contribute to the cost of infrastructure as well
as development of Muzify!

- **BTC**: bc1q3td4qznfvravr7uqjhqu47j00mtnhyd4wagp0k
- **LiberaPay**: [SkyzohKey on LiberaPay](https://liberapay.com/SkyzohKey)

## License

Muzify is licensed under the MIT license.

```text
The MIT License (MIT)

Copyright © 2021 SkyzohKey <skyzohkey@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
