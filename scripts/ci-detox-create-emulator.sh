#!/usr/bin/env bash

# set -ex
# set -o pipefail

export AVD_NAME=Pixel_4_XL_API_30

if $ANDROID_HOME/emulator/emulator -list-avds | grep -q $AVD_NAME; then
    echo "\n\n> Android virtual device already exists: ${AVD_NAME}"
else
    # Create virtual device in a relative "Pixel_4_XL_API_30" folder
    $ANDROID_HOME/tools/bin/avdmanager create avd -n $AVD_NAME -k 'system-images;android-30;google_apis;x86' -g google_apis -p $AVD_NAME -d 'pixel'

    # Copy predefined config and skin
    cp -r ./scripts/android_emulator/ $AVD_NAME/
    sed -i -e "s|skin.path = /change_to_absolute_path/pixel_4_xl_skin|skin.path = $(pwd)/${AVD_NAME}/pixel_4_xl_skin|g" $AVD_NAME/config.ini

    echo "\n\n> Android virtual device successfully created: ${AVD_NAME}"
fi